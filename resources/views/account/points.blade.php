﻿@extends('layouts.contents.default')
@include('layouts.menus.base')

@section('breadcrumbs')
{? $page_name = 'Achat de points' ?}
{!! Breadcrumbs::render('account') !!}
@stop

@section('content')

<div class="ak-title-container">
    <h1 class="ak-return-link">
        <a href=""><span class="ak-icon-big ak-shop"></span></a> Achat de jetons
    </h1>
</div>

<div class="ak-container ak-panel ak-account-login">
    <div class="ak-panel-content">
 <div class="ak-login-page panel-main">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="">
                        <div class="ak-login-block">
                            <div class="ak-container">
                            
                             {!! Form::open(['route' => 'achatcheck']) !!}

              
                    <div class="form-group @if ($errors->has('offline')) has-error @endif">
      
                    <div class="form-group">
                        <label class="control-label" for="ogrinesWeb">Acheter des jetons</label>
                        <div class="input-group">
                            <span class="input-group-addon"><span class="ak-icon-small ak-votes-icon"></span></span>
                            <input type="text" class="form-control ak-tooltip" value="{{ Utils::format_price(Auth::user()->jetons) }}" id="jetonsWeb" readonly />
                        </div>
                    </div>
            <center>
Merci de ne rien acheter l'achat de points n'est pas fonctionnel.
    <br/>
   
    <br /><br />
    <center style="color:black">
<div class="comment" style="min-height:30px;background-color: white; border: 1px solid gray;box-shadow: 0px 3px 3px #c7c7c7;font-size: 11px;line-height: 24px;"><br>
 <p>1 code = 15 jetons<b>

<div class="comment" style="min-height:30px;background-color: #d0ecfb; border: 1px solid gray;box-shadow: 0px 3px 3px #c7c7c7;font-size: 11px;line-height: 24px;"><br>
 <p><b><u>Cliquez sur l'image Starpass:</b></u></p><br>
 <center>
  <a onclick="window.open(this.href,'StarPass','width=700,height=500,scrollbars=yes,resizable=yes');return false;" href="http://hyrule-serveur.com/code.php"><img class="aImg" src="images/starpass.png" title="Script"></a>
 
                    <div class="form-group @if ($errors->has('jetons')) has-error @endif">
                        <label class="control-label" for="ogrines">Acheter des jetons</label>
                        <div class="input-group">
                            <span class="input-group-addon"><span class="ak-icon-small ak-votes-icon"></span></span>
                            <input type="text" class="form-control ak-tooltip" tabindex="1" autocomplete="off" name="code1" placeholder="Saisir le code starpass" value="{{ Input::old('jetons') }}" id="jetons" autocapitalize="off" autocorrect="off" required="required" />
                        </div>
                        @if ($errors->has('jetons')) <label class="error control-label">{{ $errors->first('jetons') }}</label> @endif
                    </div>

                    <input type="submit" role="button" class="btn btn-primary btn-lg" value="Acheter">
                    {!! Form::close() !!}
    
    
    
    
    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
