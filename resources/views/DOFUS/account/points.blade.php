﻿@extends('layouts.contents.default')
@include('layouts.menus.base')

@section('breadcrumbs')
{? $page_name = 'Achat de points' ?}
{!! Breadcrumbs::render('account') !!}
@stop

@section('content')

<div class="ak-title-container">
    <h1 class="ak-return-link">
        <a href=""><span class="ak-icon-big ak-shop"></span></a> Achat de jetons
    </h1>
</div>

<div class="ak-container ak-panel ak-account-login">
    <div class="ak-panel-content">
 <div class="ak-login-page panel-main">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="">
                        <div class="ak-login-block">
                            <div class="ak-container">
                            
                             {!! Form::open(['route' => 'achatcheck']) !!}

              
                    <div class="form-group @if ($errors->has('offline')) has-error @endif">
      
                    <div class="form-group">
                        <label class="control-label" for="ogrinesWeb">Votre réserve de jetons :</label>
                        <div class="input-group">
                            <span class="input-group-addon"><span class="ak-icon-small ak-votes-icon"></span></span>
                            <input type="text" class="form-control ak-tooltip" value="{{ Utils::format_price(Auth::user()->jetons) }}" id="jetonsWeb" readonly />
                        </div>
                    </div>
            <center>
                    <b style="color:#b10000">1 code starpass est égal à <u>25 jetons.</u></b>
					                            <div class="ak-ogrine-reserve-critical ak-ogrines-reserve">

    <br/>
                       
                           <div class="ak-ogrine-reserve-critical ak-ogrines-reserve">
                                <a href="{{ URL::route('achatPoints') }}">
                                <?php 
                                $account = new App\Account();
                                $v = $account->on('hyrule_auth')->where('Email', Auth::user()->email)->get();
                                $c = 0;
                                foreach ($v as $k) {
                                    $c += $k->Tokens;
                                }

                                ?>
                <span class="ak-reserve"><b style="color:#b10000"><p>COMPENSATION 22/08 1 jeton = <u>150<span class="ak-icon-small ak-ogrines-icon"></span>	 <div class="col-xs-4"></span>
                                    </p>
                                </a>
                            </div>
                        </div></u></b>

    <br /><br />
    
 <p><u><b style="color:#b10000">Cliquez sur l'image starpass, puis rentrez le code obtenu ci-dessous.</b></u></p><br>
 <center>
  <a onclick="window.open(this.href,'StarPass','width=700,height=500,scrollbars=yes,resizable=yes');return false;" href="https://hyrule-serveur.com/code.php"><img class="aImg" src="https://hyrule-serveur.com/public/bitmap/starpass.png" title="Script"></a>
   <br/>
   
    <br /><br />
                    <div class="form-group @if ($errors->has('jetons')) has-error @endif">
                        <label class="control-label" for="ogrines"></label>
                        <div class="input-group">
                            <span class="input-group-addon"><span class="ak-icon-small ak-votes-icon"></span></span>
                            <input type="text" class="form-control ak-tooltip" tabindex="1" autocomplete="off" name="code1" placeholder="Saisissez le code starpass." value="{{ Input::old('jetons') }}" id="jetons" autocapitalize="off" autocorrect="off" required="required" />
                        </div>
                        @if ($errors->has('jetons')) <label class="error control-label">{{ $errors->first('jetons') }}</label> @endif
                    </div>

                    <input type="submit" role="button" class="btn btn-primary btn-lg" value="Acheter">
                    {!! Form::close() !!}
    
    
    
    
    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@stop
