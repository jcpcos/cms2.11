@extends('layouts.contents.default')
@include('layouts.menus.base')

@section('breadcrumbs')
{? $page_name = 'Page Perso' ?}
{!! Breadcrumbs::render('gameaccount', [$account->server, $account->Id]) !!}
@stop

@section('content')
<div class="ak-container ak-main-center">
    <div class="ak-title-container ak-backlink">
        <h1><span class="ak-icon-big ak-bank"></span></a> Personnage - ( {{$character->Name}} )</h1>
        <a href="{{ URL::route('profile') }}" class="ak-backlink-button">Retour à mon compte</a>
    </div>



<div class="ak-container ak-panel-stack ak-directories ak-glue" style="margin-bottom:12px;">
    <div class="ak-container ak-panel ak-success-content">        
        <div class="ak-panel-title">
              <p><span class="ak-panel-title-icon"></span><?php if($characterSell != null)  echo "Prix personnage : ".$characterSell->price." Ogrines"; ?> </p>    

        </div>
        <?php 
        $is = false;
        foreach ($ownAccounts as $k) 
        {
            if($k->Id == $account->Id)
            {
                $is = true;
                break;
            }
        }
        ?>
    <?php if($characterSell != null): ?>

        <div class="ak-panel-content">
        
         <center><a href="{{ URL::route('buyCharacter',[$characterSell->id_character]) }}" class="btn btn-primary btn-lg ak-btn-unlock">Acheter le personnage</a></center>

        </div>
    <?php else : ?>
        <?php if($is == true): ?>
        <div class="ak-panel-content">
        
         <center><a href="{{ URL::route('sell',[$server, $account->Id,$character->Id]) }}" class="btn btn-primary btn-lg ak-btn-unlock">Vendre le personnage</a></center>

        </div>
        <?php endif;?>
    <?php endif;?>

    </div>
</div>
<div class="ak-container ak-panel-stack ak-directories ak-glue" style="margin-bottom:12px;">
    <div class="ak-container ak-panel ak-success-content">        
        <div class="ak-panel-title">
              <p><span class="ak-panel-title-icon"></span>Information sur {{$character->Name}} : </p>    

        </div>
        <div class="ak-panel-content">

        <p><b>Level</b> : <span>{{$character->level("hyrule") }}</span></p>    
        <p><b>Total XP </b> : <span>{{$character->Experience }}</span></p>    
        <p><b>Prestige</b> : <span>{{$character->Prestige }}</span></p>    
        <p><b>Points d'honneur</b> : <span>{{$character->Honor }}</span></p>    
        <p><b>Classe</b> : <span>{{ $character->classe() }}</span></p>    

        </div>

    </div>
</div>

<div class="ak-container ak-panel-stack ak-directories ak-glue" style="margin-bottom:12px;">
    <div class="ak-container ak-panel ak-success-content">        
        <div class="ak-panel-title">
              <p><span class="ak-panel-title-icon"></span> Caractéristiques : </p>    

        </div>
        <div class="ak-panel-content">

          <table class="ak-ladder ak-container ak-table ak-responsivetable" style="white-space: nowrap; visibility: visible;">
        <tr>
            <th>Caractéristiques </th>
            <th>Total</th>   
        </tr>
        <tr>
        <tr>
        <td class="ak-class">PA</td>
        <td class="ak-class">{{ $character->AP }}</td>
        </tr>
        <tr>
        <td class="ak-class">PM</td>
        <td class="ak-class">{{ $character->MP }}</td>
        </tr>
        <tr>
          <td class="ak-class">Prospection</td>
        <td class="ak-class">{{ $character->Prospection }}</td>
        </tr>
        <tr>
          <td class="ak-class">Strength</td>
        <td class="ak-class">{{ $character->Strength }}</td>
        </tr>
        <tr>
          <td class="ak-class">Chance</td>
        <td class="ak-class">{{ $character->Chance }}</td>
        </tr>
        <tr>
          <td class="ak-class">Vitality</td>
        <td class="ak-class">{{ $character->Vitality }}</td>
        </tr>
        <tr>
          <td class="ak-class">Wisdom</td>
        <td class="ak-class">{{ $character->Wisdom }}</td>
        </tr>
        <tr>
          <td class="ak-class">Intelligence</td>
        <td class="ak-class">{{ $character->Intelligence }}</td>
        </tr>
        <tr>
          <td class="ak-class">Agility</td>
        <td class="ak-class">{{ $character->Agility }}</td>
       
        </tr>

 
    </table>

        </div>

    </div>
</div>
<div class="ak-container ak-panel-stack ak-directories ak-glue" style="margin-bottom:12px;">
    <div class="ak-container ak-panel ak-success-content">        
        <div class="ak-panel-title">
              <p><span class="ak-panel-title-icon"></span>Inventaire : </p>    

        </div>
        <div class="ak-panel-content">

        @foreach($images as $i)

            <img src="{{URL::asset("public/bitmap/".$i)}}.png" />

        @endforeach  

        </div>

    </div>
</div>



</div>
@stop

