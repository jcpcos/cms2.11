@extends('layouts.contents.default')
@include('layouts.menus.base')

@section('breadcrumbs')
{? $page_name = 'Vente personnage' ?}
{!! Breadcrumbs::render('gameaccount', [$account->server, $account->Id]) !!}
@stop

@section('content')
<div class="ak-container ak-main-center">
    <div class="ak-title-container ak-backlink">
        <h1><span class="ak-icon-big ak-bank"></span></a>Vente Personnage - ( {{$character->Name}} )</h1>
        <a href="{{ URL::route('profile') }}" class="ak-backlink-button">Retour à mon compte</a>
    </div>

<div class="ak-container ak-panel-stack">
    <div class="ak-panel">
     
        <div class="ak-panel-content">
            <div class="panel-main">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="ak-container">
                            <div class="ak-form">
                                {!! Form::open(['route' => 'sell_action']) !!}
                           
                                    <div class="form-group @if ($errors->has('prix')) has-error @endif">
                                        <label class="control-label" for="prix">Prix de vente(TAXE : 15% du prix)</label>
                                        <input type="text" class="form-control" placeholder="Votre prix" name="prix" value="{{ Input::old('prix') }}" id="prix" />
                                        @if ($errors->has('prix')) <label class="error control-label">{{ $errors->first('prix') }}</label> @endif
                                    </div>
                                    <input type="hidden" class="form-control" placeholder="Votre prix" name="Id" value="{{$character->Id}}" id="Id" />
                                    <input type="hidden" class="form-control" placeholder="Votre prix" name="acc" value="{{$account->Id}}" id="acc" />


                                    <input type="submit" role="button" class="btn btn-primary btn-lg btn-block" value="Vendre">

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
             
                </div>
            </div>
        </div>
    </div>
</div>


</div>
@stop

