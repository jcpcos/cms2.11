@extends('layouts.contents.default')
@include('layouts.menus.base')

@section('breadcrumbs')
{? $page_name = 'Vente personnage' ?}
@stop

@section('content')
<div class="ak-container ak-main-center">
    <div class="ak-title-container ak-backlink">
        <h1><span class="ak-icon-big ak-bank"></span></a>Achat Personnage - ( {{$character->Name}} )</h1>
        <a href="{{ URL::route('profile') }}" class="ak-backlink-button">Retour à mon compte</a>
    </div>

<div class="ak-container ak-panel-stack">
    <div class="ak-panel">
     
        <div class="ak-panel-content">
            <div class="panel-main">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="ak-container">
                            <div class="ak-form">
                                {!! Form::open(['route' => 'buy_action']) !!}
                           
                                        <select name="account" class="form-control" id="account" onchange='this.form.submit()' />
                                            <option value="">Selectionnez un compte</option>
                                            <?php foreach ($accounts as $k):?>
                                            <option value="{{ $k->Id }}">{{$k->Login}}</option>
                                            <?php endforeach;?>
                                        </select>
                                        <input type="hidden" class="form-control" placeholder="Votre prix" name="Id" value="{{$character->Id}}" id="Id" />                         

                                    <noscript><input type="submit" value="Submit"></noscript>
                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
             
                </div>
            </div>
        </div>
    </div>
</div>


</div>
@stop

