<?php

return <<<'VALUE'
"namespace IPS\\Theme;\nclass class_forums_admin_livesearch extends \\IPS\\Theme\\Template\n{\n\t\t\tfunction forum( $forum ) {\n\t\t$return = '';\n\t\t$return .= <<<CONTENT\n\n<li class='ipsPad_half ipsClearfix' data-role='result'>\n\t<a href='\nCONTENT;\n\n$return .= str_replace( '&', '&amp;', \\IPS\\Http\\Url::internal( \"app=forums&module=forums&controller=forums&do=form&id=\", null, \"\", array(), 0 ) );\n$return .= <<<CONTENT\n\nCONTENT;\n$return .= htmlspecialchars( $forum->id, ENT_QUOTES | \\IPS\\HTMLENTITIES, 'UTF-8', FALSE );\n$return .= <<<CONTENT\n' class='ipsPos_left'>\nCONTENT;\n$return .= htmlspecialchars( $forum->_title, ENT_QUOTES | \\IPS\\HTMLENTITIES, 'UTF-8', FALSE );\n$return .= <<<CONTENT\n<\/a>\n<\/li>\n\n\nCONTENT;\n\n\t\treturn $return;\n}}"
VALUE;
