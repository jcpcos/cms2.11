/*
Navicat MySQL Data Transfer

Source Server         : database
Source Server Version : 50520
Source Host           : localhost:3306
Source Database       : 01-jiva

Target Server Type    : MYSQL
Target Server Version : 50520
File Encoding         : 65001

Date: 2016-10-14 11:41:58
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `area_data`
-- ----------------------------
DROP TABLE IF EXISTS `area_data`;
CREATE TABLE `area_data` (
  `id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET latin1 COLLATE latin1_bin NOT NULL,
  `superarea` int(11) NOT NULL,
  `alignement` int(11) NOT NULL DEFAULT '-1',
  `Prisme` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of area_data
-- ----------------------------
INSERT INTO `area_data` VALUES ('0', 'Amakna', '0', '0', '0');
INSERT INTO `area_data` VALUES ('1', 'L\'ile des Wabbits', '0', '0', '0');
INSERT INTO `area_data` VALUES ('2', 'L\'ile de Moon', '0', '0', '0');
INSERT INTO `area_data` VALUES ('3', 'Prison', '0', '0', '0');
INSERT INTO `area_data` VALUES ('4', 'Tainela', '0', '0', '0');
INSERT INTO `area_data` VALUES ('5', 'Sufokia', '0', '0', '0');
INSERT INTO `area_data` VALUES ('6', 'Foret des Abraknydes', '0', '0', '0');
INSERT INTO `area_data` VALUES ('7', 'Bonta', '0', '1', '0');
INSERT INTO `area_data` VALUES ('8', 'Plaine de Cania', '0', '0', '0');
INSERT INTO `area_data` VALUES ('11', 'Brakmar', '0', '2', '0');
INSERT INTO `area_data` VALUES ('12', 'Lande de Sidimote', '0', '0', '0');
INSERT INTO `area_data` VALUES ('13', 'Territoire des Dopeuls', '0', '0', '0');
INSERT INTO `area_data` VALUES ('14', 'Village des Brigandins', '0', '0', '0');
INSERT INTO `area_data` VALUES ('15', 'Foire du Trool', '0', '0', '0');
INSERT INTO `area_data` VALUES ('16', 'Foire du Trool', '0', '0', '0');
INSERT INTO `area_data` VALUES ('17', 'Tainela', '0', '0', '0');
INSERT INTO `area_data` VALUES ('18', 'Astrub', '0', '0', '0');
INSERT INTO `area_data` VALUES ('19', 'Pandala Neutre', '0', '0', '0');
INSERT INTO `area_data` VALUES ('20', 'Pandala Eau', '0', '0', '0');
INSERT INTO `area_data` VALUES ('21', 'Pandala Terre', '0', '0', '0');
INSERT INTO `area_data` VALUES ('22', 'Pandala Feu', '0', '0', '0');
INSERT INTO `area_data` VALUES ('23', 'Pandala Air', '0', '0', '0');
INSERT INTO `area_data` VALUES ('24', 'Pandala Donjon', '0', '0', '0');
INSERT INTO `area_data` VALUES ('25', 'Le Champ du Repos', '0', '0', '0');
INSERT INTO `area_data` VALUES ('26', 'Le labyrinthe du Dragon Cochon', '0', '0', '0');
INSERT INTO `area_data` VALUES ('27', 'Donjon Abraknyde', '0', '0', '0');
INSERT INTO `area_data` VALUES ('28', 'Montagne des Koalaks', '0', '0', '0');
INSERT INTO `area_data` VALUES ('29', 'Donjon des Tofus', '0', '0', '0');
INSERT INTO `area_data` VALUES ('30', 'L\'ile du Minotoror', '0', '0', '0');
INSERT INTO `area_data` VALUES ('31', 'Le labyrinthe du Minotoror', '0', '0', '0');
INSERT INTO `area_data` VALUES ('32', 'La bibliothique du Maitre Corbac', '0', '0', '0');
INSERT INTO `area_data` VALUES ('33', 'Donjon des Canid', '0', '0', '0');
INSERT INTO `area_data` VALUES ('34', 'Caverne du Koulosse', '0', '0', '0');
INSERT INTO `area_data` VALUES ('35', 'Repaire de Skeunk', '0', '0', '0');
INSERT INTO `area_data` VALUES ('36', 'Sanctuaire des Familiers', '0', '0', '0');
INSERT INTO `area_data` VALUES ('37', 'Donjon des Craqueleurs', '0', '0', '0');
INSERT INTO `area_data` VALUES ('39', 'Donjon des Bworks', '0', '0', '0');
INSERT INTO `area_data` VALUES ('40', 'Donjon des Scarafeuilles', '0', '0', '0');
INSERT INTO `area_data` VALUES ('41', 'Donjon des Champs', '0', '0', '0');
INSERT INTO `area_data` VALUES ('42', 'Zone arctique', '0', '0', '0');
INSERT INTO `area_data` VALUES ('43', 'Donjon du Dragon Cochon', '0', '0', '0');
INSERT INTO `area_data` VALUES ('44', 'Donjon des Dragoeufs', '0', '0', '0');
INSERT INTO `area_data` VALUES ('45', 'Incarnam', '3', '0', '0');
INSERT INTO `area_data` VALUES ('46', 'Ile d\'Otoma', '0', '0', '0');
INSERT INTO `area_data` VALUES ('47', 'Village des Zoths', '0', '0', '0');
INSERT INTO `area_data` VALUES ('1022', 'Duty Free', '0', '0', '0');
INSERT INTO `area_data` VALUES ('1020', 'Dimension MJ', '0', '0', '0');
INSERT INTO `area_data` VALUES ('0', 'Amakna', '0', '0', '0');
INSERT INTO `area_data` VALUES ('1', 'L\'ile des Wabbits', '0', '0', '0');
INSERT INTO `area_data` VALUES ('2', 'L\'ile de Moon', '0', '0', '0');
INSERT INTO `area_data` VALUES ('3', 'Prison', '0', '0', '0');
INSERT INTO `area_data` VALUES ('4', 'Tainela', '0', '0', '0');
INSERT INTO `area_data` VALUES ('5', 'Sufokia', '0', '0', '0');
INSERT INTO `area_data` VALUES ('6', 'Foret des Abraknydes', '0', '0', '0');
INSERT INTO `area_data` VALUES ('7', 'Bonta', '0', '2', '0');
INSERT INTO `area_data` VALUES ('8', 'Plaine de Cania', '0', '2', '0');
INSERT INTO `area_data` VALUES ('11', 'Brakmar', '0', '0', '0');
INSERT INTO `area_data` VALUES ('12', 'Lande de Sidimote', '0', '0', '0');
INSERT INTO `area_data` VALUES ('13', 'Territoire des Dopeuls', '0', '0', '0');
INSERT INTO `area_data` VALUES ('14', 'Village des Brigandins', '0', '0', '0');
INSERT INTO `area_data` VALUES ('15', 'Foire du Trool', '0', '0', '0');
INSERT INTO `area_data` VALUES ('16', 'Foire du Trool', '0', '0', '0');
INSERT INTO `area_data` VALUES ('17', 'Tainela', '0', '0', '0');
INSERT INTO `area_data` VALUES ('18', 'Astrub', '0', '0', '0');
INSERT INTO `area_data` VALUES ('19', 'Pandala Neutre', '0', '0', '0');
INSERT INTO `area_data` VALUES ('20', 'Pandala Eau', '0', '0', '0');
INSERT INTO `area_data` VALUES ('21', 'Pandala Terre', '0', '0', '0');
INSERT INTO `area_data` VALUES ('22', 'Pandala Feu', '0', '0', '0');
INSERT INTO `area_data` VALUES ('23', 'Pandala Air', '0', '0', '0');
INSERT INTO `area_data` VALUES ('24', 'Pandala Donjon', '0', '0', '0');
INSERT INTO `area_data` VALUES ('25', 'Le Champ du Repos', '0', '0', '0');
INSERT INTO `area_data` VALUES ('26', 'Le labyrinthe du Dragon Cochon', '0', '0', '0');
INSERT INTO `area_data` VALUES ('27', 'Donjon Abraknyde', '0', '0', '0');
INSERT INTO `area_data` VALUES ('28', 'Montagne des Koalaks', '0', '0', '0');
INSERT INTO `area_data` VALUES ('29', 'Donjon des Tofus', '0', '0', '0');
INSERT INTO `area_data` VALUES ('30', 'L\'ile du Minotoror', '0', '0', '0');
INSERT INTO `area_data` VALUES ('31', 'Le labyrinthe du Minotoror', '0', '0', '0');
INSERT INTO `area_data` VALUES ('32', 'La bibliothique du Maitre Corbac', '0', '0', '0');
INSERT INTO `area_data` VALUES ('33', 'Donjon des Canid', '0', '0', '0');
INSERT INTO `area_data` VALUES ('34', 'Caverne du Koulosse', '0', '0', '0');
INSERT INTO `area_data` VALUES ('35', 'Repaire de Skeunk', '0', '0', '0');
INSERT INTO `area_data` VALUES ('36', 'Sanctuaire des Familiers', '0', '0', '0');
INSERT INTO `area_data` VALUES ('37', 'Donjon des Craqueleurs', '0', '0', '0');
INSERT INTO `area_data` VALUES ('39', 'Donjon des Bworks', '0', '0', '0');
INSERT INTO `area_data` VALUES ('40', 'Donjon des Scarafeuilles', '0', '0', '0');
INSERT INTO `area_data` VALUES ('41', 'Donjon des Champs', '0', '0', '0');
INSERT INTO `area_data` VALUES ('42', 'Zone arctique', '0', '0', '0');
INSERT INTO `area_data` VALUES ('43', 'Donjon du Dragon Cochon', '0', '0', '0');
INSERT INTO `area_data` VALUES ('44', 'Donjon des Dragoeufs', '0', '0', '0');
INSERT INTO `area_data` VALUES ('45', 'Incarnam', '3', '0', '0');
INSERT INTO `area_data` VALUES ('46', 'Ile d\'Otoma', '0', '0', '0');
INSERT INTO `area_data` VALUES ('47', 'Village des Zoths', '0', '0', '0');
INSERT INTO `area_data` VALUES ('1022', 'Duty Free', '0', '0', '0');
INSERT INTO `area_data` VALUES ('1020', 'Dimension MJ', '0', '0', '0');

-- ----------------------------
-- Table structure for `bandits`
-- ----------------------------
DROP TABLE IF EXISTS `bandits`;
CREATE TABLE `bandits` (
  `mobs` text NOT NULL,
  `maps` text NOT NULL,
  `time` bigint(255) NOT NULL DEFAULT '0',
  PRIMARY KEY (`time`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of bandits
-- ----------------------------
INSERT INTO `bandits` VALUES ('374,375,377', '3590,3591,3592,3593,3552,3511,3472,3433,3394,3355,3316,3277,3232,3187,3186,3185,3184,3183,3138,3098,3058,3018,3019,3020,3021,3061,3062,3063,3064,3024,4425,4426,4429,4432,4436,4440,4443,4444,4458,4459,4472,4473,4492,4493,4530,4527,4529,4526,4522,4518,4532,4523,2725,2724,2723,2722,2721,2720,2719,2718,2717,2716,2715,2744,2745,2742,2741,2770,2769,2768,2767,2766,2795,2821,2820,2962,2963,2964,3004,3005,3045,3046,3047,3087,3086,3126,3171,3216,3261,3300,3239,3378,3417,3418,3457,3496,3537,3578,3579,3580,3581,3582,3583,3584,3543,3544,3546,3547,3548,3549,3276,3275,3274,3273,3272,3311,3310,3309,3308,3307,3306,3305,3344,3343,3382,3381,3380,3379,3017,3016,3015,3014,4380,3053,3052,3051,3050,3049,3048,3088,9505,9508,4498,4501,4505,4511,4548,4554,4558,4563,4567,4571,4575', '1476436947176');

-- ----------------------------
-- Table structure for `banks`
-- ----------------------------
DROP TABLE IF EXISTS `banks`;
CREATE TABLE `banks` (
  `id` int(11) unsigned zerofill NOT NULL,
  `kamas` int(11) NOT NULL DEFAULT '0',
  `items` text CHARACTER SET latin1 NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of banks
-- ----------------------------
INSERT INTO `banks` VALUES ('00000000001', '0', '');

-- ----------------------------
-- Table structure for `coffres`
-- ----------------------------
DROP TABLE IF EXISTS `coffres`;
CREATE TABLE `coffres` (
  `id` int(11) NOT NULL,
  `id_house` int(11) NOT NULL,
  `mapid` int(11) NOT NULL,
  `cellid` int(11) NOT NULL,
  `object` text CHARACTER SET latin1 NOT NULL,
  `kamas` int(11) NOT NULL DEFAULT '0',
  `key` varchar(8) CHARACTER SET latin1 NOT NULL DEFAULT '-',
  `owner_id` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of coffres
-- ----------------------------
INSERT INTO `coffres` VALUES ('-12', '0', '7923', '212', '83710|83714|83715|83716|83717|83718|83719|83720|83721|83722|83723|83724|83725|83726|83727|83728|83743|83750|83757|83798|83804|83806|83808|83810|83812|83814|83816|83817|83819|83821|83823|83825|83924|83964|84029|84934|84939|84944|84949|84953|84958|85041|85098|85198|85319|85341|85421|85445|85563|85589|85618|85621|85773|85803|86453|86855|86856|87032|87033|87082|87083|87084|87085|87086|87104|87105|87106|87107|87163|87164|87165|87166|87259|87260|87261|87275|87276|87290|87291|87293|87294|87314|87315|87317|87318|87432|87433|87435|87436|87479|87572|87573|87589|87590|87665|87666|87667|87668|87721|87722|87723|87749|87750|87752|87930|87931|87932|87933|88186|88187|88341|88342|88344|88345|88365|88366|88368|88369|88426|88427|88429|88430|88441|89779|90139|90140|90142|90147|90670|90671|90672|90684|90686|90687|90697|91226|91228|91229|91230|91231|91240|91241|91259|91260|91271|91859|91860|91861|91862|91863|91864|91865|91866|91871|91985|91988|91989|91990|120288|133580|133584|134658|135346|135352|136729|137119|137501|137530|138261|138263|138694|138714|139022|139057|139545|139583|140107|140108|140158|140187|140188|140189|140275|140293|140294|140295|140353|140376|140377|140378|140435|140436|140437|140481|140482|140551|140553|140628|140629|140630|140633|140634|140635|140784|140790|140791|140793|140794|140795|140894|', '0', '-', '0');
INSERT INTO `coffres` VALUES ('-11', '0', '7445', '372', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('-10', '0', '7445', '266', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('-9', '0', '7414', '263', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('-8', '0', '7414', '201', '157388|157389|157390|157391|157392|157393|157394|157395|157743|157744|157745|157746|157747|157748|157749|157750|157751|157804|157805|157807|157808|', '0', '-', '0');
INSERT INTO `coffres` VALUES ('-7', '0', '7397', '118', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('-6', '0', '7396', '219', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('-5', '0', '7377', '399', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('-4', '0', '7377', '234', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('-3', '0', '7367', '423', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('-2', '0', '7348', '352', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('-1', '0', '7348', '245', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('0', '0', '0', '0', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('1', '655', '7710', '107', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('2', '645', '7701', '156', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('3', '645', '7703', '166', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('4', '700', '7694', '88', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('5', '701', '7696', '107', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('6', '684', '7686', '156', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('7', '684', '7687', '166', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('8', '641', '7617', '107', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('9', '652', '7636', '154', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('10', '674', '7741', '156', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('11', '674', '7740', '166', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('13', '690', '7682', '88', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('14', '667', '7661', '156', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('15', '667', '7660', '166', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('16', '670', '7625', '156', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('17', '670', '7624', '166', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('18', '693', '7630', '88', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('19', '698', '7647', '107', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('20', '651', '7729', '107', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('21', '687', '7651', '169', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('22', '687', '7651', '181', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('23', '687', '7649', '130', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('24', '687', '7649', '134', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('25', '654', '7669', '154', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('71', '94', '1589', '166', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('26', '711', '7778', '154', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('27', '655', '7710', '107', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('28', '656', '7713', '156', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('29', '656', '7716', '166', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('30', '790', '8878', '195', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('31', '87', '1585', '208', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('32', '791', '8879', '165', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('33', '268', '5400', '165', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('34', '268', '5400', '176', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('35', '268', '5400', '187', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('36', '268', '5400', '198', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('37', '268', '5399', '190', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('38', '248', '5343', '190', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('39', '248', '5344', '165', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('40', '248', '5344', '176', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('41', '248', '5344', '187', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('42', '248', '5344', '198', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('43', '245', '5336', '99', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('44', '246', '5338', '106', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('45', '247', '5340', '190', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('46', '247', '5341', '120', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('47', '249', '5346', '106', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('48', '250', '5347', '106', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('49', '251', '5348', '99', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('50', '252', '5352', '190', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('51', '252', '5353', '120', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('52', '253', '5355', '190', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('53', '253', '5354', '120', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('54', '254', '5359', '165', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('55', '254', '5359', '176', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('56', '254', '5359', '187', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('57', '254', '5359', '198', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('58', '254', '5358', '190', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('59', '127', '7663', '154', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('60', '110', '1605', '214', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('61', '807', '8902', '194', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('62', '93', '1588', '179', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('63', '660', '7640', '157', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('64', '660', '7639', '165', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('65', '661', '7734', '130', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('66', '661', '7734', '134', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('67', '661', '7732', '169', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('68', '661', '7732', '181', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('69', '710', '7737', '130', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('70', '710', '7737', '134', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('72', '137', '1641', '208', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('75', '224', '2052', '179', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('73', '224', '2052', '235', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('74', '224', '2052', '207', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('76', '224', '2052', '151', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('-100', '654', '7669', '154', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('-12', '0', '7923', '212', '83710|83714|83715|83716|83717|83718|83719|83720|83721|83722|83723|83724|83725|83726|83727|83728|83743|83750|83757|83798|83804|83806|83808|83810|83812|83814|83816|83817|83819|83821|83823|83825|83924|83964|84029|84934|84939|84944|84949|84953|84958|85041|85098|85198|85319|85341|85421|85445|85563|85589|85618|85621|85773|85803|86453|86855|86856|87032|87033|87082|87083|87084|87085|87086|87104|87105|87106|87107|87163|87164|87165|87166|87259|87260|87261|87275|87276|87290|87291|87293|87294|87314|87315|87317|87318|87432|87433|87435|87436|87479|87572|87573|87589|87590|87665|87666|87667|87668|87721|87722|87723|87749|87750|87752|87930|87931|87932|87933|88186|88187|88341|88342|88344|88345|88365|88366|88368|88369|88426|88427|88429|88430|88441|89779|90139|90140|90142|90147|90670|90671|90672|90684|90686|90687|90697|91226|91228|91229|91230|91231|91240|91241|91259|91260|91271|91859|91860|91861|91862|91863|91864|91865|91866|91871|91985|91988|91989|91990|120288|133580|133584|134658|135346|135352|136729|137119|137501|137530|138261|138263|138694|138714|139022|139057|139545|139583|140107|140108|140158|140187|140188|140189|140275|140293|140294|140295|140353|140376|140377|140378|140435|140436|140437|140481|140482|140551|140553|140628|140629|140630|140633|140634|140635|140784|140790|140791|140793|140794|140795|140894|', '0', '-', '0');
INSERT INTO `coffres` VALUES ('-11', '0', '7445', '372', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('-10', '0', '7445', '266', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('-9', '0', '7414', '263', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('-8', '0', '7414', '201', '157388|157389|157390|157391|157392|157393|157394|157395|157743|157744|157745|157746|157747|157748|157749|157750|157751|157804|157805|157807|157808|', '0', '-', '0');
INSERT INTO `coffres` VALUES ('-7', '0', '7397', '118', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('-6', '0', '7396', '219', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('-5', '0', '7377', '399', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('-4', '0', '7377', '234', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('-3', '0', '7367', '423', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('-2', '0', '7348', '352', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('-1', '0', '7348', '245', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('0', '0', '0', '0', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('1', '655', '7710', '107', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('2', '645', '7701', '156', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('3', '645', '7703', '166', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('4', '700', '7694', '88', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('5', '701', '7696', '107', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('6', '684', '7686', '156', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('7', '684', '7687', '166', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('8', '641', '7617', '107', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('9', '652', '7636', '154', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('10', '674', '7741', '156', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('11', '674', '7740', '166', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('13', '690', '7682', '88', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('14', '667', '7661', '156', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('15', '667', '7660', '166', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('16', '670', '7625', '156', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('17', '670', '7624', '166', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('18', '693', '7630', '88', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('19', '698', '7647', '107', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('20', '651', '7729', '107', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('21', '687', '7651', '169', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('22', '687', '7651', '181', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('23', '687', '7649', '130', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('24', '687', '7649', '134', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('25', '654', '7669', '154', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('71', '94', '1589', '166', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('26', '711', '7778', '154', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('27', '655', '7710', '107', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('28', '656', '7713', '156', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('29', '656', '7716', '166', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('30', '790', '8878', '195', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('31', '87', '1585', '208', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('32', '791', '8879', '165', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('33', '268', '5400', '165', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('34', '268', '5400', '176', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('35', '268', '5400', '187', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('36', '268', '5400', '198', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('37', '268', '5399', '190', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('38', '248', '5343', '190', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('39', '248', '5344', '165', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('40', '248', '5344', '176', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('41', '248', '5344', '187', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('42', '248', '5344', '198', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('43', '245', '5336', '99', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('44', '246', '5338', '106', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('45', '247', '5340', '190', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('46', '247', '5341', '120', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('47', '249', '5346', '106', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('48', '250', '5347', '106', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('49', '251', '5348', '99', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('50', '252', '5352', '190', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('51', '252', '5353', '120', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('52', '253', '5355', '190', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('53', '253', '5354', '120', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('54', '254', '5359', '165', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('55', '254', '5359', '176', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('56', '254', '5359', '187', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('57', '254', '5359', '198', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('58', '254', '5358', '190', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('59', '127', '7663', '154', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('60', '110', '1605', '214', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('61', '807', '8902', '194', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('62', '93', '1588', '179', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('63', '660', '7640', '157', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('64', '660', '7639', '165', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('65', '661', '7734', '130', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('66', '661', '7734', '134', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('67', '661', '7732', '169', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('68', '661', '7732', '181', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('69', '710', '7737', '130', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('70', '710', '7737', '134', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('72', '137', '1641', '208', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('75', '224', '2052', '179', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('73', '224', '2052', '235', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('74', '224', '2052', '207', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('76', '224', '2052', '151', '', '0', '-', '0');
INSERT INTO `coffres` VALUES ('-100', '654', '7669', '154', '', '0', '-', '0');

-- ----------------------------
-- Table structure for `events`
-- ----------------------------
DROP TABLE IF EXISTS `events`;
CREATE TABLE `events` (
  `Date` text NOT NULL,
  `points` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of events
-- ----------------------------

-- ----------------------------
-- Table structure for `gifts`
-- ----------------------------
DROP TABLE IF EXISTS `gifts`;
CREATE TABLE `gifts` (
  `id` int(15) NOT NULL,
  `objects` varchar(1028) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of gifts
-- ----------------------------
INSERT INTO `gifts` VALUES ('1', '');

-- ----------------------------
-- Table structure for `guild_members`
-- ----------------------------
DROP TABLE IF EXISTS `guild_members`;
CREATE TABLE `guild_members` (
  `guid` int(11) NOT NULL,
  `guild` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `level` int(11) NOT NULL,
  `gfxid` int(11) NOT NULL,
  `rank` int(11) NOT NULL,
  `xpdone` bigint(20) NOT NULL,
  `pxp` int(11) NOT NULL,
  `rights` int(11) NOT NULL,
  `align` tinyint(4) NOT NULL,
  `lastConnection` varchar(30) CHARACTER SET latin1 NOT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of guild_members
-- ----------------------------

-- ----------------------------
-- Table structure for `guilds`
-- ----------------------------
DROP TABLE IF EXISTS `guilds`;
CREATE TABLE `guilds` (
  `id` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET latin1 NOT NULL,
  `emblem` varchar(20) CHARACTER SET latin1 NOT NULL,
  `lvl` int(11) NOT NULL DEFAULT '1',
  `xp` bigint(20) NOT NULL DEFAULT '0',
  `capital` int(11) NOT NULL DEFAULT '0',
  `nbrmax` int(11) NOT NULL DEFAULT '1',
  `sorts` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '462;0|461;0|460;0|459;0|458;0|457;0|456;0|455;0|454;0|453;0|452;0|451;0|',
  `stats` varchar(255) CHARACTER SET latin1 NOT NULL DEFAULT '176;100|158;1000|124;100|',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of guilds
-- ----------------------------

-- ----------------------------
-- Table structure for `hdvs_items`
-- ----------------------------
DROP TABLE IF EXISTS `hdvs_items`;
CREATE TABLE `hdvs_items` (
  `id` int(11) NOT NULL,
  `map` int(11) NOT NULL,
  `ownerGuid` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `count` int(3) NOT NULL,
  `sellDate` varchar(20) CHARACTER SET latin1 NOT NULL DEFAULT 'rien',
  `itemID` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of hdvs_items
-- ----------------------------

-- ----------------------------
-- Table structure for `houses`
-- ----------------------------
DROP TABLE IF EXISTS `houses`;
CREATE TABLE `houses` (
  `id` int(10) unsigned NOT NULL,
  `map_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cell_id` int(10) unsigned NOT NULL DEFAULT '0',
  `owner_id` int(10) NOT NULL DEFAULT '0',
  `sale` int(10) NOT NULL DEFAULT '-1',
  `saleBase` text NOT NULL,
  `guild_id` int(10) NOT NULL DEFAULT '-1',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `key` varchar(8) NOT NULL DEFAULT '00000000',
  `guild_rights` int(8) unsigned NOT NULL DEFAULT '0',
  `mapid` int(11) NOT NULL DEFAULT '0',
  `caseid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of houses
-- ----------------------------
INSERT INTO `houses` VALUES ('66', '675', '194', '0', '2000000', '2000000', '0', '0', '-', '0', '1522', '356');
INSERT INTO `houses` VALUES ('67', '675', '278', '0', '2000000', '2000000', '0', '0', '-', '0', '1521', '356');
INSERT INTO `houses` VALUES ('68', '30', '309', '0', '2000000', '2000000', '0', '0', '-', '0', '1524', '356');
INSERT INTO `houses` VALUES ('69', '40', '330', '0', '1500000', '1500000', '0', '0', '-', '0', '503', '192');
INSERT INTO `houses` VALUES ('70', '40', '351', '0', '1000000', '1000000', '0', '0', '-', '0', '500', '195');
INSERT INTO `houses` VALUES ('74', '36', '331', '0', '2000000', '2000000', '0', '0', '-', '0', '100', '369');
INSERT INTO `houses` VALUES ('75', '37', '213', '0', '525000', '525000', '0', '0', '-', '0', '105', '354');
INSERT INTO `houses` VALUES ('76', '37', '305', '0', '1000000', '1000000', '0', '0', '-', '0', '102', '285');
INSERT INTO `houses` VALUES ('77', '42', '248', '0', '1000000', '1000000', '0', '0', '-', '0', '509', '156');
INSERT INTO `houses` VALUES ('78', '42', '293', '0', '4000000', '4000000', '0', '0', '-', '0', '506', '156');
INSERT INTO `houses` VALUES ('79', '49', '52', '0', '1000000', '1000000', '0', '0', '-', '0', '514', '226');
INSERT INTO `houses` VALUES ('80', '49', '407', '0', '525000', '525000', '0', '0', '-', '0', '515', '150');
INSERT INTO `houses` VALUES ('81', '50', '161', '0', '2000000', '2000000', '0', '0', '-', '0', '516', '223');
INSERT INTO `houses` VALUES ('82', '50', '122', '0', '525000', '525000', '0', '0', '-', '0', '1533', '356');
INSERT INTO `houses` VALUES ('83', '44', '105', '0', '525000', '525000', '0', '0', '-', '0', '1538', '356');
INSERT INTO `houses` VALUES ('84', '48', '379', '0', '1000000', '1000000', '0', '0', '-', '0', '512', '156');
INSERT INTO `houses` VALUES ('85', '482', '195', '0', '525000', '525000', '0', '0', '-', '0', '1541', '356');
INSERT INTO `houses` VALUES ('87', '540', '323', '0', '2000000', '2000000', '0', '0', '-', '0', '1585', '299');
INSERT INTO `houses` VALUES ('88', '540', '181', '0', '1000000', '1000000', '0', '0', '-', '0', '1582', '386');
INSERT INTO `houses` VALUES ('89', '542', '148', '0', '2000000', '2000000', '0', '0', '-', '0', '1592', '384');
INSERT INTO `houses` VALUES ('90', '761', '285', '0', '525000', '525000', '0', '0', '-', '0', '1583', '246');
INSERT INTO `houses` VALUES ('91', '761', '119', '0', '525000', '525000', '0', '0', '-', '0', '1584', '233');
INSERT INTO `houses` VALUES ('92', '763', '99', '0', '525000', '525000', '0', '0', '-', '0', '1586', '222');
INSERT INTO `houses` VALUES ('93', '763', '53', '0', '1000000', '1000000', '0', '0', '-', '0', '1588', '266');
INSERT INTO `houses` VALUES ('94', '763', '162', '0', '1000000', '525000', '0', '0', '-', '0', '1589', '283');
INSERT INTO `houses` VALUES ('95', '763', '189', '0', '1000000', '1000000', '0', '0', '-', '0', '1591', '283');
INSERT INTO `houses` VALUES ('96', '166', '359', '0', '525000', '525000', '0', '0', '-', '0', '1595', '271');
INSERT INTO `houses` VALUES ('97', '166', '234', '0', '4000000', '4000000', '0', '0', '-', '0', '1596', '279');
INSERT INTO `houses` VALUES ('109', '551', '243', '0', '2000000', '2000000', '0', '0', '-', '0', '1609', '243');
INSERT INTO `houses` VALUES ('110', '548', '409', '0', '1000000', '1000000', '0', '0', '-', '0', '1605', '381');
INSERT INTO `houses` VALUES ('111', '537', '240', '0', '1000000', '1000000', '0', '0', '-', '0', '1598', '240');
INSERT INTO `houses` VALUES ('112', '537', '164', '0', '1000000', '1000000', '0', '0', '-', '0', '1601', '164');
INSERT INTO `houses` VALUES ('113', '485', '192', '0', '2000000', '2000000', '0', '0', '-', '0', '1622', '192');
INSERT INTO `houses` VALUES ('114', '165', '173', '0', '2000000', '2000000', '0', '0', '-', '0', '1602', '241');
INSERT INTO `houses` VALUES ('115', '165', '64', '0', '2000000', '2000000', '0', '0', '-', '0', '1606', '246');
INSERT INTO `houses` VALUES ('116', '165', '190', '0', '2000000', '2000000', '0', '0', '-', '0', '1603', '253');
INSERT INTO `houses` VALUES ('117', '165', '194', '0', '2000000', '2000000', '0', '0', '-', '0', '1607', '258');
INSERT INTO `houses` VALUES ('118', '165', '236', '0', '2000000', '2000000', '0', '0', '-', '0', '1608', '270');
INSERT INTO `houses` VALUES ('119', '167', '191', '0', '2000000', '2000000', '0', '0', '-', '0', '1611', '267');
INSERT INTO `houses` VALUES ('120', '167', '150', '0', '2000000', '2000000', '0', '0', '-', '0', '1614', '207');
INSERT INTO `houses` VALUES ('122', '2023', '206', '0', '2000000', '2000000', '0', '0', '-', '0', '2022', '381');
INSERT INTO `houses` VALUES ('123', '1440', '137', '0', '2000000', '2000000', '0', '0', '-', '0', '1635', '381');
INSERT INTO `houses` VALUES ('124', '1391', '139', '0', '2000000', '2000000', '0', '0', '-', '0', '1630', '139');
INSERT INTO `houses` VALUES ('125', '1144', '229', '0', '525000', '525000', '0', '0', '-', '0', '1626', '229');
INSERT INTO `houses` VALUES ('126', '1144', '286', '0', '1000000', '1000000', '0', '0', '-', '0', '1628', '286');
INSERT INTO `houses` VALUES ('127', '568', '198', '0', '2000000', '2000000', '0', '0', '-', '0', '1621', '198');
INSERT INTO `houses` VALUES ('128', '569', '236', '0', '525000', '525000', '0', '0', '-', '0', '1617', '236');
INSERT INTO `houses` VALUES ('129', '185', '241', '0', '2000000', '2000000', '0', '0', '-', '0', '1619', '270');
INSERT INTO `houses` VALUES ('130', '908', '308', '0', '2000000', '2000000', '0', '0', '-', '0', '1627', '308');
INSERT INTO `houses` VALUES ('131', '908', '171', '0', '2000000', '2000000', '0', '0', '-', '0', '1632', '171');
INSERT INTO `houses` VALUES ('132', '912', '301', '0', '1000000', '1000000', '0', '0', '-', '0', '1633', '266');
INSERT INTO `houses` VALUES ('133', '918', '239', '0', '2000000', '2000000', '0', '0', '-', '0', '1634', '270');
INSERT INTO `houses` VALUES ('134', '918', '281', '0', '2000000', '2000000', '0', '0', '-', '0', '1640', '196');
INSERT INTO `houses` VALUES ('135', '184', '137', '0', '2000000', '2000000', '0', '0', '-', '0', '1616', '266');
INSERT INTO `houses` VALUES ('136', '496', '234', '0', '2000000', '2000000', '0', '0', '-', '0', '1638', '426');
INSERT INTO `houses` VALUES ('137', '486', '273', '0', '525000', '525000', '0', '0', '-', '0', '1641', '299');
INSERT INTO `houses` VALUES ('138', '486', '226', '0', '2000000', '2000000', '0', '0', '-', '0', '1639', '226');
INSERT INTO `houses` VALUES ('139', '531', '109', '0', '2000000', '2000000', '0', '0', '-', '0', '1644', '109');
INSERT INTO `houses` VALUES ('140', '531', '154', '0', '525000', '525000', '0', '0', '-', '0', '1648', '154');
INSERT INTO `houses` VALUES ('142', '749', '297', '0', '2000000', '2000000', '0', '0', '-', '0', '1520', '297');
INSERT INTO `houses` VALUES ('143', '763', '419', '0', '4000000', '4000000', '0', '0', '-', '0', '1797', '279');
INSERT INTO `houses` VALUES ('144', '1166', '169', '0', '2000000', '2000000', '0', '0', '-', '0', '1872', '169');
INSERT INTO `houses` VALUES ('145', '1166', '177', '0', '4000000', '4000000', '0', '0', '-', '0', '1917', '177');
INSERT INTO `houses` VALUES ('146', '1909', '77', '0', '4000000', '4000000', '0', '0', '-', '0', '1918', '77');
INSERT INTO `houses` VALUES ('147', '1906', '219', '0', '2000000', '2000000', '0', '0', '-', '0', '1922', '219');
INSERT INTO `houses` VALUES ('148', '1906', '163', '0', '4000000', '4000000', '0', '0', '-', '0', '1927', '163');
INSERT INTO `houses` VALUES ('149', '1903', '229', '0', '4000000', '4000000', '0', '0', '-', '0', '1938', '229');
INSERT INTO `houses` VALUES ('150', '1903', '154', '0', '2000000', '2000000', '0', '0', '-', '0', '1933', '154');
INSERT INTO `houses` VALUES ('151', '1219', '266', '0', '2000000', '2000000', '0', '0', '-', '0', '2005', '307');
INSERT INTO `houses` VALUES ('152', '1219', '168', '0', '4000000', '4000000', '0', '0', '-', '0', '1953', '168');
INSERT INTO `houses` VALUES ('153', '1879', '183', '0', '4000000', '4000000', '0', '0', '-', '0', '2007', '183');
INSERT INTO `houses` VALUES ('154', '1879', '171', '0', '4000000', '4000000', '0', '0', '-', '0', '2009', '228');
INSERT INTO `houses` VALUES ('155', '1884', '124', '0', '2000000', '2000000', '0', '0', '-', '0', '2010', '124');
INSERT INTO `houses` VALUES ('156', '1884', '274', '0', '2000000', '2000000', '0', '0', '-', '0', '2011', '274');
INSERT INTO `houses` VALUES ('157', '1884', '199', '0', '2000000', '2000000', '0', '0', '-', '0', '2012', '199');
INSERT INTO `houses` VALUES ('158', '1877', '193', '0', '2000000', '2000000', '0', '0', '-', '0', '2013', '193');
INSERT INTO `houses` VALUES ('159', '1877', '213', '0', '2000000', '2000000', '0', '0', '-', '0', '2014', '213');
INSERT INTO `houses` VALUES ('160', '2015', '223', '0', '10000000', '10000000', '0', '0', '-', '0', '2016', '327');
INSERT INTO `houses` VALUES ('161', '1894', '151', '0', '2000000', '2000000', '0', '0', '-', '0', '1871', '151');
INSERT INTO `houses` VALUES ('162', '1896', '250', '0', '4000000', '4000000', '0', '0', '-', '0', '1920', '250');
INSERT INTO `houses` VALUES ('163', '1896', '280', '0', '2000000', '2000000', '0', '0', '-', '0', '1921', '280');
INSERT INTO `houses` VALUES ('164', '1198', '184', '0', '2000000', '2000000', '0', '0', '-', '0', '1923', '184');
INSERT INTO `houses` VALUES ('165', '1198', '206', '0', '2000000', '2000000', '0', '0', '-', '0', '1959', '206');
INSERT INTO `houses` VALUES ('166', '1891', '138', '0', '2000000', '2000000', '0', '0', '-', '0', '1925', '138');
INSERT INTO `houses` VALUES ('167', '1891', '194', '0', '2000000', '2000000', '0', '0', '-', '0', '1926', '194');
INSERT INTO `houses` VALUES ('168', '1891', '222', '0', '2000000', '2000000', '0', '0', '-', '0', '1961', '222');
INSERT INTO `houses` VALUES ('169', '1893', '368', '0', '2000000', '2000000', '0', '0', '-', '0', '1929', '368');
INSERT INTO `houses` VALUES ('170', '1893', '398', '0', '2000000', '2000000', '0', '0', '-', '0', '1930', '398');
INSERT INTO `houses` VALUES ('171', '1893', '170', '0', '2000000', '2000000', '0', '0', '-', '0', '1931', '170');
INSERT INTO `houses` VALUES ('172', '1902', '204', '0', '2000000', '2000000', '0', '0', '-', '0', '1932', '204');
INSERT INTO `houses` VALUES ('173', '1889', '151', '0', '2000000', '2000000', '0', '0', '-', '0', '1935', '151');
INSERT INTO `houses` VALUES ('174', '1889', '193', '0', '2000000', '2000000', '0', '0', '-', '0', '1939', '193');
INSERT INTO `houses` VALUES ('175', '1889', '140', '0', '4000000', '4000000', '0', '0', '-', '0', '1969', '140');
INSERT INTO `houses` VALUES ('176', '1883', '208', '0', '2000000', '2000000', '0', '0', '-', '0', '1980', '208');
INSERT INTO `houses` VALUES ('177', '1883', '225', '0', '2000000', '2000000', '0', '0', '-', '0', '1941', '225');
INSERT INTO `houses` VALUES ('178', '1899', '163', '0', '2000000', '2000000', '0', '0', '-', '0', '1943', '163');
INSERT INTO `houses` VALUES ('179', '1899', '137', '0', '2000000', '2000000', '0', '0', '-', '0', '1944', '137');
INSERT INTO `houses` VALUES ('180', '1899', '191', '0', '2000000', '2000000', '0', '0', '-', '0', '1945', '191');
INSERT INTO `houses` VALUES ('181', '1878', '162', '0', '2000000', '2000000', '0', '0', '-', '0', '1946', '162');
INSERT INTO `houses` VALUES ('182', '1878', '134', '0', '2000000', '2000000', '0', '0', '-', '0', '1947', '134');
INSERT INTO `houses` VALUES ('183', '1881', '239', '0', '2000000', '2000000', '0', '0', '-', '0', '1949', '239');
INSERT INTO `houses` VALUES ('184', '1881', '155', '0', '4000000', '4000000', '0', '0', '-', '0', '1951', '155');
INSERT INTO `houses` VALUES ('185', '1881', '110', '0', '2000000', '2000000', '0', '0', '-', '0', '1955', '110');
INSERT INTO `houses` VALUES ('186', '1881', '285', '0', '2000000', '2000000', '0', '0', '-', '0', '1956', '285');
INSERT INTO `houses` VALUES ('187', '1898', '345', '0', '4000000', '4000000', '0', '0', '-', '0', '1985', '345');
INSERT INTO `houses` VALUES ('188', '1889', '249', '0', '2000000', '2000000', '0', '0', '-', '0', '1934', '249');
INSERT INTO `houses` VALUES ('190', '1890', '365', '0', '4000000', '4000000', '0', '0', '-', '0', '1981', '365');
INSERT INTO `houses` VALUES ('191', '1890', '107', '0', '2000000', '2000000', '0', '0', '-', '0', '1984', '107');
INSERT INTO `houses` VALUES ('192', '1890', '257', '0', '2000000', '2000000', '0', '0', '-', '0', '1982', '257');
INSERT INTO `houses` VALUES ('193', '1908', '237', '0', '1000000', '10000000', '0', '0', '-', '0', '1965', '258');
INSERT INTO `houses` VALUES ('194', '1895', '125', '0', '2000000', '2000000', '0', '0', '-', '0', '1999', '125');
INSERT INTO `houses` VALUES ('195', '1895', '78', '0', '2000000', '2000000', '0', '0', '-', '0', '2000', '78');
INSERT INTO `houses` VALUES ('196', '1895', '219', '0', '2000000', '2000000', '0', '0', '-', '0', '2001', '219');
INSERT INTO `houses` VALUES ('197', '1895', '216', '0', '2000000', '2000000', '0', '0', '-', '0', '2002', '216');
INSERT INTO `houses` VALUES ('198', '1895', '185', '0', '4000000', '4000000', '0', '0', '-', '0', '2004', '185');
INSERT INTO `houses` VALUES ('199', '1914', '136', '0', '2000000', '2000000', '0', '0', '-', '0', '1973', '136');
INSERT INTO `houses` VALUES ('200', '1914', '191', '0', '2000000', '2000000', '0', '0', '-', '0', '1974', '191');
INSERT INTO `houses` VALUES ('201', '1914', '156', '0', '2000000', '2000000', '0', '0', '-', '0', '1975', '156');
INSERT INTO `houses` VALUES ('202', '1914', '298', '0', '2000000', '2000000', '0', '0', '-', '0', '1976', '298');
INSERT INTO `houses` VALUES ('203', '1914', '152', '0', '2000000', '2000000', '0', '0', '-', '0', '1977', '152');
INSERT INTO `houses` VALUES ('204', '1914', '339', '0', '2000000', '2000000', '0', '0', '-', '0', '1978', '339');
INSERT INTO `houses` VALUES ('205', '1892', '105', '0', '2000000', '2000000', '0', '0', '-', '0', '1986', '105');
INSERT INTO `houses` VALUES ('206', '1892', '280', '0', '2000000', '2000000', '0', '0', '-', '0', '1995', '280');
INSERT INTO `houses` VALUES ('207', '1892', '224', '0', '2000000', '2000000', '0', '0', '-', '0', '1989', '224');
INSERT INTO `houses` VALUES ('208', '1892', '322', '0', '2000000', '2000000', '0', '0', '-', '0', '1990', '322');
INSERT INTO `houses` VALUES ('209', '1892', '107', '0', '2000000', '2000000', '0', '0', '-', '0', '1991', '107');
INSERT INTO `houses` VALUES ('210', '1892', '146', '0', '2000000', '2000000', '0', '0', '-', '0', '1992', '146');
INSERT INTO `houses` VALUES ('211', '1897', '265', '0', '2000000', '2000000', '0', '0', '-', '0', '1993', '265');
INSERT INTO `houses` VALUES ('212', '1897', '283', '0', '2000000', '2000000', '0', '0', '-', '0', '1994', '283');
INSERT INTO `houses` VALUES ('214', '1897', '144', '0', '2000000', '2000000', '0', '0', '-', '0', '1996', '144');
INSERT INTO `houses` VALUES ('215', '1897', '82', '0', '2000000', '2000000', '0', '0', '-', '0', '1997', '82');
INSERT INTO `houses` VALUES ('216', '1897', '84', '0', '2000000', '2000000', '0', '0', '-', '0', '1998', '84');
INSERT INTO `houses` VALUES ('217', '1915', '123', '0', '2000000', '2000000', '0', '0', '-', '0', '1958', '123');
INSERT INTO `houses` VALUES ('218', '1915', '254', '0', '2000000', '2000000', '0', '0', '-', '0', '1960', '254');
INSERT INTO `houses` VALUES ('219', '1915', '147', '0', '2000000', '2000000', '0', '0', '-', '0', '1962', '147');
INSERT INTO `houses` VALUES ('220', '1915', '399', '0', '2000000', '2000000', '0', '0', '-', '0', '1963', '399');
INSERT INTO `houses` VALUES ('221', '1910', '122', '0', '2000000', '2000000', '0', '0', '-', '0', '1936', '122');
INSERT INTO `houses` VALUES ('222', '1910', '191', '0', '2000000', '2000000', '0', '0', '-', '0', '1954', '191');
INSERT INTO `houses` VALUES ('223', '709', '300', '0', '10000000', '10000000', '4', '0', '-', '0', '2026', '300');
INSERT INTO `houses` VALUES ('224', '2026', '264', '0', '10000000', '10000000', '0', '0', '-', '0', '2051', '397');
INSERT INTO `houses` VALUES ('226', '4264', '436', '0', '4000000', '4000000', '0', '0', '-', '0', '5122', '439');
INSERT INTO `houses` VALUES ('227', '4213', '394', '0', '1500000', '1500000', '0', '0', '-', '0', '5124', '394');
INSERT INTO `houses` VALUES ('228', '4213', '454', '0', '4000000', '4000000', '0', '0', '-', '0', '5129', '454');
INSERT INTO `houses` VALUES ('229', '4213', '142', '0', '2000000', '2000000', '0', '0', '-', '0', '5131', '142');
INSERT INTO `houses` VALUES ('230', '4212', '261', '0', '4000000', '4000000', '0', '0', '-', '0', '5135', '261');
INSERT INTO `houses` VALUES ('231', '4212', '329', '0', '3000000', '3000000', '0', '0', '-', '0', '5141', '329');
INSERT INTO `houses` VALUES ('232', '4212', '386', '0', '2000000', '2000000', '0', '0', '-', '0', '5146', '386');
INSERT INTO `houses` VALUES ('233', '4345', '125', '0', '4000000', '4000000', '0', '0', '-', '0', '5144', '125');
INSERT INTO `houses` VALUES ('234', '4343', '181', '0', '4000000', '4000000', '0', '0', '-', '0', '5297', '181');
INSERT INTO `houses` VALUES ('235', '4170', '373', '0', '3000000', '3000000', '0', '0', '-', '0', '5284', '373');
INSERT INTO `houses` VALUES ('236', '4343', '187', '0', '5000000', '5000000', '0', '0', '-', '0', '5303', '187');
INSERT INTO `houses` VALUES ('237', '4170', '116', '0', '4000000', '4000000', '0', '0', '-', '0', '5307', '113');
INSERT INTO `houses` VALUES ('238', '4337', '201', '0', '4000000', '4000000', '0', '0', '-', '0', '5310', '201');
INSERT INTO `houses` VALUES ('239', '4170', '528', '0', '1500000', '1500000', '0', '0', '-', '0', '5312', '528');
INSERT INTO `houses` VALUES ('240', '4170', '127', '0', '5000000', '5000000', '0', '0', '-', '0', '5316', '127');
INSERT INTO `houses` VALUES ('241', '4171', '415', '0', '3000000', '3000000', '0', '0', '-', '0', '5320', '415');
INSERT INTO `houses` VALUES ('242', '4171', '475', '0', '4000000', '4000000', '0', '0', '-', '0', '5323', '472');
INSERT INTO `houses` VALUES ('243', '4171', '403', '0', '4000000', '4000000', '0', '0', '-', '0', '5329', '403');
INSERT INTO `houses` VALUES ('244', '4171', '738', '0', '1500000', '1500000', '0', '0', '-', '0', '5325', '738');
INSERT INTO `houses` VALUES ('245', '4206', '192', '0', '2000000', '2000000', '0', '0', '-', '0', '5336', '137');
INSERT INTO `houses` VALUES ('246', '4206', '307', '0', '1500000', '1500000', '0', '0', '-', '0', '5338', '145');
INSERT INTO `houses` VALUES ('247', '4206', '367', '0', '4000000', '4000000', '0', '0', '-', '0', '5339', '228');
INSERT INTO `houses` VALUES ('248', '4206', '243', '0', '5000000', '5000000', '0', '0', '-', '0', '5342', '228');
INSERT INTO `houses` VALUES ('249', '4210', '174', '0', '1500000', '1500000', '0', '0', '-', '0', '5346', '145');
INSERT INTO `houses` VALUES ('250', '4210', '270', '0', '1500000', '1500000', '0', '0', '-', '0', '5347', '145');
INSERT INTO `houses` VALUES ('251', '4210', '628', '0', '2000000', '2000000', '0', '0', '-', '0', '5348', '137');
INSERT INTO `houses` VALUES ('252', '4210', '404', '0', '4000000', '4000000', '0', '0', '-', '0', '5351', '234');
INSERT INTO `houses` VALUES ('253', '4210', '187', '0', '4000000', '4000000', '0', '0', '-', '0', '5356', '228');
INSERT INTO `houses` VALUES ('254', '4210', '327', '0', '5000000', '5000000', '0', '0', '-', '0', '5357', '234');
INSERT INTO `houses` VALUES ('255', '4216', '287', '0', '1500000', '1500000', '0', '0', '-', '0', '5365', '287');
INSERT INTO `houses` VALUES ('256', '4216', '230', '0', '3000000', '3000000', '0', '0', '-', '0', '5368', '230');
INSERT INTO `houses` VALUES ('257', '4216', '325', '0', '3000000', '3000000', '0', '0', '-', '0', '5371', '325');
INSERT INTO `houses` VALUES ('258', '4216', '363', '0', '3000000', '3000000', '0', '0', '-', '0', '5374', '363');
INSERT INTO `houses` VALUES ('259', '4216', '423', '0', '4000000', '4000000', '0', '0', '-', '0', '5377', '420');
INSERT INTO `houses` VALUES ('260', '4216', '243', '0', '4000000', '4000000', '0', '0', '-', '0', '5380', '243');
INSERT INTO `houses` VALUES ('261', '4218', '659', '0', '1500000', '1500000', '0', '0', '-', '0', '5384', '659');
INSERT INTO `houses` VALUES ('262', '4218', '180', '0', '1500000', '1500000', '0', '0', '-', '0', '5385', '180');
INSERT INTO `houses` VALUES ('263', '2218', '621', '0', '1500000', '1500000', '0', '0', '-', '0', '5386', '621');
INSERT INTO `houses` VALUES ('264', '4214', '395', '0', '2000000', '2000000', '0', '0', '-', '0', '5388', '395');
INSERT INTO `houses` VALUES ('265', '2218', '544', '0', '4000000', '4000000', '0', '0', '-', '0', '5394', '544');
INSERT INTO `houses` VALUES ('266', '4214', '494', '0', '4000000', '4000000', '0', '0', '-', '0', '5393', '497');
INSERT INTO `houses` VALUES ('267', '2218', '756', '0', '4000000', '4000000', '0', '0', '-', '0', '5398', '756');
INSERT INTO `houses` VALUES ('268', '4214', '335', '0', '5000000', '5000000', '0', '0', '-', '0', '5397', '228');
INSERT INTO `houses` VALUES ('269', '2218', '384', '0', '5000000', '5000000', '0', '0', '-', '0', '5405', '384');
INSERT INTO `houses` VALUES ('270', '4178', '150', '0', '3000000', '3000000', '0', '0', '-', '0', '5408', '150');
INSERT INTO `houses` VALUES ('271', '2214', '728', '0', '2000000', '2000000', '0', '0', '-', '0', '5410', '728');
INSERT INTO `houses` VALUES ('272', '4178', '248', '0', '4000000', '4000000', '0', '0', '-', '0', '5413', '245');
INSERT INTO `houses` VALUES ('273', '2214', '568', '0', '4000000', '4000000', '0', '0', '-', '0', '5416', '568');
INSERT INTO `houses` VALUES ('274', '2214', '243', '0', '1500000', '1500000', '0', '0', '-', '0', '5417', '243');
INSERT INTO `houses` VALUES ('275', '4174', '679', '0', '1500000', '1500000', '0', '0', '-', '0', '5418', '679');
INSERT INTO `houses` VALUES ('276', '4173', '600', '0', '1500000', '1500000', '0', '0', '-', '0', '5419', '600');
INSERT INTO `houses` VALUES ('277', '4173', '561', '0', '3000000', '3000000', '0', '0', '-', '0', '5423', '561');
INSERT INTO `houses` VALUES ('278', '2216', '531', '0', '3000000', '3000000', '0', '0', '-', '0', '5425', '531');
INSERT INTO `houses` VALUES ('279', '2216', '227', '0', '4000000', '4000000', '0', '0', '-', '0', '5430', '227');
INSERT INTO `houses` VALUES ('280', '4173', '345', '0', '4000000', '4000000', '0', '0', '-', '0', '5431', '345');
INSERT INTO `houses` VALUES ('281', '4336', '671', '0', '4000000', '4000000', '0', '0', '-', '0', '5435', '671');
INSERT INTO `houses` VALUES ('282', '4173', '170', '0', '4000000', '4000000', '0', '0', '-', '0', '5437', '170');
INSERT INTO `houses` VALUES ('283', '4173', '279', '0', '5000000', '5000000', '0', '0', '-', '0', '5441', '279');
INSERT INTO `houses` VALUES ('284', '4336', '305', '0', '2000000', '2000000', '0', '0', '-', '0', '5443', '305');
INSERT INTO `houses` VALUES ('285', '4207', '374', '0', '2000000', '2000000', '0', '0', '-', '0', '5446', '374');
INSERT INTO `houses` VALUES ('286', '4336', '181', '0', '4000000', '4000000', '0', '0', '-', '0', '5448', '181');
INSERT INTO `houses` VALUES ('287', '4207', '450', '0', '2000000', '2000000', '0', '0', '-', '0', '5450', '450');
INSERT INTO `houses` VALUES ('288', '4207', '488', '0', '3000000', '3000000', '0', '0', '-', '0', '5455', '488');
INSERT INTO `houses` VALUES ('289', '4336', '188', '0', '5000000', '5000000', '0', '0', '-', '0', '5457', '188');
INSERT INTO `houses` VALUES ('290', '4207', '498', '0', '4000000', '4000000', '0', '0', '-', '0', '5460', '495');
INSERT INTO `houses` VALUES ('291', '4287', '81', '0', '4000000', '4000000', '0', '0', '-', '0', '5466', '78');
INSERT INTO `houses` VALUES ('292', '4207', '150', '0', '4000000', '4000000', '0', '0', '-', '0', '5465', '150');
INSERT INTO `houses` VALUES ('293', '4249', '508', '0', '4000000', '4000000', '0', '0', '-', '0', '5470', '508');
INSERT INTO `houses` VALUES ('294', '4209', '654', '0', '1500000', '1500000', '0', '0', '-', '0', '5469', '654');
INSERT INTO `houses` VALUES ('295', '4209', '730', '0', '1500000', '1500000', '0', '0', '-', '0', '5471', '730');
INSERT INTO `houses` VALUES ('296', '4209', '353', '0', '4000000', '4000000', '0', '0', '-', '0', '5474', '353');
INSERT INTO `houses` VALUES ('297', '4209', '439', '0', '4000000', '4000000', '0', '0', '-', '0', '5478', '439');
INSERT INTO `houses` VALUES ('298', '4217', '625', '0', '1500000', '1500000', '0', '0', '-', '0', '5479', '625');
INSERT INTO `houses` VALUES ('299', '4217', '506', '0', '3000000', '3000000', '0', '0', '-', '0', '5482', '506');
INSERT INTO `houses` VALUES ('300', '4219', '360', '0', '1500000', '1500000', '0', '0', '-', '0', '5483', '360');
INSERT INTO `houses` VALUES ('302', '4219', '87', '0', '1500000', '1500000', '0', '0', '-', '0', '5485', '87');
INSERT INTO `houses` VALUES ('303', '4219', '126', '0', '4000000', '4000000', '0', '0', '-', '0', '5488', '126');
INSERT INTO `houses` VALUES ('304', '4215', '327', '0', '2000000', '2000000', '0', '0', '-', '0', '5490', '327');
INSERT INTO `houses` VALUES ('305', '4215', '187', '0', '4000000', '4000000', '0', '0', '-', '0', '5493', '187');
INSERT INTO `houses` VALUES ('306', '4181', '49', '0', '1500000', '1500000', '0', '0', '-', '0', '5494', '49');
INSERT INTO `houses` VALUES ('307', '4181', '184', '0', '2000000', '2000000', '0', '0', '-', '0', '5496', '184');
INSERT INTO `houses` VALUES ('308', '4181', '89', '0', '4000000', '4000000', '0', '0', '-', '0', '5499', '89');
INSERT INTO `houses` VALUES ('309', '4181', '372', '0', '4000000', '4000000', '0', '0', '-', '0', '5503', '372');
INSERT INTO `houses` VALUES ('310', '4249', '47', '0', '4000000', '4000000', '0', '0', '-', '0', '5505', '44');
INSERT INTO `houses` VALUES ('311', '4240', '753', '0', '4000000', '4000000', '0', '0', '-', '0', '5508', '753');
INSERT INTO `houses` VALUES ('312', '4204', '88', '0', '1500000', '1500000', '0', '0', '-', '0', '5509', '88');
INSERT INTO `houses` VALUES ('313', '4241', '619', '0', '4000000', '4000000', '0', '0', '-', '0', '5512', '619');
INSERT INTO `houses` VALUES ('314', '4204', '669', '0', '4000000', '4000000', '0', '0', '-', '0', '5515', '672');
INSERT INTO `houses` VALUES ('315', '4241', '311', '0', '4000000', '4000000', '0', '0', '-', '0', '5518', '311');
INSERT INTO `houses` VALUES ('316', '4241', '279', '0', '4000000', '4000000', '0', '0', '-', '0', '5526', '279');
INSERT INTO `houses` VALUES ('317', '4342', '714', '0', '2000000', '2000000', '0', '0', '-', '0', '5530', '714');
INSERT INTO `houses` VALUES ('318', '4204', '119', '0', '5000000', '5000000', '0', '0', '-', '0', '5529', '116');
INSERT INTO `houses` VALUES ('319', '4342', '200', '0', '4000000', '4000000', '0', '0', '-', '0', '5533', '200');
INSERT INTO `houses` VALUES ('321', '2220', '216', '0', '4000000', '4000000', '0', '0', '-', '0', '5538', '216');
INSERT INTO `houses` VALUES ('322', '4177', '295', '0', '1500000', '1500000', '0', '0', '-', '0', '5537', '295');
INSERT INTO `houses` VALUES ('323', '2209', '706', '0', '2000000', '2000000', '0', '0', '-', '0', '5542', '706');
INSERT INTO `houses` VALUES ('324', '2209', '548', '0', '2000000', '2000000', '0', '0', '-', '0', '5546', '548');
INSERT INTO `houses` VALUES ('325', '4177', '139', '0', '1500000', '1500000', '0', '0', '-', '0', '5547', '139');
INSERT INTO `houses` VALUES ('326', '4177', '214', '0', '5000000', '5000000', '0', '0', '-', '0', '5544', '217');
INSERT INTO `houses` VALUES ('327', '2209', '586', '0', '3000000', '3000000', '0', '0', '-', '0', '5550', '586');
INSERT INTO `houses` VALUES ('328', '2209', '430', '0', '1500000', '1500000', '0', '0', '-', '0', '5551', '430');
INSERT INTO `houses` VALUES ('329', '4232', '591', '0', '1500000', '1500000', '0', '0', '-', '0', '5552', '591');
INSERT INTO `houses` VALUES ('330', '4231', '149', '0', '1500000', '1500000', '0', '0', '-', '0', '5555', '149');
INSERT INTO `houses` VALUES ('331', '2209', '200', '0', '4000000', '4000000', '0', '0', '-', '0', '5556', '200');
INSERT INTO `houses` VALUES ('332', '4242', '529', '0', '1500000', '1500000', '0', '0', '-', '0', '5557', '529');
INSERT INTO `houses` VALUES ('333', '2209', '143', '0', '1500000', '1500000', '0', '0', '-', '0', '5559', '143');
INSERT INTO `houses` VALUES ('334', '4242', '201', '0', '2000000', '2000000', '0', '0', '-', '0', '5561', '201');
INSERT INTO `houses` VALUES ('335', '2215', '650', '0', '1500000', '1500000', '0', '0', '-', '0', '5562', '650');
INSERT INTO `houses` VALUES ('336', '4242', '239', '0', '3000000', '3000000', '0', '0', '-', '0', '5566', '239');
INSERT INTO `houses` VALUES ('337', '2215', '725', '0', '3000000', '3000000', '0', '0', '-', '0', '5568', '725');
INSERT INTO `houses` VALUES ('338', '4242', '586', '0', '4000000', '4000000', '0', '0', '-', '0', '5574', '589');
INSERT INTO `houses` VALUES ('339', '2215', '204', '0', '4000000', '4000000', '0', '0', '-', '0', '5573', '204');
INSERT INTO `houses` VALUES ('340', '4289', '268', '0', '2000000', '2000000', '0', '0', '-', '0', '5582', '268');
INSERT INTO `houses` VALUES ('341', '4290', '279', '0', '5000000', '5000000', '0', '0', '-', '0', '5581', '282');
INSERT INTO `houses` VALUES ('342', '4290', '748', '0', '1500000', '1500000', '0', '0', '-', '0', '5585', '748');
INSERT INTO `houses` VALUES ('343', '4289', '306', '0', '3000000', '3000000', '0', '0', '-', '0', '5586', '306');
INSERT INTO `houses` VALUES ('344', '4289', '440', '0', '4000000', '4000000', '0', '0', '-', '0', '5591', '440');
INSERT INTO `houses` VALUES ('345', '4290', '146', '0', '5000000', '5000000', '0', '0', '-', '0', '5593', '146');
INSERT INTO `houses` VALUES ('346', '4289', '363', '0', '5000000', '5000000', '0', '0', '-', '0', '5597', '363');
INSERT INTO `houses` VALUES ('347', '4282', '612', '0', '4000000', '4000000', '0', '0', '-', '0', '5600', '612');
INSERT INTO `houses` VALUES ('348', '4282', '40', '0', '1500000', '1500000', '0', '0', '-', '0', '5604', '40');
INSERT INTO `houses` VALUES ('349', '4308', '299', '0', '5000000', '5000000', '0', '0', '-', '0', '5606', '299');
INSERT INTO `houses` VALUES ('350', '4236', '377', '0', '2000000', '2000000', '0', '0', '-', '0', '5609', '377');
INSERT INTO `houses` VALUES ('351', '4236', '339', '0', '3000000', '3000000', '0', '0', '-', '0', '5614', '339');
INSERT INTO `houses` VALUES ('352', '4282', '91', '0', '3000000', '3000000', '0', '0', '-', '0', '5615', '91');
INSERT INTO `houses` VALUES ('353', '4236', '220', '0', '2000000', '2000000', '0', '0', '-', '0', '5618', '220');
INSERT INTO `houses` VALUES ('354', '4280', '494', '0', '1500000', '1500000', '0', '0', '-', '0', '5617', '494');
INSERT INTO `houses` VALUES ('355', '4280', '569', '0', '3000000', '3000000', '0', '0', '-', '0', '5621', '569');
INSERT INTO `houses` VALUES ('356', '4245', '394', '0', '1500000', '1500000', '0', '0', '-', '0', '5625', '394');
INSERT INTO `houses` VALUES ('357', '4280', '645', '0', '4000000', '4000000', '0', '0', '-', '0', '5626', '645');
INSERT INTO `houses` VALUES ('358', '4245', '454', '0', '4000000', '4000000', '0', '0', '-', '0', '5629', '451');
INSERT INTO `houses` VALUES ('359', '4238', '564', '0', '1500000', '1500000', '0', '0', '-', '0', '5639', '564');
INSERT INTO `houses` VALUES ('360', '4096', '306', '0', '4000000', '4000000', '0', '0', '-', '0', '5637', '309');
INSERT INTO `houses` VALUES ('361', '4238', '488', '0', '1500000', '1500000', '0', '0', '-', '0', '5640', '488');
INSERT INTO `houses` VALUES ('362', '4238', '412', '0', '3000000', '3000000', '0', '0', '-', '0', '5644', '412');
INSERT INTO `houses` VALUES ('363', '4096', '249', '0', '3000000', '3000000', '0', '0', '-', '0', '5646', '249');
INSERT INTO `houses` VALUES ('364', '4233', '217', '0', '1500000', '1500000', '0', '0', '-', '0', '5647', '217');
INSERT INTO `houses` VALUES ('365', '4233', '44', '0', '1500000', '1500000', '0', '0', '-', '0', '5649', '44');
INSERT INTO `houses` VALUES ('366', '4233', '427', '0', '2000000', '2000000', '0', '0', '-', '0', '5653', '427');
INSERT INTO `houses` VALUES ('367', '4096', '62', '0', '3000000', '3000000', '0', '0', '-', '0', '5652', '62');
INSERT INTO `houses` VALUES ('368', '4233', '508', '0', '4000000', '4000000', '0', '0', '-', '0', '5656', '511');
INSERT INTO `houses` VALUES ('369', '4233', '83', '0', '4000000', '4000000', '0', '0', '-', '0', '5659', '83');
INSERT INTO `houses` VALUES ('370', '4096', '152', '0', '3000000', '3000000', '0', '0', '-', '0', '5662', '152');
INSERT INTO `houses` VALUES ('371', '4096', '187', '0', '4000000', '4000000', '0', '0', '-', '0', '5665', '187');
INSERT INTO `houses` VALUES ('372', '4300', '225', '0', '4000000', '4000000', '0', '0', '-', '0', '5669', '225');
INSERT INTO `houses` VALUES ('373', '4180', '651', '0', '1500000', '1500000', '0', '0', '-', '0', '5670', '651');
INSERT INTO `houses` VALUES ('374', '4094', '268', '0', '4000000', '4000000', '0', '0', '-', '0', '5673', '268');
INSERT INTO `houses` VALUES ('375', '4094', '87', '0', '3000000', '3000000', '0', '0', '-', '0', '5676', '87');
INSERT INTO `houses` VALUES ('376', '4094', '504', '0', '4000000', '4000000', '0', '0', '-', '0', '5679', '501');
INSERT INTO `houses` VALUES ('377', '4094', '163', '0', '5000000', '5000000', '0', '0', '-', '0', '5683', '163');
INSERT INTO `houses` VALUES ('378', '4095', '724', '0', '1500000', '1500000', '0', '0', '-', '0', '5684', '724');
INSERT INTO `houses` VALUES ('379', '4095', '509', '0', '1500000', '1500000', '0', '0', '-', '0', '5685', '509');
INSERT INTO `houses` VALUES ('380', '4095', '566', '0', '3000000', '3000000', '0', '0', '-', '0', '5688', '566');
INSERT INTO `houses` VALUES ('382', '4095', '347', '0', '3000000', '3000000', '0', '0', '-', '0', '5709', '347');
INSERT INTO `houses` VALUES ('383', '4244', '332', '0', '3000000', '3000000', '0', '0', '-', '0', '5726', '332');
INSERT INTO `houses` VALUES ('384', '4095', '404', '0', '3000000', '3000000', '0', '0', '-', '0', '5725', '404');
INSERT INTO `houses` VALUES ('385', '4244', '602', '0', '1500000', '1500000', '0', '0', '-', '0', '5728', '602');
INSERT INTO `houses` VALUES ('386', '4095', '461', '0', '3000000', '3000000', '0', '0', '-', '0', '5730', '461');
INSERT INTO `houses` VALUES ('387', '4244', '614', '0', '4000000', '4000000', '0', '0', '-', '0', '5733', '614');
INSERT INTO `houses` VALUES ('388', '4095', '449', '0', '4000000', '4000000', '0', '0', '-', '0', '5737', '449');
INSERT INTO `houses` VALUES ('389', '4244', '116', '0', '4000000', '4000000', '0', '0', '-', '0', '5739', '116');
INSERT INTO `houses` VALUES ('390', '2221', '272', '0', '3000000', '3000000', '0', '0', '-', '0', '5744', '272');
INSERT INTO `houses` VALUES ('391', '2210', '619', '0', '1500000', '1500000', '0', '0', '-', '0', '0', '0');
INSERT INTO `houses` VALUES ('392', '4095', '187', '0', '5000000', '5000000', '0', '0', '-', '0', '5746', '187');
INSERT INTO `houses` VALUES ('393', '2210', '533', '0', '4000000', '4000000', '0', '0', '-', '0', '5750', '530');
INSERT INTO `houses` VALUES ('394', '2210', '142', '0', '3000000', '3000000', '0', '0', '-', '0', '5754', '142');
INSERT INTO `houses` VALUES ('395', '2210', '104', '0', '2000000', '2000000', '0', '0', '-', '0', '5759', '104');
INSERT INTO `houses` VALUES ('396', '4246', '262', '0', '5000000', '5000000', '0', '0', '-', '0', '5758', '262');
INSERT INTO `houses` VALUES ('397', '2210', '391', '0', '5000000', '5000000', '0', '0', '-', '0', '5763', '394');
INSERT INTO `houses` VALUES ('398', '4104', '392', '0', '2000000', '2000000', '0', '0', '-', '0', '5765', '392');
INSERT INTO `houses` VALUES ('399', '4303', '282', '0', '4000000', '4000000', '0', '0', '-', '0', '5768', '285');
INSERT INTO `houses` VALUES ('400', '4104', '430', '0', '3000000', '3000000', '0', '0', '-', '0', '5771', '430');
INSERT INTO `houses` VALUES ('401', '4104', '490', '0', '4000000', '4000000', '0', '0', '-', '0', '5774', '487');
INSERT INTO `houses` VALUES ('402', '4303', '511', '0', '1500000', '1500000', '0', '0', '-', '0', '5775', '511');
INSERT INTO `houses` VALUES ('403', '4303', '237', '0', '5000000', '5000000', '0', '0', '-', '0', '5779', '237');
INSERT INTO `houses` VALUES ('405', '4172', '310', '0', '2000000', '2000000', '0', '0', '-', '0', '5782', '310');
INSERT INTO `houses` VALUES ('406', '4301', '139', '0', '2000000', '2000000', '0', '0', '-', '0', '5784', '139');
INSERT INTO `houses` VALUES ('407', '4301', '196', '0', '3000000', '3000000', '0', '0', '-', '0', '5789', '196');
INSERT INTO `houses` VALUES ('408', '4172', '367', '0', '2000000', '2000000', '0', '0', '-', '0', '5786', '367');
INSERT INTO `houses` VALUES ('409', '4172', '405', '0', '3000000', '3000000', '0', '0', '-', '0', '5795', '405');
INSERT INTO `houses` VALUES ('410', '4301', '189', '0', '5000000', '5000000', '0', '0', '-', '0', '5796', '186');
INSERT INTO `houses` VALUES ('411', '4172', '79', '0', '3000000', '3000000', '0', '0', '-', '0', '5799', '79');
INSERT INTO `houses` VALUES ('412', '4259', '69', '0', '1500000', '1500000', '0', '0', '-', '0', '5802', '69');
INSERT INTO `houses` VALUES ('413', '4097', '511', '0', '1500000', '1500000', '0', '0', '-', '0', '5805', '511');
INSERT INTO `houses` VALUES ('414', '4259', '126', '0', '4000000', '4000000', '0', '0', '-', '0', '5807', '126');
INSERT INTO `houses` VALUES ('415', '4097', '435', '0', '1500000', '1500000', '0', '0', '-', '0', '5808', '435');
INSERT INTO `houses` VALUES ('416', '4097', '104', '0', '1500000', '1500000', '0', '0', '-', '0', '5809', '104');
INSERT INTO `houses` VALUES ('417', '4275', '528', '0', '1500000', '1500000', '0', '0', '-', '0', '5811', '528');
INSERT INTO `houses` VALUES ('418', '4097', '180', '0', '1500000', '1500000', '0', '0', '-', '0', '5813', '180');
INSERT INTO `houses` VALUES ('419', '4275', '446', '0', '4000000', '4000000', '0', '0', '-', '0', '5823', '446');
INSERT INTO `houses` VALUES ('420', '4097', '377', '0', '3000000', '3000000', '0', '0', '-', '0', '5827', '377');
INSERT INTO `houses` VALUES ('421', '4275', '91', '0', '2000000', '2000000', '0', '0', '-', '0', '5829', '91');
INSERT INTO `houses` VALUES ('422', '4273', '402', '0', '2000000', '2000000', '0', '0', '-', '0', '5834', '402');
INSERT INTO `houses` VALUES ('423', '4097', '237', '0', '4000000', '4000000', '0', '0', '-', '0', '5833', '237');
INSERT INTO `houses` VALUES ('424', '4273', '713', '0', '4000000', '4000000', '0', '0', '-', '0', '5838', '713');
INSERT INTO `houses` VALUES ('425', '4090', '569', '0', '1500000', '1500000', '0', '0', '-', '0', '5839', '569');
INSERT INTO `houses` VALUES ('426', '4090', '216', '0', '1500000', '1500000', '0', '0', '-', '0', '5840', '216');
INSERT INTO `houses` VALUES ('427', '4243', '269', '0', '1500000', '1500000', '0', '0', '-', '0', '5841', '269');
INSERT INTO `houses` VALUES ('428', '4090', '447', '0', '2000000', '2000000', '0', '0', '-', '0', '5844', '447');
INSERT INTO `houses` VALUES ('429', '4243', '326', '0', '4000000', '4000000', '0', '0', '-', '0', '5846', '329');
INSERT INTO `houses` VALUES ('430', '4090', '201', '0', '3000000', '3000000', '0', '0', '-', '0', '5852', '201');
INSERT INTO `houses` VALUES ('431', '4243', '94', '0', '3000000', '3000000', '0', '0', '-', '0', '5855', '94');
INSERT INTO `houses` VALUES ('432', '4248', '395', '0', '1500000', '1500000', '0', '0', '-', '0', '5859', '395');
INSERT INTO `houses` VALUES ('433', '4090', '76', '0', '5000000', '5000000', '0', '0', '-', '0', '5860', '76');
INSERT INTO `houses` VALUES ('434', '4093', '640', '0', '1500000', '1500000', '0', '0', '-', '0', '5865', '640');
INSERT INTO `houses` VALUES ('435', '4248', '437', '0', '4000000', '4000000', '0', '0', '-', '0', '5864', '437');
INSERT INTO `houses` VALUES ('436', '4093', '206', '0', '1500000', '1500000', '0', '0', '-', '0', '5866', '206');
INSERT INTO `houses` VALUES ('437', '4070', '300', '0', '3000000', '3000000', '0', '0', '-', '0', '5872', '300');
INSERT INTO `houses` VALUES ('438', '4093', '387', '0', '5000000', '5000000', '0', '0', '-', '0', '5874', '384');
INSERT INTO `houses` VALUES ('439', '4077', '676', '0', '2000000', '2000000', '0', '0', '-', '0', '5876', '676');
INSERT INTO `houses` VALUES ('440', '4077', '486', '0', '3000000', '3000000', '0', '0', '-', '0', '5881', '486');
INSERT INTO `houses` VALUES ('441', '4247', '187', '0', '4000000', '4000000', '0', '0', '-', '0', '5882', '187');
INSERT INTO `houses` VALUES ('442', '4106', '383', '0', '1500000', '1500000', '0', '0', '-', '0', '5887', '383');
INSERT INTO `houses` VALUES ('443', '4077', '167', '0', '5000000', '5000000', '0', '0', '-', '0', '5886', '170');
INSERT INTO `houses` VALUES ('444', '4106', '326', '0', '3000000', '3000000', '0', '0', '-', '0', '5890', '326');
INSERT INTO `houses` VALUES ('445', '4106', '459', '0', '4000000', '4000000', '0', '0', '-', '0', '5893', '459');
INSERT INTO `houses` VALUES ('446', '4106', '353', '0', '4000000', '4000000', '0', '0', '-', '0', '5896', '353');
INSERT INTO `houses` VALUES ('447', '4169', '269', '0', '2000000', '2000000', '0', '0', '-', '0', '5898', '269');
INSERT INTO `houses` VALUES ('448', '4169', '231', '0', '3000000', '3000000', '0', '0', '-', '0', '5901', '231');
INSERT INTO `houses` VALUES ('449', '4305', '717', '0', '1500000', '1500000', '0', '0', '-', '0', '5904', '717');
INSERT INTO `houses` VALUES ('450', '4169', '174', '0', '3000000', '3000000', '0', '0', '-', '0', '5905', '174');
INSERT INTO `houses` VALUES ('451', '4304', '367', '0', '4000000', '4000000', '0', '0', '-', '0', '5909', '367');
INSERT INTO `houses` VALUES ('452', '4169', '308', '0', '4000000', '4000000', '0', '0', '-', '0', '5912', '311');
INSERT INTO `houses` VALUES ('453', '4223', '207', '0', '4000000', '4000000', '0', '0', '-', '0', '5914', '207');
INSERT INTO `houses` VALUES ('454', '4098', '489', '0', '1500000', '1500000', '0', '0', '-', '0', '5917', '489');
INSERT INTO `houses` VALUES ('455', '4098', '107', '0', '1500000', '1500000', '0', '0', '-', '0', '5921', '107');
INSERT INTO `houses` VALUES ('456', '4098', '587', '0', '1500000', '1500000', '0', '0', '-', '0', '5922', '587');
INSERT INTO `houses` VALUES ('457', '4223', '182', '0', '5000000', '5000000', '0', '0', '-', '0', '5919', '182');
INSERT INTO `houses` VALUES ('458', '4291', '584', '0', '1500000', '1500000', '0', '0', '-', '0', '5925', '584');
INSERT INTO `houses` VALUES ('459', '4098', '394', '0', '3000000', '3000000', '0', '0', '-', '0', '5926', '394');
INSERT INTO `houses` VALUES ('460', '4291', '469', '0', '1500000', '1500000', '0', '0', '-', '0', '5927', '469');
INSERT INTO `houses` VALUES ('461', '4291', '355', '0', '1500000', '1500000', '0', '0', '-', '0', '5928', '355');
INSERT INTO `houses` VALUES ('462', '4291', '545', '0', '2000000', '2000000', '0', '0', '-', '0', '5933', '545');
INSERT INTO `houses` VALUES ('463', '4098', '164', '0', '4000000', '4000000', '0', '0', '-', '0', '5932', '164');
INSERT INTO `houses` VALUES ('464', '4291', '412', '0', '3000000', '3000000', '0', '0', '-', '0', '5937', '412');
INSERT INTO `houses` VALUES ('465', '4098', '206', '0', '4000000', '4000000', '0', '0', '-', '0', '5940', '206');
INSERT INTO `houses` VALUES ('466', '4269', '214', '0', '2000000', '2000000', '0', '0', '-', '0', '5942', '214');
INSERT INTO `houses` VALUES ('467', '4269', '52', '0', '1500000', '1500000', '0', '0', '-', '0', '5943', '52');
INSERT INTO `houses` VALUES ('468', '4072', '672', '0', '1500000', '1500000', '0', '0', '-', '0', '5944', '672');
INSERT INTO `houses` VALUES ('469', '4072', '154', '0', '1500000', '1500000', '0', '0', '-', '0', '5946', '154');
INSERT INTO `houses` VALUES ('470', '4072', '152', '0', '2000000', '2000000', '0', '0', '-', '0', '5951', '152');
INSERT INTO `houses` VALUES ('471', '4269', '115', '0', '5000000', '5000000', '0', '0', '-', '0', '5949', '115');
INSERT INTO `houses` VALUES ('472', '4072', '729', '0', '3000000', '3000000', '0', '0', '-', '0', '5957', '729');
INSERT INTO `houses` VALUES ('473', '4264', '360', '0', '2000000', '2000000', '0', '0', '-', '0', '5960', '360');
INSERT INTO `houses` VALUES ('474', '4260', '646', '0', '1500000', '1500000', '0', '0', '-', '0', '5963', '646');
INSERT INTO `houses` VALUES ('476', '4072', '187', '0', '4000000', '4000000', '0', '0', '-', '0', '5967', '187');
INSERT INTO `houses` VALUES ('477', '4260', '487', '0', '4000000', '4000000', '0', '0', '-', '0', '5970', '490');
INSERT INTO `houses` VALUES ('478', '4260', '607', '0', '3000000', '3000000', '0', '0', '-', '0', '5977', '607');
INSERT INTO `houses` VALUES ('479', '4074', '453', '0', '1500000', '1500000', '0', '0', '-', '0', '5972', '453');
INSERT INTO `houses` VALUES ('480', '4074', '143', '0', '1500000', '1500000', '0', '0', '-', '0', '5973', '143');
INSERT INTO `houses` VALUES ('481', '4074', '314', '0', '1500000', '1500000', '0', '0', '-', '0', '5974', '314');
INSERT INTO `houses` VALUES ('482', '4073', '624', '0', '1500000', '1500000', '0', '0', '-', '0', '5978', '624');
INSERT INTO `houses` VALUES ('483', '4073', '548', '0', '1500000', '1500000', '0', '0', '-', '0', '5980', '548');
INSERT INTO `houses` VALUES ('484', '4073', '653', '0', '1500000', '1500000', '0', '0', '-', '0', '5983', '653');
INSERT INTO `houses` VALUES ('485', '4074', '528', '0', '3000000', '3000000', '0', '0', '-', '0', '5982', '528');
INSERT INTO `houses` VALUES ('486', '4073', '490', '0', '3000000', '3000000', '0', '0', '-', '0', '5986', '490');
INSERT INTO `houses` VALUES ('487', '4074', '566', '0', '3000000', '3000000', '0', '0', '-', '0', '5989', '566');
INSERT INTO `houses` VALUES ('488', '4073', '313', '0', '3000000', '3000000', '0', '0', '-', '0', '5992', '313');
INSERT INTO `houses` VALUES ('489', '4073', '161', '0', '3000000', '3000000', '0', '0', '-', '0', '5995', '161');
INSERT INTO `houses` VALUES ('490', '4073', '218', '0', '4000000', '4000000', '0', '0', '-', '0', '6001', '218');
INSERT INTO `houses` VALUES ('491', '4074', '206', '0', '4000000', '4000000', '0', '0', '-', '0', '6002', '206');
INSERT INTO `houses` VALUES ('492', '4278', '678', '0', '2000000', '2000000', '0', '0', '-', '0', '6009', '678');
INSERT INTO `houses` VALUES ('493', '4284', '597', '0', '1500000', '1500000', '0', '0', '-', '0', '6010', '597');
INSERT INTO `houses` VALUES ('494', '4073', '263', '0', '5000000', '5000000', '0', '0', '-', '0', '6008', '260');
INSERT INTO `houses` VALUES ('495', '4284', '528', '0', '2000000', '2000000', '0', '0', '-', '0', '6012', '528');
INSERT INTO `houses` VALUES ('496', '4284', '471', '0', '3000000', '3000000', '0', '0', '-', '0', '6019', '471');
INSERT INTO `houses` VALUES ('497', '4082', '473', '0', '1500000', '1500000', '0', '0', '-', '0', '6026', '473');
INSERT INTO `houses` VALUES ('498', '4082', '151', '0', '1500000', '1500000', '0', '0', '-', '0', '6029', '151');
INSERT INTO `houses` VALUES ('499', '4284', '146', '0', '3000000', '3000000', '0', '0', '-', '0', '6028', '146');
INSERT INTO `houses` VALUES ('500', '4082', '87', '0', '1500000', '1500000', '0', '0', '-', '0', '6030', '87');
INSERT INTO `houses` VALUES ('501', '4285', '752', '0', '3000000', '3000000', '0', '0', '-', '0', '6033', '752');
INSERT INTO `houses` VALUES ('502', '4082', '533', '0', '4000000', '4000000', '0', '0', '-', '0', '6036', '533');
INSERT INTO `houses` VALUES ('503', '4082', '302', '0', '3000000', '3000000', '0', '0', '-', '0', '6041', '302');
INSERT INTO `houses` VALUES ('504', '4285', '113', '0', '4000000', '4000000', '0', '0', '-', '0', '6060', '113');
INSERT INTO `houses` VALUES ('505', '4082', '264', '0', '3000000', '3000000', '0', '0', '-', '0', '6048', '264');
INSERT INTO `houses` VALUES ('506', '4082', '226', '0', '3000000', '3000000', '0', '0', '-', '0', '6053', '226');
INSERT INTO `houses` VALUES ('507', '4299', '429', '0', '2000000', '2000000', '0', '0', '-', '0', '6054', '429');
INSERT INTO `houses` VALUES ('508', '4082', '145', '0', '5000000', '5000000', '0', '0', '-', '0', '6059', '145');
INSERT INTO `houses` VALUES ('510', '4302', '126', '0', '4000000', '4000000', '0', '0', '-', '0', '6067', '126');
INSERT INTO `houses` VALUES ('511', '4299', '35', '0', '4000000', '4000000', '0', '0', '-', '0', '6069', '35');
INSERT INTO `houses` VALUES ('513', '4280', '206', '0', '5000000', '5000000', '0', '0', '-', '0', '5634', '209');
INSERT INTO `houses` VALUES ('514', '4219', '417', '0', '3000000', '3000000', '0', '0', '-', '0', '6142', '417');
INSERT INTO `houses` VALUES ('515', '4594', '651', '0', '4000000', '4000000', '0', '0', '-', '0', '6200', '651');
INSERT INTO `houses` VALUES ('516', '4594', '491', '0', '3000000', '3000000', '0', '0', '-', '0', '6202', '491');
INSERT INTO `houses` VALUES ('517', '4594', '151', '0', '4000000', '4000000', '0', '0', '-', '0', '6205', '151');
INSERT INTO `houses` VALUES ('518', '4594', '238', '0', '4000000', '4000000', '0', '0', '-', '0', '6206', '238');
INSERT INTO `houses` VALUES ('519', '4616', '396', '0', '4000000', '4000000', '0', '0', '-', '0', '6208', '396');
INSERT INTO `houses` VALUES ('520', '4616', '257', '0', '3000000', '3000000', '0', '0', '-', '0', '6213', '257');
INSERT INTO `houses` VALUES ('521', '4616', '456', '0', '3000000', '3000000', '0', '0', '-', '0', '6215', '456');
INSERT INTO `houses` VALUES ('522', '4631', '409', '0', '4000000', '4000000', '0', '0', '-', '0', '6217', '409');
INSERT INTO `houses` VALUES ('523', '4631', '681', '0', '3000000', '3000000', '0', '0', '-', '0', '6220', '681');
INSERT INTO `houses` VALUES ('524', '4631', '322', '0', '3000000', '3000000', '0', '0', '-', '0', '6221', '322');
INSERT INTO `houses` VALUES ('525', '4549', '647', '0', '3000000', '3000000', '0', '0', '-', '0', '6225', '647');
INSERT INTO `houses` VALUES ('526', '4549', '161', '0', '3000000', '3000000', '0', '0', '-', '0', '6226', '161');
INSERT INTO `houses` VALUES ('527', '4549', '301', '0', '3000000', '3000000', '0', '0', '-', '0', '6227', '301');
INSERT INTO `houses` VALUES ('528', '5277', '742', '0', '2000000', '2000000', '0', '0', '-', '0', '6232', '742');
INSERT INTO `houses` VALUES ('529', '5277', '400', '0', '2000000', '2000000', '0', '0', '-', '0', '6231', '400');
INSERT INTO `houses` VALUES ('530', '5277', '621', '0', '2000000', '2000000', '0', '0', '-', '0', '6234', '621');
INSERT INTO `houses` VALUES ('531', '5277', '76', '0', '3000000', '3000000', '0', '0', '-', '0', '6236', '76');
INSERT INTO `houses` VALUES ('532', '4610', '660', '0', '4000000', '4000000', '0', '0', '-', '0', '6239', '660');
INSERT INTO `houses` VALUES ('533', '4610', '602', '0', '2000000', '2000000', '0', '0', '-', '0', '6241', '602');
INSERT INTO `houses` VALUES ('534', '4610', '483', '0', '2000000', '2000000', '0', '0', '-', '0', '6243', '483');
INSERT INTO `houses` VALUES ('535', '4610', '412', '0', '3000000', '3000000', '0', '0', '-', '0', '6245', '412');
INSERT INTO `houses` VALUES ('536', '4610', '268', '0', '3000000', '3000000', '0', '0', '-', '0', '6247', '268');
INSERT INTO `houses` VALUES ('537', '4936', '454', '0', '2000000', '2000000', '0', '0', '-', '0', '6253', '454');
INSERT INTO `houses` VALUES ('538', '6248', '183', '0', '10000000', '10000000', '0', '0', '-', '0', '6250', '183');
INSERT INTO `houses` VALUES ('539', '6249', '225', '0', '10000000', '10000000', '0', '0', '-', '0', '6251', '225');
INSERT INTO `houses` VALUES ('540', '6181', '81', '0', '10000000', '10000000', '0', '0', '-', '0', '6237', '81');
INSERT INTO `houses` VALUES ('541', '1915', '229', '0', '2000000', '2000000', '0', '0', '-', '0', '1964', '229');
INSERT INTO `houses` VALUES ('542', '4595', '607', '0', '4000000', '4000000', '0', '0', '-', '0', '6278', '607');
INSERT INTO `houses` VALUES ('543', '4649', '218', '0', '2000000', '2000000', '0', '0', '-', '0', '6280', '218');
INSERT INTO `houses` VALUES ('544', '4649', '275', '0', '2000000', '2000000', '0', '0', '-', '0', '6285', '275');
INSERT INTO `houses` VALUES ('545', '4591', '215', '0', '3000000', '3000000', '0', '0', '-', '0', '6282', '215');
INSERT INTO `houses` VALUES ('546', '4591', '552', '0', '3000000', '3000000', '0', '0', '-', '0', '6286', '552');
INSERT INTO `houses` VALUES ('547', '4591', '702', '0', '2000000', '2000000', '0', '0', '-', '0', '6288', '702');
INSERT INTO `houses` VALUES ('548', '4649', '614', '0', '3000000', '3000000', '0', '0', '-', '0', '6292', '614');
INSERT INTO `houses` VALUES ('549', '4605', '534', '0', '4000000', '4000000', '0', '0', '-', '0', '6290', '155');
INSERT INTO `houses` VALUES ('550', '5108', '57', '0', '2000000', '2000000', '0', '0', '-', '0', '6294', '57');
INSERT INTO `houses` VALUES ('551', '5108', '597', '0', '2000000', '2000000', '0', '0', '-', '0', '6296', '597');
INSERT INTO `houses` VALUES ('552', '5108', '367', '0', '3000000', '3000000', '0', '0', '-', '0', '6298', '367');
INSERT INTO `houses` VALUES ('553', '4622', '378', '0', '3000000', '3000000', '0', '0', '-', '0', '6300', '378');
INSERT INTO `houses` VALUES ('554', '4622', '164', '0', '3000000', '3000000', '0', '0', '-', '0', '6302', '164');
INSERT INTO `houses` VALUES ('555', '4666', '75', '0', '2000000', '2000000', '0', '0', '-', '0', '6305', '75');
INSERT INTO `houses` VALUES ('556', '4666', '493', '0', '2000000', '2000000', '0', '0', '-', '0', '6307', '493');
INSERT INTO `houses` VALUES ('557', '4666', '132', '0', '2000000', '2000000', '0', '0', '-', '0', '6309', '132');
INSERT INTO `houses` VALUES ('558', '4666', '578', '0', '3000000', '3000000', '0', '0', '-', '0', '6311', '578');
INSERT INTO `houses` VALUES ('559', '4605', '438', '0', '2000000', '2000000', '0', '0', '-', '0', '6313', '438');
INSERT INTO `houses` VALUES ('560', '5279', '261', '0', '2000000', '2000000', '0', '0', '-', '0', '6317', '261');
INSERT INTO `houses` VALUES ('561', '5279', '640', '0', '2000000', '2000000', '0', '0', '-', '0', '6318', '640');
INSERT INTO `houses` VALUES ('562', '5279', '276', '0', '2000000', '2000000', '0', '0', '-', '0', '6319', '276');
INSERT INTO `houses` VALUES ('563', '5317', '429', '0', '2000000', '2000000', '0', '0', '-', '0', '6323', '429');
INSERT INTO `houses` VALUES ('564', '5279', '283', '0', '3000000', '3000000', '0', '0', '-', '0', '6321', '283');
INSERT INTO `houses` VALUES ('565', '5317', '160', '0', '2000000', '2000000', '0', '0', '-', '0', '6325', '160');
INSERT INTO `houses` VALUES ('566', '5317', '217', '0', '2000000', '2000000', '0', '0', '-', '0', '6327', '217');
INSERT INTO `houses` VALUES ('568', '5317', '737', '0', '2000000', '2000000', '0', '0', '-', '0', '6329', '737');
INSERT INTO `houses` VALUES ('569', '4611', '449', '0', '3000000', '3000000', '0', '0', '-', '0', '6333', '449');
INSERT INTO `houses` VALUES ('570', '4606', '344', '0', '4000000', '4000000', '0', '0', '-', '0', '6331', '344');
INSERT INTO `houses` VALUES ('571', '4611', '437', '0', '2000000', '2000000', '0', '0', '-', '0', '6335', '437');
INSERT INTO `houses` VALUES ('572', '5326', '522', '0', '3000000', '3000000', '0', '0', '-', '0', '6337', '522');
INSERT INTO `houses` VALUES ('573', '5326', '361', '0', '2000000', '2000000', '0', '0', '-', '0', '6339', '361');
INSERT INTO `houses` VALUES ('574', '4606', '506', '0', '3000000', '3000000', '0', '0', '-', '0', '6345', '506');
INSERT INTO `houses` VALUES ('575', '5326', '713', '0', '2000000', '2000000', '0', '0', '-', '0', '6341', '713');
INSERT INTO `houses` VALUES ('576', '5326', '69', '0', '3000000', '3000000', '0', '0', '-', '0', '6343', '69');
INSERT INTO `houses` VALUES ('577', '4644', '94', '0', '4000000', '4000000', '0', '0', '-', '0', '6347', '94');
INSERT INTO `houses` VALUES ('578', '4644', '571', '0', '4000000', '4000000', '0', '0', '-', '0', '6349', '571');
INSERT INTO `houses` VALUES ('579', '4644', '402', '0', '3000000', '3000000', '0', '0', '-', '0', '6351', '402');
INSERT INTO `houses` VALUES ('580', '4644', '380', '0', '2000000', '2000000', '0', '0', '-', '0', '6353', '380');
INSERT INTO `houses` VALUES ('581', '4646', '402', '0', '4000000', '4000000', '0', '0', '-', '0', '6355', '402');
INSERT INTO `houses` VALUES ('582', '4646', '80', '0', '2000000', '2000000', '0', '0', '-', '0', '6357', '80');
INSERT INTO `houses` VALUES ('583', '4586', '432', '0', '4000000', '4000000', '0', '0', '-', '0', '6579', '192');
INSERT INTO `houses` VALUES ('584', '4647', '223', '0', '3000000', '3000000', '0', '0', '-', '0', '6578', '223');
INSERT INTO `houses` VALUES ('585', '4937', '115', '0', '2000000', '2000000', '0', '0', '-', '0', '6585', '115');
INSERT INTO `houses` VALUES ('586', '4647', '600', '0', '3000000', '3000000', '0', '0', '-', '0', '6580', '600');
INSERT INTO `houses` VALUES ('587', '4647', '578', '0', '2000000', '2000000', '0', '0', '-', '0', '6583', '578');
INSERT INTO `houses` VALUES ('588', '4604', '174', '0', '3000000', '3000000', '0', '0', '-', '0', '6591', '174');
INSERT INTO `houses` VALUES ('589', '4647', '455', '0', '2000000', '2000000', '0', '0', '-', '0', '6584', '455');
INSERT INTO `houses` VALUES ('590', '4937', '91', '0', '2000000', '2000000', '0', '0', '-', '0', '6595', '91');
INSERT INTO `houses` VALUES ('591', '4614', '618', '0', '3000000', '3000000', '0', '0', '-', '0', '6589', '618');
INSERT INTO `houses` VALUES ('592', '4614', '492', '0', '3000000', '3000000', '0', '0', '-', '0', '6590', '492');
INSERT INTO `houses` VALUES ('593', '4614', '644', '0', '2000000', '2000000', '0', '0', '-', '0', '6593', '644');
INSERT INTO `houses` VALUES ('594', '4603', '280', '0', '2000000', '2000000', '0', '0', '-', '0', '6598', '280');
INSERT INTO `houses` VALUES ('595', '4603', '718', '0', '2000000', '2000000', '0', '0', '-', '0', '6599', '718');
INSERT INTO `houses` VALUES ('596', '4936', '757', '0', '2000000', '2000000', '0', '0', '-', '0', '6601', '757');
INSERT INTO `houses` VALUES ('597', '4615', '614', '0', '2000000', '2000000', '0', '0', '-', '0', '6604', '614');
INSERT INTO `houses` VALUES ('598', '4604', '453', '0', '3000000', '3000000', '0', '0', '-', '0', '6605', '146');
INSERT INTO `houses` VALUES ('599', '4615', '404', '0', '4000000', '4000000', '0', '0', '-', '0', '6607', '404');
INSERT INTO `houses` VALUES ('600', '5280', '108', '0', '4000000', '4000000', '0', '0', '-', '0', '6609', '108');
INSERT INTO `houses` VALUES ('601', '4600', '264', '0', '4000000', '4000000', '0', '0', '-', '0', '6623', '264');
INSERT INTO `houses` VALUES ('602', '4588', '170', '0', '4000000', '4000000', '0', '0', '-', '0', '6612', '170');
INSERT INTO `houses` VALUES ('603', '4588', '174', '0', '3000000', '3000000', '0', '0', '-', '0', '6617', '174');
INSERT INTO `houses` VALUES ('604', '4588', '250', '0', '4000000', '4000000', '0', '0', '-', '0', '6614', '250');
INSERT INTO `houses` VALUES ('605', '4593', '505', '0', '2000000', '2000000', '0', '0', '-', '0', '6625', '505');
INSERT INTO `houses` VALUES ('606', '4593', '381', '0', '3000000', '3000000', '0', '0', '-', '0', '6627', '381');
INSERT INTO `houses` VALUES ('607', '4588', '326', '0', '4000000', '4000000', '0', '0', '-', '0', '6621', '326');
INSERT INTO `houses` VALUES ('608', '4936', '112', '0', '4000000', '4000000', '0', '0', '-', '0', '6615', '112');
INSERT INTO `houses` VALUES ('609', '5280', '115', '0', '3000000', '3000000', '0', '0', '-', '0', '6619', '115');
INSERT INTO `houses` VALUES ('610', '4620', '450', '0', '3000000', '3000000', '0', '0', '-', '0', '6629', '450');
INSERT INTO `houses` VALUES ('611', '4620', '256', '0', '4000000', '4000000', '0', '0', '-', '0', '6631', '256');
INSERT INTO `houses` VALUES ('612', '4612', '132', '0', '3000000', '3000000', '0', '0', '-', '0', '6636', '132');
INSERT INTO `houses` VALUES ('613', '4640', '413', '0', '2000000', '2000000', '0', '0', '-', '0', '6633', '413');
INSERT INTO `houses` VALUES ('614', '4640', '471', '0', '4000000', '4000000', '0', '0', '-', '0', '6637', '471');
INSERT INTO `houses` VALUES ('615', '4600', '258', '0', '3000000', '3000000', '0', '0', '-', '0', '6647', '258');
INSERT INTO `houses` VALUES ('616', '4640', '384', '0', '3000000', '3000000', '0', '0', '-', '0', '6643', '384');
INSERT INTO `houses` VALUES ('617', '5136', '524', '0', '3000000', '3000000', '0', '0', '-', '0', '6649', '524');
INSERT INTO `houses` VALUES ('618', '5136', '289', '0', '2000000', '2000000', '0', '0', '-', '0', '6652', '289');
INSERT INTO `houses` VALUES ('619', '4640', '683', '0', '3000000', '3000000', '0', '0', '-', '0', '6645', '683');
INSERT INTO `houses` VALUES ('620', '5304', '455', '0', '2000000', '2000000', '0', '0', '-', '0', '6640', '455');
INSERT INTO `houses` VALUES ('621', '5304', '375', '0', '2000000', '2000000', '0', '0', '-', '0', '6644', '375');
INSERT INTO `houses` VALUES ('622', '4596', '346', '0', '3000000', '3000000', '0', '0', '-', '0', '6659', '346');
INSERT INTO `houses` VALUES ('623', '4597', '105', '0', '4000000', '4000000', '0', '0', '-', '0', '6663', '105');
INSERT INTO `houses` VALUES ('624', '4623', '331', '0', '2000000', '2000000', '0', '0', '-', '0', '6661', '331');
INSERT INTO `houses` VALUES ('625', '4613', '245', '0', '3000000', '3000000', '0', '0', '-', '0', '6653', '245');
INSERT INTO `houses` VALUES ('626', '4613', '101', '0', '3000000', '3000000', '0', '0', '-', '0', '6655', '101');
INSERT INTO `houses` VALUES ('627', '4613', '675', '0', '2000000', '2000000', '0', '0', '-', '0', '6657', '675');
INSERT INTO `houses` VALUES ('628', '4584', '467', '0', '3000000', '3000000', '0', '0', '-', '0', '6665', '467');
INSERT INTO `houses` VALUES ('629', '4941', '682', '0', '3000000', '3000000', '0', '0', '-', '0', '6667', '682');
INSERT INTO `houses` VALUES ('630', '4941', '144', '0', '4000000', '4000000', '0', '0', '-', '0', '6673', '144');
INSERT INTO `houses` VALUES ('631', '4628', '353', '0', '3000000', '3000000', '0', '0', '-', '0', '6669', '353');
INSERT INTO `houses` VALUES ('632', '4628', '543', '0', '4000000', '4000000', '0', '0', '-', '0', '6671', '543');
INSERT INTO `houses` VALUES ('633', '5139', '261', '0', '2000000', '2000000', '0', '0', '-', '0', '6676', '261');
INSERT INTO `houses` VALUES ('634', '5139', '377', '0', '3000000', '3000000', '0', '0', '-', '0', '6679', '377');
INSERT INTO `houses` VALUES ('635', '5139', '569', '0', '2000000', '2000000', '0', '0', '-', '0', '6677', '569');
INSERT INTO `houses` VALUES ('636', '2209', '506', '0', '2000000', '2000000', '0', '0', '-', '0', '6716', '506');
INSERT INTO `houses` VALUES ('637', '4646', '137', '0', '2000000', '2000000', '0', '0', '-', '0', '6359', '137');
INSERT INTO `houses` VALUES ('638', '1897', '431', '0', '2000000', '2000000', '0', '0', '-', '0', '6982', '431');
INSERT INTO `houses` VALUES ('639', '4302', '739', '0', '2000000', '2000000', '0', '0', '-', '0', '7270', '739');
INSERT INTO `houses` VALUES ('640', '7441', '361', '0', '2000000', '2000000', '0', '0', '-', '0', '7731', '142');
INSERT INTO `houses` VALUES ('641', '7444', '163', '0', '2000000', '2000000', '0', '0', '-', '0', '7622', '150');
INSERT INTO `houses` VALUES ('642', '7445', '234', '0', '2000000', '2000000', '0', '0', '-', '0', '7714', '150');
INSERT INTO `houses` VALUES ('643', '7445', '389', '0', '3000000', '3000000', '0', '0', '-', '0', '7721', '141');
INSERT INTO `houses` VALUES ('644', '7447', '118', '0', '2000000', '2000000', '0', '0', '-', '0', '7653', '150');
INSERT INTO `houses` VALUES ('645', '7426', '133', '0', '3000000', '3000000', '0', '0', '-', '0', '7699', '141');
INSERT INTO `houses` VALUES ('647', '7428', '162', '0', '3000000', '3000000', '0', '0', '-', '0', '7629', '141');
INSERT INTO `houses` VALUES ('648', '7430', '442', '0', '2000000', '2000000', '0', '0', '-', '0', '7691', '142');
INSERT INTO `houses` VALUES ('649', '7430', '177', '0', '2000000', '2000000', '0', '0', '-', '0', '7693', '150');
INSERT INTO `houses` VALUES ('650', '7415', '147', '0', '3000000', '3000000', '0', '0', '-', '0', '7658', '147');
INSERT INTO `houses` VALUES ('651', '7413', '74', '0', '2000000', '2000000', '0', '0', '-', '0', '7730', '150');
INSERT INTO `houses` VALUES ('652', '7412', '156', '0', '2000000', '2000000', '0', '0', '-', '0', '7635', '203');
INSERT INTO `houses` VALUES ('653', '7412', '103', '0', '2000000', '2000000', '0', '0', '-', '0', '7637', '150');
INSERT INTO `houses` VALUES ('654', '7411', '170', '0', '2000000', '2000000', '0', '0', '-', '0', '7668', '203');
INSERT INTO `houses` VALUES ('655', '7410', '216', '0', '2000000', '2000000', '0', '0', '-', '0', '7708', '150');
INSERT INTO `houses` VALUES ('656', '7410', '294', '0', '3000000', '3000000', '0', '0', '-', '0', '7712', '141');
INSERT INTO `houses` VALUES ('657', '7409', '323', '0', '10000000', '10000000', '0', '0', '-', '0', '7739', '271');
INSERT INTO `houses` VALUES ('658', '7408', '185', '0', '2000000', '2000000', '0', '0', '-', '0', '7707', '203');
INSERT INTO `houses` VALUES ('659', '7408', '208', '0', '2000000', '2000000', '0', '0', '-', '0', '7646', '142');
INSERT INTO `houses` VALUES ('660', '7392', '162', '0', '3000000', '3000000', '0', '0', '-', '0', '7642', '141');
INSERT INTO `houses` VALUES ('661', '7289', '189', '0', '4000000', '4000000', '0', '0', '-', '0', '7732', '271');
INSERT INTO `houses` VALUES ('662', '7289', '246', '0', '2000000', '2000000', '0', '0', '-', '0', '7743', '150');
INSERT INTO `houses` VALUES ('663', '7394', '284', '0', '2000000', '2000000', '0', '0', '-', '0', '7723', '142');
INSERT INTO `houses` VALUES ('664', '7394', '339', '0', '3000000', '3000000', '0', '0', '-', '0', '7724', '141');
INSERT INTO `houses` VALUES ('666', '7397', '143', '0', '2000000', '2000000', '0', '0', '-', '0', '7736', '150');
INSERT INTO `houses` VALUES ('667', '7399', '133', '0', '3000000', '3000000', '0', '0', '-', '0', '7662', '141');
INSERT INTO `houses` VALUES ('668', '7399', '127', '0', '2000000', '2000000', '0', '0', '-', '0', '7664', '203');
INSERT INTO `houses` VALUES ('669', '7400', '105', '0', '3000000', '3000000', '0', '0', '-', '0', '7620', '141');
INSERT INTO `houses` VALUES ('670', '7384', '150', '0', '3000000', '3000000', '0', '0', '-', '0', '7626', '141');
INSERT INTO `houses` VALUES ('671', '7383', '342', '0', '2000000', '2000000', '0', '0', '-', '0', '7670', '142');
INSERT INTO `houses` VALUES ('672', '7383', '172', '0', '3000000', '3000000', '0', '0', '-', '0', '7673', '141');
INSERT INTO `houses` VALUES ('674', '7364', '143', '0', '3000000', '3000000', '0', '0', '-', '0', '7742', '141');
INSERT INTO `houses` VALUES ('675', '7380', '99', '0', '2000000', '2000000', '0', '0', '-', '0', '7641', '203');
INSERT INTO `houses` VALUES ('676', '7380', '107', '0', '2000000', '2000000', '0', '0', '-', '0', '7644', '150');
INSERT INTO `houses` VALUES ('677', '7379', '144', '0', '2000000', '2000000', '0', '0', '-', '0', '7683', '142');
INSERT INTO `houses` VALUES ('678', '7379', '163', '0', '2000000', '2000000', '0', '0', '-', '0', '7676', '150');
INSERT INTO `houses` VALUES ('679', '7377', '221', '0', '2000000', '2000000', '0', '0', '-', '0', '7725', '142');
INSERT INTO `houses` VALUES ('680', '7377', '126', '0', '2000000', '2000000', '0', '0', '-', '0', '7728', '203');
INSERT INTO `houses` VALUES ('681', '7360', '208', '0', '2000000', '2000000', '0', '0', '-', '0', '7631', '142');
INSERT INTO `houses` VALUES ('682', '7360', '278', '0', '2000000', '2000000', '0', '0', '-', '0', '7621', '158');
INSERT INTO `houses` VALUES ('683', '7360', '158', '0', '3000000', '3000000', '0', '0', '-', '0', '7634', '141');
INSERT INTO `houses` VALUES ('684', '7363', '118', '0', '3000000', '3000000', '0', '0', '-', '0', '7685', '141');
INSERT INTO `houses` VALUES ('685', '7363', '201', '0', '4000000', '4000000', '0', '0', '-', '0', '7690', '271');
INSERT INTO `houses` VALUES ('686', '7363', '397', '0', '2000000', '2000000', '0', '0', '-', '0', '7684', '142');
INSERT INTO `houses` VALUES ('687', '7381', '221', '0', '4000000', '4000000', '0', '0', '-', '0', '7651', '271');
INSERT INTO `houses` VALUES ('690', '7367', '441', '0', '2000000', '2000000', '0', '0', '-', '0', '7682', '142');
INSERT INTO `houses` VALUES ('691', '7367', '135', '0', '4000000', '4000000', '0', '0', '-', '0', '7677', '177');
INSERT INTO `houses` VALUES ('692', '7367', '172', '0', '3000000', '3000000', '0', '0', '-', '0', '7681', '141');
INSERT INTO `houses` VALUES ('693', '7368', '105', '0', '2000000', '2000000', '0', '0', '-', '0', '7630', '142');
INSERT INTO `houses` VALUES ('694', '7366', '104', '0', '2000000', '2000000', '0', '0', '-', '0', '7709', '150');
INSERT INTO `houses` VALUES ('695', '7347', '285', '0', '2000000', '2000000', '0', '0', '-', '0', '7665', '142');
INSERT INTO `houses` VALUES ('696', '7382', '447', '0', '2000000', '2000000', '0', '0', '-', '0', '7702', '150');
INSERT INTO `houses` VALUES ('697', '7348', '273', '0', '3000000', '3000000', '0', '0', '-', '0', '7654', '141');
INSERT INTO `houses` VALUES ('698', '7352', '114', '0', '2000000', '2000000', '0', '0', '-', '0', '7648', '150');
INSERT INTO `houses` VALUES ('699', '7347', '99', '0', '2000000', '2000000', '0', '0', '-', '0', '7666', '150');
INSERT INTO `houses` VALUES ('700', '7331', '124', '0', '2000000', '2000000', '0', '0', '-', '0', '7694', '142');
INSERT INTO `houses` VALUES ('701', '7331', '446', '0', '2000000', '2000000', '0', '0', '-', '0', '7695', '150');
INSERT INTO `houses` VALUES ('702', '7331', '129', '0', '2000000', '2000000', '0', '0', '-', '0', '7698', '150');
INSERT INTO `houses` VALUES ('703', '7345', '335', '0', '2000000', '2000000', '0', '0', '-', '0', '7715', '203');
INSERT INTO `houses` VALUES ('704', '7425', '425', '0', '2000000', '2000000', '0', '0', '-', '0', '7746', '142');
INSERT INTO `houses` VALUES ('705', '7425', '184', '0', '2000000', '2000000', '0', '0', '-', '0', '7749', '150');
INSERT INTO `houses` VALUES ('706', '7334', '258', '0', '2000000', '2000000', '0', '0', '-', '0', '7756', '203');
INSERT INTO `houses` VALUES ('707', '7335', '239', '0', '4000000', '4000000', '0', '0', '-', '0', '7753', '177');
INSERT INTO `houses` VALUES ('708', '7335', '388', '0', '2000000', '2000000', '0', '0', '-', '0', '7755', '203');
INSERT INTO `houses` VALUES ('709', '7334', '268', '0', '2000000', '2000000', '0', '0', '-', '0', '7758', '150');
INSERT INTO `houses` VALUES ('710', '7409', '267', '0', '10000000', '10000000', '0', '0', '-', '0', '7737', '177');
INSERT INTO `houses` VALUES ('711', '7414', '236', '0', '2000000', '2000000', '0', '0', '-', '0', '7779', '206');
INSERT INTO `houses` VALUES ('712', '7786', '165', '0', '1000000', '1000000', '0', '0', '-', '0', '7788', '165');
INSERT INTO `houses` VALUES ('713', '7786', '210', '0', '1000000', '1000000', '0', '0', '-', '0', '7789', '210');
INSERT INTO `houses` VALUES ('714', '7786', '255', '0', '1000000', '1000000', '0', '0', '-', '0', '7790', '255');
INSERT INTO `houses` VALUES ('715', '7786', '300', '0', '1000000', '1000000', '0', '0', '-', '0', '7791', '300');
INSERT INTO `houses` VALUES ('716', '7786', '345', '0', '1000000', '1000000', '0', '0', '-', '0', '7792', '345');
INSERT INTO `houses` VALUES ('717', '7912', '265', '0', '2000000', '2000000', '0', '0', '-', '0', '8378', '265');
INSERT INTO `houses` VALUES ('718', '8016', '256', '0', '2000000', '2000000', '0', '0', '-', '0', '8379', '256');
INSERT INTO `houses` VALUES ('719', '7912', '286', '0', '3000000', '3000000', '0', '0', '-', '0', '8381', '286');
INSERT INTO `houses` VALUES ('720', '7995', '299', '0', '3000000', '3000000', '0', '0', '-', '0', '8384', '299');
INSERT INTO `houses` VALUES ('721', '8003', '67', '0', '2000000', '2000000', '0', '0', '-', '0', '8385', '195');
INSERT INTO `houses` VALUES ('722', '7983', '269', '0', '2000000', '2000000', '0', '0', '-', '0', '8388', '269');
INSERT INTO `houses` VALUES ('723', '7984', '226', '0', '2000000', '2000000', '0', '0', '-', '0', '8390', '226');
INSERT INTO `houses` VALUES ('724', '7971', '343', '0', '2000000', '2000000', '0', '0', '-', '0', '8393', '343');
INSERT INTO `houses` VALUES ('725', '7971', '446', '0', '2000000', '2000000', '0', '0', '-', '0', '8396', '446');
INSERT INTO `houses` VALUES ('726', '8011', '270', '0', '2000000', '2000000', '0', '0', '-', '0', '8398', '270');
INSERT INTO `houses` VALUES ('727', '7998', '220', '0', '2000000', '2000000', '0', '0', '-', '0', '8401', '220');
INSERT INTO `houses` VALUES ('728', '7998', '212', '0', '2000000', '2000000', '0', '0', '-', '0', '8404', '212');
INSERT INTO `houses` VALUES ('729', '7972', '241', '0', '2000000', '2000000', '0', '0', '-', '0', '8407', '241');
INSERT INTO `houses` VALUES ('730', '8002', '46', '0', '2000000', '2000000', '0', '0', '-', '0', '8409', '46');
INSERT INTO `houses` VALUES ('731', '8002', '211', '0', '2000000', '2000000', '0', '0', '-', '0', '8414', '211');
INSERT INTO `houses` VALUES ('732', '7972', '186', '0', '2000000', '2000000', '0', '0', '-', '0', '8412', '186');
INSERT INTO `houses` VALUES ('733', '8002', '257', '0', '2000000', '2000000', '0', '0', '-', '0', '8419', '257');
INSERT INTO `houses` VALUES ('734', '7972', '347', '0', '2000000', '2000000', '0', '0', '-', '0', '8417', '347');
INSERT INTO `houses` VALUES ('735', '8015', '225', '0', '2000000', '2000000', '0', '0', '-', '0', '8422', '225');
INSERT INTO `houses` VALUES ('736', '8028', '235', '0', '2000000', '2000000', '0', '0', '-', '0', '8429', '235');
INSERT INTO `houses` VALUES ('737', '7973', '168', '0', '2000000', '2000000', '0', '0', '-', '0', '8425', '168');
INSERT INTO `houses` VALUES ('738', '7988', '100', '0', '2000000', '2000000', '0', '0', '-', '0', '8435', '100');
INSERT INTO `houses` VALUES ('739', '7973', '122', '0', '2000000', '2000000', '0', '0', '-', '0', '8432', '122');
INSERT INTO `houses` VALUES ('741', '7974', '206', '0', '2000000', '2000000', '0', '0', '-', '0', '8436', '206');
INSERT INTO `houses` VALUES ('742', '8001', '263', '0', '2000000', '2000000', '0', '0', '-', '0', '8440', '263');
INSERT INTO `houses` VALUES ('743', '7974', '108', '0', '2000000', '2000000', '0', '0', '-', '0', '8441', '108');
INSERT INTO `houses` VALUES ('744', '8001', '170', '0', '2000000', '2000000', '0', '0', '-', '0', '8447', '170');
INSERT INTO `houses` VALUES ('745', '7974', '71', '0', '2000000', '2000000', '0', '0', '-', '0', '8445', '71');
INSERT INTO `houses` VALUES ('746', '7987', '103', '0', '2000000', '2000000', '0', '0', '-', '0', '8449', '103');
INSERT INTO `houses` VALUES ('747', '8039', '104', '0', '2000000', '2000000', '0', '0', '-', '0', '8453', '104');
INSERT INTO `houses` VALUES ('748', '7987', '84', '0', '2000000', '2000000', '0', '0', '-', '0', '8452', '84');
INSERT INTO `houses` VALUES ('749', '7987', '269', '0', '2000000', '2000000', '0', '0', '-', '0', '8455', '269');
INSERT INTO `houses` VALUES ('750', '8000', '109', '0', '2000000', '2000000', '0', '0', '-', '0', '8457', '109');
INSERT INTO `houses` VALUES ('751', '8001', '239', '0', '2000000', '2000000', '0', '0', '-', '0', '8443', '239');
INSERT INTO `houses` VALUES ('752', '8038', '421', '0', '2000000', '2000000', '0', '0', '-', '0', '8459', '421');
INSERT INTO `houses` VALUES ('753', '8025', '296', '0', '3000000', '3000000', '0', '0', '-', '0', '8461', '296');
INSERT INTO `houses` VALUES ('754', '7986', '152', '0', '2000000', '2000000', '0', '0', '-', '0', '8464', '152');
INSERT INTO `houses` VALUES ('755', '8012', '271', '0', '2000000', '2000000', '0', '0', '-', '0', '8465', '271');
INSERT INTO `houses` VALUES ('756', '7986', '263', '0', '3000000', '3000000', '0', '0', '-', '0', '8468', '263');
INSERT INTO `houses` VALUES ('757', '7999', '176', '0', '2000000', '2000000', '0', '0', '-', '0', '8470', '176');
INSERT INTO `houses` VALUES ('759', '9455', '397', '0', '2000000', '2000000', '0', '0', '-', '0', '9666', '343');
INSERT INTO `houses` VALUES ('761', '9451', '385', '0', '2000000', '2000000', '0', '0', '-', '510', '9669', '343');
INSERT INTO `houses` VALUES ('762', '9464', '306', '0', '2000000', '2000000', '0', '0', '-', '0', '9670', '343');
INSERT INTO `houses` VALUES ('763', '9464', '184', '0', '3000000', '3000000', '0', '0', '-', '0', '9672', '414');
INSERT INTO `houses` VALUES ('764', '9450', '137', '0', '2000000', '2000000', '0', '0', '-', '0', '9674', '343');
INSERT INTO `houses` VALUES ('765', '9450', '434', '0', '2000000', '2000000', '0', '0', '-', '0', '9676', '343');
INSERT INTO `houses` VALUES ('766', '9450', '114', '0', '3000000', '3000000', '0', '0', '-', '0', '9678', '414');
INSERT INTO `houses` VALUES ('767', '9454', '129', '0', '2000000', '2000000', '1', '0', '-', '0', '9680', '343');
INSERT INTO `houses` VALUES ('768', '9454', '88', '0', '3000000', '3000000', '1', '0', '-', '0', '9682', '414');
INSERT INTO `houses` VALUES ('769', '9457', '218', '0', '4000000', '4000000', '0', '0', '-', '0', '9684', '381');
INSERT INTO `houses` VALUES ('770', '9461', '349', '0', '2000000', '2000000', '0', '0', '-', '0', '9687', '343');
INSERT INTO `houses` VALUES ('771', '9461', '129', '0', '3000000', '3000000', '0', '0', '-', '0', '9689', '414');
INSERT INTO `houses` VALUES ('772', '9449', '382', '0', '2000000', '2000000', '0', '0', '-', '0', '9691', '343');
INSERT INTO `houses` VALUES ('773', '9449', '302', '0', '2000000', '2000000', '0', '0', '-', '0', '9693', '343');
INSERT INTO `houses` VALUES ('774', '9449', '99', '0', '4000000', '4000000', '0', '0', '-', '0', '9695', '381');
INSERT INTO `houses` VALUES ('775', '9453', '76', '0', '4000000', '4000000', '0', '0', '-', '0', '9698', '381');
INSERT INTO `houses` VALUES ('776', '9453', '442', '0', '3000000', '3000000', '0', '0', '-', '0', '9701', '414');
INSERT INTO `houses` VALUES ('777', '9453', '233', '0', '2000000', '2000000', '0', '0', '-', '0', '9703', '343');
INSERT INTO `houses` VALUES ('778', '9456', '86', '0', '2000000', '2000000', '0', '0', '-', '0', '9705', '343');
INSERT INTO `houses` VALUES ('779', '9456', '448', '0', '3000000', '3000000', '0', '0', '-', '0', '9707', '414');
INSERT INTO `houses` VALUES ('780', '9460', '119', '0', '2000000', '2000000', '0', '0', '-', '0', '9709', '343');
INSERT INTO `houses` VALUES ('781', '9460', '107', '0', '2000000', '2000000', '0', '0', '-', '0', '9711', '343');
INSERT INTO `houses` VALUES ('782', '9460', '399', '0', '4000000', '4000000', '0', '0', '-', '0', '9713', '381');
INSERT INTO `houses` VALUES ('783', '220', '84', '0', '3000000', '3000000', '0', '0', '-', '0', '9616', '414');
INSERT INTO `houses` VALUES ('784', '8779', '178', '0', '2000000', '2000000', '0', '0', '-', '0', '8870', '309');
INSERT INTO `houses` VALUES ('785', '8780', '196', '0', '2000000', '2000000', '0', '0', '-', '0', '8876', '309');
INSERT INTO `houses` VALUES ('786', '8781', '294', '0', '4000000', '4000000', '0', '0', '-', '0', '8874', '356');
INSERT INTO `houses` VALUES ('787', '8817', '295', '0', '2000000', '2000000', '0', '0', '-', '0', '8877', '295');
INSERT INTO `houses` VALUES ('788', '8784', '235', '0', '4000000', '4000000', '0', '0', '-', '0', '8871', '327');
INSERT INTO `houses` VALUES ('789', '8785', '76', '0', '3000000', '3000000', '0', '0', '-', '0', '8872', '313');
INSERT INTO `houses` VALUES ('790', '8785', '155', '0', '2000000', '2000000', '0', '0', '-', '0', '8878', '309');
INSERT INTO `houses` VALUES ('791', '8786', '271', '0', '3000000', '3000000', '0', '0', '-', '0', '8879', '313');
INSERT INTO `houses` VALUES ('792', '8821', '83', '0', '4000000', '4000000', '0', '0', '-', '0', '8882', '356');
INSERT INTO `houses` VALUES ('793', '8821', '218', '0', '2000000', '2000000', '0', '0', '-', '0', '8880', '327');
INSERT INTO `houses` VALUES ('794', '8789', '172', '0', '2000000', '2000000', '0', '0', '-', '0', '8885', '309');
INSERT INTO `houses` VALUES ('795', '8789', '111', '0', '2000000', '2000000', '0', '0', '-', '0', '8884', '309');
INSERT INTO `houses` VALUES ('796', '8790', '444', '0', '3000000', '3000000', '0', '0', '-', '0', '8888', '313');
INSERT INTO `houses` VALUES ('797', '8790', '309', '0', '2000000', '2000000', '0', '0', '-', '0', '8886', '327');
INSERT INTO `houses` VALUES ('798', '8825', '198', '0', '3000000', '3000000', '0', '0', '-', '0', '8889', '313');
INSERT INTO `houses` VALUES ('799', '8794', '127', '0', '3000000', '3000000', '0', '0', '-', '0', '8890', '313');
INSERT INTO `houses` VALUES ('800', '8794', '282', '0', '4000000', '4000000', '0', '0', '-', '0', '8892', '356');
INSERT INTO `houses` VALUES ('801', '8795', '250', '0', '2000000', '2000000', '0', '0', '-', '0', '8893', '309');
INSERT INTO `houses` VALUES ('802', '8795', '235', '0', '3000000', '3000000', '0', '0', '-', '0', '8894', '313');
INSERT INTO `houses` VALUES ('803', '8795', '286', '0', '4000000', '4000000', '0', '0', '-', '0', '8895', '356');
INSERT INTO `houses` VALUES ('804', '8796', '370', '0', '4000000', '4000000', '0', '0', '-', '0', '8897', '327');
INSERT INTO `houses` VALUES ('805', '8829', '271', '0', '3000000', '3000000', '0', '0', '-', '0', '8899', '313');
INSERT INTO `houses` VALUES ('806', '8800', '161', '0', '2000000', '2000000', '0', '0', '-', '0', '8900', '309');
INSERT INTO `houses` VALUES ('807', '8832', '163', '0', '2000000', '2000000', '0', '0', '-', '0', '8901', '327');
INSERT INTO `houses` VALUES ('808', '8833', '235', '0', '4000000', '4000000', '0', '0', '-', '0', '8903', '356');
INSERT INTO `houses` VALUES ('813', '10744', '307', '0', '2000000', '2000000', '0', '0', '-', '0', '10859', '307');
INSERT INTO `houses` VALUES ('814', '10744', '274', '0', '2000000', '2000000', '0', '0', '-', '0', '10860', '274');
INSERT INTO `houses` VALUES ('815', '10744', '214', '0', '3000000', '3000000', '0', '0', '-', '0', '10861', '214');
INSERT INTO `houses` VALUES ('817', '10745', '227', '0', '3000000', '3000000', '0', '0', '-', '0', '10864', '227');
INSERT INTO `houses` VALUES ('818', '10635', '301', '0', '2000000', '2000000', '0', '0', '-', '0', '10866', '301');
INSERT INTO `houses` VALUES ('819', '10635', '195', '0', '3000000', '3000000', '0', '0', '-', '0', '10867', '195');
INSERT INTO `houses` VALUES ('820', '10635', '336', '0', '3000000', '3000000', '0', '0', '-', '0', '10868', '336');
INSERT INTO `houses` VALUES ('821', '10746', '220', '0', '3000000', '3000000', '0', '0', '-', '0', '10870', '220');
INSERT INTO `houses` VALUES ('822', '10638', '419', '0', '2000000', '2000000', '0', '0', '-', '0', '10871', '419');
INSERT INTO `houses` VALUES ('823', '10638', '223', '0', '2000000', '2000000', '0', '0', '-', '0', '10872', '223');
INSERT INTO `houses` VALUES ('824', '10638', '154', '0', '3000000', '3000000', '0', '0', '-', '0', '10873', '154');
INSERT INTO `houses` VALUES ('825', '10638', '176', '0', '4000000', '4000000', '0', '0', '-', '0', '10874', '176');
INSERT INTO `houses` VALUES ('826', '10640', '328', '0', '4000000', '4000000', '0', '0', '-', '0', '10876', '328');
INSERT INTO `houses` VALUES ('827', '10640', '223', '0', '3000000', '3000000', '0', '0', '-', '0', '10877', '223');
INSERT INTO `houses` VALUES ('828', '10640', '350', '0', '2000000', '2000000', '0', '0', '-', '0', '10878', '350');
INSERT INTO `houses` VALUES ('829', '10642', '448', '0', '2000000', '2000000', '0', '0', '-', '0', '10879', '448');
INSERT INTO `houses` VALUES ('830', '10642', '186', '0', '2000000', '2000000', '0', '0', '-', '0', '10880', '186');
INSERT INTO `houses` VALUES ('831', '10642', '326', '0', '4000000', '4000000', '0', '0', '-', '0', '10882', '326');
INSERT INTO `houses` VALUES ('832', '10644', '337', '0', '4000000', '4000000', '0', '0', '-', '0', '10884', '337');
INSERT INTO `houses` VALUES ('833', '10646', '228', '0', '4000000', '4000000', '0', '0', '-', '0', '10886', '228');
INSERT INTO `houses` VALUES ('834', '10649', '365', '0', '4000000', '4000000', '0', '0', '-', '0', '10887', '365');
INSERT INTO `houses` VALUES ('835', '10748', '370', '0', '4000000', '4000000', '0', '0', '-', '0', '10888', '370');
INSERT INTO `houses` VALUES ('836', '10641', '356', '0', '4000000', '4000000', '0', '0', '-', '0', '10889', '356');
INSERT INTO `houses` VALUES ('837', '10643', '216', '0', '4000000', '4000000', '0', '0', '-', '0', '10891', '216');
INSERT INTO `houses` VALUES ('838', '10647', '455', '0', '4000000', '4000000', '0', '0', '-', '0', '10892', '455');
INSERT INTO `houses` VALUES ('839', '10650', '183', '0', '4000000', '4000000', '0', '0', '-', '0', '10893', '183');
INSERT INTO `houses` VALUES ('840', '10750', '299', '0', '4000000', '4000000', '0', '0', '-', '0', '10895', '299');
INSERT INTO `houses` VALUES ('841', '10751', '204', '0', '4000000', '4000000', '0', '0', '-', '0', '10896', '204');
INSERT INTO `houses` VALUES ('842', '10752', '326', '0', '4000000', '4000000', '0', '0', '-', '0', '10897', '326');
INSERT INTO `houses` VALUES ('843', '10753', '211', '0', '4000000', '4000000', '0', '0', '-', '0', '10898', '211');
INSERT INTO `houses` VALUES ('844', '10755', '296', '0', '4000000', '4000000', '0', '0', '-', '0', '10899', '296');
INSERT INTO `houses` VALUES ('846', '10649', '312', '0', '3000000', '3000000', '0', '0', '-', '0', '10905', '312');
INSERT INTO `houses` VALUES ('847', '10748', '147', '0', '3000000', '3000000', '0', '0', '-', '0', '10906', '147');
INSERT INTO `houses` VALUES ('848', '10641', '216', '0', '3000000', '3000000', '0', '0', '-', '0', '10907', '216');
INSERT INTO `houses` VALUES ('849', '10645', '257', '0', '3000000', '3000000', '0', '0', '-', '0', '10908', '257');
INSERT INTO `houses` VALUES ('850', '10645', '409', '0', '3000000', '3000000', '0', '0', '-', '0', '10909', '409');
INSERT INTO `houses` VALUES ('851', '10650', '265', '0', '3000000', '3000000', '0', '0', '-', '0', '10910', '265');
INSERT INTO `houses` VALUES ('852', '10750', '204', '0', '3000000', '3000000', '0', '0', '-', '0', '10911', '204');
INSERT INTO `houses` VALUES ('853', '10751', '328', '0', '3000000', '3000000', '0', '0', '-', '0', '10912', '328');
INSERT INTO `houses` VALUES ('854', '10752', '199', '0', '3000000', '3000000', '0', '0', '-', '0', '10913', '199');
INSERT INTO `houses` VALUES ('855', '10753', '373', '0', '3000000', '3000000', '0', '0', '-', '0', '10914', '373');
INSERT INTO `houses` VALUES ('856', '10644', '360', '0', '2000000', '2000000', '0', '0', '-', '0', '10915', '360');
INSERT INTO `houses` VALUES ('857', '10646', '283', '0', '2000000', '2000000', '0', '0', '-', '0', '10916', '283');
INSERT INTO `houses` VALUES ('858', '10646', '352', '0', '2000000', '2000000', '0', '0', '-', '0', '10917', '352');
INSERT INTO `houses` VALUES ('859', '10747', '207', '0', '2000000', '2000000', '0', '0', '-', '0', '10918', '207');
INSERT INTO `houses` VALUES ('860', '10748', '407', '0', '2000000', '2000000', '0', '0', '-', '0', '10919', '407');
INSERT INTO `houses` VALUES ('861', '10641', '396', '0', '2000000', '2000000', '0', '0', '-', '0', '10920', '396');
INSERT INTO `houses` VALUES ('862', '10643', '365', '0', '2000000', '2000000', '0', '0', '-', '0', '10921', '327');
INSERT INTO `houses` VALUES ('864', '10647', '330', '0', '2000000', '2000000', '0', '0', '-', '0', '10923', '330');
INSERT INTO `houses` VALUES ('865', '10647', '394', '0', '2000000', '2000000', '0', '0', '-', '0', '10924', '394');
INSERT INTO `houses` VALUES ('866', '10650', '412', '0', '2000000', '2000000', '0', '0', '-', '0', '10925', '412');
INSERT INTO `houses` VALUES ('867', '10749', '136', '0', '2000000', '2000000', '0', '0', '-', '0', '10926', '136');
INSERT INTO `houses` VALUES ('868', '10750', '354', '0', '2000000', '2000000', '0', '0', '-', '0', '10927', '354');
INSERT INTO `houses` VALUES ('869', '10751', '180', '0', '2000000', '2000000', '0', '0', '-', '0', '10928', '180');
INSERT INTO `houses` VALUES ('870', '10751', '361', '0', '2000000', '2000000', '0', '0', '-', '0', '10929', '361');
INSERT INTO `houses` VALUES ('871', '10645', '152', '0', '2000000', '2000000', '0', '0', '-', '0', '10931', '152');
INSERT INTO `houses` VALUES ('872', '10753', '352', '0', '2000000', '2000000', '0', '0', '-', '0', '10932', '352');
INSERT INTO `houses` VALUES ('873', '10755', '235', '0', '2000000', '2000000', '0', '0', '-', '0', '10933', '235');
INSERT INTO `houses` VALUES ('874', '10616', '411', '0', '10000000', '10000000', '0', '0', '-', '0', '10991', '411');
INSERT INTO `houses` VALUES ('875', '10618', '170', '0', '10000000', '10000000', '0', '0', '-', '0', '10996', '170');
INSERT INTO `houses` VALUES ('876', '10630', '324', '0', '10000000', '10000000', '0', '0', '-', '0', '11001', '324');
INSERT INTO `houses` VALUES ('877', '10622', '431', '0', '10000000', '10000000', '0', '0', '-', '0', '11006', '431');
INSERT INTO `houses` VALUES ('878', '10606', '162', '0', '10000000', '10000000', '0', '0', '-', '0', '11011', '162');
INSERT INTO `houses` VALUES ('879', '10607', '249', '0', '10000000', '10000000', '0', '0', '-', '0', '11016', '249');
INSERT INTO `houses` VALUES ('880', '10609', '422', '0', '10000000', '10000000', '0', '0', '-', '0', '11021', '422');
INSERT INTO `houses` VALUES ('881', '10611', '201', '0', '10000000', '10000000', '0', '0', '-', '0', '11026', '201');
INSERT INTO `houses` VALUES ('882', '10599', '440', '0', '5000000', '5000000', '0', '0', '-', '0', '11030', '440');
INSERT INTO `houses` VALUES ('883', '10600', '439', '0', '5000000', '5000000', '0', '0', '-', '0', '11034', '439');
INSERT INTO `houses` VALUES ('884', '10601', '448', '0', '5000000', '5000000', '0', '0', '-', '0', '11038', '448');
INSERT INTO `houses` VALUES ('885', '10602', '161', '0', '5000000', '5000000', '0', '0', '-', '0', '11042', '161');
INSERT INTO `houses` VALUES ('886', '10561', '161', '0', '5000000', '5000000', '0', '0', '-', '0', '11046', '161');
INSERT INTO `houses` VALUES ('887', '10559', '433', '0', '5000000', '5000000', '0', '0', '-', '0', '11050', '433');

-- ----------------------------
-- Table structure for `item_liee`
-- ----------------------------
DROP TABLE IF EXISTS `item_liee`;
CREATE TABLE `item_liee` (
  `objid` int(11) NOT NULL,
  `annee` int(11) NOT NULL,
  `moi` int(11) NOT NULL,
  `jour` int(11) NOT NULL,
  `heure` int(11) NOT NULL,
  `minute` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of item_liee
-- ----------------------------

-- ----------------------------
-- Table structure for `items`
-- ----------------------------
DROP TABLE IF EXISTS `items`;
CREATE TABLE `items` (
  `guid` int(11) NOT NULL,
  `template` int(11) NOT NULL,
  `qua` int(11) NOT NULL,
  `pos` int(11) NOT NULL,
  `stats` text CHARACTER SET latin1 NOT NULL,
  `puit` int(11) NOT NULL DEFAULT '0',
  `mode` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`guid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of items
-- ----------------------------

-- ----------------------------
-- Table structure for `mountpark_data`
-- ----------------------------
DROP TABLE IF EXISTS `mountpark_data`;
CREATE TABLE `mountpark_data` (
  `mapid` int(11) NOT NULL,
  `cellid` int(11) NOT NULL,
  `size` int(11) NOT NULL,
  `sizeObj` int(11) NOT NULL,
  `owner` int(11) NOT NULL,
  `guild` int(11) NOT NULL DEFAULT '-1',
  `price` int(11) NOT NULL,
  `priceBase` int(11) NOT NULL,
  `data` text NOT NULL COMMENT 'Etable',
  `enclos` text NOT NULL,
  `cellMount` int(11) NOT NULL,
  `cellPorte` int(11) NOT NULL,
  `ObjetPlacer` text NOT NULL,
  `cellEnclos` text NOT NULL,
  `durabilite` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of mountpark_data
-- ----------------------------
INSERT INTO `mountpark_data` VALUES ('8760', '753', '3', '3', '0', '-1', '770000', '770000', '', '', '717', '735', '', '660;642;624;606;588;570;589;607;625;643;661;679;698;680;662;644;626;608;627;645;663;681;699;717;736;718;700;682;646;665;683;701;719;737;755;774;756;738;720;702', '');
INSERT INTO `mountpark_data` VALUES ('2221', '242', '5', '5', '0', '-1', '1210000', '1210000', '', '', '206', '224', '', '206;187;168;149;130;111;74;93;112;131;150;169;188;170;151;132;113;94;75;56;37;19;38;57;76;95;114;133;152;134;115;96;77;58;39;20;1;2;21;40;59;78;97;116;98;79;60;41;22;3;4;23;42;61;80;62;43;24;5;44;25', '');
INSERT INTO `mountpark_data` VALUES ('4308', '507', '5', '5', '0', '-1', '1210000', '1210000', '', '', '543', '525', '', '486;504;522;540;558;576;595;577;559;541;523;505;524;542;560;578;596;614;633;615;597;579;561;543;562;580;598;616;634;652;671;690;672;653;635;654;636;617;599;618;600;581', '');
INSERT INTO `mountpark_data` VALUES ('8848', '549', '10', '10', '0', '-1', '2282500', '2282500', '', '', '511', '530', '120;7616;1|172;7616;1|215;7616;1|231;7616;1|311;7616;1|340;7616;1|363;7616;1|490;7616;1', '', '120;500;999998249|172;500;999986483|215;500;999998406|231;500;999996809|311;500;999999478|340;500;999986827|363;500;999998436|490;500;999994311');
INSERT INTO `mountpark_data` VALUES ('8744', '561', '10', '10', '0', '-1', '2282500', '2282500', '', '', '543', '561', '250;7741;1|362;7741;1|378;7741;1|383;7741;1|412;7741;1|492;7741;1|513;7741;1|545;7741;1', '', '250;500;999991912|362;500;999991864|378;500;999995911|383;500;999989153|412;500;999996698|492;500;999991100|513;500;999995932|545;500;999996402');
INSERT INTO `mountpark_data` VALUES ('8743', '586', '10', '10', '0', '-1', '2282500', '2282500', '', '', '567', '586', '', '', '268;500;999998086|320;500;999994690|327;500;999997815|415;500;999986083|454;500;999998185|457;500;999999493|486;500;999998597|583;500;999996433');
INSERT INTO `mountpark_data` VALUES ('9748', '356', '14', '14', '0', '-1', '3135000', '3135000', '', '', '326', '341', '', '242;256;270;284;298;312;326;340;354;368;382;396;410;424;409;395;381;367;353;339;325;311;297;283;269;255;241;227;212;226;240;254;268;282;296;310;324;338;352;366;380;394;351;337;323;309;295;281;267;253;239;225;211;197;182;196;210;224;238;252;266;280;294;308;322;336;293;279;265;251;237;223;209;195;181;167;152;166;180;194;208;222;236;250;264;278;263;249;235;221;207;193;179;165;151;137;192;177;162;176;190;204;218;233;219;205;191;206;220;234', '');
INSERT INTO `mountpark_data` VALUES ('9747', '78', '12', '12', '0', '-1', '2695000', '2695000', '', '', '106', '92', '', '91;105;119;133;147;162;176;148;134;120;106;121;135;149;163;177;192;178;164;150;136;137;123;109;95;151;165;179;193;207;221;250;236;222;208;194;180;166;152;138;124;110;125;139;153;167;181;195;209;223;237;251;265;280;266;252;238;224;210;140;154;168;182;169;155;170;184;239;253;267;281;295;310;296;282;268', '');
INSERT INTO `mountpark_data` VALUES ('9746', '169', '12', '12', '0', '-1', '2695000', '2695000', '', '', '197', '183', '', '197;182;167;152;137;122;212;227;242;257;272;287;301;286;271;256;241;226;211;196;181;166;151;136;150;165;180;195;210;225;240;255;270;285;300;315;329;314;299;284;269;254;239;224;209;194;179;164;178;193;208;223;238;253;268;283;298;343;328;342;356;371;357;297;282;267;252;237;222;207;192;206;221;236;251;266;281;296;311', '');
INSERT INTO `mountpark_data` VALUES ('8747', '615', '10', '10', '0', '-1', '2282500', '2282500', '', '', '579', '597', '228;7596;1|253;7596;1|269;7596;1|340;7596;1|380;7596;1|401;7596;1|467;7596;1|581;7596;1', '', '228;500;999996395|253;500;999981461|269;500;999989303|340;500;999986308|380;500;999991713|401;500;999991152|467;500;999993457|581;500;999992907');
INSERT INTO `mountpark_data` VALUES ('9745', '168', '8', '8', '0', '-1', '1952500', '1952500', '', '', '196', '182', '', '181;166;151;136;121;196;211;226;241;256;271;286;301;316;330;315;300;285;270;255;240;225;210;195;180;165;150;135;149;164;179;194;209;224;239;254;269;284;299;314;329;344;358;343;328;313;298;283;268;253;238;223;208;193;178;163;177;192;207;222;237;252;267;282;297;312;327;342;357;372;386;371;356;341;326;311;296;281;266;251;265;280;295;310;325;340;355;370;385;400;414;399;384;369;354;339;324;309;294', '');
INSERT INTO `mountpark_data` VALUES ('9744', '164', '7', '7', '0', '-1', '1173500', '1173500', '', '', '194', '179', '', '166;180;194;208;222;236;251;237;223;209;195;181;196;210;224;238;252;266;281;267;253;239;225;211;212;198;184;170;226;240;254;268;282;296;310;324;338;352;367;353;339;325;311;297;283;269;255;241;227;213;199;185;200;214;228;242;256;270;284;298;312;326;340;354;368;382;397;383;369;355;341;327;313;299;314;328;342;356;370;384;398;412;427;413;399;385;371;357;343;329;344;358;372;386;400', '');
INSERT INTO `mountpark_data` VALUES ('9743', '208', '8', '8', '0', '-1', '1952500', '1952500', '', '', '238', '223', '', '196;210;224;238;252;266;280;294;309;295;281;267;253;239;225;211;226;240;254;268;282;296;310;324;339;325;311;297;283;269;255;241;256;270;284;298;312;326;340;354;355;341;327;313;299;285;271;286;300;314;328;342;356;370;384;399;385;371;357;343;329;301;358;372;386;400;414;429;415;401;387;373;359;360;374;388;402;416;430;431;417;403;389;375;404;418', '');
INSERT INTO `mountpark_data` VALUES ('9740', '178', '5', '5', '0', '-1', '1210000', '1210000', '', '', '208', '193', '', '180;194;208;222;236;251;237;223;209;195;210;224;238;252;266;295;309;323;281;267;253;239;225;211;197;183;198;212;226;240;296;310;324;338;353;339;325;311;297;298;299;270;241;227;213;228;242;256;313;327;341;355;369;368;354;340;326;312;383;285;271;257;243;300;314;328;342;356;371;357;343;329;315;330;344;358;372', '');
INSERT INTO `mountpark_data` VALUES ('9741', '298', '5', '5', '0', '-1', '1210000', '1210000', '', '', '268', '283', '', '198;212;226;240;254;268;282;296;310;324;338;323;309;295;281;267;253;239;225;211;197;183;168;182;196;210;224;238;252;266;280;294;308;293;279;265;251;237;223;209;195;181;167;153;138;124;110;152;166;180;194;278;264;250;236;222;95;80;94;109;123;137;151;165;179;164;135;106;91;76;61;90;105;62;77;92;107;122;207;221;235;249;263;248;234;220;206;192;177;191;205;219;233;190;176;162;161;175;160;146;132;118;103;117;131;102', '');
INSERT INTO `mountpark_data` VALUES ('9728', '359', '14', '14', '0', '-1', '3135000', '3135000', '', '', '329', '344', '140;7776;5505|152;7757;5505|155;7776;3636|166;7757;5505|222;7741;5505|236;7741;5505|272;7595;5505|287;7595;5505|308;7695;5505|323;7695;5505|356;7616;5505|371;7616;5505', '329;343;357;371;315;301;287;272;286;300;314;328;342;356;341;327;313;299;285;271;257;242;256;270;284;298;312;326;340;354;368;353;339;325;311;297;283;269;255;241;227;198;184;170;212;226;240;254;268;282;296;310;324;338;323;309;295;281;267;253;239;225;211;197;183;169;155;140;154;168;182;196;210;224;238;252;266;280;294;308;251;237;223;209;195;181;167;152;166;180;194;208;222;236;221;207;193;179;165;151;137;122;136;150;164;178;192;206;177;163;149;135;121', '140;3000;1537|152;3000;2754|155;3000;1949|166;3000;2428|222;3000;2717|236;3000;2872|272;3000;2639|287;3000;2657|299;3000;1428|308;3000;1928|323;3000;2170|356;3000;2757|371;3000;2772');
INSERT INTO `mountpark_data` VALUES ('9742', '214', '12', '12', '0', '-1', '2695000', '2695000', '', '', '242', '228', '', '242;227;212;257;272;287;301;286;271;256;241;226;240;255;270;285;300;315;329;314;299;284;269;254;253;238;223;268;283;298;313;328;343;357;342;327;312;297;282;267;252;237;251;266;281;296;311;326;341;356;371;386;401;416;430;415;400;385;325;310;295;280;265;279;294;309;324;339;340;399;414;429;444;458;443;428;413;412;411;382;353;338;323;308;293;307;322;337;352;367;426;441;456;457;442;427;396;381;366;351;321;380;395;410;425;440;455;439;424;409;394;408;423;438', '');
INSERT INTO `mountpark_data` VALUES ('9732', '112', '12', '12', '0', '-1', '2695000', '2695000', '', '', '155', '141', '', '155;140;170;185;199;184;169;154;168;183;198;213;242;257;272;287;227;212;197;182;167;152;137;122;107;121;136;151;166;181;196;211;226;241;256;271;286;301;315;300;285;270;135;150;165;180;195;210;225;240;314;329;299;254;239;224;209;194;179;164;149;163;178;193;208;223;238;253;282;297;312;327;328;343;357;342;267;252;237;222;207;192;177;191;206;221;236;251;266;281;296;311;326;341;356;371;385;370;355;340;325;310;295;280;265;250;235;220', '');
INSERT INTO `mountpark_data` VALUES ('8746', '208', '10', '10', '0', '-1', '2282500', '2282500', '', '', '246', '227', '249;7695;1|325;7695;1|356;7695;1|377;7695;1|455;7695;1|526;7695;1|548;7695;1|560;7695;1', '', '249;500;999988566|325;500;999995805|356;500;999995408|377;500;999997261|455;500;999968856|526;500;999996082|548;500;999984573|560;500;999993595');
INSERT INTO `mountpark_data` VALUES ('9733', '345', '5', '5', '0', '-1', '1210000', '1210000', '', '', '315', '330', '', '315;329;301;287;272;286;300;314;299;285;271;257;242;256;270;284;269;255;241;227;212;226;240;254;268;282;296;281;267;253;239;225;211;197;182;196;210;224;238;252;266;251;237;223;209;195;181;167;194;208;222;236;221;207;193;179;164;178;192;206;191;177;163', '');
INSERT INTO `mountpark_data` VALUES ('9734', '279', '5', '5', '0', '-1', '1210000', '1210000', '', '', '251', '265', '', '251;266;281;296;311;236;221;206;191;177;192;207;222;237;252;267;282;297;283;268;253;238;223;208;193;178;163;149;164;179;194;209;224;239;254;269;255;241;240;225;210;195;180;165;150;135;121;136;151;166;196;152;137;122;107;93;108;123', '');
INSERT INTO `mountpark_data` VALUES ('8745', '235', '10', '10', '0', '-1', '2282500', '2282500', '', '', '271', '253', '250;7784;1|266;7784;1|364;7784;1|379;7784;1|412;7784;1|507;7784;1|510;7784;1|587;7784;1', '', '250;500;999993838|266;500;999995268|364;500;999992940|379;500;999974111|412;500;999991994|507;500;999988404|510;500;999996234|587;500;999997893');
INSERT INTO `mountpark_data` VALUES ('9735', '342', '5', '5', '0', '-1', '1210000', '1210000', '', '', '312', '327', '', '312;298;284;270;256;242;326;340;354;368;382;367;353;339;325;311;297;283;269;255;241;227;212;226;240;254;268;282;296;310;324;338;352;337;323;309;295;281;267;253;239;225;211;197;182;196;210;224;238;252;266;280;294;308;322;307;279;265;251;237;223;209;195;181;167;180;194;208;222;236;221;207;193;179;165;150;164;178;192;206;191;177;163', '');
INSERT INTO `mountpark_data` VALUES ('9356', '399', '5', '5', '0', '-1', '1210000', '1210000', '', '', '369', '384', '', '262;248;234;220;206;192;178;193;207;221;235;249;263;277;292;278;264;250;236;222;208;223;237;251;265;279;293;307;322;308;294;280;266;252;238;253;267;281;295;309;323;337;352;338;324;310;296;282;268;283;297;311;325;339;353;367;382;368;354;340;326;312;298;313;327;341;355;369;383;397', '');
INSERT INTO `mountpark_data` VALUES ('8752', '602', '10', '10', '0', '-1', '2282500', '2282500', '', '', '564', '602', '210;7784;1|232;7784;1|341;7784;1|420;7784;1|449;7784;1|473;7784;1|562;7784;1', '', '210;500;999993470|232;500;999997031|341;500;999997097|420;500;999986063|449;500;999997978|473;500;999998529|562;500;999995456');
INSERT INTO `mountpark_data` VALUES ('9357', '294', '5', '5', '0', '-1', '1210000', '1210000', '', '', '324', '309', '', '254;268;282;296;310;324;338;352;366;380;394;409;395;381;367;353;339;325;311;297;283;269;284;298;312;326;340;354;368;382;396;410;424;439;425;411;397;383;369;355;341;327;313;299;314;328;342;356;370;384;398;412;426;440;454;455;441;427;413;385;371;357;343;329;344;358;372;386;400;414;428;442;456;443;429;387;359', '');
INSERT INTO `mountpark_data` VALUES ('9354', '264', '12', '12', '0', '-1', '2695000', '2695000', '', '', '236', '250', '', '251;266;252;238;224;209;194;179;164;149;163;177;191;206;236;223;237;222;208;193;207;192;178;165;151;137;123;138;152;166;180;195;181;167;153;168;183;197;198;212;226;240;254;268;296;310;324;339;325;311;297;283;255;227;213;228;242;256;270;284;312;326;354;221;281', '');
INSERT INTO `mountpark_data` VALUES ('9353', '324', '8', '8', '0', '-1', '1952500', '1952500', '', '', '296', '310', '', '296;311;326;341;356;371;281;266;251;236;221;207;222;237;252;267;282;297;312;327;342;357;343;328;313;298;283;268;253;223;208;193;179;194;254;269;284;299;314;329;315;300;285;270;255;240;226;212;183;182;181;195;180;165;151;166;241;256;271;286;301;287;272;257;242;227;167;152;137;123;138;153;168;198;213;199;184;169;154;139;124;109;95;110;125;140;155;170', '');
INSERT INTO `mountpark_data` VALUES ('9355', '308', '5', '5', '0', '-1', '1210000', '1210000', '', '', '280', '294', '', '280;265;250;235;295;310;325;340;355;341;326;311;296;281;266;251;236;221;207;222;237;252;267;282;297;312;327;313;298;283;268;253;238;223;208;193;179;194;209;224;239;254;269;284;299;210;195;180;165;151;166;181;196;182;167;152;137;123;138;153', '');
INSERT INTO `mountpark_data` VALUES ('9358', '252', '5', '5', '0', '-1', '1210000', '1210000', '', '', '282', '267', '', '282;268;254;240;296;310;324;339;325;311;297;283;269;255;270;284;298;312;326;340;354;355;341;327;313;299;285;300;314;328;342;356;370;399;385;371;357;343;329;315;330;344;358;372;386;400;414;429;415;401;387;373;359;345;360;374;388;402;416;430;444;459;445;431;417;403;375;404;418;432;446;460', '');
INSERT INTO `mountpark_data` VALUES ('9352', '112', '8', '8', '0', '-1', '1952500', '1952500', '', '', '140', '126', '', '140;125;155;170;184;169;154;139;153;168;183;198;212;197;182;167;181;196;211;226;240;225;210;195;209;224;239;254;268;253;238;223;208;193;178;163;177;192;207;222;237;252;267;282;311;326;341;356;296;281;266;251;236;221;206;191;250;265;280;295;310;325;340;355;370;384;369;354;339;324;309;294;279;264;293;308;323;338;353;368;383', '');
INSERT INTO `mountpark_data` VALUES ('9729', '355', '5', '5', '0', '-1', '1210000', '1210000', '', '', '327', '341', '', '327;342;357;312;297;282;267;252;237;222;207;221;236;251;266;281;296;310;295;280;265;250;235;249;264;279;294;309;324;208;193;223;238;253;268;283;298;313;328;343;329;314;299;284;269;254;239;224;209;194;179;165;180;195;210;225;240;255;270;285;300;315;301;286;271;256;241;226;211;196;181;166;151;137;152;167;182;197;183;168;153;138;123;109;124;139;154;169;155;140;125', '');
INSERT INTO `mountpark_data` VALUES ('9730', '282', '5', '5', '0', '-1', '1210000', '1210000', '', '', '252', '267', '', '252;266;280;294;308;238;224;210;196;182;168;153;167;181;195;209;223;237;251;265;279;293;307;292;278;264;250;236;222;208;194;180;166;152;138;123;137;151;165;179;193;207;221;235;249;263;277;262;248;234;220;206;192;178;164;150;136;122;108;93;107;121;135;149;163;177;191;205;219;233;247;218;204;190;176;162;148;133;147;161;175;189;160;146;132;118;103;117;131;102', '');
INSERT INTO `mountpark_data` VALUES ('9731', '279', '12', '12', '0', '-1', '2695000', '2695000', '', '', '251', '265', '', '251;266;281;282;311;236;221;206;191;176;147;146;131;132;162;177;192;207;222;237;252;267;297;283;268;253;238;223;208;193;178;163;148;133;118;103;74;89;104;119;134;149;164;179;194;209;224;239;254;269;255;240;225;210;195;180;165;150;135;120;105;90;75;60;45;59;31;46;61;76;91;106;121;136;151;166;181;196;211;226;241;167;152;137;122;108;123;138;153;139;124;109', '');
INSERT INTO `mountpark_data` VALUES ('9726', '343', '8', '8', '0', '-1', '1952500', '1952500', '', '', '313', '328', '', '313;299;285;271;327;341;355;369;354;340;326;312;298;284;270;256;242;227;241;255;269;283;297;311;325;339;324;310;296;282;268;254;240;226;212;197;211;225;239;253;267;281;295;309;294;280;266;252;238;224;210;196;182;167;181;195;209;223;237;251;265;279;264;250;236;222;208;194;180;166', '');
INSERT INTO `mountpark_data` VALUES ('10249', '164', '5', '5', '0', '-1', '1210000', '1210000', '', '', '194', '179', '', '194;180;166;208;222;236;251;237;223;209;195;181;196;210;224;238;252;266;281;267;253;239;225;211;212;198;184;170;226;240;254;268;282;296;310;324;338;352;367;353;339;325;311;297;283;269;255;241;227;213;199;185;200;214;228;242;256;270;284;298;312;326;340;354;368;382;397;383;369;355;341;327;313;299;314;328;342;356;370;384;398;412;427;413;399;385;371;357;343;329;344;358;372;386;400', '');
INSERT INTO `mountpark_data` VALUES ('9349', '354', '12', '12', '0', '-1', '2695000', '2695000', '', '', '326', '340', '', '326;341;327;313;299;285;271;257;242;256;270;284;298;312;311;297;283;269;255;241;227;213;198;212;226;240;254;268;282;296;310;324;338;323;309;295;281;267;253;239;225;211;197;183;168;182;196;210;224;238;252;266;280;294;308;293;279;265;251;237;223;209;195;181;167;153;138;152;278;264;250;236;222;208;194;180', '');
INSERT INTO `mountpark_data` VALUES ('9350', '125', '5', '5', '0', '-1', '1210000', '1210000', '', '', '153', '139', '', '153;138;168;183;198;213;228;242;227;212;197;182;167;152;166;181;196;211;226;241;256;271;286;300;285;270;255;240;225;210;195;180;194;209;224;239;254;269;284;299;314;328;313;298;283;268;253;238;223;208;222;237;252;267;282;297;312;327;342;356;341;326;311;296;281;266;251;236;250;265;280;295;310;325;340;355', '');
INSERT INTO `mountpark_data` VALUES ('9346', '226', '8', '8', '0', '-1', '1952500', '1952500', '', '', '254', '240', '', '254;239;224;209;194;269;284;299;314;328;313;298;283;268;253;238;223;208;222;237;252;267;282;297;312;327;342;356;341;326;311;296;281;266;251;236;250;265;280;295;310;325;340;355;370;384;369;354;339;324;309;294;279;264;278;293;308;323;338;353;368;383', '');
INSERT INTO `mountpark_data` VALUES ('9345', '226', '7', '7', '0', '-1', '1173500', '1173500', '', '', '254', '240', '', '254;239;224;209;194;269;284;299;314;328;313;298;283;268;253;238;223;208;222;237;252;267;282;297;312;327;342;356;341;326;311;296;281;266;251;236;250;265;280;295;310;325;340;355;370;384;369;354;339;324;338;353;368;383;398;412;397;382;367;352;366;381;396;411;426;440;425;410;395', '');
INSERT INTO `mountpark_data` VALUES ('9725', '199', '12', '12', '0', '-1', '2695000', '2695000', '', '', '227', '213', '', '227;212;197;182;242;257;272;287;301;286;271;256;241;226;211;196;195;210;225;240;255;270;285;300;315;329;314;299;284;269;254;239;224;209;223;238;253;268;283;298;313;328;343;357;342;327;312;297;282;267;252;237;251;266;281;296;311;326;341;356;371;385;370;355;340;325;310;295;280;265;279;294;309;324;339;354;369;384;399;413;398;383;368;353;338;323;308', '');
INSERT INTO `mountpark_data` VALUES ('9342', '327', '4', '4', '0', '-1', '962500', '962500', '', '', '297', '312', '', '297;311;325;283;269;254;268;282;296;310;295;281;267;253;239;224;238;252;266;280;265;251;237;223;209;194;208;222;236;250;235;221;207;193', '');
INSERT INTO `mountpark_data` VALUES ('10561', '380', '5', '5', '0', '-1', '1210000', '1210000', '', '', '352', '0', '', '352;337;322;367;382;368;353;338;323;308;294;309;324;339;354;340;325;310;295;280;251;236;221;206;191;266;281;296;311;326;341;356;371;386;372;357;342;327;312;297;282;267;252;237;222;207;192;177;193;208;223;238;253;268;283;298;313;328;343;358;344;329;314;299;284;269;254;239;224;209;194;179;165;180;195;210;225;240;255;270;285;300;315;330;256;241;226;211;196;181;166;151;122;107;137;152;167;182;197;212;227;242;228;213;198;183;168;154;169;184;199;214;200;185;170;155', '');
INSERT INTO `mountpark_data` VALUES ('3673', '149', '5', '5', '0', '-1', '1210000', '1210000', '', '', '179', '164', '', '249;235;221;207;193;179;165;151;137;123;109;124;166;180;194;208;222;236;250;264;279;265;251;237;223;209;195;181;167;139;168;182;196;210;224;238;252;280;294;309;295;310;324;339;325;267;253;239;225;211;197;183;169;184;198;212;226;240;254;282;297;283;269;255;241;227;213;199;270;284;298;312;327;313;299;285;300;314;328;342;357;343;329;315', '');
INSERT INTO `mountpark_data` VALUES ('3113', '235', '5', '5', '0', '-1', '1210000', '1210000', '', '', '207', '221', '', '177;192;207;222;237;252;267;282;297;283;298;284;270;256;242;268;253;238;223;208;193;178;163;149;164;179;194;209;224;239;254;269;255;240;225;210;195;180;165;150;135;121;136;151;166;181;196;211;182;167;152;137;122;107', '');
INSERT INTO `mountpark_data` VALUES ('3328', '367', '10', '10', '0', '-1', '2282500', '2282500', '', '', '353', '367', '', '383;368;353;338;323;308;293;279;294;309;324;339;354;369;355;340;310;295;280;265;251;266;281;296;311;326;341;327;312;297;267;252;237;223;238;253;268;283;298;299;284;269;224;209;254;195;210;225;240;255;270;285;271;256;241;226;211;181;151;137;152;167;182;212;227;242;257;243;228;213;198;183;168;153;138;123;139;154;169;184;199;214;229', '');
INSERT INTO `mountpark_data` VALUES ('3367', '368', '5', '5', '0', '-1', '1210000', '1210000', '', '', '340', '354', '', '220;206;192;178;164;150;136;122;137;151;165;179;193;207;221;235;250;236;222;194;208;180;166;152;167;153;139;125;181;195;209;265;251;237;223;140;154;168;182;196;210;224;238;252;280;295;281;267;253;239;225;211;197;183;169;155;170;184;198;212;226;240;254;268;282;296;310;325;311;297;283;269;255;241;227;213;199;200;214;228;242;256;270;284;312;326;340;355;341;327;313;299;285;271;257;272;286;300;314;328;342;356;370;385;357;371;315;287', '');
INSERT INTO `mountpark_data` VALUES ('10559', '380', '5', '5', '0', '-1', '1210000', '1210000', '', '', '352', '366', '', '322;337;352;367;353;338;323;308;294;309;324;339;280;295;310;325;340;355;370;385;400;386;371;356;341;326;311;296;281;266;252;267;282;297;312;327;342;357;372;358;343;328;313;298;283;268;253;238;223;208;193;178;163;149;164;179;194;209;224;239;254;269;284;299;314;329;344;315;330;300;285;270;255;240;225;210;195;180;165;150;135;121;136;151;166;181;196;211;226;241;256;271;286;301;316;182;197;212;227;213;199;198;184;169;183;168', '');
INSERT INTO `mountpark_data` VALUES ('10557', '287', '5', '5', '0', '-1', '1210000', '1210000', '', '', '257', '272', '', '369;354;339;325;340;355;341;326;311;297;312;327;313;298;283;269;284;299;285;270;255;240;225;210;195;180;165;179;194;208;193;207;222;236;221;235;250;264;249;271;256;241;226;211;196;181;166;151;137;152;167;182;197;212;227;242;257;243;228;213;198;183;168;153;138;123;109;124;139;154;169;184;199;214;229', '');
INSERT INTO `mountpark_data` VALUES ('10554', '374', '5', '5', '0', '-1', '1210000', '1210000', '', '', '344', '359', '', '344;358;330;315;329;343;328;314;300;271;257;243;229;285;299;313;327;341;355;369;383;397;382;368;354;340;326;312;298;284;270;256;242;228;214;199;213;227;241;255;269;283;297;311;325;339;353;367;352;338;324;310;296;282;268;254;240;226;212;198;184;169;183;197;211;225;239;253;267;281;295;309;323;337;252;238;224;210;196;182;168;154;139;153;167;181;195;209;223;237;222;208;194;180;166;152;138;124;109;123;137;151;165;179;193;207;192;178;164;150;135;149;163;177;162;148;134', '');
INSERT INTO `mountpark_data` VALUES ('10602', '365', '5', '5', '0', '-1', '1210000', '1210000', '', '', '337', '0', '', '337;352;322;308;323;338;324;309;294;280;295;310;311;326;341;356;296;281;266;251;236;221;206;191;177;192;207;222;237;252;267;282;297;312;327;342;328;313;298;283;268;253;238;223;208;193;178;163;149;164;179;194;209;224;239;254;269;284;299;314;300;285;270;255;240;225;210;195;180;165;150;135;121;136;151;166;181;196;211;226;241;256;271;286;272;257;242;227;212;197;182;167;152;137;122;107;93;108;123;138;153;168;183;198;213;228;243;258;184;169;154;140;155;170;156;141;126;112;127', '');
INSERT INTO `mountpark_data` VALUES ('10601', '366', '5', '5', '0', '-1', '1210000', '1210000', '', '', '338', '352', '', '308;323;338;353;368;354;339;324;309;294;280;295;310;325;340;221;236;251;266;281;296;311;326;341;356;371;386;401;372;357;342;327;312;297;282;267;252;237;222;207;178;193;208;223;238;253;268;283;298;313;328;343;358;344;329;314;299;269;254;239;224;209;194;179;164;150;165;180;195;210;225;240;255;270;285;300;315;330;286;271;256;241;226;211;196;181;166;151;136;122;137;152;167;182;197;212;227;242;257;272;258;243;228;213;198;183;168;153;138;123;108;154;169;184;199;185;170;155;140', '');
INSERT INTO `mountpark_data` VALUES ('10600', '373', '5', '5', '0', '-1', '1210000', '1210000', '', '', '343', '358', '', '343;357;371;329;315;301;287;272;286;300;314;328;342;356;341;327;313;299;285;271;257;228;214;200;242;256;270;284;298;312;326;340;354;368;382;367;353;339;325;311;297;283;269;255;241;227;213;199;185;170;184;198;212;226;240;254;268;282;296;310;324;338;352;366;351;337;323;309;295;281;267;253;239;225;211;197;183;169;155;140;154;168;182;196;210;224;238;252;266;280;294;308;322;336;251;237;223;209;195;181;167;153;139;125;138;152;166;180;194;208;222;236;221;207;193;179;165;151;137;122;136;150;164;178;192;206;191;177;163;149;135;121', '');
INSERT INTO `mountpark_data` VALUES ('10599', '293', '5', '5', '0', '-1', '1210000', '1210000', '', '', '265', '279', '', '265;250;235;280;295;310;325;340;355;356;370;385;400;386;371;341;326;311;296;281;266;251;236;221;192;177;162;207;222;237;252;267;282;297;312;327;342;357;372;358;343;328;313;298;283;268;253;238;223;208;193;178;163;148;134;149;164;179;194;209;224;239;254;269;284;299;314;329;344;330;315;300;285;270;255;240;225;210;195;180;165;150;135;120;106;121;136;151;166;181;196;211;226;241;256;271;286;301;316;197;183;182;167;152;137;123;138;153;168;169;154;139;124;109;95;110;125;140;155;141;126;111;96', '');
INSERT INTO `mountpark_data` VALUES ('10606', '308', '5', '5', '0', '-1', '1210000', '1210000', '', '', '280', '0', '', '280;265;250;235;295;310;325;340;355;341;326;311;296;281;266;251;236;221;192;207;222;237;252;267;282;297;312;327;313;298;283;268;253;238;223;208;193;178;164;179;194;209;224;239;254;269;284;299;285;270;255;240;225;210;195;180;165;150;136;151;166;181;196;211;226;241;256;271;257;242;227;212;197;182;167;152;137;122;108;123;138;153;168;183;198;213;228;243;229;214;199;184;169;154;139;124;109;110;125;140;155;170;185', '');
INSERT INTO `mountpark_data` VALUES ('10607', '325', '5', '5', '0', '-1', '1210000', '1210000', '', '', '297', '0', '', '297;282;267;252;312;327;328;313;298;283;268;253;238;224;209;239;254;269;284;299;314;300;285;270;255;240;225;210;195;181;196;211;226;241;227;212;197;182;167;153;168;183;198;213;199;184;169;154;139;125;140;155;170;185;171;156;141;126', '');
INSERT INTO `mountpark_data` VALUES ('10609', '331', '5', '5', '0', '-1', '1210000', '1210000', '', '', '301', '316', '', '287;301;315;329;343;357;342;328;314;300;286;272;257;271;285;299;313;327;312;340;354;368;382;298;284;270;256;242;228;214;200;185;199;213;227;241;255;269;283;297;311;325;339;353;367;352;338;324;310;296;282;268;254;240;226;212;198;184;155;169;183;197;211;225;239;253;267;281;295;309;323;337;322;308;294;280;266;252;238;224;210;196;182;168;154;140;125;139;153;167;181;195;209;223;237;251;265;279;293;307;208;194;180;166;152;138;124;110;95;109;123;137;151;165;179;193;178;164;150;136;122;108;94;80;107;121;135;149;163;148;134;120;106;92;77;91;105;119;133', '');
INSERT INTO `mountpark_data` VALUES ('10611', '381', '5', '5', '0', '-1', '1210000', '1210000', '', '', '353', '367', '', '383;368;353;338;323;308;293;279;294;309;324;339;354;369;355;340;325;310;295;280;265;251;266;281;296;311;326;341;327;342;357;372;387;373;358;343;328;313;299;314;329;344;359;285;270;255;240;225;210;195;181;196;211;226;241;256;271;257;242;227;212;197;182;167;153;168;183;198;213;228;243;229;214;199;184;169;154;139;284;298;312;297;283;269;254;268;282;267;253;239;224;238;252;237;223;209;194;208;222;207;193;179;164;178;192;177;163;149;134;148;162', '');
INSERT INTO `mountpark_data` VALUES ('10622', '323', '5', '5', '0', '-1', '1210000', '1210000', '', '', '295', '309', '', '295;280;265;250;310;325;340;355;370;385;371;356;341;326;311;296;281;266;251;236;222;237;252;267;282;297;312;327;342;357;372;358;343;328;313;298;283;268;253;238;223;208;194;209;224;239;254;269;284;299;314;329;344;330;315;300;285;270;255;240;225;210;195;180;165;150;135;120;106;121;136;151;166;181;196;211;226;241;256;271;286;301;316;302;287;272;257;242;227;212;197;182;167;152;137;122;107;92;93;108;123;138;153;168;183;198;213;228;243;258;273;288;274;259;244;229;214;199;184;169;154;139;124;110;125;140;155;170;185;200;215;230;245;260;96;111;126;141;156;171;186;201;216', '');
INSERT INTO `mountpark_data` VALUES ('10630', '412', '5', '5', '0', '-1', '1210000', '1210000', '', '', '384', '0', '', '384;399;385;371;357;343;329;315;301;287;272;286;300;314;328;342;356;370;369;355;341;327;313;299;285;271;257;242;256;270;284;269;255;241;227;198;184;170;156;142;212;226;240;254;239;225;211;197;183;169;155;141;127;112;126;140;154;168;182;196;210;224;238;252;251;265;279;237;223;209;195;181;167;153;139;125;111;97;152;166;180;194;208;222;236;250;264;249;235;221;207;193;179;165;151', '');
INSERT INTO `mountpark_data` VALUES ('10618', '380', '5', '5', '0', '-1', '1210000', '1210000', '', '', '352', '0', '', '352;367;337;323;338;353;339;324;309;280;265;250;235;220;295;310;325;340;355;370;356;341;326;311;296;281;266;251;236;221;206;192;207;222;237;252;267;282;297;312;327;342;328;313;298;283;268;253;238;223;208;193;178;164;179;194;209;224;239;254;269;284;299;314;300;285;270;255;240;225;210;195;180;165;150;136;151;166;181;196;211;226;241;256;271;286;272;257;242;227;212;197;182;167;152;137;122;108;123;138;153;183;198;213;228;243;258;94', '');
INSERT INTO `mountpark_data` VALUES ('8750', '468', '10', '10', '0', '-1', '2282500', '2282500', '', '', '432', '450', '64;7596;1|174;7596;1|196;7596;1|286;7596;1|320;7596;1|363;7596;1|472;7596;1', '', '64;500;999996354|174;500;999997948|196;500;999993189|286;500;999995323|320;500;999996021|363;500;999991061|472;500;999992182');
INSERT INTO `mountpark_data` VALUES ('8851', '578', '10', '10', '0', '-1', '2282500', '2282500', '', '', '542', '578', '101;7616;1|190;7616;1|230;7616;1|309;7616;1|380;7616;1|394;7616;1|507;7616;1', '', '101;500;999999822|190;500;999999850|230;500;999999591|309;500;999998805|380;500;999994832|394;500;999999450|507;500;999997248');
INSERT INTO `mountpark_data` VALUES ('8749', '614', '10', '10', '0', '-1', '2282500', '2282500', '', '', '578', '614', '159;7695;1|290;7695;1|305;7695;1|376;7695;1|474;7695;1|490;7695;1|505;7695;1', '', '159;500;999998138|290;500;999969962|305;500;999997140|376;500;999997432|474;500;999994773|490;500;999996516|505;500;999991956');
INSERT INTO `mountpark_data` VALUES ('8748', '550', '10', '10', '0', '-1', '2282500', '2282500', '', '', '512', '550', '99;7761;1|213;7761;1|265;7761;1|325;7761;1|393;7761;1|403;7761;1|433;7761;1|583;7761;1', '', '99;500;999998953|213;500;999999289|265;500;999986863|325;500;999998188|393;500;999995995|403;500;999998191|433;500;999997292|583;500;999995127');
INSERT INTO `mountpark_data` VALUES ('8751', '356', '10', '10', '0', '-1', '2282500', '2282500', '', '', '412', '374', '158;7741;1|235;7741;1|286;7741;1|363;7741;1|366;7741;1|475;7741;1|490;7741;1|523;7741;1', '', '158;500;999998440|235;500;999997249|286;500;999994995|363;500;999998911|366;500;999999131|475;500;999996923|490;500;999996150|523;500;999995614');
INSERT INTO `mountpark_data` VALUES ('9450', '128', '3', '3', '0', '-1', '770000', '770000', '', '-5648', '216', '172', '', '156;171;141;126;140;154;168;182;210;225;240;255;241;227;213;199;185;155;169;183;197;211;226;212;198;184;170', '');
INSERT INTO `mountpark_data` VALUES ('9449', '268', '2', '2', '0', '-1', '522500', '522500', '', '', '240', '253', '', '225;211;197;183;169;155;170;184;198;212;226;240;255;241;227;213;199;185', '');
INSERT INTO `mountpark_data` VALUES ('9451', '442', '2', '2', '0', '-1', '522500', '522500', '', '-5549;-5546', '412', '427', '', '380;366;352;353;367;381;395;410;396;382;368;425;411;397;383;398;412;426;440', '');
INSERT INTO `mountpark_data` VALUES ('9455', '416', '3', '3', '0', '-1', '770000', '770000', '', '', '388', '402', '', '388;403;373;358;343;329;344;359;374;389;375;360;345;330;346;361', '');
INSERT INTO `mountpark_data` VALUES ('9453', '381', '3', '3', '0', '-1', '770000', '770000', '', '', '409', '395', '', '394;408;422;436;450;451;437;423;409;424;438;452;439;454;453', '');
INSERT INTO `mountpark_data` VALUES ('9456', '181', '4', '4', '0', '-1', '962500', '962500', '', '', '211', '196', '', '169;183;197;211;225;239;253;267;282;268;254;240;226;212;198;184;199;213;227;241;255;269;283;297;312;298;284;270;256;242;214;228;229;243;257;271;285;299;313;327;342;328;314;300;272;286;258;244;259;273;287;301;315;329;343;357;372;358;344;330;316;302;288;274', '');
INSERT INTO `mountpark_data` VALUES ('9457', '284', '7', '7', '0', '-1', '1173500', '1173500', '', '', '314', '299', '', '398;384;370;342;328;314;300;286;272;258;244;259;273;287;301;315;329;343;357;371;385;399;413;428;414;400;386;372;358;344;330;316;302;288;274;289;303;317;331;345;359;373;387;401;415;429;443;458;444;430;416;402;388;374;360;346;332', '');
INSERT INTO `mountpark_data` VALUES ('9458', '401', '1', '1', '0', '-1', '412500', '412500', '', '', '373', '387', '', '373;388;343;358;374;360;345;359;344;330;301;315;329', '373;3000;2764|374;3000;2871');
INSERT INTO `mountpark_data` VALUES ('9459', '454', '2', '2', '0', '-1', '522500', '522500', '', '', '424', '439', '', '365;380;395;410;424;438;452;437;422;407;393;379;394;409;423;408', '');
INSERT INTO `mountpark_data` VALUES ('9462', '426', '2', '2', '0', '-1', '522500', '522500', '', '', '398', '412', '', '398;383;368;353;338;324;339;354;369;384;310;325;340;355;370;385;400;386;371', '');
INSERT INTO `mountpark_data` VALUES ('9461', '151', '10', '10', '0', '-1', '2282500', '2282500', '', '', '181', '166', '', '97;112;127;141;126;111;125;140;155;170;185;199;184;169;154;139;153;168;183;198;213;227;242;257;272;212;197;182;167;181;196;211;226;241;256;271;286;300;285;270;255;240;225;210;195;209;224;239;254;269;284;299;314;328;313;298;283;268;253;238;223;237;252;267;282;297;312;327;342;356;341;326;311;296;281;266;251;280;295;310;325;340;355;370', '');
INSERT INTO `mountpark_data` VALUES ('9460', '308', '2', '2', '0', '-1', '522500', '522500', '', '', '338', '323', '', '310;325;340;354;339;324;338;353;368;382;367;352;366;381;396', '');
INSERT INTO `mountpark_data` VALUES ('9463', '268', '4', '4', '0', '-1', '962500', '962500', '', '', '296', '282', '', '296;281;311;326;341;356;371;386;401;415;400;385;370;355;340;325;310;295;309;324;339;354;369;384;399;414;429;443;428;413;398;383;368;353;338', '');
INSERT INTO `mountpark_data` VALUES ('9464', '381', '7', '7', '0', '-1', '1173500', '1173500', '', '-7172;-7199;-7313;-7343', '353', '367', '254;7619;258|269;7619;258|344;7590;258|359;7590;258|373;7590;258|413;7763;258', '338;324;310;296;282;268;254;269;283;297;311;325;339;353;368;354;340;326;312;298;284;299;313;327;341;355;369;383;398;384;370;356;342;328;314;329;343;357;371;385;399;413;428;414;400;386;372;358;344;359;373;387;401;415;429;443', '254;3000;2921|269;3000;2900|344;3000;2885|359;3000;2943|373;3000;2887|413;3000;2941');
INSERT INTO `mountpark_data` VALUES ('9465', '315', '1', '1', '0', '-1', '412500', '412500', '', '', '287', '301', '', '287;302;272;257;242;228;243;258;273;288;274;259;244;229', '');
INSERT INTO `mountpark_data` VALUES ('9466', '307', '4', '4', '0', '-1', '962500', '962500', '', '', '279', '293', '', '279;264;249;235;250;265;280;294;266;251;236;221;267;281;295;309;323;337;351;365;380;366;352;338;324;310;296;282;297;311;325;339;353;367;381', '');
INSERT INTO `mountpark_data` VALUES ('8757', '604', '5', '5', '0', '-1', '1210000', '1210000', '', '', '640', '622', '', '691;673;655;637;619;601;583;602;620;638;656;674;692;710;729;748;767;711;693;675;657;639;621;640;658;676;694;712;730;749;731;713;695;677;659;678;696;714;732;750;768;769;751;733;715;697', '621;3000;1923|639;3000;1988|659;3000;1390|676;3000;1752|677;3000;1690');
INSERT INTO `mountpark_data` VALUES ('8758', '181', '5', '5', '0', '-1', '1210000', '1210000', '', '', '143', '162', '', '103;85;49;67;31;32;51;69;87;68;50;86;122;104;141;160;179;161;142;124;105;107;125;106', '');
INSERT INTO `mountpark_data` VALUES ('8759', '655', '5', '5', '0', '-1', '1210000', '1210000', '', '', '617', '636', '', '595;614;633;652;671;653;634;615;596;577;559;578;597;616;635;617;598;579;560;541;523;542;561;580;581;562;543;524;505;487;506;525;544;563;599', '');
INSERT INTO `mountpark_data` VALUES ('9268', '598', '12', '12', '0', '-1', '2695000', '2695000', '', '', '562', '580', '', '562;581;543;524;505;486;467;448;430;449;468;487;506;525;544;563;545;526;507;488;469;450;431;412;394;413;432;451;470;489;508;527;509;490;471;434;433;414;395;376;358;377;396;415;453;472;491;473;454;435;416;397;378;359;340;322;341;360;379;398;417;436;455;437;418;399;380;361;342;323', '507;3000;2930|508;3000;2948|525;3000;2997|543;3000;1969|544;3000;1967|545;3000;2945|563;3000;3000|581;3000;1965');
INSERT INTO `mountpark_data` VALUES ('9270', '252', '12', '12', '0', '-1', '2695000', '2695000', '', '', '216', '234', '', '32;140;122;104;86;68;50;69;87;105;123;141;159;178;160;142;124;106;88;107;143;161;179;197;216;198;180;162;144;126;145;163;181;199;235;254;236;218;200;182;164;183;201;219;237;255;273;217', '179;3000;2739|180;3000;2727|197;3000;2997|216;3000;2748|217;3000;2705|235;3000;2986');
INSERT INTO `mountpark_data` VALUES ('9273', '271', '10', '10', '0', '-1', '2282500', '2282500', '', '', '235', '253', '', '159;178;197;216;235;254;273;292;311;293;275;256;236;217;198;179;160;141;123;142;161;180;199;218;237;257;238;219;200;181;162;143;124;125;144;163;182;201;220;239;221;202;183;164;145;126;107;106;105;184;165;146;127;108;90;109;128;147', '');
INSERT INTO `mountpark_data` VALUES ('9274', '655', '5', '5', '0', '-1', '1210000', '1210000', '', '', '617', '636', '', '487;506;525;544;563;581;562;543;524;505;523;542;561;580;599;617;598;579;560;541;559;578;597;616;635;653;634;615;596;577;595;614;633;652;671', '');
INSERT INTO `mountpark_data` VALUES ('9278', '753', '5', '5', '0', '-1', '1210000', '1210000', '', '', '717', '735', '', '717;698;679;660;736;755;774;756;737;718;699;680;661;642;624;643;662;681;700;719;738;720;701;682;663;644;625;606;588;607;626;645;664;683;702;665;646;627;608;589', '');
INSERT INTO `mountpark_data` VALUES ('9277', '361', '5', '5', '0', '-1', '1210000', '1210000', '', '', '325', '343', '', '325;344;363;306;287;268;249;230;211;193;212;231;250;269;288;307;326;345;327;308;289;270;251;232;213;194;175;157;176;195;214;233;252;271;290;309;291;272;253;234;215;196;177;158;139;140;159;178;197;216;235;254;273;255;236;217;198;179;160;141;142;123;161;180;199;218', '');
INSERT INTO `mountpark_data` VALUES ('4250', '635', '3', '3', '0', '-1', '770000', '770000', '', '', '673', '654', '', '637;656;675;694;713;732;750;731;712;693;674;655;673;692;711;730;749;768;767;748;729;710;691;709;728;747;746;727;745;764', '');
INSERT INTO `mountpark_data` VALUES ('4246', '180', '5', '5', '0', '-1', '1210000', '1210000', '', '', '164', '172', '', '201;182;144;106;88;70;52;34;53;54;72', '');
INSERT INTO `mountpark_data` VALUES ('4245', '587', '5', '5', '0', '-1', '1210000', '1210000', '', '', '625', '606', '', '679;661;643;625;607;608;626;644;662;680;698;717;699;681;663;645;627;646;664;682;700;718;736;755;737;719;701;683;665;702;720;756;774', '');
INSERT INTO `mountpark_data` VALUES ('4242', '727', '5', '5', '0', '-1', '1210000', '1210000', '', '', '689', '708', '', '276;558;576;596;595;594;612;614;634;633;632;631;667;669;652;653;671;689;707;725;743;724;706;705', '');
INSERT INTO `mountpark_data` VALUES ('4207', '636', '5', '5', '0', '-1', '1210000', '1210000', '', '', '672', '654', '', '615;634;633;651;653;672;671;670;669;688;706;724;743;762;725;689;690;691;710;728;709;708;744;726;745;764', '');
INSERT INTO `mountpark_data` VALUES ('4206', '579', '5', '5', '0', '-1', '1210000', '1210000', '', '', '615', '597', '', '558;577;576;594;595;596;615;614;613;630;631;632;633;634;653;652;651;650;649;648;667;668;669;670;671;691;690;689;688;687;705;686;724;706;708;709;727;726;763', '');
INSERT INTO `mountpark_data` VALUES ('4079', '418', '5', '5', '0', '-1', '1210000', '1210000', '', '', '382', '400', '', '382;401;420;363;344;325;307;326;345;364;383;402;384;365;346;327;308;289;271;290;309;328;347', '');
INSERT INTO `mountpark_data` VALUES ('4211', '253', '5', '5', '0', '-1', '1210000', '1210000', '', '', '215', '234', '', '215;233;251;269;287;197;179;161;142;160;178;196;214;232;250;268;249;231;213;195;177;159;141;123;122;140;158;176;194;212;230;175;157;139;121;103;85;66;84;102;120;101;83;65;46', '');
INSERT INTO `mountpark_data` VALUES ('4210', '603', '5', '5', '0', '-1', '1210000', '1210000', '', '', '639', '621', '', '601;620;619;637;638;639;658;657;656;655;673;674;675;676;677;695;694;693;692;691;710;711;712;713;731;730;729;748;767;749', '');
INSERT INTO `mountpark_data` VALUES ('4209', '651', '5', '5', '0', '-1', '1210000', '1210000', '', '', '687', '669', '', '649;667;685;722;704;686;668;687;705;723;741;759;760;742;724;706;725;743;761', '');
INSERT INTO `mountpark_data` VALUES ('4252', '542', '5', '5', '0', '-1', '1210000', '1210000', '', '', '578', '560', '', '521;540;559;578;597;616;635;539;558;577;596;615;634;653;671;652;633;595;576;557;575;594;613;632;651;670;689;707;688;669;650;631;612;593;630;649;668;687;725', '');
INSERT INTO `mountpark_data` VALUES ('4225', '485', '5', '5', '0', '-1', '1210000', '1210000', '', '', '521', '503', '', '483;502;521;540;559;577;558;539;520;501;482;500;519;538;557;576;595;613;594;575;556;537;574;593;612;631;649;630;611;648;667;685', '');
INSERT INTO `mountpark_data` VALUES ('4309', '541', '5', '5', '0', '-1', '1210000', '1210000', '', '', '505', '0', '', '505;524;543;562;581;486;467;448;429;410;391;373;392;411;430;449;468;487;506;525;544;563;545;526;507;488;469;450;431;412;393;374;355;337;356;375;394;413;432;451;470;489;508;527;509;490;471;452;433;414;395;376;357;338;319;301;320;339;358;377;396;415;434;453;472;491;473;454;435;416;397;378;359;340;321;302;283;265;284;303;322;341;360;379;398;417;436;455;437;418;399;380;361;342;323;304;285;266;247;229;248;267;286;305;324;343;362;381;400;419;401;382;363;344;325;306;287;268;249;230;211;193;212;231;250;269;288;307;326;345;364;383;365;346;327;308;289;270;251;232;213;194;175;157;176;195;214;233;252;271;290;309;328', '');
INSERT INTO `mountpark_data` VALUES ('4258', '400', '5', '5', '0', '-1', '1210000', '1210000', '', '', '364', '382', '', '364;383;402;345;326;307;289;308;327;346;365;384;366;347;328;309;290;271;253;272;291;310;329;348;235', '');
INSERT INTO `mountpark_data` VALUES ('4260', '576', '5', '5', '0', '-1', '1210000', '1210000', '', '', '614', '595', '', '614;633;652;671;690;708;689;670;651;632;650;669;688;707;726;744;725;706;687;668;686;705;762;743;704', '');
INSERT INTO `mountpark_data` VALUES ('4248', '393', '5', '5', '0', '-1', '1210000', '1210000', '', '', '429', '411', '', '429;410;391;372;353;448;467;486;504;485;466;447;428;409;390;371;389;408;427;446;465;484;503;522;540;521;502;483;464;445;426;463;482;501;520;539;558', '');
INSERT INTO `mountpark_data` VALUES ('4342', '458', '5', '5', '0', '-1', '1210000', '1210000', '', '', '494', '476', '', '494;513;532;551;570;588;569;550;531;512;493;511;492;473;454;530;549;568;587;606;624;605;586;567;548;529;510;491;472;490;509;528;547;566;585;604;623', '');
INSERT INTO `mountpark_data` VALUES ('4241', '595', '5', '5', '0', '-1', '1210000', '1210000', '', '', '631', '613', '', '631;650;669;688;707;726;744;725;706;687;668;649;667;686;705;724;743;762;761;742;723;704;685;722;741;760', '');
INSERT INTO `mountpark_data` VALUES ('4240', '326', '5', '5', '0', '-1', '1210000', '1210000', '', '', '288', '307', '', '288;324;306;270;252;234;216;197;179;215;233;251;269;287;305;286;268;250;232;214;196;178;160;141;159;177;195;213;231;249;267;248;230;212;194;176;158;140;122;103;121;139;157;175;193;211;229;210;192;174;156;138;120;84;65;101;119;137;155;173;191;172;154;136;81;100;82;64;46;27;45;63;99;135;153;134;79;98;80;62;44;26;25;43;61;97;115;96;78;60;42;24;23;59', '');
INSERT INTO `mountpark_data` VALUES ('4238', '255', '5', '5', '0', '-1', '1210000', '1210000', '', '', '187', '206', '', '187;169;151;133;115;97;79;60;78;96;114;132;150;168;149;131;113;95;77;59;41;40;22;58;76;94;112;130;93;75;57;39', '');
INSERT INTO `mountpark_data` VALUES ('4233', '523', '5', '5', '0', '-1', '1210000', '1210000', '', '', '485', '504', '', '485;467;449;431;413;395;377;358;376;394;412;430;448;466;447;429;393;375;411;357;339;320;338;356;374;355;337;301;282;300;281;263', '');
INSERT INTO `mountpark_data` VALUES ('4243', '559', '5', '5', '0', '-1', '1210000', '1210000', '', '', '597', '578', '', '597;615;633;651;579;561;543;525;544;562;580;598;616;634;652;670;689;671;653;635;617;599;581;563;582;600;618;636;654;672;690;708;727;709;691;673;655;637;619;601;620;638;656;674;692;710;728;746;765;747;729;711;693;675;657', '');
INSERT INTO `mountpark_data` VALUES ('4273', '247', '5', '5', '0', '-1', '1210000', '1210000', '', '', '209', '228', '', '209;227;245;263;191;173;155;136;154;172;190;208;226;244;225;207;189;171;153;135;117;98;116;134;152;170;188;206;187;169;151;133;115;97;79;60;78;96;114;132;150;168;149;131;113;95;77;59;41;22;40;58;76;94;75;56;38;39', '');
INSERT INTO `mountpark_data` VALUES ('4269', '560', '5', '5', '0', '-1', '1210000', '1210000', '', '', '596', '578', '', '596;577;558;539;615;634;653;671;652;633;614;595;576;557;575;594;613;632;651;670;689;707;688;669;650;631;612;593;611;630;649;668;687;706;725;743;724;705;686;667;648;685;704;723;742;761;760', '');
INSERT INTO `mountpark_data` VALUES ('4264', '617', '5', '5', '0', '-1', '1210000', '1210000', '', '', '653', '635', '', '653;634;615;672;691;709;690;671;652;633;651;670;689;708;727;745;726;707;688;669;687;706;725;744;763;743;724;705', '');
INSERT INTO `mountpark_data` VALUES ('4278', '427', '5', '5', '0', '-1', '1210000', '1210000', '', '', '391', '409', '', '391;372;353;334;410;429;411;392;373;354;335;316;298;317;336;355;374;393;375;356;337;318;299;280;262;281;300;319;338;357;339;320;301;282;263', '');
INSERT INTO `mountpark_data` VALUES ('4272', '397', '5', '5', '0', '-1', '1210000', '1210000', '', '', '361', '379', '', '361;342;323;304;380;399;418;400;381;362;343;324;305;286;268;287;306;325;344;363;382;364;345;326;307;288;269;250;232;251;270;289;308;327', '');
INSERT INTO `mountpark_data` VALUES ('4271', '116', '5', '5', '0', '-1', '1210000', '1210000', '', '', '152', '134', '', '209;190;152;133;114;132;150;151;170;189;208;227;245;226;207;188;169;168;187;206;225;244;263;281;262;243;224;205;186;167;149;204;223;242;261;280;299;93;57', '189;3000;0');
INSERT INTO `mountpark_data` VALUES ('4265', '620', '5', '5', '0', '-1', '1210000', '1210000', '', '', '584', '602', '', '470;452;434;380;416;489;471;453;435;417;399;641;622;603;584;565;546;527;508;490;509;528;547;566;585;604;623;605;586;567;548;529;510;491;472;454;473;492;511;530;549;531;550;569;551;532;513;494;475;456;437;418', '');
INSERT INTO `mountpark_data` VALUES ('4262', '587', '5', '5', '0', '-1', '1210000', '1210000', '', '', '625', '606', '', '571;589;607;625;643;661;679;698;680;662;644;626;608;590;609;627;645;663;681;699;717;736;718;700;682;664;646;628;665;701;683;719;737;755;774;756;738;720;702;775', '');
INSERT INTO `mountpark_data` VALUES ('4261', '325', '5', '5', '0', '-1', '1210000', '1210000', '', '', '287', '306', '', '137;155;174;192;210;191;209;227;156;175;173;193;194;213;212;211;246;247;248;249;250;232;251;265;266;267;268;269;287;286;285;284;303;304;305;323;322;341', '');
INSERT INTO `mountpark_data` VALUES ('4217', '669', '5', '5', '0', '-1', '1210000', '1210000', '', '', '633', '651', '', '448;467;485;486;504;503;502;520;521;522;541;540;539;538;557;558;559;560;579;577;576;595;596;597;615;614;633', '');
INSERT INTO `mountpark_data` VALUES ('4219', '431', '5', '5', '0', '-1', '1210000', '1210000', '', '', '467', '449', '', '429;448;447;465;466;467;486;485;484;483;501;520;521;522;523;524;542;541;540;558;577;578;596;503;504', '');
INSERT INTO `mountpark_data` VALUES ('4218', '448', '5', '5', '0', '-1', '1210000', '1210000', '', '', '486', '467', '', '432;451;450;468;469;470;489;488;487;486;504;505;506;507;508;526;525;524;523;522;540;541;542;543;544;562;561;560;559;578;579;580;598;597;616', '');
INSERT INTO `mountpark_data` VALUES ('4213', '597', '5', '5', '0', '-1', '1210000', '1210000', '', '', '633', '615', '', '595;613;631;649;667;704;686;668;650;632;614;633;651;669;687;705;723;741;742;724;706;688;670;652;671;689;707;725;743;761;762;744;726;708;690', '');
INSERT INTO `mountpark_data` VALUES ('4216', '302', '5', '5', '0', '-1', '1210000', '1210000', '', '', '264', '283', '', '169;151;152;134;116;135;136;155;154;153;170;207;189;171;173;192;210;191;172;190;208;226;245;264;246;228;209;227;137', '');
INSERT INTO `mountpark_data` VALUES ('4215', '155', '5', '5', '0', '-1', '1210000', '1210000', '', '', '117', '136', '', '117;135;153;171;189;99;81;63;44;62;80;98;116;134;152;170;133;115;97;79;61;43;25;24;42;60;78;96;114;95;77;59;41;23;22;40;58;76;57;39', '');
INSERT INTO `mountpark_data` VALUES ('4270', '714', '5', '5', '0', '-1', '1210000', '1210000', '', '', '678', '696', '', '678;659;640;697;716;735;736;717;698;679;660;641;622;604;623;642;661;680;681;700;662;643;624;605;586;568;587;606;625;644;663;682;701;720;739;550;569;588;607;626;645;664;683;702;721', '');
INSERT INTO `mountpark_data` VALUES ('4096', '652', '5', '5', '0', '-1', '1210000', '1210000', '', '', '614', '633', '', '614;632;650;668;596;578;560;541;559;577;595;613;631;649;630;612;594;576;558;540;522;503;521;539;557;575;593;611;556;538;520;502;484;465;483;501;519;537;500;482;464', '');
INSERT INTO `mountpark_data` VALUES ('4104', '162', '5', '5', '0', '-1', '1210000', '1210000', '', '', '126', '144', '', '126;107;88;145;164;146;127;108;89;70;52;71;90;109;128;34;53', '');
INSERT INTO `mountpark_data` VALUES ('4284', '283', '5', '5', '0', '-1', '1210000', '1210000', '', '', '321', '302', '', '321;303;285;304;323;342;361;360;341;322;340;359;378;397;415;396;377;358;339;357;376;395;414', '');
INSERT INTO `mountpark_data` VALUES ('4291', '532', '5', '5', '0', '-1', '1210000', '1210000', '', '', '494', '513', '', '494;476;458;440;512;530;511;493;475;457;439;421;402;420;438;456;474;492;455;437;419;401;383;364;382;400;418;436;435;454;417;398;379;360;378;397;416;473', '');
INSERT INTO `mountpark_data` VALUES ('4275', '451', '5', '5', '0', '-1', '1210000', '1210000', '', '', '415', '433', '', '415;396;434;453;472;454;435;416;397;378;360;379;398;417;436;455;437;418;399;380;361;342;324;343;362;381;400;419;401;382;363;344;325', '');
INSERT INTO `mountpark_data` VALUES ('4280', '196', '5', '5', '0', '-1', '1210000', '1210000', '', '', '158', '177', '', '158;140;122;176;194;212;193;175;157;139;121;103;84;102;120;138;156;174;155;137;119;101;83;65;46;64;82;100;118;136;117;99;81;63;45;27;26;44;62;80;98;79;61;43;25;24;42;60;41', '');
INSERT INTO `mountpark_data` VALUES ('4249', '528', '5', '5', '0', '-1', '1210000', '1210000', '', '', '492', '510', '', '492;473;511;530;549;568;587;569;550;531;512;493;474;455;437;475;494;513;532;551;533;514;495;476;458;477;496', '');
INSERT INTO `mountpark_data` VALUES ('4287', '257', '5', '5', '0', '-1', '1210000', '1210000', '', '', '219', '238', '', '219;201;183;165;147;110;128;146;164;182;200;181;163;145;127;109;91;73;72;90;108;126;144;162;143;125;107;89;71;53;34;52;70;88;106', '');
INSERT INTO `mountpark_data` VALUES ('4282', '172', '5', '5', '0', '-1', '1210000', '1210000', '', '', '136', '154', '', '136;155;174;193;117;98;79;61;80;99;118;137;156;175;157;138;119;100;81;62;43;25;44;63;82;101;120;139;121;102;83;64;45', '');
INSERT INTO `mountpark_data` VALUES ('4169', '490', '5', '5', '0', '-1', '1210000', '1210000', '', '', '528', '509', '', '528;510;492;546;564;582;601;583;565;547;529;511;530;548;566;584;602;620;639;621;603;585;567;549;568;586;604;622;640', '');
INSERT INTO `mountpark_data` VALUES ('4172', '615', '5', '5', '0', '-1', '1210000', '1210000', '', '', '651', '633', '', '613;632;651;670;689;707;688;669;650;631;649;668;687;706;725;743;724;705;686;667;685;704;723;742;761;760;741;722;759', '');
INSERT INTO `mountpark_data` VALUES ('4300', '193', '5', '5', '0', '-1', '1210000', '1210000', '', '', '155', '174', '', '24;42;41;77;76;114;59;60;79;61;43;62;80;134;152;171;209;191;137;119;100;118;136;81;173;154', '');
INSERT INTO `mountpark_data` VALUES ('4289', '506', '5', '5', '0', '-1', '1210000', '1210000', '', '', '544', '525', '', '580;562;544;526;508;527;545;563;581;599;618;600;582;564;546;565;583;601;619;637;656;620;602;584;603;621;639;657;675;694;676;658;640;622', '');
INSERT INTO `mountpark_data` VALUES ('4181', '678', '5', '5', '0', '-1', '1210000', '1210000', '', '', '716', '697', '', '716;680;698;734;752;771;753;735;717;699;718;736;754;772;773;755;737;756;774', '');
INSERT INTO `mountpark_data` VALUES ('4178', '541', '5', '5', '0', '-1', '1210000', '1210000', '', '', '577', '559', '', '577;596;615;634;653;558;539;557;576;595;614;633;652;671;689;670;651;632;613;594;575;593;612;631;650;669;688;707;725;706;687;668;649;630;611;667;704;705;724;743;761;742;723;685;741;760', '');
INSERT INTO `mountpark_data` VALUES ('4212', '232', '5', '5', '0', '-1', '1210000', '1210000', '', '', '194', '213', '', '194;176;158;140;212;230;248;229;211;193;175;157;139;121;138;156;174;192;210;191;173;155;137;119;118;136;154', '');
INSERT INTO `mountpark_data` VALUES ('4170', '467', '5', '5', '0', '-1', '1210000', '1210000', '', '', '431', '449', '', '339;357;375;393;412;394;376;358;377;395;413;431;450;432;414;396;415;433;451;469;488;470;452;434;453;471;489;507', '');
INSERT INTO `mountpark_data` VALUES ('4204', '455', '5', '5', '0', '-1', '1210000', '1210000', '', '', '491', '473', '', '491;472;453;434;510;529;548;566;547;528;509;490;471;452;470;489;508;527;546;565;584;602;583;564;545;526;507;488;506;525;544;563;582;601', '');
INSERT INTO `mountpark_data` VALUES ('4182', '308', '5', '5', '0', '-1', '1210000', '1210000', '', '', '272', '290', '', '272;253;234;216;198;180;162;144;163;181;199;217;235;254;236;218;200;182;201;219;237;255;273;291;310;292;274;256;238;220;202;239;257;275;293;311', '');
INSERT INTO `mountpark_data` VALUES ('4208', '208', '5', '5', '0', '-1', '1210000', '1210000', '', '', '170', '189', '', '170;188;206;224;205;186;167;130;149;168;187;169;150;131;112;93;56;75;94;113;132;151;152;133;114;95;76;57;38;19;20;39;58;77;96;115;134;116;97;78;59;40', '');
INSERT INTO `mountpark_data` VALUES ('4299', '472', '5', '5', '0', '-1', '1210000', '1210000', '', '', '434', '453', '', '434;452;470;488;506;524;453;472;491;505;487;469;451;433;415;396;414;432;450;468;486;467;449;431;413;395', '');
INSERT INTO `mountpark_data` VALUES ('4304', '414', '5', '5', '0', '-1', '1210000', '1210000', '', '', '378', '396', '', '378;397;416;435;454;473;492;474;455;436;417;398;379;360;342;361;380;399;418;437;456;438;419;400;381;362;343;306;325;344;363;382;401;420', '');
INSERT INTO `mountpark_data` VALUES ('4301', '620', '5', '5', '0', '-1', '1210000', '1210000', '', '', '656', '638', '', '656;637;618;636;655;674;692;673;654;635;616;597;615;634;671;672;691;710;728;709;690;652;633;670;689;708;727;746;726', '');
INSERT INTO `mountpark_data` VALUES ('4290', '325', '5', '5', '0', '-1', '1210000', '1210000', '', '', '287', '306', '', '323;305;287;269;304;286;268;250;232;213;249;285;266;248;230;212;194;175;193;211;229;247;228;210;192;174;156;137;155;173;191;209;190;172;154;136;118', '');
INSERT INTO `mountpark_data` VALUES ('4336', '437', '5', '5', '0', '-1', '1210000', '1210000', '', '', '401', '419', '', '401;382;363;325;307;326;345;364;383;365;346;327;308;271;290;309;328;347', '');
INSERT INTO `mountpark_data` VALUES ('2216', '675', '5', '5', '0', '-1', '1210000', '1210000', '', '', '637', '656', '', '637;655;673;691;619;601;583;565;546;564;582;600;618;636;654;672;653;635;617;599;581;563;545;527;508;526;544;562;580;598;616;634;615;597;579;561;543;525;507;489;470;488;506;524;542;560;578;596;451;469;487;505;523;541;559', '');
INSERT INTO `mountpark_data` VALUES ('2215', '567', '5', '5', '0', '-1', '1210000', '1210000', '', '', '529', '548', '', '529;547;565;511;493;474;492;510;528;546;527;509;491;473;455;418;400;382;436;454;472;490;508;526;544;562;543;525;507;489;471;363;381;399;417;435;344;362;380;398;470;488;506;524;505;487;469;451;433;396;378;379;361;343;325;306;324;342;360;414;432;450;468;486;413;395;377;359;341;322;340;358;376;394;339;357', '');
INSERT INTO `mountpark_data` VALUES ('2209', '674', '5', '5', '0', '-1', '1210000', '1210000', '', '', '636', '655', '', '636;618;600;582;564;546;528;509;527;545;563;581;599;617;598;580;562;544;526;508', '');
INSERT INTO `mountpark_data` VALUES ('2210', '472', '5', '5', '0', '-1', '1210000', '1210000', '', '', '436', '454', '', '436;455;417;398;379;360;341;323;342;361;380;399;418;437;456;438;419;400;381;362;343;324;305;287;306;325;344;363;382;401;420;402;383;364;345;326;307;288', '');
INSERT INTO `mountpark_data` VALUES ('4303', '679', '5', '5', '0', '-1', '1210000', '1210000', '', '', '643', '661', '', '643;624;605;586;567;662;681;700;719;701;682;663;644;625;606;587;568;549;531;550;569;588;607;626;645;664;646;627;608;589;570;551;532', '');
INSERT INTO `mountpark_data` VALUES ('4305', '438', '5', '5', '0', '-1', '1210000', '1210000', '', '', '402', '420', '', '383;364;345;327;346;365;384;366;347;328;309;291;310;329;348;330;311;292;273', '');
INSERT INTO `mountpark_data` VALUES ('4077', '403', '5', '5', '0', '-1', '1210000', '1210000', '', '', '365', '384', '', '365;347;329;311;293;383;401;382;364;346;328;310;292;274;255;273;291;309;327;345;363;344;326;308;290;272;253;271;289;307;306;325', '');
INSERT INTO `mountpark_data` VALUES ('4082', '414', '4', '4', '0', '-1', '962500', '962500', '', '', '378', '396', '', '378;359;340;322;304;286;268;250;269;287;305;323;341;360;342;324;306;288;307;325;343;361;379;397;416;398;380;362;344', '');
INSERT INTO `mountpark_data` VALUES ('4302', '617', '5', '5', '0', '-1', '1210000', '1210000', '', '', '579', '598', '', '579;561;543;597;615;596;578;560;542;524;505;523;541;559;577;558;540;522;504;486;467;485;503;521;539;520;502;484;466;448;429;447;465;483;501;482;464;446;428', '');
INSERT INTO `mountpark_data` VALUES ('4072', '287', '5', '5', '0', '-1', '1210000', '1210000', '', '', '251', '269', '', '251;232;213;270;289;308;290;271;252;233;214;195;177;196;215;234;253;272;254;235;216;198;217', '');
INSERT INTO `mountpark_data` VALUES ('4090', '697', '5', '5', '0', '-1', '1210000', '1210000', '', '', '661', '679', '', '661;643;625;607;589;571;590;608;626;644;662;680;699;718;700;682;664;646;628;665;683;701;719;737;756;738;720;702;739;757', '');
INSERT INTO `mountpark_data` VALUES ('4097', '707', '5', '5', '0', '-1', '1210000', '1210000', '', '', '671', '689', '', '671;652;633;614;595;690;709;728;747;729;710;691;672;653;634;615;596;577;559;578;597;616;654;673;692;711;693;674;655;636;617;598;579;560;541;523;542;561;580;599;618;637;656', '');
INSERT INTO `mountpark_data` VALUES ('4180', '621', '5', '5', '0', '-1', '1210000', '1210000', '', '', '659', '640', '', '659;641;623;605;677;695;713;732;714;696;678;660;642;624;643;661;679;697;715;733;751;770;752;734;716;698;680;662;681;699;717;735;753;771;772;754;736;718;700;719;737;755;773;756', '');
INSERT INTO `mountpark_data` VALUES ('4094', '529', '5', '5', '0', '-1', '1210000', '1210000', '', '', '491', '510', '', '491;509;527;545;473;455;436;454;472;490;508;526;507;489;471;453;435;417;398;416;434;452;470;488;469;451;433;415;397;379;396;378;360;341;359', '');
INSERT INTO `mountpark_data` VALUES ('4236', '578', '5', '5', '0', '-1', '1210000', '1210000', '', '', '614', '596', '', '614;595;576;557;575;593;611;648;630;612;594;613;631;649;667;686;668;650;632;633;651;669;687;705;742;724;706;688;670;652;671;689;707;725;743', '');
INSERT INTO `mountpark_data` VALUES ('4177', '556', '4', '4', '0', '-1', '962500', '962500', '', '', '594', '575', '', '594;576;558;612;630;648;685;667;649;631;613;595;577;596;614;632;650;668;686;704;722;741;723;705;687;669;651;633;615;634;652;670;688;706;724;742;760;653;671;689;707;725;726;708;690', '');
INSERT INTO `mountpark_data` VALUES ('4232', '717', '3', '3', '0', '-1', '770000', '770000', '', '', '679', '698', '', '733;715;697;679;643;661;625;606;624;642;660;678;696;714;695;677;659;641;623;605;587;568;586;604;622;640;658;676;657;639;621;603;585;567;549;530;548;584;602;620;638', '567;3000;3000|639;3000;3000');
INSERT INTO `mountpark_data` VALUES ('4173', '660', '2', '2', '0', '-1', '522500', '522500', '', '', '698', '679', '', '698;680;662;681;700;719;738;757;775;756;737;718;699;717;736;755;774;773;754;735;716;734;753', '');
INSERT INTO `mountpark_data` VALUES ('8479', '267', '2', '2', '0', '-1', '522500', '522500', '', '', '231', '249', '', '231;250;269;212;193;174;155;136;118;137;156;175;194;213;232;251;233;214;195;176;157;138;119;100;101;158;177;196;215;140;159;178', '');
INSERT INTO `mountpark_data` VALUES ('8480', '342', '5', '5', '0', '-1', '1210000', '1210000', '', '', '306', '324', '', '268;250;232;214;196;178;197;215;233;251;269;287;306;288;270;252;234;271;289;307;325;344;326;308', '');
INSERT INTO `mountpark_data` VALUES ('4231', '708', '3', '3', '0', '-1', '770000', '770000', '', '', '670', '689', '', '616;634;652;670;688;706;724;742;760;759;741;723;705;687;669;651;633;615;597;578;559;577;595;613;631;649', '');
INSERT INTO `mountpark_data` VALUES ('4229', '543', '5', '5', '0', '-1', '1210000', '1210000', '', '', '507', '525', '', '507;526;545;527;509;491;473;455;436;454;472;490;508;489;471;453;435;417;398;416;434;452;470;488;469;451;433;415;397;379;414', '');
INSERT INTO `mountpark_data` VALUES ('4093', '730', '5', '5', '0', '-1', '1210000', '1210000', '', '', '692', '711', '', '746;728;710;692;656;674;638;620;601;619;637;655;673;691;709;727;708;690;672;654;636;618;600;582;563;581;599;635;653;671;689', '');
INSERT INTO `mountpark_data` VALUES ('4070', '468', '6', '6', '0', '-1', '1512500', '1512500', '', '', '432', '450', '', '432;451;470;489;508;413;394;375;356;338;376;395;414;433;452;471;490;472;453;434;397;396;377;358;339;320;302;321;340;359;378;435;454;436;417;398;379;360;341;322;303;284;266;285;304;323;342;361;380;399', '');
INSERT INTO `mountpark_data` VALUES ('2220', '373', '5', '5', '0', '-1', '1210000', '1210000', '', '', '337', '355', '', '337;318;299;280;261;243;225;207;189;171;153;135;154;172;190;208;226;244;262;281;263;245;227;209;191;173;192;210;228;246;264;282;300;319;301;283;265;247;229', '');
INSERT INTO `mountpark_data` VALUES ('2218', '410', '5', '5', '0', '-1', '1210000', '1210000', '', '', '374', '392', '', '374;355;336;356;337;318;300;319;338;320;301;282;264;283;302;284;265;246;227;209;228;247;266;190;208;226;244;262;280;298;261;243;225;207;189;171;152;170;188;206;224;242', '');
INSERT INTO `mountpark_data` VALUES ('9216', '210', '14', '14', '0', '-1', '3135000', '3135000', '', '', '180', '194', '', '208;193;178;163;148;133;103;88;74;89;104;119;134;149;179;194;180;165;150;135;120;105;90;75;60;32;46;61;76;91;106;121;136;151;166;137;123;138;124;109;110;95;81;96;82;67;66;80;51;65', '');
INSERT INTO `mountpark_data` VALUES ('9204', '369', '8', '8', '0', '-1', '1952500', '1952500', '', '', '341', '355', '', '341;356;371;326;311;296;281;266;251;237;252;267;282;297;312;327;342;357;343;328;313;298;238;223;209;224;210;211;226;255;269;284;299;314;329;315;300;285;270;196;195;181;241;256;271;286;301;287;272;257;242;227;212;197;182;167;153;168;183;198;213;228;243;258', '');
INSERT INTO `mountpark_data` VALUES ('9158', '585', '12', '12', '0', '-1', '2695000', '2695000', '', '', '547', '566', '', '547;529;511;493;475;565;583;601;619;637;618;600;582;564;546;528;510;492;474;456;437;455;473;491;509;527;545;526;508;490;472;454;436;418;381;363;345;327;309;399;417;435;453;471;489;507;525;543;561;542;524;506;488;470;452;434;416;398;380;362;344;326;308;290;271;289;307;325;343;361;379;397;415;433;451;469;487;505;504;486;468;450;432;414;396;378;360;342;324;306;288;270;252;233;251;269;287;305;323;341;359;377;395;413;431;412;394;376;358;375;393;357;320;338;356;374;355;337;319', '');
INSERT INTO `mountpark_data` VALUES ('9156', '605', '5', '5', '0', '-1', '1210000', '1210000', '', '', '567', '586', '', '567;585;603;549;531;513;495;477;459;440;458;476;494;512;530;548;566;584;565;547;529;511;493;475;457;439;421;402;420;438;456;474;492;510;528;546;527;509;491;473;455;418;400;382;383;346;328;310;364;436;454;472;490;508;489;471;434;435;417;399;381;363;345;327;309;291;272;290;308;326;344;362;380;398;416;452;470;451;433;415;397;379;361;343;325;307;289;271;253;234;252;270;288;306;324;342;360;378;396;414;432;413;395;377;359;341;323;305;287;269;251;250;586;605;624;401;419;233', '');
INSERT INTO `mountpark_data` VALUES ('9157', '523', '5', '5', '0', '-1', '1210000', '1210000', '', '', '487', '505', '', '487;506;468;449;430;393;392;374;412;431;450;469;488;507;508;489;470;451;432;413;395;414;433;452;471;490;472;453;434;415;396;377;359;378;397;416;435;454;436;417;398;379;360;341;322;303;284;266;285;304;323;342;361;380;399;418;343;306;305;286;267;248;230;249;268;287;325;307;288;269;250;231', '');
INSERT INTO `mountpark_data` VALUES ('9159', '676', '14', '14', '0', '-1', '3135000', '3135000', '', '', '640', '658', '', '640;622;604;586;568;550;531;549;567;585;603;621;602;584;566;512;493;511;529;547;565;583;475;457;439;421;403;420;438;456;474;492;510;528;546;564;527;509;491;473;455;437;419;400;418;436;454;472;490;508;526;507;525;524;542;561;506;488;470;452;434;416;398;380;362;344;326;308;327;523;505;487;469;451;433;415;397;379;361;343;325;307;289;270;288;306;324;342;360;378;396;414;432;450;468;486;504;485;467;449;431;413;395;377;359;341;323;305;287;269;251;232;250;268;286;304;322;340;358;376;394;412;430;448;466;447;429;411;393;375;357;339;321;303;285;267;249;231', '');
INSERT INTO `mountpark_data` VALUES ('9162', '558', '14', '14', '0', '-1', '3135000', '3135000', '', '', '522', '540', '', '503;484;465;447;541;560;579;598;617;636;655;674;656;637;618;599;580;561;542;523;504;485;466;429;448;467;486;505;524;543;562;581;600;619;638;620;601;582;563;544;525;506;487;468;449;430;411;412;431;450;469;488;507;526;545;564;583;602;565;546;527;508;489;470;451;414;432;377;433;452;471;490;509;528;547;566;548;529;510;491;472;453;434;415;396;359;378;397;416;435;454;473;492;511;530;512;493;474;455;436;417;398;379;360;341;322;304;285;323;342;361;380;399;400;381;362;343;324;305;286;267;249;268;287;306;325;344;363;382;401;419;402;421;440;458;476;494;383;364;345;326;231;250;269;288', '');
INSERT INTO `mountpark_data` VALUES ('9209', '220', '5', '5', '0', '-1', '1210000', '1210000', '', '', '192', '206', '', '192;207;222;177;148;133;132;117;103;118;163;178;193;208;179;164;149;134;119;104;89;75;90;105;120;135;150;151;166;136;121;106;91;76;62;77;92;107;122;137;152;138;123;108;93;78;63', '');
INSERT INTO `mountpark_data` VALUES ('9218', '226', '8', '8', '0', '-1', '1952500', '1952500', '', '', '196', '211', '', '196;210;224;182;168;154;140;126;112;97;111;125;139;153;167;181;195;209;180;166;152;138;124;110;96;82;81;95;109;123;137;151;165;179;164;150;136;122;108;94;80;66;79;93;107;121;135;149;134;120;106;92;78;64;49;63;77;91;105;119;104;90;76;62;75;74;89;60;45', '');
INSERT INTO `mountpark_data` VALUES ('9163', '599', '7', '7', '0', '-1', '1173500', '1173500', '', '', '563', '581', '', '563;582;601;602;639;544;525;506;507;489;452;451;450;468;449;430;411;392;374;393;412;431;471;490;509;528;547;584;621;603;565;546;527;508;432;413;394;375;356;338;357;376;395;414;433;566;585;567;548;529;510;491;472;453;434;415;396;377;358;339;320;302;321;340;359;378;397;416;435;454;455;474;511;530;549;531;512;493;436;417;398;379;360;341;322;303;284;266;285;304;323;342;361;399;418;437;456;475;494;513;495;476;457;438;419;362;343;324;305;286;268;287;306;401;420;439;458', '');
INSERT INTO `mountpark_data` VALUES ('9160', '375', '5', '5', '0', '-1', '1210000', '1210000', '', '', '339', '357', '', '339;358;377;396;415;320;301;282;263;226;245;264;283;359;378;397;379;360;341;322;303;266;247;246;227;208;190;209;228;285;304;323;342;361;343;324;305;286;249;230;211;192;173;172;154;268;287;306;325;307;288;269;250;231;193;174;155', '');
INSERT INTO `mountpark_data` VALUES ('9164', '691', '5', '5', '0', '-1', '1210000', '1210000', '', '', '655', '673', '', '655;636;617;674;675;656;637;618;599;580;561;542;523;504;485;467;486;505;524;543;562;581;600;619;638;657;639;620;601;563;544;525;506;487;468;449;431;450;469;488;507;526;583;602;621;603;584;565;528;509;490;489;470;451;432;413;471;453;472;491;510;529;548;567;547;566;585;549;530;511;492;473;454;435;417;380;361;342;341;359;512;493;474;455;436;323;494;495;476;457;438;419;418;343;324;305;287;306;325;420;439;458;477;459;440;421;344;345;364;326;307;288', '');
INSERT INTO `mountpark_data` VALUES ('9207', '313', '6', '6', '0', '-1', '1512500', '1512500', '', '', '283', '298', '', '283;297;269;255;241;227;213;198;212;226;240;254;268;282;267;253;239;225;211;197;183;168;182;238;252;237;223;194;180;166;167;153;138;152;208;222;207;193;179;165;151;137;123;108;122;136;150;164;178;192;177;163;149;135;121;107', '');
INSERT INTO `mountpark_data` VALUES ('9219', '161', '12', '12', '0', '-1', '2695000', '2695000', '', '', '133', '147', '', '133;118;103;148;163;178;193;208;223;238;253;239;224;209;194;179;164;149;134;105;104;89;75;120;135;150;165;180;195;210;225;211;196;181;166;151;136;121;106;91;62;77;92;107;137;152;167;182;197;183;168;153;138;123;108;93;78;63;48;49;64;79;94;109;124;139;154;169;155;140;125;110;95;80;65;50;81;96;111;126', '');
INSERT INTO `mountpark_data` VALUES ('9220', '205', '8', '8', '0', '-1', '1952500', '1952500', '', '', '177', '191', '', '177;162;147;132;117;102;178;207;222;237;223;208;193;163;148;133;118;103;88;74;89;104;119;134;149;164;179;194;165;150;135;120;105;90;75;60;61;76;91;106;121;136;151;166;181;167;152;137;122;107;92;47', '');
INSERT INTO `mountpark_data` VALUES ('9208', '429', '8', '8', '0', '-1', '1952500', '1952500', '', '', '401', '415', '', '326;341;356;371;386;401;416;431;446;461;447;433;419;405;376;390;404;418;432;417;403;389;375;361;346;360;374;388;402;387;373;359;345;331;316;330;344;358;372;357;343;329;315;301;286;300;314;328;342;327;313;299;285;271;256;270;284;298;312', '');
INSERT INTO `mountpark_data` VALUES ('9165', '433', '7', '7', '0', '-1', '1173500', '1173500', '', '', '397', '415', '416;7590;2379|417;7590;2379|436;7590;2379', '302;321;340;359;378;397;416;435;454;436;417;379;360;341;322;303;284;265;247;266;285;304;323;342;361;380;399;418;400;381;362;343;324;305;286;267;248;229;211;230;249;268;287;306;325;344;363;382;364;345;326;307;288;269;250;231;212;193;175;194;213;232;251;270;289;308;327;346;328;309;290;271;252;233;214;195;176', '416;3000;2847|417;3000;2512|436;3000;2964');
INSERT INTO `mountpark_data` VALUES ('9152', '503', '14', '14', '0', '-1', '3135000', '3135000', '', '', '467', '485', '', '429;448;467;486;505;524;543;562;581;600;582;563;544;525;506;487;468;449;411;412;450;469;488;507;526;545;564;546;527;508;489;470;451;432;413;394;357;376;395;433;452;471;509;528;510;491;472;453;434;415;396;377;358;339;321;340;378;397;416;435;454;473;492;474;455;436;417;398;379;360;341;322;303;285;304;323;342;361;380;399;418;437;456;438;419;400;381;362;343;324;305;286;249;268;287;306;325;344;363;382;401;420', '');
INSERT INTO `mountpark_data` VALUES ('9166', '560', '7', '7', '0', '-1', '1173500', '1173500', '', '', '524', '542', '', '524;543;562;581;600;505;486;467;449;468;487;506;525;544;563;582;564;545;526;507;488;469;450;432;451;470;489;508;527;546;528;509;490;471;452;433;414;395;510;491;472;453;434;415;396;377;358;339;320;301;302;283;321;416;435;454;473;492;474;455;436;417;398;379;378;284;265;247;266;267;286;305;324;361;380;399;418;437;456;438;419;400;381;362;343;248;229;287;306;325;344;363;382;401;420;269;288;307;326;308;289;270', '');
INSERT INTO `mountpark_data` VALUES ('9222', '222', '7', '7', '0', '-1', '1173500', '1173500', '', '', '194', '208', '', '194;209;224;179;164;149;134;119;104;89;75;90;105;120;135;150;165;180;195;210;196;181;166;151;136;121;106;91;76;92;107;122;152;167;182;168;153;124;95;94;108;93;78;79;110;139;154;80;65;125;140;126;111;96;81;66;52;67;82;97;112;98;83;68;53;54', '');
INSERT INTO `mountpark_data` VALUES ('9210', '226', '5', '5', '0', '-1', '1210000', '1210000', '', '', '198', '212', '', '198;183;168;213;228;214;199;184;169;154;125;110;95;140;155;170;200;186;171;156;127;126;111;96;81;67;82;97;112;142;157;172;158;143;128;113;98;83;68;53;39;54;69;84;99;114;129;144;115;100;85;70', '');
INSERT INTO `mountpark_data` VALUES ('9167', '449', '12', '12', '0', '-1', '2695000', '2695000', '', '', '413', '431', '', '413;432;451;470;489;508;394;375;356;337;318;299;280;262;281;300;319;338;357;376;395;414;433;452;471;490;472;453;434;415;396;377;358;339;320;301;282;263;244;226;245;264;283;302;321;340;359;378;397;416;435;454;436;417;398;379;360;341;322;303;246;227;208;190;209;228;247;304;323;342;361;380;399;418;400;381;362;343;324;305;286;229;210;191;172;192;211;287;344;363;382;364;345;174;156;175;193;268;250;232;251;270;289;308;327;346;328;309;290;271;252;233;214;176;157', '');
INSERT INTO `mountpark_data` VALUES ('9153', '603', '8', '8', '0', '-1', '1952500', '1952500', '', '', '567', '585', '', '567;586;605;624;643;548;529;510;491;473;511;530;549;568;587;606;625;607;588;569;550;531;512;493;474;455;589;570;551;532;513;494;475;456;437;418;399;380;343;342;324;362;381;400;419;438;457;476;495;514;496;477;458;439;420;401;382;363;344;325;306;288;307;326;345;364;383;402;421;440;459;478;270;289;308;327;346;365;347;328;309;290;271;252;234;253;272;291;310;329;311;292;273;254', '');
INSERT INTO `mountpark_data` VALUES ('9168', '468', '8', '8', '0', '-1', '1952500', '1952500', '', '', '432', '450', '', '432;451;470;489;508;527;546;565;413;394;375;356;338;357;376;395;414;433;452;471;490;509;547;529;510;491;472;453;434;415;396;377;358;339;320;302;321;340;359;378;397;416;435;454;473;492;511;493;474;455;436;417;398;379;360;341;322;303;284;266;285;304;323;342;418;437;456;475;438;419;400;363;344;325;324;305;286;267;248;230;249;268;287;306;382;401;420;439;421;402;383;364;345;326;307;288;269;250;231;212;194;213;232;251;270;289;308;327;346;365;384', '');
INSERT INTO `mountpark_data` VALUES ('9211', '429', '6', '6', '0', '-1', '1512500', '1512500', '', '', '401', '415', '', '401;416;431;446;386;371;356;341;327;342;357;372;387;402;417;432;418;403;388;373;358;343;328;313;299;314;329;344;359;374;389;404;390;375;360;345;330;315;300;285;271;286;301;316;331;346;361;376;347;332;317;302;287;272', '');
INSERT INTO `mountpark_data` VALUES ('9223', '197', '4', '4', '0', '-1', '962500', '962500', '', '', '169', '183', '', '169;154;139;124;109;94;79;184;199;185;170;155;140;125;110;95;80;65;51;66;96;111;126;141;156;171;157;142;127;112;97;82;67;83;143;128;113;98;69;84;99;114;129;115;100;85;70', '');
INSERT INTO `mountpark_data` VALUES ('9169', '414', '7', '7', '0', '-1', '1173500', '1173500', '', '', '378', '396', '', '378;397;416;435;359;322;285;266;265;284;341;360;379;398;417;399;400;401;438;475;512;530;494;380;361;342;323;304;247;248;267;286;305;324;343;362;381;457;439;420;382;363;344;325;306;287;268;249;230;211;193;212;231;250;269;288;307;326;345;364;383;402;421;422;403;384;365;346;327;308;289;270;251;232;213;194;175;214;233;252;271;253;234;215;196;178;197;216;235;217;198;179', '');
INSERT INTO `mountpark_data` VALUES ('9213', '206', '5', '5', '0', '-1', '1210000', '1210000', '', '', '178', '192', '', '178;193;179;165;151;122;108;109;94;136;150;164;163;149;135;121;107;93;79;64;78;92;106;120;119;105;91;77;63;49;34;48;62;76;90;104;118;103;89;75', '');
INSERT INTO `mountpark_data` VALUES ('9225', '219', '6', '6', '0', '-1', '1512500', '1512500', '', '', '191', '205', '', '191;176;206;207;178;163;148;147;118;103;133;193;179;164;149;134;119;104;89;74;75;90;105;120;135;150;165;151;136;121;106;91;92;107;122;137;123;108;93;78', '');
INSERT INTO `mountpark_data` VALUES ('8773', '270', '3', '3', '0', '-1', '770000', '770000', '', '', '242', '256', '', '242;257;258;243;228;213;198;183;168;227;169;154;184;199;214;200;185;170;155;140;126;141;156;171;186;201;216;202;187;172;157;142;127;112', '');
INSERT INTO `mountpark_data` VALUES ('8778', '263', '5', '5', '0', '-1', '1210000', '1210000', '', '', '235', '249', '', '235;250;265;220;205;190;161;176;191;206;221;236;251;237;222;207;192;177;162;148;163;178;223;209;194;165;164;149;134;119;105;120;135;150;180;195;181;166;151', '');
INSERT INTO `mountpark_data` VALUES ('8783', '326', '5', '5', '0', '-1', '1210000', '1210000', '', '', '298', '312', '', '298;313;328;343;283;268;269;284;299;314;329;315;300;285;270;255;240;241;256;271;286;301;272;257;242;228;213;198;183;168;153;138;123;109;124;139;154;169;184;199;137;152;167;182;196;195;209', '');
INSERT INTO `mountpark_data` VALUES ('8788', '384', '6', '6', '0', '-1', '1512500', '1512500', '', '', '356', '370', '', '356;341;326;311;296;371;386;401;387;372;357;342;327;312;297;282;268;283;298;313;328;343;358;373;254;269;284;299;314;329;344;359;345;330;315;300;285;270;255;240;256;301;316', '');
INSERT INTO `mountpark_data` VALUES ('8793', '370', '5', '5', '0', '-1', '1210000', '1210000', '', '', '299', '314', '', '299;313;327;285;271;257;228;214;242;256;270;284;298;312;297;283;269;255;226;240;254;268;282;197;183;184;198;211;225;239;253;267;252;238;224;210;196;182;237;223;209;195;181;167;153;139;154;169', '');
INSERT INTO `mountpark_data` VALUES ('8798', '323', '6', '6', '0', '-1', '1512500', '1512500', '', '', '295', '309', '', '295;310;325;340;326;312;298;284;270;256;242;228;214;199;213;227;241;255;269;283;297;311;296;282;268;254;240;226;212;198;184;169;183;197;211;225;239;253;267;281;280;266;252;238;224;210;196;182;168;154;139;153;167;181;166;152;138', '');
INSERT INTO `mountpark_data` VALUES ('8803', '160', '3', '3', '0', '-1', '770000', '770000', '', '', '132', '146', '', '132;117;102;147;162;177;192;178;163;148;133;118;103;88;74;89;60;75;46;61;76;91;106;121;136;107;92;77;62;47', '');
INSERT INTO `mountpark_data` VALUES ('8804', '125', '6', '6', '0', '-1', '1512500', '1512500', '', '', '153', '139', '', '153;138;123;108;168;183;198;213;228;243;258;272;257;227;212;197;182;167;152;137;122;136;151;166;181;196;211;226;241;271;286;300;285;284;255;240;225;210;195;180;165;150;164;179;194;209;224;239;254;269;299', '');
INSERT INTO `mountpark_data` VALUES ('8799', '383', '6', '6', '0', '-1', '1512500', '1512500', '', '', '355', '369', '', '355;370;385;400;340;325;310;295;281;296;311;326;341;356;371;386;372;357;342;327;312;297;282;267;253;268;283;298;313;328;314;299;284;269;254;239;224;210;225;240;255;270;285;300;286;271;256;241;226;211', '');
INSERT INTO `mountpark_data` VALUES ('8794', '398', '4', '4', '0', '-1', '962500', '962500', '', '', '370', '384', '', '370;385;400;356;371;386;372;357;342;328;343;358;373;388;403;418;433;404;389;374;359;344;329;314;299;284;269;255;270;285;300;315;330;345;360;375;390;241', '');
INSERT INTO `mountpark_data` VALUES ('8789', '180', '12', '12', '0', '-1', '2695000', '2695000', '', '', '208', '194', '', '208;223;238;253;268;283;193;178;163;148;133;132;103;117;147;162;177;192;207;222;237;252;267;282;297;311;296;281;266;251;236;221;206;191;176;161;146;131;160;175;190;205;220;235;250;265;280;295;310;325;339;324;309;294;279;264;249;234;219;204;189;218;233;248;263;278;293;308;323;338;353;352;337;322;307;292;277;306;321;336;351', '');
INSERT INTO `mountpark_data` VALUES ('8779', '157', '6', '6', '0', '-1', '1512500', '1512500', '', '', '127', '142', '', '183;168;153;169;154;139;124;109;94;79;65;51;37;23;24;39;54;69;84;99;113;127;141;155;140;125;110;95;80;66;81;96;111;126;112;97;82;67;52;38;53;68;83;98', '140;3000;2997|141;3000;3000|153;3000;2990|154;3000;2983');
INSERT INTO `mountpark_data` VALUES ('8774', '298', '2', '2', '0', '-1', '522500', '522500', '', '', '326', '312', '', '326;311;296;310;324;338;352;366;381;367;353;339;325;340;354;368;382;396;411;397;383;369;355;341;356;370;384;398;412', '');
INSERT INTO `mountpark_data` VALUES ('8770', '236', '5', '5', '0', '-1', '1210000', '1210000', '', '', '208', '222', '', '208;223;238;193;178;149;164;179;194;209;224;210;195;180;165;150;135;120;105;91;106;121;136;151;166;181;196;182;167;152;137;122;107;92;78;93;108;123;138;153', '');
INSERT INTO `mountpark_data` VALUES ('8780', '341', '4', '4', '0', '-1', '962500', '962500', '', '', '313', '327', '', '358;343;328;313;283;268;254;240;226;241;227;242;256;271;257;272;286;301;287;302;316;330;344;329;315;300;314;299;285;284;298', '');
INSERT INTO `mountpark_data` VALUES ('8805', '314', '5', '5', '0', '-1', '1210000', '1210000', '', '', '284', '299', '', '228;242;256;270;284;298;312;326;297;283;269;255;212;241;254;268;282;296;281;267;253;239;225;211;197;168;196;210;224;238;252;266;251;237;223;209;167;181;153;138;152;166;180;194;208;222;236;221;207;193;179;165;151;137;123', '');
INSERT INTO `mountpark_data` VALUES ('8806', '324', '6', '6', '0', '-1', '1512500', '1512500', '', '', '296', '310', '', '296;281;266;252;238;224;210;196;182;168;154;169;183;197;211;225;239;253;267;282;268;254;240;226;212;198;184;199;213;227;241;255;269;283;297;311;326;312;298;284;270;242;228;214;229;243;257;271;285;299;313;327', '');
INSERT INTO `mountpark_data` VALUES ('8832', '295', '8', '8', '0', '-1', '1952500', '1952500', '', '', '267', '281', '', '357;342;327;312;297;282;252;237;222;208;194;180;166;152;138;139;125;153;167;181;195;209;223;238;224;210;196;182;168;154;140;155;169;198;212;226;240;254;268;253;239;225;211;197;183;170;184;283;269;255;241;227;213;199;185;200;214;228;242;256;270;284;298;313;299;285;271;286;300;314;328;343;329;301;315', '');
INSERT INTO `mountpark_data` VALUES ('8791', '295', '4', '4', '0', '-1', '962500', '962500', '', '', '267', '280', '', '297;282;252;237;222;208;223;238;253;268;283;269;254;239;224;209;194;180;195;210;225;240;255;241;226;211;196;181;166;152;167;182;197;212;227', '');
INSERT INTO `mountpark_data` VALUES ('8786', '294', '4', '4', '0', '-1', '962500', '962500', '', '', '266', '280', '', '296;281;251;237;252;267;282;268;253;238;223;209;224;239;254;240;225;210;195;181;196;211;226;212;197;182;167;153;168;183;198;184;169;154;139', '251;3000;1733|252;3000;1217|267;3000;1537|296;3000;1108');
INSERT INTO `mountpark_data` VALUES ('8781', '314', '3', '3', '0', '-1', '770000', '770000', '', '', '284', '299', '', '284;298;312;326;270;256;242;228;213;227;241;255;269;283;297;311;296;282;268;254;240;226;212;198;183;197;211;225;239;253;267', '');
INSERT INTO `mountpark_data` VALUES ('8776', '196', '3', '3', '0', '-1', '770000', '770000', '', '', '224', '210', '', '224;209;194;208;222;236;250;279;293;292;278;307;265;251;237;223;238;252;266;280;309;323;322;337;295;281;267;253;239;254;268;282;310;324;338', '');
INSERT INTO `mountpark_data` VALUES ('8813', '298', '4', '4', '0', '-1', '962500', '962500', '', '', '270', '284', '', '270;285;300;315;330;345;255;241;256;271;286;301;316;331;302;287;272;257;242;227;213;228;243;258;273;288;303;229;214;199', '');
INSERT INTO `mountpark_data` VALUES ('8817', '315', '3', '3', '0', '-1', '770000', '770000', '', '', '285', '300', '', '285;271;299;313;298;284;270;256;241;255;269;254;240;226;211;225;239;238;224;210;196;181;195;209;223;208;194;180', '');
INSERT INTO `mountpark_data` VALUES ('8821', '278', '4', '4', '0', '-1', '962500', '962500', '', '', '250', '264', '', '250;265;280;295;310;235;221;236;251;266;281;296;282;267;252;237;222;207;208;223;238;253;268;254;239;224;209;194;180;195;210;225', '');
INSERT INTO `mountpark_data` VALUES ('8825', '326', '3', '3', '0', '-1', '770000', '770000', '', '', '298', '312', '', '298;313;328;343;358;344;329;314;299;284;270;285;300;315;330;256;271;286;242;257;272;228;243;258;244;229;214;200;215', '');
INSERT INTO `mountpark_data` VALUES ('8829', '298', '2', '2', '0', '-1', '522500', '522500', '', '', '268', '283', '', '268;282;254;240;226;212;197;211;225;239;253;267;252;238;224;210;196;182;167;181;195;209;223;237;222', '');
INSERT INTO `mountpark_data` VALUES ('8833', '283', '2', '2', '0', '-1', '522500', '522500', '', '', '253', '268', '', '253;239;225;211;267;281;266;252;238;224;210;196;181;195;209;251;237;208;194;180;166', '');
INSERT INTO `mountpark_data` VALUES ('8837', '215', '4', '4', '0', '-1', '962500', '962500', '', '', '185', '200', '', '185;171;157;143;199;213;227;241;255;269;254;240;226;212;198;184;170;156;142;113;112;141;155;169;183;197;211;225;239;224;209;195;196;182;168;154;126;98;83;97', '');
INSERT INTO `mountpark_data` VALUES ('8838', '313', '5', '5', '0', '-1', '1210000', '1210000', '', '', '283', '298', '', '283;297;269;255;241;227;213;199;185;171;156;170;169;198;212;226;240;254;268;282;267;253;239;225;211;197;183;155;141;126;140;154;168;182;196;210;167;153;139;125', '');
INSERT INTO `mountpark_data` VALUES ('8834', '146', '2', '2', '0', '-1', '522500', '522500', '', '', '90', '118', '', '146;89;104;119;134;148;133;118;46;61;76;91;106;121;135;120;105;90;75;60;88', '');
INSERT INTO `mountpark_data` VALUES ('8830', '366', '6', '6', '0', '-1', '1512500', '1512500', '', '', '338', '352', '', '338;353;368;383;323;308;293;279;294;309;324;339;354;369;355;340;325;310;295;280;265;251;266;281;296;311;326;341;327;312;297;282;267;252;237;222;223;238;253;268;283;298;313;299;284;269;254;239;224;209;225;240;255;241;226', '');
INSERT INTO `mountpark_data` VALUES ('8826', '295', '8', '8', '0', '-1', '1952500', '1952500', '', '', '267', '281', '', '267;252;237;222;207;192;282;297;312;327;342;328;313;298;283;268;253;238;223;208;193;178;179;194;209;224;239;254;269;284;299;314;300;285;270;255;240;225;210;195;180;166;181;196;211;226;241;256;271;286;272;257;242;227;212;197;182;167;152', '');
INSERT INTO `mountpark_data` VALUES ('8822', '190', '4', '4', '0', '-1', '962500', '962500', '', '', '162', '176', '', '162;177;147;132;117;102;73;88;103;118;133;148;163;149;134;119;104;89;74;59;45;60;75;90;105;120;135;121;106;91;76;61;46', '');
INSERT INTO `mountpark_data` VALUES ('8818', '299', '4', '4', '0', '-1', '962500', '962500', '', '', '269', '284', '', '269;255;241;227;283;297;311;325;339;353;338;324;310;281;282;268;254;240;226;212;197;211;225;253;267;295;309;323;308;293;279;265;251;237;223;209;195;181;167;152;166;180;194;208;222;236;250;264', '');
INSERT INTO `mountpark_data` VALUES ('8814', '396', '6', '6', '0', '-1', '1512500', '1512500', '', '', '368', '382', '', '368;383;353;338;323;308;293;279;294;309;324;339;354;369;355;340;325;296;281;266;265;251;295;280;311;326;341;327;312;297;282;267;252;223;208;238;253;268;283;298;313;299;284;269;254;239;224;209;194', '');
INSERT INTO `mountpark_data` VALUES ('4590', '622', '5', '5', '0', '-1', '1210000', '1210000', '', '', '586', '604', '', '586;567;548;605;624;643;625;606;587;568;549;530;512;531;550;569;588;607;589;570;551;532;513;494;476;495;514;533;552;515;496;477;458;440;459;478;422;441', '');
INSERT INTO `mountpark_data` VALUES ('4586', '511', '5', '5', '0', '-1', '1210000', '1210000', '', '', '475', '493', '', '475;456;437;418;494;513;495;476;457;438;419;400;382;401;420;439;458;477;459;440;403;402;383;364;346;365;384;422;423;404;385;366;347;328;386', '');
INSERT INTO `mountpark_data` VALUES ('4605', '395', '5', '5', '0', '-1', '1210000', '1210000', '', '', '359', '377', '', '321;340;359;378;397;379;360;341;322;303;285;304;323;342;361;343;324;305;286;267;249;268;287;306;325;307;288;269;250;231;213;232;251;270;289', '');
INSERT INTO `mountpark_data` VALUES ('4600', '494', '5', '5', '0', '-1', '1210000', '1210000', '', '', '456', '475', '', '456;474;492;510;438;420;402;384;365;383;401;419;437;455;473;491;472;454;436;418;400;382;364;346;327;345;363;381;399;417;435', '');
INSERT INTO `mountpark_data` VALUES ('4596', '674', '5', '5', '0', '-1', '1210000', '1210000', '', '', '620', '647', '', '582;564;546;547;585;623;661', '');
INSERT INTO `mountpark_data` VALUES ('4606', '454', '5', '5', '0', '-1', '1210000', '1210000', '', '', '416', '435', '', '380;379;360;341;322;285;304;323;342;303;321;339;357;376;358;340;359;377;395;414;396;378;397;416;434;452;433;415', '');
INSERT INTO `mountpark_data` VALUES ('4584', '561', '5', '5', '0', '-1', '1210000', '1210000', '', '', '434', '498', '', '487;469;414;396;378', '');
INSERT INTO `mountpark_data` VALUES ('4595', '190', '5', '5', '0', '-1', '1210000', '1210000', '', '', '226', '208', '', '169;188;207;226;245;264;282;263;244;225;206;187;205;224;243;262;281;300;318;299;280;261;242;223;241;260;279;298;317;336;354;335;316;297', '');
INSERT INTO `mountpark_data` VALUES ('4624', '181', '5', '5', '0', '-1', '1210000', '1210000', '', '', '145', '163', '', '183;164;145;126;107;88;70;89;108;127;146;165;147;128;109;90;71;52;34;53;72;91;110;73;54;35;36', '');
INSERT INTO `mountpark_data` VALUES ('4625', '694', '5', '5', '0', '-1', '1210000', '1210000', '', '', '640', '667', '', '602;584;566;620', '');
INSERT INTO `mountpark_data` VALUES ('4627', '177', '5', '5', '0', '-1', '1210000', '1210000', '', '', '141', '159', '', '141;122;160;179;198;217;199;180;161;142;123;104;86;105;124;143;162;181;163;144;125;106;87;68;50;69;88;107;126;145;127;108;89;70;51;32;33;34;53;72;91;54;90', '');
INSERT INTO `mountpark_data` VALUES ('4626', '697', '5', '5', '0', '-1', '1210000', '1210000', '', '', '661', '679', '', '661;642;680;699;718;737;756;775;776;757;738;719;700;681;662;643;624;606;625;644;663;682;701;720;739;702;683;664;645;626;607;588;570;589;608;627;646;665;628;609;590;571', '');
INSERT INTO `mountpark_data` VALUES ('8753', '226', '5', '5', '0', '-1', '1210000', '1210000', '', '', '188', '207', '', '224;205;186;167;149;130;168;187;206;188;169;150;131;112;93;75;113;151;170;152;133;114;77;95;76;20;57;39;58;97;116;134;59', '');
INSERT INTO `mountpark_data` VALUES ('8754', '598', '5', '5', '0', '-1', '1210000', '1210000', '', '', '634', '616', '', '577;596;615;634;653;672;690;671;652;633;614;595;613;632;651;670;689;708;726;707;688;669;650;631;649;668;687;706;725;744', '');
INSERT INTO `mountpark_data` VALUES ('8755', '560', '5', '5', '0', '-1', '1210000', '1210000', '', '', '615', '597', '', '615;634;653;672;691;596;577;558;576;595;614;633;652;671;690;709;727;708;689;670;651;632;613;594;612;631;650;669;688;707;726', '');
INSERT INTO `mountpark_data` VALUES ('8756', '606', '5', '5', '0', '-1', '1210000', '1210000', '', '', '644', '625', '', '644;626;608;590;662;680;698;717;699;681;663;645;627;609;628;646;664;682;700;718;736;755;737;719;701;683;665;702;720;738;756;774;775;757;739', '');
INSERT INTO `mountpark_data` VALUES ('4591', '525', '5', '5', '0', '-1', '1210000', '1210000', '', '', '487', '506', '', '451;469;487;505;523;541;522;504;486;468;450;432;413;431;449;467;485;503;484;466;448;430;412;394;375;411;429;447;465', '');
INSERT INTO `mountpark_data` VALUES ('4593', '225', '5', '5', '0', '-1', '1210000', '1210000', '', '', '189', '207', '', '189;208;170;151;132;113;76;95;114;133;152;171;190;172;153;134;115;96;77;58;39;21;40;59;78;97;116;135;154;136;117;98;79;60;41;22;23;42;61;80;99', '');
INSERT INTO `mountpark_data` VALUES ('4628', '675', '5', '5', '0', '-1', '1210000', '1210000', '', '', '639', '657', '', '529;548;567;586;605;624;642;623;604;585;566;584;603;622;641;660;678;659;640;621;602;583;601;620;639;658;677;696', '');
INSERT INTO `mountpark_data` VALUES ('4620', '263', '5', '5', '0', '-1', '1210000', '1210000', '', '', '225', '244', '', '225;243;207;189;171;153;134;152;170;188;206;224;205;187;169;151;133;115;96;114;132;150', '');
INSERT INTO `mountpark_data` VALUES ('4616', '618', '5', '5', '0', '-1', '1210000', '1210000', '', '', '654', '636', '', '654;635;616;634;652;670;688;706;724;743;725;707;689;671;653;672;690;708;726;744;762;763;745;727;709;691;673;692;710;728;746', '');
INSERT INTO `mountpark_data` VALUES ('4622', '712', '5', '5', '0', '-1', '1210000', '1210000', '', '', '674', '693', '', '638;656;674;692;710;691;673;655;637;619;600;618;636;654;672;690;671;653;635;617;599;581;562;580;598;616;634;652;633;615;597;579;561;543;524;542;560;578;596;577;559;541;523;505', '');
INSERT INTO `mountpark_data` VALUES ('4621', '378', '5', '5', '0', '-1', '1210000', '1210000', '', '', '342', '360', '', '285;304;323;342;361;380;399;381;362;343;324;305;286;267;248;230;249;268;287;306;325;344;363;345;326;307;288;269;250;231;212;194;213;232;251;270;289;308;327;309;290;271;252;233;214;195', '');
INSERT INTO `mountpark_data` VALUES ('4614', '355', '5', '5', '0', '-1', '1210000', '1210000', '', '', '317', '336', '', '353;335;317;281;299;263;244;262;280;298;316;334;315;297;279;243;261;225;206;224;242;260;278;241;223;205;187;168;186;204;167;149;130', '');
INSERT INTO `mountpark_data` VALUES ('4615', '204', '5', '5', '0', '-1', '1210000', '1210000', '', '', '168', '186', '', '168;187;149;130;93;112;131;150;169;151;132;113;94;75;56;19;38;57;76;95;114;133;115;96;77;58;39;20;21;40;59;78;97;79;60;41;22;23', '');
INSERT INTO `mountpark_data` VALUES ('4592', '542', '5', '5', '0', '-1', '1210000', '1210000', '', '', '506', '524', '', '506;487;525;544;563;545;526;507;488;469;451;470;489;508;527;509;490;471;452;433;491', '');
INSERT INTO `mountpark_data` VALUES ('4589', '269', '5', '5', '0', '-1', '1210000', '1210000', '', '', '233', '251', '', '233;214;195;252;271;253;234;215;196;177;159;178;197;216;235;217;198;179;160;141;123;142;161;180;199;181;162;143;124', '');
INSERT INTO `mountpark_data` VALUES ('4583', '752', '5', '5', '0', '-1', '1210000', '1210000', '', '', '716', '734', '', '716;697;678;659;735;754;773;774;755;736;717;698;679;660;641;642;661;680;699;718;737;756;775;776;757;738;719;700;681;662;643;624;605;587;606;625;644;663;682;701;720', '');
INSERT INTO `mountpark_data` VALUES ('4598', '598', '5', '5', '0', '-1', '1210000', '1210000', '', '', '634', '616', '', '634;615;596;577;653;672;690;671;652;633;614;595;613;632;651;670;689;708;726;707;688;669;650;631;649;668;687;706;725;744;762;686;667;704;723;722', '');
INSERT INTO `mountpark_data` VALUES ('5139', '641', '5', '5', '0', '-1', '1210000', '1210000', '', '', '679', '660', '', '607;625;643;661;679;697;716;698;680;662;644;626;645;663;681;699;717;735;754;736;718;700;682;664;683;719;737;755;773;774;756;738;720;702;739;757;775;776', '');
INSERT INTO `mountpark_data` VALUES ('5136', '563', '5', '5', '0', '-1', '1210000', '1210000', '', '', '527', '545', '', '527;508;546;565;547;528;509;490;453;472;491;510;529;511;492;473;454;435;416;397;379;398;417;436;455;474;493;475;456;437;418;399;380', '');
INSERT INTO `mountpark_data` VALUES ('4930', '421', '5', '5', '0', '-1', '1210000', '1210000', '', '', '385', '403', '', '385;366;348;330;312;294;276;258;295;313;331;349;367;404;386;368;350;332;369;387;405', '');
INSERT INTO `mountpark_data` VALUES ('4607', '578', '5', '5', '0', '-1', '1210000', '1210000', '', '', '614', '596', '', '614;595;576;557;575;593;611;648;630;612;594;613;631;649;667;685;722;704;686;668;650;632;633;651;669;687;705;723;741;760;742;724;706;688;670;652;671;689;707;725', '');
INSERT INTO `mountpark_data` VALUES ('4582', '359', '5', '5', '0', '-1', '1210000', '1210000', '', '', '395', '377', '', '395;376;357;375;393;411;429;447;465;484;466;448;430;412;394;413;431;449;467;485;503;522;504;486;468;450;432;414;433;469;487;505;523;541;560;542;524;506;488;470', '');
INSERT INTO `mountpark_data` VALUES ('4599', '376', '5', '5', '0', '-1', '1210000', '1210000', '', '', '340', '358', '', '340;359;378;321;302;283;265;284;303;322;341;360;342;323;304;285;266;247;229;248;267;286;305;324;306;287;268;249;230;211;193;212;231;250;269;288;270;251;232;213;194', '');
INSERT INTO `mountpark_data` VALUES ('4601', '363', '5', '5', '0', '-1', '1210000', '1210000', '', '', '292', '328', '', '270;252;234;216', '');
INSERT INTO `mountpark_data` VALUES ('4629', '599', '5', '5', '0', '-1', '1210000', '1210000', '', '', '635', '617', '', '635;616;597;578;559;654;673;691;672;653;634;615;596;577;614;633;652;671;690;709;727;708;726;744;763', '');
INSERT INTO `mountpark_data` VALUES ('4644', '583', '5', '5', '0', '-1', '1210000', '1210000', '', '', '619', '601', '', '505;524;543;562;581;600;619;638;657;675;656;637;618;599;580;561;542;523;541;560;579;598;617;636;655;674;693;711;692;673;654;635;616;597;578;559;577;596;615;634;653;672;691;710;729;595;614;633', '');
INSERT INTO `mountpark_data` VALUES ('4646', '578', '5', '5', '0', '-1', '1210000', '1210000', '', '', '614', '596', '', '614;633;652;671;595;576;557;575;594;613;632;651;670;689;707;688;669;650;631;612;593;611;630;649;668;687;706;725;743;724;705;686;667;648;685;704;723;742', '');
INSERT INTO `mountpark_data` VALUES ('4603', '580', '5', '5', '0', '-1', '1210000', '1210000', '', '', '542', '561', '', '542;560;578;524;506;488;469;487;505;523;541;559;540;522;504;486;468;450;431;449;467;485;503;521;502;484;466;448;430', '');
INSERT INTO `mountpark_data` VALUES ('4597', '452', '5', '5', '0', '-1', '1210000', '1210000', '', '', '414', '433', '', '391;337;319;265;284;302;320;338;356;374;392;410;429;411;393;375;357;321;339;303;322;340;358;376;394;412;430;448;467;449;431;413;395;377;359;341;360;378;396;414;432;450;468;486', '');
INSERT INTO `mountpark_data` VALUES ('4609', '245', '5', '5', '0', '-1', '1210000', '1210000', '', '', '209', '227', '', '209;190;171;228;210;191;172;153;135;154;173;192;174;155;136;117;99;118;137;156;138;119;100', '');
INSERT INTO `mountpark_data` VALUES ('4931', '618', '5', '5', '0', '-1', '1210000', '1210000', '', '', '582', '600', '', '620;601;582;563;525;506;487;469;488;526;545;564;583;602;584;565;546;508;489;470;451;433;452;471;490;509;528;547;566;548;529;510;491;472;453;434;415;397;416;435;454;473;492;511;530;512;493;474;455;436;417;398;379;361;380;399;418;437;456;475;494', '');
INSERT INTO `mountpark_data` VALUES ('5127', '617', '5', '5', '0', '-1', '1210000', '1210000', '', '', '579', '598', '', '579;597;561;543;525;507;488;506;524;542;560;578;559;541;523;505;487;469;450;468;486;504;522;540;521;503;485;467;449;431;412;430;448;466;484', '');
INSERT INTO `mountpark_data` VALUES ('5133', '578', '5', '5', '0', '-1', '1210000', '1210000', '', '', '540', '559', '', '612;594;576;558;540;522;504;503;521;539;557;575;593;574;556;538;520;502;483;501;519;537', '');
INSERT INTO `mountpark_data` VALUES ('5278', '322', '5', '5', '0', '-1', '1210000', '1210000', '', '', '358', '340', '', '358;339;320;338;356;374;392;410;428;447;429;411;393;375;357;376;394;412;430;448;466;485;467;449;431;413;395;377;396;414;432;450;468;486', '');
INSERT INTO `mountpark_data` VALUES ('5334', '251', '5', '5', '0', '-1', '1210000', '1210000', '', '', '215', '233', '', '215;196;177;234;253;272;291;273;254;235;216;197;178;159;141;160;179;198;217;236;255;237;218;199;180;142;123;105;124;125;162;181;200;219;238;257;239;220;201;182;163;144;106;87;69;88;107;126;145;164;183;202', '');
INSERT INTO `mountpark_data` VALUES ('4932', '637', '5', '5', '0', '-1', '1210000', '1210000', '', '', '601', '619', '', '601;582;620;639;621;602;583;564;545;527;546;565;584;603;585;566;547;528;509', '');
INSERT INTO `mountpark_data` VALUES ('4562', '524', '5', '5', '0', '-1', '1210000', '1210000', '', '', '560', '542', '', '560;579;598;541;522;503;521;540;559;578;597;616;634;615;596;577;558;539;557;576;595;614;633;652;670;651;632;613;594;575;593;612;631;650;669;688;706;687;668;649;630;611;705;686;667;648;724', '');
INSERT INTO `mountpark_data` VALUES ('4549', '418', '5', '5', '0', '-1', '1210000', '1210000', '', '', '380', '399', '', '321;303;285;267;249;213;232;250;268;286;304;322;340;359;341;323;305;287;269;251;270;288;306;324;342;360;378;397;379;361;343;325;307;289;308;326;344;362;380;398;416', '');
INSERT INTO `mountpark_data` VALUES ('4649', '154', '5', '5', '0', '-1', '1210000', '1210000', '', '', '116', '135', '', '116;98;80;134;152;170;151;133;115;97;79;61;42;60;78;96;114;132;113;95;77;59;41;23;22;40;58;76;94;75;57;39;21;20;38;56', '');
INSERT INTO `mountpark_data` VALUES ('4647', '731', '5', '5', '0', '-1', '1210000', '1210000', '', '', '695', '713', '', '695;676;714;733;752;771;772;753;734;715;696;677;658;640;659;678;716;735;754;773;755;774;736;717;698;679;660;641;622;604;623;642;661;680;699;718;737;756;775;757;738;719;700;681;662;643;624;605;586;568;587;606;625;644;663;682;701;720', '');
INSERT INTO `mountpark_data` VALUES ('4631', '579', '5', '5', '0', '-1', '1210000', '1210000', '', '', '543', '561', '', '543;562;581;524;505;486;467;449;468;487;506;525;544;563;545;526;507;488;469;450;432;451;470;489;508;527;509;490;471;452;433', '');
INSERT INTO `mountpark_data` VALUES ('4630', '523', '5', '5', '0', '-1', '1210000', '1210000', '', '', '485', '504', '', '485;467;449;431;413;503;521;539;520;502;484;466;448;430;412;394;375;393;411;429;447;465;483;501;482;464;446;428;410;392;374;356;337;355;373;391;409;427;445;463;354;336', '');
INSERT INTO `mountpark_data` VALUES ('4633', '544', '5', '5', '0', '-1', '1210000', '1210000', '', '', '580', '562', '', '580;561;542;560;578;596;614;632;650;668;687;706;725;744;745;727;709;691;673;655;637;618;599;579;598;617;636;654;635;616;597;615;634;653;672;690;671;652;633;651;670;689;708;726;707;688;669', '');
INSERT INTO `mountpark_data` VALUES ('4640', '616', '5', '5', '0', '-1', '1210000', '1210000', '', '', '652', '634', '', '652;671;690;708;726;744;762;761;743;725;707;689;670;688;706;724;742;760;759;741;723;705;687;669;651;633;614;632;650;668;686;704;722;685;667;649;631;613', '');
INSERT INTO `mountpark_data` VALUES ('4666', '231', '5', '5', '0', '-1', '1210000', '1210000', '', '', '193', '212', '', '193;175;157;139;211;229;247;228;210;192;174;156;138;120;101;119;137;155;173;191;209;190;172;154;136;118;100;82;171;153;135;117;99;81;63;44;62;80;116;152;61', '');
INSERT INTO `mountpark_data` VALUES ('4588', '620', '5', '5', '0', '-1', '1210000', '1210000', '', '', '656', '638', '', '656;675;693;711;729;747;765;764;746;728;710;692;674;637;655;673;691;709;727;745;763;762;744;726;708;690;672;654;636;618;599;617;635;653;671;689;707;725;743;761;760;742;724;706;688;670;652;634;616;598', '');
INSERT INTO `mountpark_data` VALUES ('4934', '709', '5', '5', '0', '-1', '1210000', '1210000', '', '', '673', '691', '', '692;673;655;674;693;636;618;600;582;564;546;527;545;563;565;583;601;619;637;656;638;620;602;584;603;621;639;657;675;711', '');
INSERT INTO `mountpark_data` VALUES ('5333', '488', '5', '5', '0', '-1', '1210000', '1210000', '', '', '452', '470', '', '452;471;490;509;433;414;396;415;434;453;472;491;473;454;435;416;397;378;360;379;398;417;436;455;437;418;399;380;361', '');
INSERT INTO `mountpark_data` VALUES ('4617', '303', '5', '5', '0', '-1', '1210000', '1210000', '', '', '339', '321', '', '339;358;377;396;320;338;357;376;395;414;432;413;394;375;356;374;393;412;431;450;468;449;430;411;392;410;429;448;467', '');
INSERT INTO `mountpark_data` VALUES ('4618', '434', '5', '5', '0', '-1', '1210000', '1210000', '', '', '470', '452', '', '470;489;508;527;451;432;413;431;450;469;488;507;526;545;563;544;525;506;487;468;449;467;486;505;524;543;562;581;599;580;561;542;523;504', '');
INSERT INTO `mountpark_data` VALUES ('5280', '619', '5', '5', '0', '-1', '1210000', '1210000', '', '', '583', '601', '', '583;602;621;640;659;678;564;545;526;508;527;546;565;584;603;622;641;660;642;623;604;585;566;547;528;509;490;472;491;510;529;624;587;568;549;531;494;493;492;473;454;435;416;398;417;436;455;474;550;569;588;570;551;532;513;475;456;437;418;399;380;362;381;400;419;438;457;476;495;496;515;552;534;477;458;439;420;401;382;364;383;402;421;440;459;478;497;516;498;479;460;441;422;403;384;365', '');
INSERT INTO `mountpark_data` VALUES ('5279', '734', '5', '5', '0', '-1', '1210000', '1210000', '', '', '698', '716', '', '698;717;736;755;679;756;775;737;718;699;680;661;624;643;662;681;700;719;738;757;776;739;720;701;682;663;644;625;606;588;607;626;645;664;683', '');
INSERT INTO `mountpark_data` VALUES ('5112', '152', '5', '5', '0', '-1', '1210000', '1210000', '', '', '114', '133', '', '114;96;78;132;150;131;113;95;77;59;40;58;76;94;112;93;75;57;39;21;20;38', '');
INSERT INTO `mountpark_data` VALUES ('5111', '266', '5', '5', '0', '-1', '1210000', '1210000', '', '', '228', '247', '', '228;210;246;264;282;300;263;262;243;206;225;244;245;226;207;188;151;169;170;189;208;227;209;190;171;152;133;115;134;153;172;191', '');
INSERT INTO `mountpark_data` VALUES ('5108', '433', '5', '5', '0', '-1', '1210000', '1210000', '', '', '471', '452', '', '471;453;489;507;525;472;490;508;526;544;562;580;598;616;634;652;670;688;706;724;742;760;778;796;814;832;850;868;886;904;922;940;958;976;994;1012;1030;1048;1066;1084;1102;1120;1138;1156;1174;1192;1210;1228;1246;1264;1282;1300;1318;1336;1354;1372;1390;1408;1426;1444;1462;1480;1498;1516;1534;1552;1570;1588;1606;1624;1642;1660;1678;1696;491;509;527;545;563;581;599;617;636;618;600;582;564;546;528;510;529;547;565;583;601;619;637;655;674;656;638;620;602;584;566', '');
INSERT INTO `mountpark_data` VALUES ('4941', '396', '5', '5', '0', '-1', '1210000', '1210000', '', '', '360', '378', '', '360;341;322;303;379;398;285;304;323;342;361;380;362;343;324;305;286;267;306;325;344;326;307;288;269;250;231;213;232;251;270;289;308;290;271;252;233;214;195;177;196;215;234;253', '');
INSERT INTO `mountpark_data` VALUES ('4937', '579', '5', '5', '0', '-1', '1210000', '1210000', '', '', '615', '597', '', '615;596;614;632;650;668;686;704;722;759;741;723;705;687;669;651;633;634;652;670;688;706;724;742;760;761;743;725;707;689;671;653;690;708;726;744', '');
INSERT INTO `mountpark_data` VALUES ('4639', '245', '5', '5', '0', '-1', '1210000', '1210000', '', '', '207', '226', '', '207;189;171;153;225;243;261;279;260;242;224;206;188;170;152;134;115;133;151;169;187;205;223;241;204;186;168;150;132;114;96;77;95;113;131;149;167;130;112;94;76;58;39;57;75;93;56;38', '');
INSERT INTO `mountpark_data` VALUES ('4637', '542', '5', '5', '0', '-1', '1210000', '1210000', '', '', '578', '560', '', '578;559;540;521;597;616;653;634;615;596;577;558;539;557;576;595;614;633;652;671;689;670;651;632;613;594;575;593;612;631;650;669;688;707;725;706;687;668;649;630;611;648;667;686;705;724;743;761;742;723;704;685;760;741', '');
INSERT INTO `mountpark_data` VALUES ('4690', '716', '5', '5', '0', '-1', '1210000', '1210000', '', '', '680', '698', '', '680;699;718;661;642;623;604;586;605;624;643;662;681;644;625;606;587;568;550;569;588;607;570;551;532;514;533', '');
INSERT INTO `mountpark_data` VALUES ('4935', '566', '5', '5', '0', '-1', '1210000', '1210000', '', '', '530', '548', '', '530;511;492;473;454;435;549;568;587;606;625;644;607;588;569;550;531;512;493;474;455;436;417;399;418;437;456;475;494;513;532;551;570;533;514;495;476;457;438;419;400;381;363;382;401;420;439;458;477;496;345;364;383;402;421;440', '');
INSERT INTO `mountpark_data` VALUES ('4936', '642', '5', '5', '0', '-1', '1210000', '1210000', '', '', '606', '624', '', '606;625;644;663;682;701;587;568;549;512;531;550;569;588;607;645;664;608;589;570;551;532;513;627;646;665;495;514;533;552;571;590;609;628;477;496;515;534;553;572', '');
INSERT INTO `mountpark_data` VALUES ('5277', '570', '5', '5', '0', '-1', '1210000', '1210000', '', '', '532', '551', '', '532;514;496;478;550;568;531;513;495;477;459;440;458;476;494;512;530;511;493;475;457;439;421;402;420;438;456;474', '');
INSERT INTO `mountpark_data` VALUES ('5324', '456', '5', '5', '0', '-1', '1210000', '1210000', '', '', '420', '438', '', '420;401;382;363;326;345;364;383;402;384;365;346;327;308;290;309;328;347;366;348;329;310;291', '');
INSERT INTO `mountpark_data` VALUES ('5113', '560', '5', '5', '0', '-1', '1210000', '1210000', '', '', '596', '578', '', '596;577;558;576;594;612;630;667;685;649;631;613;595;614;632;650;668;686;704;722;759;741;723;705;687;669;651;633;634;653;671;689;707;725;743;724;706;688;670;652;615', '');
INSERT INTO `mountpark_data` VALUES ('5304', '596', '5', '5', '0', '-1', '1210000', '1210000', '', '', '632', '614', '', '632;613;594;651;670;689;707;688;669;650;631;612;630;649;668;687;706;725;743;724;705;686;667;648;685;704;723;742;761;760;741;722', '');
INSERT INTO `mountpark_data` VALUES ('5311', '598', '5', '5', '0', '-1', '1210000', '1210000', '', '', '634', '616', '', '634;615;596;653;672;690;671;652;633;614;632;651;670;689;708;726;707;688;669;650;687;706;725;744;762;743;724;705;723;742;761;760;741', '');
INSERT INTO `mountpark_data` VALUES ('5326', '531', '5', '5', '0', '-1', '1210000', '1210000', '', '', '495', '513', '', '495;514;533;476;457;438;419;400;381;363;382;401;420;439;458;477;496;459;440;421;402;383;364;345;327;346;365;384;403;422;385;366;347;328;309;291;310;329', '');
INSERT INTO `mountpark_data` VALUES ('5331', '658', '5', '5', '0', '-1', '1210000', '1210000', '', '', '620', '639', '', '620;638;656;674;602;584;566;548;529;547;565;583;601;619;637;655;636;618;600;582;564;546;528;510;509;527;545;563;581;599;617;544;526;508;489;507;525;506;488', '');
INSERT INTO `mountpark_data` VALUES ('4611', '632', '5', '5', '0', '-1', '1210000', '1210000', '', '', '670', '651', '', '670;652;634;616;688;706;725;707;689;671;653;635;654;672;690;708;726;744;763;745;727;709;691;673;692;710;728;746;764;765;747;729;711;730;748;766', '');
INSERT INTO `mountpark_data` VALUES ('4613', '329', '5', '5', '0', '-1', '1210000', '1210000', '', '', '293', '311', '', '331;312;293;256;255;236;274;313;294;275;237;218;200;219;238;257;276;295;258;239;220;201', '');
INSERT INTO `mountpark_data` VALUES ('3821', '236', '4', '4', '0', '-1', '962500', '962500', '', '', '208', '222', '', '208;193;223;224;225;240;269;298;284;210;195;180;179;165;255;270;256;241;226;211;196;181;166;151;137;152;167;182;197;212;227;242;228;213;198;183;168;153;138', '');
INSERT INTO `mountpark_data` VALUES ('3786', '300', '5', '5', '0', '-1', '1210000', '1210000', '', '', '270', '285', '', '270;284;298;312;297;282;267;252;237;222;207;193;208;223;238;253;268;283;269;254;239;224;209;194;179;165;180;195;210;225;240;255;256;241;226;211;196;181;166;151;137;152;167;182;197;212;227;242;228;213;198;183;168;153;138', '');
INSERT INTO `mountpark_data` VALUES ('9154', '598', '14', '14', '0', '-1', '3135000', '3135000', '', '', '562', '580', '', '600;581;562;543;524;505;486;467;448;429;410;391;373;392;411;430;449;468;487;506;525;544;563;582;545;526;507;488;469;450;431;412;393;374;355;337;356;375;394;413;432;451;470;489;508;527;528;509;490;471;452;433;414;395;376;357;338;319;301;320;339;358;377;396;415;434;453;472;491;510;473;454;435;416;397;378;359;340;321;302;283;265;284;303;322;341;342;361;380;399;418;437;474;456;455;436;417;398;379;323;304;285;266;343;362;324;305;286;267;248;229;211;230;249;268;287;306;325;344;326;308;289;270;288;307;193;212;231;250;232;213;194', '');
INSERT INTO `mountpark_data` VALUES ('9205', '205', '12', '12', '0', '-1', '2695000', '2695000', '', '', '177', '191', '', '177;162;147;132;117;102;192;207;222;237;223;208;193;178;163;148;133;118;103;88;74;89;104;119;134;149;164;179;194;209;195;180;165;150;135;120;105;90;75;60;46;61;76;91;106;121;136;151;166;181;167;152;137;122;107;92;77;62;47;32;18;33;63;78;93;108;123;138;153', '');
INSERT INTO `mountpark_data` VALUES ('9736', '194', '12', '12', '0', '-1', '2695000', '2695000', '', '', '224', '209', '', '224;210;196;182;168;154;238;252;266;280;294;309;295;281;267;253;239;225;211;197;183;169;184;198;212;226;240;254;268;282;296;310;324;339;325;311;297;283;269;255;241;227;213;199;214;228;242;256;270;284;298;312;326;340;354;369;355;341;327;313;299;285;271;257;243;229;244;258;272;286;300;314;328;342;356;370;384;259;273;287;301;316;302;288;274;289;303;317;331;346;332', '');
INSERT INTO `mountpark_data` VALUES ('9737', '243', '12', '12', '0', '-1', '2695000', '2695000', '', '', '271', '257', '', '271;286;300;314;328;342;356;370;384;369;355;341;327;313;299;285;256;270;284;298;312;326;340;354;339;325;311;297;283;269;255;241;226;212;198;184;240;254;268;282;296;310;281;267;253;239;225;211;197;183;169;154;168;182;196;210;224;238;252;266;251;237;223;209;195;181;167;153;139;124;138;152;166;180;194;208', '');
INSERT INTO `mountpark_data` VALUES ('9738', '109', '5', '5', '0', '-1', '1210000', '1210000', '', '', '139', '124', '', '139;125;111;153;167;181;195;209;223;237;251;265;180;194;208;193;178;164;150;165;179;182;168;154;140;126;196;210;224;238;252;266;280;295;281;267;253;239;225;211;197;183;169;155;141;198;212;226;240;254;268;282;296;310;325;311;297;283;269;255;241;227;213;228;242;256;270;284;298;312;313;327;299;285;271;257;243;258;272;286;300;314;328;315;329;343;357;371;330;344;358;372;386;345;359;373;387', '');
INSERT INTO `mountpark_data` VALUES ('9739', '388', '5', '5', '0', '-1', '1210000', '1210000', '', '', '358', '373', '', '358;372;344;330;315;329;343;357;342;328;314;300;271;257;243;285;299;313;327;341;355;369;354;340;326;312;298;284;270;256;242;228;213;227;241;255;269;283;297;311;325;339;324;310;296;254;240;226;212;198;183;197;211;225;239;253;267;281;295;309;294;280;266;252;238;224;210;196;182;168;153;167;181;195;209;223;237;251;265;279;264;250;236;222;208;194;180;166;152', '');
INSERT INTO `mountpark_data` VALUES ('3713', '212', '5', '5', '0', '-1', '1210000', '1210000', '', '', '182', '197', '', '182;196;210;168;154;140;126;111;125;139;153;167;181;195;180;166;152;138;124;110;96;137;122;107;92;106;121;136;151;165;150;135', '');
INSERT INTO `mountpark_data` VALUES ('3672', '196', '5', '5', '0', '-1', '1210000', '1210000', '', '', '166', '181', '', '166;180;194;208;152;138;123;137;151;165;164;193;178;150;136;122;108;93;107;121;135;149;163;148;134;120;106;92', '');
INSERT INTO `mountpark_data` VALUES ('8598', '374', '6', '6', '0', '-1', '1512500', '1512500', '', '', '344', '359', '', '344;358;330;316;301;315;329;343;286;271;285;299;313;284;270;256;269;255;241;227;213;199;185;170;184;198;212;226;240;254;268;253;239;225;211;197;183;169;155;168;182;196;181;167;153;138;152', '');
INSERT INTO `mountpark_data` VALUES ('8604', '337', '2', '2', '0', '-1', '522500', '522500', '', '', '365', '351', '', '365;380;395;350;335;349;364;379;394;409;423;408;393;378;363;392;407;422;437;451;436;421', '');
INSERT INTO `mountpark_data` VALUES ('8564', '168', '5', '5', '0', '-1', '1210000', '1210000', '', '', '140', '154', '', '140;155;170;185;200;125;110;96;111;126;141;156;171;186;172;157;142;127;112;97;82;68;83;98;113;128;143;158;144;129;114;99;84;69;54;55;70;85;100;115;41;56;71', '');
INSERT INTO `mountpark_data` VALUES ('8567', '323', '9', '9', '0', '-1', '2117500', '2117500', '', '', '295', '309', '', '295;310;325;340;355;280;265;250;235;221;236;251;266;281;296;311;326;341;327;312;297;282;267;252;237;222;207;193;208;223;238;253;268;283;298;313;299;284;269;254;239;224;209;194;179;165;180;166;151;152;210;196;182;168;154;140;211;197;183;169;155;170;184;198;212;226;225;240;270;256;242;257;271', '');
INSERT INTO `mountpark_data` VALUES ('8570', '299', '4', '4', '0', '-1', '962500', '962500', '', '', '269', '284', '', '269;283;297;282;267;252;237;222;207;192;178;193;208;223;238;253;268;254;239;224;209;194;179;164;150;165;180;195;210;225;240;255;241;226;211;196;181;166', '');
INSERT INTO `mountpark_data` VALUES ('8610', '236', '6', '6', '0', '-1', '1512500', '1512500', '', '', '208', '222', '', '208;223;238;253;193;178;163;148;134;149;164;179;194;209;224;239;254;269;255;240;225;210;195;180;165;150;135;120;106;121;136;151;166;181;196;211;197;182;167;152;137;122;107;92;93;108;123;138;153;168', '');
INSERT INTO `mountpark_data` VALUES ('8607', '354', '6', '6', '0', '-1', '1512500', '1512500', '', '', '326', '340', '', '326;341;327;313;299;285;271;257;242;256;270;284;298;312;311;297;283;269;255;241;227;212;226;240;254;268;282;267;253;239;225;211;196;210;224;238;252;266;251;237;223;209;195;181;166;180;194;208;222', '');
INSERT INTO `mountpark_data` VALUES ('3816', '382', '10', '10', '0', '-1', '2282500', '2282500', '', '', '354', '368', '', '354;369;384;339;324;309;294;279;265;280;295;310;325;340;355;370;356;341;326;311;296;281;266;251;237;252;267;282;297;312;327;342;328;313;298;283;268;253;238;223;209;224;239;254;269;284;299;314;300;285;270;255;240;225;210;195;181;196;211;226;241;256;271', '');
INSERT INTO `mountpark_data` VALUES ('3817', '151', '6', '6', '0', '-1', '1512500', '1512500', '', '', '121', '136', '', '121;107;93;135;149;163;177;162;148;134;120;106;92;78;63;77;91;105;119;133;147;132;118;104;90;76;62;48;33;47;61;75;89;103;117;102;88;74;60;46;32;18;17;31;45;59;73;44;30', '');
INSERT INTO `mountpark_data` VALUES ('3782', '384', '8', '8', '0', '-1', '1952500', '1952500', '', '', '356', '370', '', '356;371;341;326;311;296;281;266;251;237;252;267;282;297;312;327;342;357;343;328;313;298;283;268;239;238;223;209;224;254;269;284;299;314;329;315;300;285;270;255;240;225;210;195;181;196;211;226;241;256;271;286;301;287;272;257;242;227;212;197;182;167;153;168;183;198;213;228;243;258', '');
INSERT INTO `mountpark_data` VALUES ('3737', '324', '8', '8', '0', '-1', '1952500', '1952500', '', '', '296', '310', '', '296;281;266;251;236;311;326;341;356;342;313;298;283;268;253;238;223;222;208;328;314;299;284;269;254;239;224;209;194;180;195;210;225;240;255;270;285;286;271;256;241;226;211;196;181;166;152;167;182;197;212;227;242;257;272;258;243;228;213;198;183;168;153', '');
INSERT INTO `mountpark_data` VALUES ('3079', '353', '14', '14', '0', '-1', '3135000', '3135000', '', '', '325', '339', '', '325;340;355;370;385;310;295;280;265;250;235;221;236;251;266;281;296;311;326;341;356;371;357;342;327;312;297;282;267;252;237;222;207;193;208;223;238;253;268;283;298;313;328;343;329;314;299;284;269;254;239;224;209;194;179;165;180;195;210;225;240;255;270;285;300;315;301;286;271;256;241;226;211;196;181;166;151;287;272;257;242;227;212;197;182;167;152;137;123;138;153;168;183;198;213;199;184;169;154;139;124;109;95;110;125;140;155;170;185;171;156;141;126', '');
INSERT INTO `mountpark_data` VALUES ('2708', '251', '12', '12', '0', '-1', '2695000', '2695000', '', '', '223', '237', '', '223;208;193;178;163;148;238;253;268;283;298;284;270;256;241;226;211;196;181;166;151;136;121;106;120;135;150;165;180;195;210;225;240;255;269;254;239;224;209;194;179;164;149;134;327;341;355;313;299;285;271;257;243;229;244;258;272;286;370;385;371;357;343;329;315;301;287;273;259;274;288;302;316;330;344;358;372;386', '');
INSERT INTO `mountpark_data` VALUES ('4471', '353', '6', '6', '0', '-1', '1512500', '1512500', '', '', '325', '339', '', '325;340;355;310;295;280;265;251;266;281;296;311;326;341;327;312;297;282;267;252;237;223;238;253;268;283;298;313;299;284;269;254;239;224;209;195;210;225;240;255;270;285;271;256;241;226;211;196;181;167;182;197;227;242', '');
INSERT INTO `mountpark_data` VALUES ('4489', '127', '5', '5', '0', '-1', '1210000', '1210000', '', '', '155', '141', '', '155;140;125;170;199;184;169;154;139;124;138;153;168;183;198;213;228;242;227;212;197;182;167;152;166;181;196;211;226;241;256;270;255;240;225;210;195;180;194;209;284;298;283;254;208;222;237;252;238;312', '');
INSERT INTO `mountpark_data` VALUES ('4485', '300', '4', '4', '0', '-1', '962500', '962500', '', '', '270', '285', '', '270;256;242;228;284;298;312;326;340;354;339;325;296;297;283;269;255;241;227;213;198;212;226;240;254;310;324;309;295;281;267;253;239;225;211;197;183;182;196;210;224;238;252;266;280;294;279;265;251;237;223;209;195;181', '');
INSERT INTO `mountpark_data` VALUES ('4468', '93', '4', '4', '0', '-1', '962500', '962500', '', '', '123', '108', '', '123;109;137;151;165;179;194;180;166;152;138;124;110;125;139;153;167;181;195;209;224;210;196;182;168;154;140;155;183;197;211;225;239;212;198;184;170;185;199;213;227;241', '');
INSERT INTO `mountpark_data` VALUES ('2879', '428', '6', '6', '0', '-1', '1512500', '1512500', '', '', '400', '414', '', '400;385;415;430;445;460;461;446;431;416;401;386;371;357;372;387;402;417;432;447;462;448;433;418;403;388;373;358;343;329;344;359;374;389;404;419;434;405;390;375;360;345;330;315;301;316;331;346;361;376;347;332;317;302', '');
INSERT INTO `mountpark_data` VALUES ('2924', '219', '6', '6', '0', '-1', '1512500', '1512500', '', '', '191', '205', '', '191;206;221;176;161;146;131;102;117;132;147;162;177;192;207;193;178;163;148;133;118;103;88;73;44;59;74;89;104;119;134;149;164;179;165;150;135;120;105;90;75;60;30;16;17;32;47;76;91;106;121;136;151;137;122;107;92;77;48;18;33;63;78;93;108', '');
INSERT INTO `mountpark_data` VALUES ('3736', '323', '8', '8', '0', '-1', '1952500', '1952500', '', '', '295', '309', '', '295;310;325;340;355;280;265;250;235;221;236;251;266;281;296;311;326;341;327;312;297;282;267;252;237;222;207;193;208;223;238;253;268;283;298;313;299;284;269;254;239;224;209;194;179;165;180;195;210;225;240;255;270;285;271;256;241;226;211;196;181;166;151;137;152;167;182;197;212;227;242;257;243;228;213;198;183;168;153;138', '');
INSERT INTO `mountpark_data` VALUES ('3534', '299', '5', '5', '0', '-1', '1210000', '1210000', '', '', '271', '285', '', '271;286;256;241;226;211;196;181;167;182;197;212;227;242;257;272;258;243;228;213;198;183;168;153;139;154;169;184;199;214;229;244;230;215;200;185;170;155;140;125;111;126;141;156;171;186;201', '');
INSERT INTO `mountpark_data` VALUES ('4432', '211', '4', '4', '0', '-1', '962500', '962500', '', '', '183', '197', '', '183;198;213;228;168;153;138;124;139;154;169;184;199;214;200;185;170;155;140;125;110;96;111;126;141;156;171;186;172;157;142;127;82;143', '');
INSERT INTO `mountpark_data` VALUES ('4757', '324', '5', '5', '0', '-1', '1210000', '1210000', '', '', '296', '310', '', '296;311;326;341;281;266;251;252;267;282;297;312;327;313;298;283;268;253;238;223;209;224;239;254;269;284;299;285;256;255;240;225;210;195;181;196;211;226;241;271;257;242;227;212;197;182;167;168;183;198;228', '');
INSERT INTO `mountpark_data` VALUES ('4634', '268', '4', '4', '0', '-1', '962500', '962500', '', '', '240', '254', '', '240;225;255;241;226;211;197;212;227;228;243;213;198;183;168;139;138;124;154;169;184;199;214;229;215;186;185;170;155;126;125;110;96;111;141;156;171;201;187;172;157;142;127;112;97;143;158;173;129;144', '');
INSERT INTO `mountpark_data` VALUES ('4805', '381', '5', '5', '0', '-1', '1210000', '1210000', '', '', '353', '367', '', '353;368;338;323;309;324;339;354;340;325;310;295;281;296;311;326;312;298;284;270;256;242;228;213;227;241;255;297;282;268;254;240;226;212;198;183;197;211;225;239;253;267;252;238;224;210;196;182;168;153;167;181;195;209;223;237;222;208;194;180;166;152', '');
INSERT INTO `mountpark_data` VALUES ('4806', '342', '8', '8', '0', '-1', '1952500', '1952500', '', '', '312', '327', '', '312;326;325;339;368;354;311;297;283;269;255;241;227;228;213;353;212;183;197;211;225;239;253;267;281;295;309;323;308;294;280;266;252;238;224;210;196;182;168;153;167;181;195;209;223;237;251;265;279;293;208;194;180;166;152;138;123;137;151;165;179;193;178;164;150;136;122;108;163;149;135;121', '');
INSERT INTO `mountpark_data` VALUES ('4810', '358', '6', '6', '0', '-1', '1512500', '1512500', '', '', '328', '343', '', '328;342;356;370;314;300;286;271;285;299;313;327;341;355;340;326;312;298;256;227;198;184;185;212;226;240;254;268;282;311;310;296;170;155;169;183;197;211;225;239;253;267;281;295;280;266;252;238;224;210;196;182;168;154;125;139;153;167;181;195;209;223;237;251;265;250;236;222;208;194;180;166;152;138;124', '');
INSERT INTO `mountpark_data` VALUES ('4809', '357', '5', '5', '0', '-1', '1210000', '1210000', '', '', '327', '342', '', '327;341;355;340;325;310;295;280;265;250;236;251;266;281;296;311;326;312;297;282;267;252;237;222;208;223;238;253;268;283;298;313;299;284;269;254;239;224;209;194;180;195;210;225;240;255;270;285;271;256;241;226;211;196;181', '');
INSERT INTO `mountpark_data` VALUES ('4852', '371', '6', '6', '0', '-1', '1512500', '1512500', '', '', '341', '356', '', '341;355;327;313;299;285;271;257;242;256;270;284;298;312;326;340;325;311;297;283;269;255;241;227;212;226;240;254;268;282;296;310;324;338;323;309;295;281;267;253;239;225;211;197;182;196;210;224;238;252;266;280;294;308;265;251;237;223;209;195;181;167;152;166;180;194;208;222;236;250;235;221;207;193;179;165', '');
INSERT INTO `mountpark_data` VALUES ('4851', '256', '5', '5', '0', '-1', '1210000', '1210000', '', '', '284', '270', '', '284;299;269;254;239;224;209;194;180;166;152;208;222;236;250;264;279;265;251;237;223;195;181;167;182;196;210;238;252;266;280;294;309;295;281;267;253;225;211;197;268;282;296;310;324;339;325;311;297;283;298;312;326;340;354;369;355;341;327;313', '');
INSERT INTO `mountpark_data` VALUES ('4855', '329', '8', '8', '0', '-1', '1952500', '1952500', '', '', '299', '314', '', '299;313;312;326;355;341;298;284;270;256;242;243;228;340;325;311;297;283;269;255;241;227;213;199;184;198;212;226;240;254;268;282;296;310;295;281;267;253;239;225;211;197;183;169;154;168;182;196;210;224;238;252;266;280;265;139;124;138;152;166;180;194;208;222;236', '');
INSERT INTO `mountpark_data` VALUES ('4856', '330', '5', '5', '0', '-1', '1210000', '1210000', '', '', '300', '315', '', '300;314;328;327;356;313;299;285;271;257;258;243;228;213;198;183;168;153;138;152;167;182;197;212;227;242;286;256;241;226;211;196;181;166;151;165;180;195;210;225;240;255;270;284;269;254;239;224;209;194;179;193;208;223;238;253;268;283;298;312;297;282;267;252;237;222;207;221;236;251;266;281;296;311;326;341', '');
INSERT INTO `mountpark_data` VALUES ('3441', '327', '8', '8', '0', '-1', '1952500', '1952500', '', '', '299', '313', '', '299;314;329;315;301;287;272;257;242;227;198;183;168;153;138;124;139;154;169;184;170;155;140;125;110;96;111;126;141;156;82;97;112;127;142;137;122;107;92;77;152;167;182;197;212;286;271;256;241;226;211;196;181;166;151;136;121;106;91;105;120;135;150;165;180;195;210;225;240;255;270;285;300;284;269;254;239;224;119;134;149;164;179;208;237;252;267;268;282;222;236;251;266;281;296;310;295;280;265;250;264;279;294;309', '');
INSERT INTO `mountpark_data` VALUES ('3440', '338', '6', '6', '0', '-1', '1512500', '1512500', '', '', '310', '324', '', '310;325;340;355;295;280;251;236;235;221;266;281;296;311;326;341;327;312;297;282;267;252;237;222;207;193;208;223;238;253;268;283;298;313;299;284;269;179;165;180;195;210;211;226;241;256;285;271;196;181;166;151;137;152;167;182;197;212;227;242;257;243;228;213;198;183;168;153;138', '');
INSERT INTO `mountpark_data` VALUES ('3479', '338', '8', '8', '0', '-1', '1952500', '1952500', '', '', '310', '324', '', '310;325;340;355;370;385;295;280;265;250;236;251;266;281;296;311;326;341;356;371;357;342;327;312;297;282;267;252;237;222;208;223;238;343;329;314;299;284;269;254;225;210;195;194;180;240;255;270;285;300;315;301;286;271;256;241;226;211;196;181;166;152;167;182;197;212;227;242;257;272;287;273;258;243;228;213;198;183;168;153;138;124;139;154;169;184;170;155;140;126;141;142;127', '');
INSERT INTO `mountpark_data` VALUES ('3480', '338', '8', '8', '0', '-1', '1952500', '1952500', '', '', '310', '324', '', '310;325;340;355;295;280;265;250;236;251;266;281;296;311;326;341;327;312;297;282;267;252;237;222;207;192;177;162;148;163;178;193;208;223;238;253;268;283;298;313;299;284;269;254;239;224;209;194;179;164;149;134;120;135;150;165;180;195;210;225;240;255;270;285;271;256;241;226;181;166;151;136;121;106;92;107;122;137;152;167;168;183;198;227;242;257;243;228;213;153;138;123;108;93;78;124;139;154;169;184;170;155;140;125;110;96;111;126;141', '');
INSERT INTO `mountpark_data` VALUES ('3187', '268', '6', '6', '0', '-1', '1512500', '1512500', '', '', '296', '282', '', '296;311;326;341;355;340;325;310;295;280;294;309;324;339;354;369;383;368;353;338;323;308;322;337;352;367;382;397;411;396;381;366;351;336;350;365;380;395;410;425;439;424;409;364;378;393;422;437;452;453;407', '');
INSERT INTO `mountpark_data` VALUES ('3142', '140', '6', '6', '0', '-1', '1512500', '1512500', '', '', '168', '154', '', '168;153;183;198;213;227;212;197;182;167;152;137;151;166;181;196;211;226;241;255;240;225;210;195;180;165;179;194;209;224;239;254;269;283;268;253;238;223;208;193;207;222;237;252;267;282;297;311;296;281;266;251;236;221;265;280;295;309;294;279;293;308', '');
INSERT INTO `mountpark_data` VALUES ('3312', '365', '5', '5', '0', '-1', '1210000', '1210000', '', '', '337', '351', '', '337;352;322;308;323;338;324;309;294;265;250;235;280;295;310;325;340;355;370;356;341;326;311;296;281;266;251;236;221;207;222;237;252;267;282;297;312;327;342;328;313;298;283;268;253;238;223;208;193;194;179;209;224;239;254;269;284;299;270;255;240;225;210;195;180', '');
INSERT INTO `mountpark_data` VALUES ('8582', '400', '5', '5', '0', '-1', '1210000', '1210000', '', '', '370', '385', '', '370;384;356;327;313;299;285;271;257;258;243;341;355;369;354;340;326;312;298;284;270;256;242;228;213;227;241;255;269;283;297;311;325;339;324;310;296;282;268;254;240;226;212;198;225;239;253;267;281;295;309;294;280;266;252;238;224;210;195;209;223;237;251;265', '');
INSERT INTO `mountpark_data` VALUES ('3664', '353', '6', '6', '0', '-1', '1512500', '1512500', '', '', '325', '339', '', '325;340;355;310;295;280;265;250;235;221;236;251;266;281;296;311;326;341;327;312;297;282;267;252;237;222;207;193;208;223;238;253;268;283;298;313;299;284;269;254;239;224;209;194;179;165;180;195;210;225;240;255;285;271;256;241;226;211;196;151;137;152;167;182;197;212;227;242;257;243;228;213;198;183;168;153;138', '');
INSERT INTO `mountpark_data` VALUES ('9467', '221', '3', '3', '0', '-1', '500000', '500000', '', '', '206', '221', '', '132;147;177;192;146;161;176;191;206;220;205;190;175;160', '');
INSERT INTO `mountpark_data` VALUES ('8760', '753', '3', '3', '0', '-1', '770000', '770000', '', '-1016', '717', '735', '699;7733;2379|736;7590;2379', '660;642;624;606;588;570;589;607;625;643;661;679;698;680;662;644;626;608;627;645;663;681;699;717;736;718;700;682;646;665;683;701;719;737;755;774;756;738;720;702', '699;3000;2406|736;3000;2875');
INSERT INTO `mountpark_data` VALUES ('2221', '242', '5', '5', '0', '-1', '1210000', '1210000', '', '', '206', '224', '', '206;187;168;149;130;111;74;93;112;131;150;169;188;170;151;132;113;94;75;56;37;19;38;57;76;95;114;133;152;134;115;96;77;58;39;20;1;2;21;40;59;78;97;116;98;79;60;41;22;3;4;23;42;61;80;62;43;24;5;44;25', '');
INSERT INTO `mountpark_data` VALUES ('4308', '507', '5', '5', '0', '-1', '1210000', '1210000', '', '', '543', '525', '', '486;504;522;540;558;576;595;577;559;541;523;505;524;542;560;578;596;614;633;615;597;579;561;543;562;580;598;616;634;652;671;690;672;653;635;654;636;617;599;618;600;581', '');
INSERT INTO `mountpark_data` VALUES ('8848', '549', '10', '10', '0', '-1', '2282500', '2282500', '', '', '511', '530', '120;7616;1|172;7616;1|215;7616;1|231;7616;1|311;7616;1|340;7616;1|363;7616;1|490;7616;1', '', '120;500;999998472|172;500;999990541|215;500;999998548|231;500;999997512|311;500;999999585|340;500;999989132|363;500;999998700|490;500;999994826');
INSERT INTO `mountpark_data` VALUES ('8744', '561', '10', '10', '0', '-1', '2282500', '2282500', '', '', '543', '561', '250;7741;1|362;7741;1|378;7741;1|383;7741;1|412;7741;1|492;7741;1|513;7741;1|545;7741;1', '', '250;500;999992200|362;500;999992578|378;500;999996070|383;500;999990645|412;500;999996720|492;500;999991476|513;500;999996190|545;500;999996542');
INSERT INTO `mountpark_data` VALUES ('8743', '586', '10', '10', '0', '-1', '2282500', '2282500', '', '', '567', '586', '', '', '268;500;999998086|320;500;999994690|327;500;999997815|415;500;999986083|454;500;999998185|457;500;999999493|486;500;999998597|583;500;999996433');
INSERT INTO `mountpark_data` VALUES ('9748', '356', '14', '14', '0', '-1', '3135000', '3135000', '', '', '326', '341', '', '242;256;270;284;298;312;326;340;354;368;382;396;410;424;409;395;381;367;353;339;325;311;297;283;269;255;241;227;212;226;240;254;268;282;296;310;324;338;352;366;380;394;351;337;323;309;295;281;267;253;239;225;211;197;182;196;210;224;238;252;266;280;294;308;322;336;293;279;265;251;237;223;209;195;181;167;152;166;180;194;208;222;236;250;264;278;263;249;235;221;207;193;179;165;151;137;192;177;162;176;190;204;218;233;219;205;191;206;220;234', '');
INSERT INTO `mountpark_data` VALUES ('9747', '78', '12', '12', '0', '-1', '2695000', '2695000', '', '', '106', '92', '', '91;105;119;133;147;162;176;148;134;120;106;121;135;149;163;177;192;178;164;150;136;137;123;109;95;151;165;179;193;207;221;250;236;222;208;194;180;166;152;138;124;110;125;139;153;167;181;195;209;223;237;251;265;280;266;252;238;224;210;140;154;168;182;169;155;170;184;239;253;267;281;295;310;296;282;268', '');
INSERT INTO `mountpark_data` VALUES ('9746', '169', '12', '12', '0', '-1', '2695000', '2695000', '', '', '197', '183', '', '197;182;167;152;137;122;212;227;242;257;272;287;301;286;271;256;241;226;211;196;181;166;151;136;150;165;180;195;210;225;240;255;270;285;300;315;329;314;299;284;269;254;239;224;209;194;179;164;178;193;208;223;238;253;268;283;298;343;328;342;356;371;357;297;282;267;252;237;222;207;192;206;221;236;251;266;281;296;311', '');
INSERT INTO `mountpark_data` VALUES ('8747', '615', '10', '10', '0', '-1', '2282500', '2282500', '', '', '579', '597', '228;7596;1|253;7596;1|269;7596;1|340;7596;1|380;7596;1|401;7596;1|467;7596;1|581;7596;1', '', '228;500;999996502|253;500;999985506|269;500;999990912|340;500;999985719|380;500;999992507|401;500;999991850|467;500;999993469|581;500;999993135');
INSERT INTO `mountpark_data` VALUES ('9745', '168', '8', '8', '0', '-1', '1952500', '1952500', '', '', '196', '182', '', '181;166;151;136;121;196;211;226;241;256;271;286;301;316;330;315;300;285;270;255;240;225;210;195;180;165;150;135;149;164;179;194;209;224;239;254;269;284;299;314;329;344;358;343;328;313;298;283;268;253;238;223;208;193;178;163;177;192;207;222;237;252;267;282;297;312;327;342;357;372;386;371;356;341;326;311;296;281;266;251;265;280;295;310;325;340;355;370;385;400;414;399;384;369;354;339;324;309;294', '');
INSERT INTO `mountpark_data` VALUES ('9744', '164', '7', '7', '0', '-1', '1173500', '1173500', '', '', '194', '179', '', '166;180;194;208;222;236;251;237;223;209;195;181;196;210;224;238;252;266;281;267;253;239;225;211;212;198;184;170;226;240;254;268;282;296;310;324;338;352;367;353;339;325;311;297;283;269;255;241;227;213;199;185;200;214;228;242;256;270;284;298;312;326;340;354;368;382;397;383;369;355;341;327;313;299;314;328;342;356;370;384;398;412;427;413;399;385;371;357;343;329;344;358;372;386;400', '');
INSERT INTO `mountpark_data` VALUES ('9743', '208', '8', '8', '0', '-1', '1952500', '1952500', '', '', '238', '223', '', '196;210;224;238;252;266;280;294;309;295;281;267;253;239;225;211;226;240;254;268;282;296;310;324;339;325;311;297;283;269;255;241;256;270;284;298;312;326;340;354;355;341;327;313;299;285;271;286;300;314;328;342;356;370;384;399;385;371;357;343;329;301;358;372;386;400;414;429;415;401;387;373;359;360;374;388;402;416;430;431;417;403;389;375;404;418', '');
INSERT INTO `mountpark_data` VALUES ('9740', '178', '5', '5', '0', '-1', '1210000', '1210000', '', '', '208', '193', '', '180;194;208;222;236;251;237;223;209;195;210;224;238;252;266;295;309;323;281;267;253;239;225;211;197;183;198;212;226;240;296;310;324;338;353;339;325;311;297;298;299;270;241;227;213;228;242;256;313;327;341;355;369;368;354;340;326;312;383;285;271;257;243;300;314;328;342;356;371;357;343;329;315;330;344;358;372', '');
INSERT INTO `mountpark_data` VALUES ('9741', '298', '5', '5', '0', '-1', '1210000', '1210000', '', '', '268', '283', '', '198;212;226;240;254;268;282;296;310;324;338;323;309;295;281;267;253;239;225;211;197;183;168;182;196;210;224;238;252;266;280;294;308;293;279;265;251;237;223;209;195;181;167;153;138;124;110;152;166;180;194;278;264;250;236;222;95;80;94;109;123;137;151;165;179;164;135;106;91;76;61;90;105;62;77;92;107;122;207;221;235;249;263;248;234;220;206;192;177;191;205;219;233;190;176;162;161;175;160;146;132;118;103;117;131;102', '');
INSERT INTO `mountpark_data` VALUES ('9728', '359', '14', '14', '0', '-1', '3135000', '3135000', '', '', '329', '344', '140;7776;5505|152;7757;5505|155;7776;3636|166;7757;5505|222;7741;5505|236;7741;5505|272;7595;5505|287;7595;5505|308;7695;5505|323;7695;5505|356;7616;5505|371;7616;5505', '329;343;357;371;315;301;287;272;286;300;314;328;342;356;341;327;313;299;285;271;257;242;256;270;284;298;312;326;340;354;368;353;339;325;311;297;283;269;255;241;227;198;184;170;212;226;240;254;268;282;296;310;324;338;323;309;295;281;267;253;239;225;211;197;183;169;155;140;154;168;182;196;210;224;238;252;266;280;294;308;251;237;223;209;195;181;167;152;166;180;194;208;222;236;221;207;193;179;165;151;137;122;136;150;164;178;192;206;177;163;149;135;121', '140;3000;1537|152;3000;2754|155;3000;1949|166;3000;2428|222;3000;2717|236;3000;2872|272;3000;2639|287;3000;2657|299;3000;1428|308;3000;1928|323;3000;2170|356;3000;2757|371;3000;2772');
INSERT INTO `mountpark_data` VALUES ('9742', '214', '12', '12', '0', '-1', '2695000', '2695000', '', '', '242', '228', '', '242;227;212;257;272;287;301;286;271;256;241;226;240;255;270;285;300;315;329;314;299;284;269;254;253;238;223;268;283;298;313;328;343;357;342;327;312;297;282;267;252;237;251;266;281;296;311;326;341;356;371;386;401;416;430;415;400;385;325;310;295;280;265;279;294;309;324;339;340;399;414;429;444;458;443;428;413;412;411;382;353;338;323;308;293;307;322;337;352;367;426;441;456;457;442;427;396;381;366;351;321;380;395;410;425;440;455;439;424;409;394;408;423;438', '');
INSERT INTO `mountpark_data` VALUES ('9732', '112', '12', '12', '0', '-1', '2695000', '2695000', '', '', '155', '141', '', '155;140;170;185;199;184;169;154;168;183;198;213;242;257;272;287;227;212;197;182;167;152;137;122;107;121;136;151;166;181;196;211;226;241;256;271;286;301;315;300;285;270;135;150;165;180;195;210;225;240;314;329;299;254;239;224;209;194;179;164;149;163;178;193;208;223;238;253;282;297;312;327;328;343;357;342;267;252;237;222;207;192;177;191;206;221;236;251;266;281;296;311;326;341;356;371;385;370;355;340;325;310;295;280;265;250;235;220', '');
INSERT INTO `mountpark_data` VALUES ('8746', '208', '10', '10', '0', '-1', '2282500', '2282500', '', '', '246', '227', '249;7695;1|325;7695;1|356;7695;1|377;7695;1|455;7695;1|526;7695;1|548;7695;1|560;7695;1', '', '249;500;999988814|325;500;999996034|356;500;999995692|377;500;999997419|455;500;999971190|526;500;999996852|548;500;999990379|560;500;999993648');
INSERT INTO `mountpark_data` VALUES ('9733', '345', '5', '5', '0', '-1', '1210000', '1210000', '', '', '315', '330', '', '315;329;301;287;272;286;300;314;299;285;271;257;242;256;270;284;269;255;241;227;212;226;240;254;268;282;296;281;267;253;239;225;211;197;182;196;210;224;238;252;266;251;237;223;209;195;181;167;194;208;222;236;221;207;193;179;164;178;192;206;191;177;163', '');
INSERT INTO `mountpark_data` VALUES ('9734', '279', '5', '5', '0', '-1', '1210000', '1210000', '', '', '251', '265', '', '251;266;281;296;311;236;221;206;191;177;192;207;222;237;252;267;282;297;283;268;253;238;223;208;193;178;163;149;164;179;194;209;224;239;254;269;255;241;240;225;210;195;180;165;150;135;121;136;151;166;196;152;137;122;107;93;108;123', '');
INSERT INTO `mountpark_data` VALUES ('8745', '235', '10', '10', '0', '-1', '2282500', '2282500', '', '', '271', '253', '250;7784;1|266;7784;1|364;7784;1|379;7784;1|412;7784;1|507;7784;1|510;7784;1|587;7784;1', '', '250;500;999993958|266;500;999995485|364;500;999993067|379;500;999975533|412;500;999993424|507;500;999993694|510;500;999997336|587;500;999998122');
INSERT INTO `mountpark_data` VALUES ('9735', '342', '5', '5', '0', '-1', '1210000', '1210000', '', '', '312', '327', '', '312;298;284;270;256;242;326;340;354;368;382;367;353;339;325;311;297;283;269;255;241;227;212;226;240;254;268;282;296;310;324;338;352;337;323;309;295;281;267;253;239;225;211;197;182;196;210;224;238;252;266;280;294;308;322;307;279;265;251;237;223;209;195;181;167;180;194;208;222;236;221;207;193;179;165;150;164;178;192;206;191;177;163', '');
INSERT INTO `mountpark_data` VALUES ('9356', '399', '5', '5', '0', '-1', '1210000', '1210000', '', '', '369', '384', '', '262;248;234;220;206;192;178;193;207;221;235;249;263;277;292;278;264;250;236;222;208;223;237;251;265;279;293;307;322;308;294;280;266;252;238;253;267;281;295;309;323;337;352;338;324;310;296;282;268;283;297;311;325;339;353;367;382;368;354;340;326;312;298;313;327;341;355;369;383;397', '');
INSERT INTO `mountpark_data` VALUES ('8752', '602', '10', '10', '0', '-1', '2282500', '2282500', '', '', '564', '602', '210;7784;1|232;7784;1|341;7784;1|420;7784;1|449;7784;1|473;7784;1|562;7784;1', '', '210;500;999994696|232;500;999997213|341;500;999997608|420;500;999986374|449;500;999998430|473;500;999998687|562;500;999996044');
INSERT INTO `mountpark_data` VALUES ('9357', '294', '5', '5', '0', '-1', '1210000', '1210000', '', '', '324', '309', '', '254;268;282;296;310;324;338;352;366;380;394;409;395;381;367;353;339;325;311;297;283;269;284;298;312;326;340;354;368;382;396;410;424;439;425;411;397;383;369;355;341;327;313;299;314;328;342;356;370;384;398;412;426;440;454;455;441;427;413;385;371;357;343;329;344;358;372;386;400;414;428;442;456;443;429;387;359', '');
INSERT INTO `mountpark_data` VALUES ('9354', '264', '12', '12', '0', '-1', '2695000', '2695000', '', '', '236', '250', '', '251;266;252;238;224;209;194;179;164;149;163;177;191;206;236;223;237;222;208;193;207;192;178;165;151;137;123;138;152;166;180;195;181;167;153;168;183;197;198;212;226;240;254;268;296;310;324;339;325;311;297;283;255;227;213;228;242;256;270;284;312;326;354;221;281', '');
INSERT INTO `mountpark_data` VALUES ('9353', '324', '8', '8', '0', '-1', '1952500', '1952500', '', '', '296', '310', '', '296;311;326;341;356;371;281;266;251;236;221;207;222;237;252;267;282;297;312;327;342;357;343;328;313;298;283;268;253;223;208;193;179;194;254;269;284;299;314;329;315;300;285;270;255;240;226;212;183;182;181;195;180;165;151;166;241;256;271;286;301;287;272;257;242;227;167;152;137;123;138;153;168;198;213;199;184;169;154;139;124;109;95;110;125;140;155;170', '');
INSERT INTO `mountpark_data` VALUES ('9355', '308', '5', '5', '0', '-1', '1210000', '1210000', '', '', '280', '294', '', '280;265;250;235;295;310;325;340;355;341;326;311;296;281;266;251;236;221;207;222;237;252;267;282;297;312;327;313;298;283;268;253;238;223;208;193;179;194;209;224;239;254;269;284;299;210;195;180;165;151;166;181;196;182;167;152;137;123;138;153', '');
INSERT INTO `mountpark_data` VALUES ('9358', '252', '5', '5', '0', '-1', '1210000', '1210000', '', '', '282', '267', '', '282;268;254;240;296;310;324;339;325;311;297;283;269;255;270;284;298;312;326;340;354;355;341;327;313;299;285;300;314;328;342;356;370;399;385;371;357;343;329;315;330;344;358;372;386;400;414;429;415;401;387;373;359;345;360;374;388;402;416;430;444;459;445;431;417;403;375;404;418;432;446;460', '');
INSERT INTO `mountpark_data` VALUES ('9352', '112', '8', '8', '0', '-1', '1952500', '1952500', '', '', '140', '126', '', '140;125;155;170;184;169;154;139;153;168;183;198;212;197;182;167;181;196;211;226;240;225;210;195;209;224;239;254;268;253;238;223;208;193;178;163;177;192;207;222;237;252;267;282;311;326;341;356;296;281;266;251;236;221;206;191;250;265;280;295;310;325;340;355;370;384;369;354;339;324;309;294;279;264;293;308;323;338;353;368;383', '');
INSERT INTO `mountpark_data` VALUES ('9729', '355', '5', '5', '0', '-1', '1210000', '1210000', '', '', '327', '341', '', '327;342;357;312;297;282;267;252;237;222;207;221;236;251;266;281;296;310;295;280;265;250;235;249;264;279;294;309;324;208;193;223;238;253;268;283;298;313;328;343;329;314;299;284;269;254;239;224;209;194;179;165;180;195;210;225;240;255;270;285;300;315;301;286;271;256;241;226;211;196;181;166;151;137;152;167;182;197;183;168;153;138;123;109;124;139;154;169;155;140;125', '');
INSERT INTO `mountpark_data` VALUES ('9730', '282', '5', '5', '0', '-1', '1210000', '1210000', '', '', '252', '267', '', '252;266;280;294;308;238;224;210;196;182;168;153;167;181;195;209;223;237;251;265;279;293;307;292;278;264;250;236;222;208;194;180;166;152;138;123;137;151;165;179;193;207;221;235;249;263;277;262;248;234;220;206;192;178;164;150;136;122;108;93;107;121;135;149;163;177;191;205;219;233;247;218;204;190;176;162;148;133;147;161;175;189;160;146;132;118;103;117;131;102', '');
INSERT INTO `mountpark_data` VALUES ('9731', '279', '12', '12', '0', '-1', '2695000', '2695000', '', '', '251', '265', '', '251;266;281;282;311;236;221;206;191;176;147;146;131;132;162;177;192;207;222;237;252;267;297;283;268;253;238;223;208;193;178;163;148;133;118;103;74;89;104;119;134;149;164;179;194;209;224;239;254;269;255;240;225;210;195;180;165;150;135;120;105;90;75;60;45;59;31;46;61;76;91;106;121;136;151;166;181;196;211;226;241;167;152;137;122;108;123;138;153;139;124;109', '');
INSERT INTO `mountpark_data` VALUES ('9726', '343', '8', '8', '0', '-1', '1952500', '1952500', '', '', '313', '328', '', '313;299;285;271;327;341;355;369;354;340;326;312;298;284;270;256;242;227;241;255;269;283;297;311;325;339;324;310;296;282;268;254;240;226;212;197;211;225;239;253;267;281;295;309;294;280;266;252;238;224;210;196;182;167;181;195;209;223;237;251;265;279;264;250;236;222;208;194;180;166', '');
INSERT INTO `mountpark_data` VALUES ('10249', '164', '5', '5', '0', '-1', '1210000', '1210000', '', '', '194', '179', '', '194;180;166;208;222;236;251;237;223;209;195;181;196;210;224;238;252;266;281;267;253;239;225;211;212;198;184;170;226;240;254;268;282;296;310;324;338;352;367;353;339;325;311;297;283;269;255;241;227;213;199;185;200;214;228;242;256;270;284;298;312;326;340;354;368;382;397;383;369;355;341;327;313;299;314;328;342;356;370;384;398;412;427;413;399;385;371;357;343;329;344;358;372;386;400', '');
INSERT INTO `mountpark_data` VALUES ('9349', '354', '12', '12', '0', '-1', '2695000', '2695000', '', '', '326', '340', '', '326;341;327;313;299;285;271;257;242;256;270;284;298;312;311;297;283;269;255;241;227;213;198;212;226;240;254;268;282;296;310;324;338;323;309;295;281;267;253;239;225;211;197;183;168;182;196;210;224;238;252;266;280;294;308;293;279;265;251;237;223;209;195;181;167;153;138;152;278;264;250;236;222;208;194;180', '');
INSERT INTO `mountpark_data` VALUES ('9350', '125', '5', '5', '0', '-1', '1210000', '1210000', '', '', '153', '139', '', '153;138;168;183;198;213;228;242;227;212;197;182;167;152;166;181;196;211;226;241;256;271;286;300;285;270;255;240;225;210;195;180;194;209;224;239;254;269;284;299;314;328;313;298;283;268;253;238;223;208;222;237;252;267;282;297;312;327;342;356;341;326;311;296;281;266;251;236;250;265;280;295;310;325;340;355', '');
INSERT INTO `mountpark_data` VALUES ('9346', '226', '8', '8', '0', '-1', '1952500', '1952500', '', '', '254', '240', '', '254;239;224;209;194;269;284;299;314;328;313;298;283;268;253;238;223;208;222;237;252;267;282;297;312;327;342;356;341;326;311;296;281;266;251;236;250;265;280;295;310;325;340;355;370;384;369;354;339;324;309;294;279;264;278;293;308;323;338;353;368;383', '');
INSERT INTO `mountpark_data` VALUES ('9345', '226', '7', '7', '0', '-1', '1173500', '1173500', '', '', '254', '240', '', '254;239;224;209;194;269;284;299;314;328;313;298;283;268;253;238;223;208;222;237;252;267;282;297;312;327;342;356;341;326;311;296;281;266;251;236;250;265;280;295;310;325;340;355;370;384;369;354;339;324;338;353;368;383;398;412;397;382;367;352;366;381;396;411;426;440;425;410;395', '');
INSERT INTO `mountpark_data` VALUES ('9725', '199', '12', '12', '0', '-1', '2695000', '2695000', '', '', '227', '213', '', '227;212;197;182;242;257;272;287;301;286;271;256;241;226;211;196;195;210;225;240;255;270;285;300;315;329;314;299;284;269;254;239;224;209;223;238;253;268;283;298;313;328;343;357;342;327;312;297;282;267;252;237;251;266;281;296;311;326;341;356;371;385;370;355;340;325;310;295;280;265;279;294;309;324;339;354;369;384;399;413;398;383;368;353;338;323;308', '');
INSERT INTO `mountpark_data` VALUES ('9342', '327', '4', '4', '0', '-1', '962500', '962500', '', '', '297', '312', '', '297;311;325;283;269;254;268;282;296;310;295;281;267;253;239;224;238;252;266;280;265;251;237;223;209;194;208;222;236;250;235;221;207;193', '');
INSERT INTO `mountpark_data` VALUES ('10561', '380', '5', '5', '0', '-1', '1210000', '1210000', '', '', '352', '0', '', '352;337;322;367;382;368;353;338;323;308;294;309;324;339;354;340;325;310;295;280;251;236;221;206;191;266;281;296;311;326;341;356;371;386;372;357;342;327;312;297;282;267;252;237;222;207;192;177;193;208;223;238;253;268;283;298;313;328;343;358;344;329;314;299;284;269;254;239;224;209;194;179;165;180;195;210;225;240;255;270;285;300;315;330;256;241;226;211;196;181;166;151;122;107;137;152;167;182;197;212;227;242;228;213;198;183;168;154;169;184;199;214;200;185;170;155', '');
INSERT INTO `mountpark_data` VALUES ('3673', '149', '5', '5', '0', '-1', '1210000', '1210000', '', '', '179', '164', '', '249;235;221;207;193;179;165;151;137;123;109;124;166;180;194;208;222;236;250;264;279;265;251;237;223;209;195;181;167;139;168;182;196;210;224;238;252;280;294;309;295;310;324;339;325;267;253;239;225;211;197;183;169;184;198;212;226;240;254;282;297;283;269;255;241;227;213;199;270;284;298;312;327;313;299;285;300;314;328;342;357;343;329;315', '');
INSERT INTO `mountpark_data` VALUES ('3113', '235', '5', '5', '0', '-1', '1210000', '1210000', '', '', '207', '221', '', '177;192;207;222;237;252;267;282;297;283;298;284;270;256;242;268;253;238;223;208;193;178;163;149;164;179;194;209;224;239;254;269;255;240;225;210;195;180;165;150;135;121;136;151;166;181;196;211;182;167;152;137;122;107', '');
INSERT INTO `mountpark_data` VALUES ('3328', '367', '10', '10', '0', '-1', '2282500', '2282500', '', '', '353', '367', '', '383;368;353;338;323;308;293;279;294;309;324;339;354;369;355;340;310;295;280;265;251;266;281;296;311;326;341;327;312;297;267;252;237;223;238;253;268;283;298;299;284;269;224;209;254;195;210;225;240;255;270;285;271;256;241;226;211;181;151;137;152;167;182;212;227;242;257;243;228;213;198;183;168;153;138;123;139;154;169;184;199;214;229', '');
INSERT INTO `mountpark_data` VALUES ('3367', '368', '5', '5', '0', '-1', '1210000', '1210000', '', '', '340', '354', '', '220;206;192;178;164;150;136;122;137;151;165;179;193;207;221;235;250;236;222;194;208;180;166;152;167;153;139;125;181;195;209;265;251;237;223;140;154;168;182;196;210;224;238;252;280;295;281;267;253;239;225;211;197;183;169;155;170;184;198;212;226;240;254;268;282;296;310;325;311;297;283;269;255;241;227;213;199;200;214;228;242;256;270;284;312;326;340;355;341;327;313;299;285;271;257;272;286;300;314;328;342;356;370;385;357;371;315;287', '');
INSERT INTO `mountpark_data` VALUES ('10559', '380', '5', '5', '0', '-1', '1210000', '1210000', '', '', '352', '366', '', '322;337;352;367;353;338;323;308;294;309;324;339;280;295;310;325;340;355;370;385;400;386;371;356;341;326;311;296;281;266;252;267;282;297;312;327;342;357;372;358;343;328;313;298;283;268;253;238;223;208;193;178;163;149;164;179;194;209;224;239;254;269;284;299;314;329;344;315;330;300;285;270;255;240;225;210;195;180;165;150;135;121;136;151;166;181;196;211;226;241;256;271;286;301;316;182;197;212;227;213;199;198;184;169;183;168', '');
INSERT INTO `mountpark_data` VALUES ('10557', '287', '5', '5', '0', '-1', '1210000', '1210000', '', '', '257', '272', '', '369;354;339;325;340;355;341;326;311;297;312;327;313;298;283;269;284;299;285;270;255;240;225;210;195;180;165;179;194;208;193;207;222;236;221;235;250;264;249;271;256;241;226;211;196;181;166;151;137;152;167;182;197;212;227;242;257;243;228;213;198;183;168;153;138;123;109;124;139;154;169;184;199;214;229', '');
INSERT INTO `mountpark_data` VALUES ('10554', '374', '5', '5', '0', '-1', '1210000', '1210000', '', '', '344', '359', '', '344;358;330;315;329;343;328;314;300;271;257;243;229;285;299;313;327;341;355;369;383;397;382;368;354;340;326;312;298;284;270;256;242;228;214;199;213;227;241;255;269;283;297;311;325;339;353;367;352;338;324;310;296;282;268;254;240;226;212;198;184;169;183;197;211;225;239;253;267;281;295;309;323;337;252;238;224;210;196;182;168;154;139;153;167;181;195;209;223;237;222;208;194;180;166;152;138;124;109;123;137;151;165;179;193;207;192;178;164;150;135;149;163;177;162;148;134', '');
INSERT INTO `mountpark_data` VALUES ('10602', '365', '5', '5', '0', '-1', '1210000', '1210000', '', '', '337', '0', '', '337;352;322;308;323;338;324;309;294;280;295;310;311;326;341;356;296;281;266;251;236;221;206;191;177;192;207;222;237;252;267;282;297;312;327;342;328;313;298;283;268;253;238;223;208;193;178;163;149;164;179;194;209;224;239;254;269;284;299;314;300;285;270;255;240;225;210;195;180;165;150;135;121;136;151;166;181;196;211;226;241;256;271;286;272;257;242;227;212;197;182;167;152;137;122;107;93;108;123;138;153;168;183;198;213;228;243;258;184;169;154;140;155;170;156;141;126;112;127', '');
INSERT INTO `mountpark_data` VALUES ('10601', '366', '5', '5', '0', '-1', '1210000', '1210000', '', '', '338', '352', '', '308;323;338;353;368;354;339;324;309;294;280;295;310;325;340;221;236;251;266;281;296;311;326;341;356;371;386;401;372;357;342;327;312;297;282;267;252;237;222;207;178;193;208;223;238;253;268;283;298;313;328;343;358;344;329;314;299;269;254;239;224;209;194;179;164;150;165;180;195;210;225;240;255;270;285;300;315;330;286;271;256;241;226;211;196;181;166;151;136;122;137;152;167;182;197;212;227;242;257;272;258;243;228;213;198;183;168;153;138;123;108;154;169;184;199;185;170;155;140', '');
INSERT INTO `mountpark_data` VALUES ('10600', '373', '5', '5', '0', '-1', '1210000', '1210000', '', '', '343', '358', '', '343;357;371;329;315;301;287;272;286;300;314;328;342;356;341;327;313;299;285;271;257;228;214;200;242;256;270;284;298;312;326;340;354;368;382;367;353;339;325;311;297;283;269;255;241;227;213;199;185;170;184;198;212;226;240;254;268;282;296;310;324;338;352;366;351;337;323;309;295;281;267;253;239;225;211;197;183;169;155;140;154;168;182;196;210;224;238;252;266;280;294;308;322;336;251;237;223;209;195;181;167;153;139;125;138;152;166;180;194;208;222;236;221;207;193;179;165;151;137;122;136;150;164;178;192;206;191;177;163;149;135;121', '');
INSERT INTO `mountpark_data` VALUES ('10599', '293', '5', '5', '0', '-1', '1210000', '1210000', '', '', '265', '279', '', '265;250;235;280;295;310;325;340;355;356;370;385;400;386;371;341;326;311;296;281;266;251;236;221;192;177;162;207;222;237;252;267;282;297;312;327;342;357;372;358;343;328;313;298;283;268;253;238;223;208;193;178;163;148;134;149;164;179;194;209;224;239;254;269;284;299;314;329;344;330;315;300;285;270;255;240;225;210;195;180;165;150;135;120;106;121;136;151;166;181;196;211;226;241;256;271;286;301;316;197;183;182;167;152;137;123;138;153;168;169;154;139;124;109;95;110;125;140;155;141;126;111;96', '');
INSERT INTO `mountpark_data` VALUES ('10606', '308', '5', '5', '0', '-1', '1210000', '1210000', '', '', '280', '0', '', '280;265;250;235;295;310;325;340;355;341;326;311;296;281;266;251;236;221;192;207;222;237;252;267;282;297;312;327;313;298;283;268;253;238;223;208;193;178;164;179;194;209;224;239;254;269;284;299;285;270;255;240;225;210;195;180;165;150;136;151;166;181;196;211;226;241;256;271;257;242;227;212;197;182;167;152;137;122;108;123;138;153;168;183;198;213;228;243;229;214;199;184;169;154;139;124;109;110;125;140;155;170;185', '');
INSERT INTO `mountpark_data` VALUES ('10607', '325', '5', '5', '0', '-1', '1210000', '1210000', '', '', '297', '0', '', '297;282;267;252;312;327;328;313;298;283;268;253;238;224;209;239;254;269;284;299;314;300;285;270;255;240;225;210;195;181;196;211;226;241;227;212;197;182;167;153;168;183;198;213;199;184;169;154;139;125;140;155;170;185;171;156;141;126', '');
INSERT INTO `mountpark_data` VALUES ('10609', '331', '5', '5', '0', '-1', '1210000', '1210000', '', '', '301', '316', '', '287;301;315;329;343;357;342;328;314;300;286;272;257;271;285;299;313;327;312;340;354;368;382;298;284;270;256;242;228;214;200;185;199;213;227;241;255;269;283;297;311;325;339;353;367;352;338;324;310;296;282;268;254;240;226;212;198;184;155;169;183;197;211;225;239;253;267;281;295;309;323;337;322;308;294;280;266;252;238;224;210;196;182;168;154;140;125;139;153;167;181;195;209;223;237;251;265;279;293;307;208;194;180;166;152;138;124;110;95;109;123;137;151;165;179;193;178;164;150;136;122;108;94;80;107;121;135;149;163;148;134;120;106;92;77;91;105;119;133', '');
INSERT INTO `mountpark_data` VALUES ('10611', '381', '5', '5', '0', '-1', '1210000', '1210000', '', '', '353', '367', '', '383;368;353;338;323;308;293;279;294;309;324;339;354;369;355;340;325;310;295;280;265;251;266;281;296;311;326;341;327;342;357;372;387;373;358;343;328;313;299;314;329;344;359;285;270;255;240;225;210;195;181;196;211;226;241;256;271;257;242;227;212;197;182;167;153;168;183;198;213;228;243;229;214;199;184;169;154;139;284;298;312;297;283;269;254;268;282;267;253;239;224;238;252;237;223;209;194;208;222;207;193;179;164;178;192;177;163;149;134;148;162', '');
INSERT INTO `mountpark_data` VALUES ('10622', '323', '5', '5', '0', '-1', '1210000', '1210000', '', '', '295', '309', '', '295;280;265;250;310;325;340;355;370;385;371;356;341;326;311;296;281;266;251;236;222;237;252;267;282;297;312;327;342;357;372;358;343;328;313;298;283;268;253;238;223;208;194;209;224;239;254;269;284;299;314;329;344;330;315;300;285;270;255;240;225;210;195;180;165;150;135;120;106;121;136;151;166;181;196;211;226;241;256;271;286;301;316;302;287;272;257;242;227;212;197;182;167;152;137;122;107;92;93;108;123;138;153;168;183;198;213;228;243;258;273;288;274;259;244;229;214;199;184;169;154;139;124;110;125;140;155;170;185;200;215;230;245;260;96;111;126;141;156;171;186;201;216', '');
INSERT INTO `mountpark_data` VALUES ('10630', '412', '5', '5', '0', '-1', '1210000', '1210000', '', '', '384', '0', '', '384;399;385;371;357;343;329;315;301;287;272;286;300;314;328;342;356;370;369;355;341;327;313;299;285;271;257;242;256;270;284;269;255;241;227;198;184;170;156;142;212;226;240;254;239;225;211;197;183;169;155;141;127;112;126;140;154;168;182;196;210;224;238;252;251;265;279;237;223;209;195;181;167;153;139;125;111;97;152;166;180;194;208;222;236;250;264;249;235;221;207;193;179;165;151', '');
INSERT INTO `mountpark_data` VALUES ('10618', '380', '5', '5', '0', '-1', '1210000', '1210000', '', '', '352', '0', '', '352;367;337;323;338;353;339;324;309;280;265;250;235;220;295;310;325;340;355;370;356;341;326;311;296;281;266;251;236;221;206;192;207;222;237;252;267;282;297;312;327;342;328;313;298;283;268;253;238;223;208;193;178;164;179;194;209;224;239;254;269;284;299;314;300;285;270;255;240;225;210;195;180;165;150;136;151;166;181;196;211;226;241;256;271;286;272;257;242;227;212;197;182;167;152;137;122;108;123;138;153;183;198;213;228;243;258;94', '');
INSERT INTO `mountpark_data` VALUES ('8750', '468', '10', '10', '0', '-1', '2282500', '2282500', '', '', '432', '450', '64;7596;1|174;7596;1|196;7596;1|286;7596;1|320;7596;1|363;7596;1|472;7596;1', '', '64;500;999996476|174;500;999997257|196;500;999994344|286;500;999995694|320;500;999997700|363;500;999992491|472;500;999995668');
INSERT INTO `mountpark_data` VALUES ('8851', '578', '10', '10', '0', '-1', '2282500', '2282500', '', '', '542', '578', '101;7616;1|190;7616;1|230;7616;1|309;7616;1|380;7616;1|394;7616;1|507;7616;1', '', '101;500;999999857|190;500;999999854|230;500;999999609|309;500;999999063|380;500;999995300|394;500;999999560|507;500;999998150');
INSERT INTO `mountpark_data` VALUES ('8749', '614', '10', '10', '0', '-1', '2282500', '2282500', '', '', '578', '614', '159;7695;1|290;7695;1|305;7695;1|376;7695;1|474;7695;1|490;7695;1|505;7695;1', '', '159;500;999998167|290;500;999970482|305;500;999997258|376;500;999997468|474;500;999995142|490;500;999996637|505;500;999992622');
INSERT INTO `mountpark_data` VALUES ('8748', '550', '10', '10', '0', '-1', '2282500', '2282500', '', '', '512', '550', '99;7761;1|213;7761;1|265;7761;1|325;7761;1|393;7761;1|403;7761;1|433;7761;1|583;7761;1', '', '99;500;999999385|213;500;999999512|265;500;999990055|325;500;999998364|393;500;999997629|403;500;999998639|433;500;999997822|583;500;999995868');
INSERT INTO `mountpark_data` VALUES ('8751', '356', '10', '10', '0', '-1', '2282500', '2282500', '', '', '412', '374', '158;7741;1|235;7741;1|286;7741;1|363;7741;1|366;7741;1|475;7741;1|490;7741;1|523;7741;1', '', '158;500;999998748|235;500;999997987|286;500;999995550|363;500;999999254|366;500;999999415|475;500;999997697|490;500;999996692|523;500;999996910');
INSERT INTO `mountpark_data` VALUES ('9450', '128', '3', '3', '0', '-1', '770000', '770000', '', '', '216', '172', '', '156;171;141;126;140;154;168;182;210;225;240;255;241;227;213;199;185;155;169;183;197;211;226;212;198;184;170', '');
INSERT INTO `mountpark_data` VALUES ('9449', '268', '2', '2', '0', '-1', '522500', '522500', '', '', '240', '253', '', '225;211;197;183;169;155;170;184;198;212;226;240;255;241;227;213;199;185', '');
INSERT INTO `mountpark_data` VALUES ('9451', '442', '2', '2', '0', '-1', '522500', '522500', '', '', '412', '427', '', '380;366;352;353;367;381;395;410;396;382;368;425;411;397;383;398;412;426;440', '');
INSERT INTO `mountpark_data` VALUES ('9455', '416', '3', '3', '0', '-1', '770000', '770000', '', '', '388', '402', '', '388;403;373;358;343;329;344;359;374;389;375;360;345;330;346;361', '');
INSERT INTO `mountpark_data` VALUES ('9453', '381', '3', '3', '0', '-1', '770000', '770000', '', '', '409', '395', '', '394;408;422;436;450;451;437;423;409;424;438;452;439;454;453', '');
INSERT INTO `mountpark_data` VALUES ('9456', '181', '4', '4', '0', '-1', '962500', '962500', '', '', '211', '196', '', '169;183;197;211;225;239;253;267;282;268;254;240;226;212;198;184;199;213;227;241;255;269;283;297;312;298;284;270;256;242;214;228;229;243;257;271;285;299;313;327;342;328;314;300;272;286;258;244;259;273;287;301;315;329;343;357;372;358;344;330;316;302;288;274', '');
INSERT INTO `mountpark_data` VALUES ('9457', '284', '7', '7', '0', '-1', '1173500', '1173500', '', '', '314', '299', '', '398;384;370;342;328;314;300;286;272;258;244;259;273;287;301;315;329;343;357;371;385;399;413;428;414;400;386;372;358;344;330;316;302;288;274;289;303;317;331;345;359;373;387;401;415;429;443;458;444;430;416;402;388;374;360;346;332', '');
INSERT INTO `mountpark_data` VALUES ('9458', '401', '1', '1', '0', '-1', '412500', '412500', '', '', '373', '387', '', '373;388;343;358;374;360;345;359;344;330;301;315;329', '373;3000;2764|374;3000;2871');
INSERT INTO `mountpark_data` VALUES ('9459', '454', '2', '2', '0', '-1', '522500', '522500', '', '', '424', '439', '', '365;380;395;410;424;438;452;437;422;407;393;379;394;409;423;408', '');
INSERT INTO `mountpark_data` VALUES ('9462', '426', '2', '2', '0', '-1', '522500', '522500', '', '', '398', '412', '', '398;383;368;353;338;324;339;354;369;384;310;325;340;355;370;385;400;386;371', '');
INSERT INTO `mountpark_data` VALUES ('9461', '151', '10', '10', '0', '-1', '2282500', '2282500', '', '', '181', '166', '', '97;112;127;141;126;111;125;140;155;170;185;199;184;169;154;139;153;168;183;198;213;227;242;257;272;212;197;182;167;181;196;211;226;241;256;271;286;300;285;270;255;240;225;210;195;209;224;239;254;269;284;299;314;328;313;298;283;268;253;238;223;237;252;267;282;297;312;327;342;356;341;326;311;296;281;266;251;280;295;310;325;340;355;370', '');
INSERT INTO `mountpark_data` VALUES ('9460', '308', '2', '2', '0', '-1', '522500', '522500', '', '', '338', '323', '', '310;325;340;354;339;324;338;353;368;382;367;352;366;381;396', '');
INSERT INTO `mountpark_data` VALUES ('9463', '268', '4', '4', '0', '-1', '962500', '962500', '', '', '296', '282', '', '296;281;311;326;341;356;371;386;401;415;400;385;370;355;340;325;310;295;309;324;339;354;369;384;399;414;429;443;428;413;398;383;368;353;338', '');
INSERT INTO `mountpark_data` VALUES ('9464', '381', '7', '7', '0', '-1', '1173500', '1173500', '', '', '353', '367', '296;7619;5602|325;7619;5602|354;7619;5602|383;7619;5602', '338;324;310;296;282;268;254;269;283;297;311;325;339;353;368;354;340;326;312;298;284;299;313;327;341;355;369;383;398;384;370;356;342;328;314;329;343;357;371;385;399;413;428;414;400;386;372;358;344;359;373;387;401;415;429;443', '296;3000;3000|325;3000;3000|354;3000;3000|383;3000;3000');
INSERT INTO `mountpark_data` VALUES ('9465', '315', '1', '1', '0', '-1', '412500', '412500', '', '', '287', '301', '', '287;302;272;257;242;228;243;258;273;288;274;259;244;229', '');
INSERT INTO `mountpark_data` VALUES ('9466', '307', '4', '4', '0', '-1', '962500', '962500', '', '', '279', '293', '', '279;264;249;235;250;265;280;294;266;251;236;221;267;281;295;309;323;337;351;365;380;366;352;338;324;310;296;282;297;311;325;339;353;367;381', '');
INSERT INTO `mountpark_data` VALUES ('8757', '604', '5', '5', '0', '-1', '1210000', '1210000', '', '', '640', '622', '', '691;673;655;637;619;601;583;602;620;638;656;674;692;710;729;748;767;711;693;675;657;639;621;640;658;676;694;712;730;749;731;713;695;677;659;678;696;714;732;750;768;769;751;733;715;697', '621;3000;1923|639;3000;1988|659;3000;1390|676;3000;1752|677;3000;1690');
INSERT INTO `mountpark_data` VALUES ('8758', '181', '5', '5', '0', '-1', '1210000', '1210000', '', '', '143', '162', '', '103;85;49;67;31;32;51;69;87;68;50;86;122;104;141;160;179;161;142;124;105;107;125;106', '');
INSERT INTO `mountpark_data` VALUES ('8759', '655', '5', '5', '0', '-1', '1210000', '1210000', '', '', '617', '636', '541;7763;747|595;7763;747|598;7590;747|599;7590;747|635;7590;747', '595;614;633;652;671;653;634;615;596;577;559;578;597;616;635;617;598;579;560;541;523;542;561;580;581;562;543;524;505;487;506;525;544;563;599', '541;3000;3000|595;3000;3000|598;3000;2935|599;3000;2938|635;3000;2943');
INSERT INTO `mountpark_data` VALUES ('9268', '598', '12', '12', '0', '-1', '2695000', '2695000', '', '', '562', '580', '', '562;581;543;524;505;486;467;448;430;449;468;487;506;525;544;563;545;526;507;488;469;450;431;412;394;413;432;451;470;489;508;527;509;490;471;434;433;414;395;376;358;377;396;415;453;472;491;473;454;435;416;397;378;359;340;322;341;360;379;398;417;436;455;437;418;399;380;361;342;323', '507;3000;2930|508;3000;2948|525;3000;2997|543;3000;1969|544;3000;1967|545;3000;2945|563;3000;3000|581;3000;1965');
INSERT INTO `mountpark_data` VALUES ('9270', '252', '12', '12', '0', '-1', '2695000', '2695000', '', '', '216', '234', '', '32;140;122;104;86;68;50;69;87;105;123;141;159;178;160;142;124;106;88;107;143;161;179;197;216;198;180;162;144;126;145;163;181;199;235;254;236;218;200;182;164;183;201;219;237;255;273;217', '179;3000;2739|180;3000;2727|197;3000;2997|216;3000;2748|217;3000;2705|235;3000;2986');
INSERT INTO `mountpark_data` VALUES ('9273', '271', '10', '10', '0', '-1', '2282500', '2282500', '', '', '235', '253', '', '159;178;197;216;235;254;273;292;311;293;275;256;236;217;198;179;160;141;123;142;161;180;199;218;237;257;238;219;200;181;162;143;124;125;144;163;182;201;220;239;221;202;183;164;145;126;107;106;105;184;165;146;127;108;90;109;128;147', '');
INSERT INTO `mountpark_data` VALUES ('9274', '655', '5', '5', '0', '-1', '1210000', '1210000', '', '', '617', '636', '', '487;506;525;544;563;581;562;543;524;505;523;542;561;580;599;617;598;579;560;541;559;578;597;616;635;653;634;615;596;577;595;614;633;652;671', '');
INSERT INTO `mountpark_data` VALUES ('9278', '753', '5', '5', '0', '-1', '1210000', '1210000', '', '', '717', '735', '', '717;698;679;660;736;755;774;756;737;718;699;680;661;642;624;643;662;681;700;719;738;720;701;682;663;644;625;606;588;607;626;645;664;683;702;665;646;627;608;589', '');
INSERT INTO `mountpark_data` VALUES ('9277', '361', '5', '5', '0', '-1', '1210000', '1210000', '', '', '325', '343', '', '325;344;363;306;287;268;249;230;211;193;212;231;250;269;288;307;326;345;327;308;289;270;251;232;213;194;175;157;176;195;214;233;252;271;290;309;291;272;253;234;215;196;177;158;139;140;159;178;197;216;235;254;273;255;236;217;198;179;160;141;142;123;161;180;199;218', '');
INSERT INTO `mountpark_data` VALUES ('4250', '635', '3', '3', '0', '-1', '770000', '770000', '', '', '673', '654', '', '637;656;675;694;713;732;750;731;712;693;674;655;673;692;711;730;749;768;767;748;729;710;691;709;728;747;746;727;745;764', '');
INSERT INTO `mountpark_data` VALUES ('4246', '180', '5', '5', '0', '-1', '1210000', '1210000', '', '', '164', '172', '', '201;182;144;106;88;70;52;34;53;54;72', '');
INSERT INTO `mountpark_data` VALUES ('4245', '587', '5', '5', '0', '-1', '1210000', '1210000', '', '', '625', '606', '', '679;661;643;625;607;608;626;644;662;680;698;717;699;681;663;645;627;646;664;682;700;718;736;755;737;719;701;683;665;702;720;756;774', '');
INSERT INTO `mountpark_data` VALUES ('4242', '727', '5', '5', '0', '-1', '1210000', '1210000', '', '', '689', '708', '', '276;558;576;596;595;594;612;614;634;633;632;631;667;669;652;653;671;689;707;725;743;724;706;705', '');
INSERT INTO `mountpark_data` VALUES ('4207', '636', '5', '5', '0', '-1', '1210000', '1210000', '', '', '672', '654', '', '615;634;633;651;653;672;671;670;669;688;706;724;743;762;725;689;690;691;710;728;709;708;744;726;745;764', '');
INSERT INTO `mountpark_data` VALUES ('4206', '579', '5', '5', '0', '-1', '1210000', '1210000', '', '', '615', '597', '', '558;577;576;594;595;596;615;614;613;630;631;632;633;634;653;652;651;650;649;648;667;668;669;670;671;691;690;689;688;687;705;686;724;706;708;709;727;726;763', '');
INSERT INTO `mountpark_data` VALUES ('4079', '418', '5', '5', '0', '-1', '1210000', '1210000', '', '', '382', '400', '', '382;401;420;363;344;325;307;326;345;364;383;402;384;365;346;327;308;289;271;290;309;328;347', '');
INSERT INTO `mountpark_data` VALUES ('4211', '253', '5', '5', '0', '-1', '1210000', '1210000', '', '', '215', '234', '', '215;233;251;269;287;197;179;161;142;160;178;196;214;232;250;268;249;231;213;195;177;159;141;123;122;140;158;176;194;212;230;175;157;139;121;103;85;66;84;102;120;101;83;65;46', '');
INSERT INTO `mountpark_data` VALUES ('4210', '603', '5', '5', '0', '-1', '1210000', '1210000', '', '', '639', '621', '', '601;620;619;637;638;639;658;657;656;655;673;674;675;676;677;695;694;693;692;691;710;711;712;713;731;730;729;748;767;749', '');
INSERT INTO `mountpark_data` VALUES ('4209', '651', '5', '5', '0', '-1', '1210000', '1210000', '', '', '687', '669', '', '649;667;685;722;704;686;668;687;705;723;741;759;760;742;724;706;725;743;761', '');
INSERT INTO `mountpark_data` VALUES ('4252', '542', '5', '5', '0', '-1', '1210000', '1210000', '', '', '578', '560', '', '521;540;559;578;597;616;635;539;558;577;596;615;634;653;671;652;633;595;576;557;575;594;613;632;651;670;689;707;688;669;650;631;612;593;630;649;668;687;725', '');
INSERT INTO `mountpark_data` VALUES ('4225', '485', '5', '5', '0', '-1', '1210000', '1210000', '', '', '521', '503', '', '483;502;521;540;559;577;558;539;520;501;482;500;519;538;557;576;595;613;594;575;556;537;574;593;612;631;649;630;611;648;667;685', '');
INSERT INTO `mountpark_data` VALUES ('4309', '541', '5', '5', '0', '-1', '1210000', '1210000', '', '', '505', '0', '', '505;524;543;562;581;486;467;448;429;410;391;373;392;411;430;449;468;487;506;525;544;563;545;526;507;488;469;450;431;412;393;374;355;337;356;375;394;413;432;451;470;489;508;527;509;490;471;452;433;414;395;376;357;338;319;301;320;339;358;377;396;415;434;453;472;491;473;454;435;416;397;378;359;340;321;302;283;265;284;303;322;341;360;379;398;417;436;455;437;418;399;380;361;342;323;304;285;266;247;229;248;267;286;305;324;343;362;381;400;419;401;382;363;344;325;306;287;268;249;230;211;193;212;231;250;269;288;307;326;345;364;383;365;346;327;308;289;270;251;232;213;194;175;157;176;195;214;233;252;271;290;309;328', '');
INSERT INTO `mountpark_data` VALUES ('4258', '400', '5', '5', '0', '-1', '1210000', '1210000', '', '', '364', '382', '', '364;383;402;345;326;307;289;308;327;346;365;384;366;347;328;309;290;271;253;272;291;310;329;348;235', '');
INSERT INTO `mountpark_data` VALUES ('4260', '576', '5', '5', '0', '-1', '1210000', '1210000', '', '', '614', '595', '', '614;633;652;671;690;708;689;670;651;632;650;669;688;707;726;744;725;706;687;668;686;705;762;743;704', '');
INSERT INTO `mountpark_data` VALUES ('4248', '393', '5', '5', '0', '-1', '1210000', '1210000', '', '', '429', '411', '', '429;410;391;372;353;448;467;486;504;485;466;447;428;409;390;371;389;408;427;446;465;484;503;522;540;521;502;483;464;445;426;463;482;501;520;539;558', '');
INSERT INTO `mountpark_data` VALUES ('4342', '458', '5', '5', '0', '-1', '1210000', '1210000', '', '', '494', '476', '', '494;513;532;551;570;588;569;550;531;512;493;511;492;473;454;530;549;568;587;606;624;605;586;567;548;529;510;491;472;490;509;528;547;566;585;604;623', '');
INSERT INTO `mountpark_data` VALUES ('4241', '595', '5', '5', '0', '-1', '1210000', '1210000', '', '', '631', '613', '', '631;650;669;688;707;726;744;725;706;687;668;649;667;686;705;724;743;762;761;742;723;704;685;722;741;760', '');
INSERT INTO `mountpark_data` VALUES ('4240', '326', '5', '5', '0', '-1', '1210000', '1210000', '', '', '288', '307', '', '288;324;306;270;252;234;216;197;179;215;233;251;269;287;305;286;268;250;232;214;196;178;160;141;159;177;195;213;231;249;267;248;230;212;194;176;158;140;122;103;121;139;157;175;193;211;229;210;192;174;156;138;120;84;65;101;119;137;155;173;191;172;154;136;81;100;82;64;46;27;45;63;99;135;153;134;79;98;80;62;44;26;25;43;61;97;115;96;78;60;42;24;23;59', '');
INSERT INTO `mountpark_data` VALUES ('4238', '255', '5', '5', '0', '-1', '1210000', '1210000', '', '', '187', '206', '', '187;169;151;133;115;97;79;60;78;96;114;132;150;168;149;131;113;95;77;59;41;40;22;58;76;94;112;130;93;75;57;39', '');
INSERT INTO `mountpark_data` VALUES ('4233', '523', '5', '5', '0', '-1', '1210000', '1210000', '', '', '485', '504', '', '485;467;449;431;413;395;377;358;376;394;412;430;448;466;447;429;393;375;411;357;339;320;338;356;374;355;337;301;282;300;281;263', '');
INSERT INTO `mountpark_data` VALUES ('4243', '559', '5', '5', '0', '-1', '1210000', '1210000', '', '', '597', '578', '', '597;615;633;651;579;561;543;525;544;562;580;598;616;634;652;670;689;671;653;635;617;599;581;563;582;600;618;636;654;672;690;708;727;709;691;673;655;637;619;601;620;638;656;674;692;710;728;746;765;747;729;711;693;675;657', '');
INSERT INTO `mountpark_data` VALUES ('4273', '247', '5', '5', '0', '-1', '1210000', '1210000', '', '', '209', '228', '', '209;227;245;263;191;173;155;136;154;172;190;208;226;244;225;207;189;171;153;135;117;98;116;134;152;170;188;206;187;169;151;133;115;97;79;60;78;96;114;132;150;168;149;131;113;95;77;59;41;22;40;58;76;94;75;56;38;39', '');
INSERT INTO `mountpark_data` VALUES ('4269', '560', '5', '5', '0', '-1', '1210000', '1210000', '', '', '596', '578', '', '596;577;558;539;615;634;653;671;652;633;614;595;576;557;575;594;613;632;651;670;689;707;688;669;650;631;612;593;611;630;649;668;687;706;725;743;724;705;686;667;648;685;704;723;742;761;760', '');
INSERT INTO `mountpark_data` VALUES ('4264', '617', '5', '5', '0', '-1', '1210000', '1210000', '', '', '653', '635', '', '653;634;615;672;691;709;690;671;652;633;651;670;689;708;727;745;726;707;688;669;687;706;725;744;763;743;724;705', '');
INSERT INTO `mountpark_data` VALUES ('4278', '427', '5', '5', '0', '-1', '1210000', '1210000', '', '', '391', '409', '', '391;372;353;334;410;429;411;392;373;354;335;316;298;317;336;355;374;393;375;356;337;318;299;280;262;281;300;319;338;357;339;320;301;282;263', '');
INSERT INTO `mountpark_data` VALUES ('4272', '397', '5', '5', '0', '-1', '1210000', '1210000', '', '', '361', '379', '', '361;342;323;304;380;399;418;400;381;362;343;324;305;286;268;287;306;325;344;363;382;364;345;326;307;288;269;250;232;251;270;289;308;327', '');
INSERT INTO `mountpark_data` VALUES ('4271', '116', '5', '5', '0', '-1', '1210000', '1210000', '', '', '152', '134', '', '209;190;152;133;114;132;150;151;170;189;208;227;245;226;207;188;169;168;187;206;225;244;263;281;262;243;224;205;186;167;149;204;223;242;261;280;299;93;57', '189;3000;0');
INSERT INTO `mountpark_data` VALUES ('4265', '620', '5', '5', '0', '-1', '1210000', '1210000', '', '', '584', '602', '', '470;452;434;380;416;489;471;453;435;417;399;641;622;603;584;565;546;527;508;490;509;528;547;566;585;604;623;605;586;567;548;529;510;491;472;454;473;492;511;530;549;531;550;569;551;532;513;494;475;456;437;418', '');
INSERT INTO `mountpark_data` VALUES ('4262', '587', '5', '5', '0', '-1', '1210000', '1210000', '', '', '625', '606', '', '571;589;607;625;643;661;679;698;680;662;644;626;608;590;609;627;645;663;681;699;717;736;718;700;682;664;646;628;665;701;683;719;737;755;774;756;738;720;702;775', '');
INSERT INTO `mountpark_data` VALUES ('4261', '325', '5', '5', '0', '-1', '1210000', '1210000', '', '', '287', '306', '', '137;155;174;192;210;191;209;227;156;175;173;193;194;213;212;211;246;247;248;249;250;232;251;265;266;267;268;269;287;286;285;284;303;304;305;323;322;341', '');
INSERT INTO `mountpark_data` VALUES ('4217', '669', '5', '5', '0', '-1', '1210000', '1210000', '', '', '633', '651', '', '448;467;485;486;504;503;502;520;521;522;541;540;539;538;557;558;559;560;579;577;576;595;596;597;615;614;633', '');
INSERT INTO `mountpark_data` VALUES ('4219', '431', '5', '5', '0', '-1', '1210000', '1210000', '', '', '467', '449', '', '429;448;447;465;466;467;486;485;484;483;501;520;521;522;523;524;542;541;540;558;577;578;596;503;504', '');
INSERT INTO `mountpark_data` VALUES ('4218', '448', '5', '5', '0', '-1', '1210000', '1210000', '', '', '486', '467', '', '432;451;450;468;469;470;489;488;487;486;504;505;506;507;508;526;525;524;523;522;540;541;542;543;544;562;561;560;559;578;579;580;598;597;616', '');
INSERT INTO `mountpark_data` VALUES ('4213', '597', '5', '5', '0', '-1', '1210000', '1210000', '', '', '633', '615', '', '595;613;631;649;667;704;686;668;650;632;614;633;651;669;687;705;723;741;742;724;706;688;670;652;671;689;707;725;743;761;762;744;726;708;690', '');
INSERT INTO `mountpark_data` VALUES ('4216', '302', '5', '5', '0', '-1', '1210000', '1210000', '', '', '264', '283', '', '169;151;152;134;116;135;136;155;154;153;170;207;189;171;173;192;210;191;172;190;208;226;245;264;246;228;209;227;137', '');
INSERT INTO `mountpark_data` VALUES ('4215', '155', '5', '5', '0', '-1', '1210000', '1210000', '', '', '117', '136', '', '117;135;153;171;189;99;81;63;44;62;80;98;116;134;152;170;133;115;97;79;61;43;25;24;42;60;78;96;114;95;77;59;41;23;22;40;58;76;57;39', '');
INSERT INTO `mountpark_data` VALUES ('4270', '714', '5', '5', '0', '-1', '1210000', '1210000', '', '', '678', '696', '', '678;659;640;697;716;735;736;717;698;679;660;641;622;604;623;642;661;680;681;700;662;643;624;605;586;568;587;606;625;644;663;682;701;720;739;550;569;588;607;626;645;664;683;702;721', '');
INSERT INTO `mountpark_data` VALUES ('4096', '652', '5', '5', '0', '-1', '1210000', '1210000', '', '', '614', '633', '', '614;632;650;668;596;578;560;541;559;577;595;613;631;649;630;612;594;576;558;540;522;503;521;539;557;575;593;611;556;538;520;502;484;465;483;501;519;537;500;482;464', '');
INSERT INTO `mountpark_data` VALUES ('4104', '162', '5', '5', '0', '-1', '1210000', '1210000', '', '', '126', '144', '', '126;107;88;145;164;146;127;108;89;70;52;71;90;109;128;34;53', '');
INSERT INTO `mountpark_data` VALUES ('4284', '283', '5', '5', '0', '-1', '1210000', '1210000', '', '', '321', '302', '', '321;303;285;304;323;342;361;360;341;322;340;359;378;397;415;396;377;358;339;357;376;395;414', '');
INSERT INTO `mountpark_data` VALUES ('4291', '532', '5', '5', '0', '-1', '1210000', '1210000', '', '', '494', '513', '', '494;476;458;440;512;530;511;493;475;457;439;421;402;420;438;456;474;492;455;437;419;401;383;364;382;400;418;436;435;454;417;398;379;360;378;397;416;473', '');
INSERT INTO `mountpark_data` VALUES ('4275', '451', '5', '5', '0', '-1', '1210000', '1210000', '', '', '415', '433', '', '415;396;434;453;472;454;435;416;397;378;360;379;398;417;436;455;437;418;399;380;361;342;324;343;362;381;400;419;401;382;363;344;325', '');
INSERT INTO `mountpark_data` VALUES ('4280', '196', '5', '5', '0', '-1', '1210000', '1210000', '', '', '158', '177', '', '158;140;122;176;194;212;193;175;157;139;121;103;84;102;120;138;156;174;155;137;119;101;83;65;46;64;82;100;118;136;117;99;81;63;45;27;26;44;62;80;98;79;61;43;25;24;42;60;41', '');
INSERT INTO `mountpark_data` VALUES ('4249', '528', '5', '5', '0', '-1', '1210000', '1210000', '', '', '492', '510', '', '492;473;511;530;549;568;587;569;550;531;512;493;474;455;437;475;494;513;532;551;533;514;495;476;458;477;496', '');
INSERT INTO `mountpark_data` VALUES ('4287', '257', '5', '5', '0', '-1', '1210000', '1210000', '', '', '219', '238', '', '219;201;183;165;147;110;128;146;164;182;200;181;163;145;127;109;91;73;72;90;108;126;144;162;143;125;107;89;71;53;34;52;70;88;106', '');
INSERT INTO `mountpark_data` VALUES ('4282', '172', '5', '5', '0', '-1', '1210000', '1210000', '', '', '136', '154', '', '136;155;174;193;117;98;79;61;80;99;118;137;156;175;157;138;119;100;81;62;43;25;44;63;82;101;120;139;121;102;83;64;45', '');
INSERT INTO `mountpark_data` VALUES ('4169', '490', '5', '5', '0', '-1', '1210000', '1210000', '', '', '528', '509', '', '528;510;492;546;564;582;601;583;565;547;529;511;530;548;566;584;602;620;639;621;603;585;567;549;568;586;604;622;640', '');
INSERT INTO `mountpark_data` VALUES ('4172', '615', '5', '5', '0', '-1', '1210000', '1210000', '', '', '651', '633', '', '613;632;651;670;689;707;688;669;650;631;649;668;687;706;725;743;724;705;686;667;685;704;723;742;761;760;741;722;759', '');
INSERT INTO `mountpark_data` VALUES ('4300', '193', '5', '5', '0', '-1', '1210000', '1210000', '', '', '155', '174', '', '24;42;41;77;76;114;59;60;79;61;43;62;80;134;152;171;209;191;137;119;100;118;136;81;173;154', '');
INSERT INTO `mountpark_data` VALUES ('4289', '506', '5', '5', '0', '-1', '1210000', '1210000', '', '', '544', '525', '', '580;562;544;526;508;527;545;563;581;599;618;600;582;564;546;565;583;601;619;637;656;620;602;584;603;621;639;657;675;694;676;658;640;622', '');
INSERT INTO `mountpark_data` VALUES ('4181', '678', '5', '5', '0', '-1', '1210000', '1210000', '', '', '716', '697', '', '716;680;698;734;752;771;753;735;717;699;718;736;754;772;773;755;737;756;774', '');
INSERT INTO `mountpark_data` VALUES ('4178', '541', '5', '5', '0', '-1', '1210000', '1210000', '', '', '577', '559', '', '577;596;615;634;653;558;539;557;576;595;614;633;652;671;689;670;651;632;613;594;575;593;612;631;650;669;688;707;725;706;687;668;649;630;611;667;704;705;724;743;761;742;723;685;741;760', '');
INSERT INTO `mountpark_data` VALUES ('4212', '232', '5', '5', '0', '-1', '1210000', '1210000', '', '', '194', '213', '', '194;176;158;140;212;230;248;229;211;193;175;157;139;121;138;156;174;192;210;191;173;155;137;119;118;136;154', '');
INSERT INTO `mountpark_data` VALUES ('4170', '467', '5', '5', '0', '-1', '1210000', '1210000', '', '', '431', '449', '', '339;357;375;393;412;394;376;358;377;395;413;431;450;432;414;396;415;433;451;469;488;470;452;434;453;471;489;507', '');
INSERT INTO `mountpark_data` VALUES ('4204', '455', '5', '5', '0', '-1', '1210000', '1210000', '', '', '491', '473', '', '491;472;453;434;510;529;548;566;547;528;509;490;471;452;470;489;508;527;546;565;584;602;583;564;545;526;507;488;506;525;544;563;582;601', '');
INSERT INTO `mountpark_data` VALUES ('4182', '308', '5', '5', '0', '-1', '1210000', '1210000', '', '', '272', '290', '', '272;253;234;216;198;180;162;144;163;181;199;217;235;254;236;218;200;182;201;219;237;255;273;291;310;292;274;256;238;220;202;239;257;275;293;311', '');
INSERT INTO `mountpark_data` VALUES ('4208', '208', '5', '5', '0', '-1', '1210000', '1210000', '', '', '170', '189', '', '170;188;206;224;205;186;167;130;149;168;187;169;150;131;112;93;56;75;94;113;132;151;152;133;114;95;76;57;38;19;20;39;58;77;96;115;134;116;97;78;59;40', '');
INSERT INTO `mountpark_data` VALUES ('4299', '472', '5', '5', '0', '-1', '1210000', '1210000', '', '', '434', '453', '', '434;452;470;488;506;524;453;472;491;505;487;469;451;433;415;396;414;432;450;468;486;467;449;431;413;395', '');
INSERT INTO `mountpark_data` VALUES ('4304', '414', '5', '5', '0', '-1', '1210000', '1210000', '', '', '378', '396', '', '378;397;416;435;454;473;492;474;455;436;417;398;379;360;342;361;380;399;418;437;456;438;419;400;381;362;343;306;325;344;363;382;401;420', '');
INSERT INTO `mountpark_data` VALUES ('4301', '620', '5', '5', '0', '-1', '1210000', '1210000', '', '', '656', '638', '', '656;637;618;636;655;674;692;673;654;635;616;597;615;634;671;672;691;710;728;709;690;652;633;670;689;708;727;746;726', '');
INSERT INTO `mountpark_data` VALUES ('4290', '325', '5', '5', '0', '-1', '1210000', '1210000', '', '', '287', '306', '', '323;305;287;269;304;286;268;250;232;213;249;285;266;248;230;212;194;175;193;211;229;247;228;210;192;174;156;137;155;173;191;209;190;172;154;136;118', '');
INSERT INTO `mountpark_data` VALUES ('4336', '437', '5', '5', '0', '-1', '1210000', '1210000', '', '', '401', '419', '', '401;382;363;325;307;326;345;364;383;365;346;327;308;271;290;309;328;347', '');
INSERT INTO `mountpark_data` VALUES ('2216', '675', '5', '5', '0', '-1', '1210000', '1210000', '', '', '637', '656', '', '637;655;673;691;619;601;583;565;546;564;582;600;618;636;654;672;653;635;617;599;581;563;545;527;508;526;544;562;580;598;616;634;615;597;579;561;543;525;507;489;470;488;506;524;542;560;578;596;451;469;487;505;523;541;559', '');
INSERT INTO `mountpark_data` VALUES ('2215', '567', '5', '5', '0', '-1', '1210000', '1210000', '', '', '529', '548', '', '529;547;565;511;493;474;492;510;528;546;527;509;491;473;455;418;400;382;436;454;472;490;508;526;544;562;543;525;507;489;471;363;381;399;417;435;344;362;380;398;470;488;506;524;505;487;469;451;433;396;378;379;361;343;325;306;324;342;360;414;432;450;468;486;413;395;377;359;341;322;340;358;376;394;339;357', '');
INSERT INTO `mountpark_data` VALUES ('2209', '674', '5', '5', '0', '-1', '1210000', '1210000', '', '', '636', '655', '', '636;618;600;582;564;546;528;509;527;545;563;581;599;617;598;580;562;544;526;508', '');
INSERT INTO `mountpark_data` VALUES ('2210', '472', '5', '5', '0', '-1', '1210000', '1210000', '', '', '436', '454', '', '436;455;417;398;379;360;341;323;342;361;380;399;418;437;456;438;419;400;381;362;343;324;305;287;306;325;344;363;382;401;420;402;383;364;345;326;307;288', '');
INSERT INTO `mountpark_data` VALUES ('4303', '679', '5', '5', '0', '-1', '1210000', '1210000', '', '', '643', '661', '', '643;624;605;586;567;662;681;700;719;701;682;663;644;625;606;587;568;549;531;550;569;588;607;626;645;664;646;627;608;589;570;551;532', '');
INSERT INTO `mountpark_data` VALUES ('4305', '438', '5', '5', '0', '-1', '1210000', '1210000', '', '', '402', '420', '', '383;364;345;327;346;365;384;366;347;328;309;291;310;329;348;330;311;292;273', '');
INSERT INTO `mountpark_data` VALUES ('4077', '403', '5', '5', '0', '-1', '1210000', '1210000', '', '', '365', '384', '', '365;347;329;311;293;383;401;382;364;346;328;310;292;274;255;273;291;309;327;345;363;344;326;308;290;272;253;271;289;307;306;325', '');
INSERT INTO `mountpark_data` VALUES ('4082', '414', '4', '4', '0', '-1', '962500', '962500', '', '', '378', '396', '', '378;359;340;322;304;286;268;250;269;287;305;323;341;360;342;324;306;288;307;325;343;361;379;397;416;398;380;362;344', '');
INSERT INTO `mountpark_data` VALUES ('4302', '617', '5', '5', '0', '-1', '1210000', '1210000', '', '', '579', '598', '', '579;561;543;597;615;596;578;560;542;524;505;523;541;559;577;558;540;522;504;486;467;485;503;521;539;520;502;484;466;448;429;447;465;483;501;482;464;446;428', '');
INSERT INTO `mountpark_data` VALUES ('4072', '287', '5', '5', '0', '-1', '1210000', '1210000', '', '', '251', '269', '', '251;232;213;270;289;308;290;271;252;233;214;195;177;196;215;234;253;272;254;235;216;198;217', '');
INSERT INTO `mountpark_data` VALUES ('4090', '697', '5', '5', '0', '-1', '1210000', '1210000', '', '', '661', '679', '', '661;643;625;607;589;571;590;608;626;644;662;680;699;718;700;682;664;646;628;665;683;701;719;737;756;738;720;702;739;757', '');
INSERT INTO `mountpark_data` VALUES ('4097', '707', '5', '5', '0', '-1', '1210000', '1210000', '', '', '671', '689', '', '671;652;633;614;595;690;709;728;747;729;710;691;672;653;634;615;596;577;559;578;597;616;654;673;692;711;693;674;655;636;617;598;579;560;541;523;542;561;580;599;618;637;656', '');
INSERT INTO `mountpark_data` VALUES ('4180', '621', '5', '5', '0', '-1', '1210000', '1210000', '', '', '659', '640', '', '659;641;623;605;677;695;713;732;714;696;678;660;642;624;643;661;679;697;715;733;751;770;752;734;716;698;680;662;681;699;717;735;753;771;772;754;736;718;700;719;737;755;773;756', '');
INSERT INTO `mountpark_data` VALUES ('4094', '529', '5', '5', '0', '-1', '1210000', '1210000', '', '', '491', '510', '', '491;509;527;545;473;455;436;454;472;490;508;526;507;489;471;453;435;417;398;416;434;452;470;488;469;451;433;415;397;379;396;378;360;341;359', '');
INSERT INTO `mountpark_data` VALUES ('4236', '578', '5', '5', '0', '-1', '1210000', '1210000', '', '', '614', '596', '', '614;595;576;557;575;593;611;648;630;612;594;613;631;649;667;686;668;650;632;633;651;669;687;705;742;724;706;688;670;652;671;689;707;725;743', '');
INSERT INTO `mountpark_data` VALUES ('4177', '556', '4', '4', '0', '-1', '962500', '962500', '', '', '594', '575', '', '594;576;558;612;630;648;685;667;649;631;613;595;577;596;614;632;650;668;686;704;722;741;723;705;687;669;651;633;615;634;652;670;688;706;724;742;760;653;671;689;707;725;726;708;690', '');
INSERT INTO `mountpark_data` VALUES ('4232', '717', '3', '3', '0', '-1', '770000', '770000', '', '', '679', '698', '', '733;715;697;679;643;661;625;606;624;642;660;678;696;714;695;677;659;641;623;605;587;568;586;604;622;640;658;676;657;639;621;603;585;567;549;530;548;584;602;620;638', '567;3000;3000|639;3000;3000');
INSERT INTO `mountpark_data` VALUES ('4173', '660', '2', '2', '0', '-1', '522500', '522500', '', '', '698', '679', '', '698;680;662;681;700;719;738;757;775;756;737;718;699;717;736;755;774;773;754;735;716;734;753', '');
INSERT INTO `mountpark_data` VALUES ('8479', '267', '2', '2', '0', '-1', '522500', '522500', '', '', '231', '249', '', '231;250;269;212;193;174;155;136;118;137;156;175;194;213;232;251;233;214;195;176;157;138;119;100;101;158;177;196;215;140;159;178', '');
INSERT INTO `mountpark_data` VALUES ('8480', '342', '5', '5', '0', '-1', '1210000', '1210000', '', '', '306', '324', '', '268;250;232;214;196;178;197;215;233;251;269;287;306;288;270;252;234;271;289;307;325;344;326;308', '');
INSERT INTO `mountpark_data` VALUES ('4231', '708', '3', '3', '0', '-1', '770000', '770000', '', '', '670', '689', '', '616;634;652;670;688;706;724;742;760;759;741;723;705;687;669;651;633;615;597;578;559;577;595;613;631;649', '');
INSERT INTO `mountpark_data` VALUES ('4229', '543', '5', '5', '0', '-1', '1210000', '1210000', '', '', '507', '525', '', '507;526;545;527;509;491;473;455;436;454;472;490;508;489;471;453;435;417;398;416;434;452;470;488;469;451;433;415;397;379;414', '');
INSERT INTO `mountpark_data` VALUES ('4093', '730', '5', '5', '0', '-1', '1210000', '1210000', '', '', '692', '711', '', '746;728;710;692;656;674;638;620;601;619;637;655;673;691;709;727;708;690;672;654;636;618;600;582;563;581;599;635;653;671;689', '');
INSERT INTO `mountpark_data` VALUES ('4070', '468', '6', '6', '0', '-1', '1512500', '1512500', '', '', '432', '450', '', '432;451;470;489;508;413;394;375;356;338;376;395;414;433;452;471;490;472;453;434;397;396;377;358;339;320;302;321;340;359;378;435;454;436;417;398;379;360;341;322;303;284;266;285;304;323;342;361;380;399', '');
INSERT INTO `mountpark_data` VALUES ('2220', '373', '5', '5', '0', '-1', '1210000', '1210000', '', '', '337', '355', '', '337;318;299;280;261;243;225;207;189;171;153;135;154;172;190;208;226;244;262;281;263;245;227;209;191;173;192;210;228;246;264;282;300;319;301;283;265;247;229', '');
INSERT INTO `mountpark_data` VALUES ('2218', '410', '5', '5', '0', '-1', '1210000', '1210000', '', '', '374', '392', '', '374;355;336;356;337;318;300;319;338;320;301;282;264;283;302;284;265;246;227;209;228;247;266;190;208;226;244;262;280;298;261;243;225;207;189;171;152;170;188;206;224;242', '');
INSERT INTO `mountpark_data` VALUES ('9216', '210', '14', '14', '0', '-1', '3135000', '3135000', '', '', '180', '194', '', '208;193;178;163;148;133;103;88;74;89;104;119;134;149;179;194;180;165;150;135;120;105;90;75;60;32;46;61;76;91;106;121;136;151;166;137;123;138;124;109;110;95;81;96;82;67;66;80;51;65', '');
INSERT INTO `mountpark_data` VALUES ('9204', '369', '8', '8', '0', '-1', '1952500', '1952500', '', '', '341', '355', '', '341;356;371;326;311;296;281;266;251;237;252;267;282;297;312;327;342;357;343;328;313;298;238;223;209;224;210;211;226;255;269;284;299;314;329;315;300;285;270;196;195;181;241;256;271;286;301;287;272;257;242;227;212;197;182;167;153;168;183;198;213;228;243;258', '');
INSERT INTO `mountpark_data` VALUES ('9158', '585', '12', '12', '0', '-1', '2695000', '2695000', '', '', '547', '566', '', '547;529;511;493;475;565;583;601;619;637;618;600;582;564;546;528;510;492;474;456;437;455;473;491;509;527;545;526;508;490;472;454;436;418;381;363;345;327;309;399;417;435;453;471;489;507;525;543;561;542;524;506;488;470;452;434;416;398;380;362;344;326;308;290;271;289;307;325;343;361;379;397;415;433;451;469;487;505;504;486;468;450;432;414;396;378;360;342;324;306;288;270;252;233;251;269;287;305;323;341;359;377;395;413;431;412;394;376;358;375;393;357;320;338;356;374;355;337;319', '');
INSERT INTO `mountpark_data` VALUES ('9156', '605', '5', '5', '0', '-1', '1210000', '1210000', '', '', '567', '586', '', '567;585;603;549;531;513;495;477;459;440;458;476;494;512;530;548;566;584;565;547;529;511;493;475;457;439;421;402;420;438;456;474;492;510;528;546;527;509;491;473;455;418;400;382;383;346;328;310;364;436;454;472;490;508;489;471;434;435;417;399;381;363;345;327;309;291;272;290;308;326;344;362;380;398;416;452;470;451;433;415;397;379;361;343;325;307;289;271;253;234;252;270;288;306;324;342;360;378;396;414;432;413;395;377;359;341;323;305;287;269;251;250;586;605;624;401;419;233', '');
INSERT INTO `mountpark_data` VALUES ('9157', '523', '5', '5', '0', '-1', '1210000', '1210000', '', '', '487', '505', '', '487;506;468;449;430;393;392;374;412;431;450;469;488;507;508;489;470;451;432;413;395;414;433;452;471;490;472;453;434;415;396;377;359;378;397;416;435;454;436;417;398;379;360;341;322;303;284;266;285;304;323;342;361;380;399;418;343;306;305;286;267;248;230;249;268;287;325;307;288;269;250;231', '');
INSERT INTO `mountpark_data` VALUES ('9159', '676', '14', '14', '0', '-1', '3135000', '3135000', '', '', '640', '658', '', '640;622;604;586;568;550;531;549;567;585;603;621;602;584;566;512;493;511;529;547;565;583;475;457;439;421;403;420;438;456;474;492;510;528;546;564;527;509;491;473;455;437;419;400;418;436;454;472;490;508;526;507;525;524;542;561;506;488;470;452;434;416;398;380;362;344;326;308;327;523;505;487;469;451;433;415;397;379;361;343;325;307;289;270;288;306;324;342;360;378;396;414;432;450;468;486;504;485;467;449;431;413;395;377;359;341;323;305;287;269;251;232;250;268;286;304;322;340;358;376;394;412;430;448;466;447;429;411;393;375;357;339;321;303;285;267;249;231', '');
INSERT INTO `mountpark_data` VALUES ('9162', '558', '14', '14', '0', '-1', '3135000', '3135000', '', '', '522', '540', '', '503;484;465;447;541;560;579;598;617;636;655;674;656;637;618;599;580;561;542;523;504;485;466;429;448;467;486;505;524;543;562;581;600;619;638;620;601;582;563;544;525;506;487;468;449;430;411;412;431;450;469;488;507;526;545;564;583;602;565;546;527;508;489;470;451;414;432;377;433;452;471;490;509;528;547;566;548;529;510;491;472;453;434;415;396;359;378;397;416;435;454;473;492;511;530;512;493;474;455;436;417;398;379;360;341;322;304;285;323;342;361;380;399;400;381;362;343;324;305;286;267;249;268;287;306;325;344;363;382;401;419;402;421;440;458;476;494;383;364;345;326;231;250;269;288', '');
INSERT INTO `mountpark_data` VALUES ('9209', '220', '5', '5', '0', '-1', '1210000', '1210000', '', '', '192', '206', '', '192;207;222;177;148;133;132;117;103;118;163;178;193;208;179;164;149;134;119;104;89;75;90;105;120;135;150;151;166;136;121;106;91;76;62;77;92;107;122;137;152;138;123;108;93;78;63', '');
INSERT INTO `mountpark_data` VALUES ('9218', '226', '8', '8', '0', '-1', '1952500', '1952500', '', '', '196', '211', '', '196;210;224;182;168;154;140;126;112;97;111;125;139;153;167;181;195;209;180;166;152;138;124;110;96;82;81;95;109;123;137;151;165;179;164;150;136;122;108;94;80;66;79;93;107;121;135;149;134;120;106;92;78;64;49;63;77;91;105;119;104;90;76;62;75;74;89;60;45', '');
INSERT INTO `mountpark_data` VALUES ('9163', '599', '7', '7', '0', '-1', '1173500', '1173500', '', '', '563', '581', '', '563;582;601;602;639;544;525;506;507;489;452;451;450;468;449;430;411;392;374;393;412;431;471;490;509;528;547;584;621;603;565;546;527;508;432;413;394;375;356;338;357;376;395;414;433;566;585;567;548;529;510;491;472;453;434;415;396;377;358;339;320;302;321;340;359;378;397;416;435;454;455;474;511;530;549;531;512;493;436;417;398;379;360;341;322;303;284;266;285;304;323;342;361;399;418;437;456;475;494;513;495;476;457;438;419;362;343;324;305;286;268;287;306;401;420;439;458', '');
INSERT INTO `mountpark_data` VALUES ('9160', '375', '5', '5', '0', '-1', '1210000', '1210000', '', '', '339', '357', '', '339;358;377;396;415;320;301;282;263;226;245;264;283;359;378;397;379;360;341;322;303;266;247;246;227;208;190;209;228;285;304;323;342;361;343;324;305;286;249;230;211;192;173;172;154;268;287;306;325;307;288;269;250;231;193;174;155', '');
INSERT INTO `mountpark_data` VALUES ('9164', '691', '5', '5', '0', '-1', '1210000', '1210000', '', '', '655', '673', '', '655;636;617;674;675;656;637;618;599;580;561;542;523;504;485;467;486;505;524;543;562;581;600;619;638;657;639;620;601;563;544;525;506;487;468;449;431;450;469;488;507;526;583;602;621;603;584;565;528;509;490;489;470;451;432;413;471;453;472;491;510;529;548;567;547;566;585;549;530;511;492;473;454;435;417;380;361;342;341;359;512;493;474;455;436;323;494;495;476;457;438;419;418;343;324;305;287;306;325;420;439;458;477;459;440;421;344;345;364;326;307;288', '');
INSERT INTO `mountpark_data` VALUES ('9207', '313', '6', '6', '0', '-1', '1512500', '1512500', '', '', '283', '298', '', '283;297;269;255;241;227;213;198;212;226;240;254;268;282;267;253;239;225;211;197;183;168;182;238;252;237;223;194;180;166;167;153;138;152;208;222;207;193;179;165;151;137;123;108;122;136;150;164;178;192;177;163;149;135;121;107', '');
INSERT INTO `mountpark_data` VALUES ('9219', '161', '12', '12', '0', '-1', '2695000', '2695000', '', '', '133', '147', '', '133;118;103;148;163;178;193;208;223;238;253;239;224;209;194;179;164;149;134;105;104;89;75;120;135;150;165;180;195;210;225;211;196;181;166;151;136;121;106;91;62;77;92;107;137;152;167;182;197;183;168;153;138;123;108;93;78;63;48;49;64;79;94;109;124;139;154;169;155;140;125;110;95;80;65;50;81;96;111;126', '');
INSERT INTO `mountpark_data` VALUES ('9220', '205', '8', '8', '0', '-1', '1952500', '1952500', '', '', '177', '191', '', '177;162;147;132;117;102;178;207;222;237;223;208;193;163;148;133;118;103;88;74;89;104;119;134;149;164;179;194;165;150;135;120;105;90;75;60;61;76;91;106;121;136;151;166;181;167;152;137;122;107;92;47', '');
INSERT INTO `mountpark_data` VALUES ('9208', '429', '8', '8', '0', '-1', '1952500', '1952500', '', '', '401', '415', '', '326;341;356;371;386;401;416;431;446;461;447;433;419;405;376;390;404;418;432;417;403;389;375;361;346;360;374;388;402;387;373;359;345;331;316;330;344;358;372;357;343;329;315;301;286;300;314;328;342;327;313;299;285;271;256;270;284;298;312', '');
INSERT INTO `mountpark_data` VALUES ('9165', '433', '7', '7', '0', '-1', '1173500', '1173500', '', '', '397', '415', '', '302;321;340;359;378;397;416;435;454;436;417;379;360;341;322;303;284;265;247;266;285;304;323;342;361;380;399;418;400;381;362;343;324;305;286;267;248;229;211;230;249;268;287;306;325;344;363;382;364;345;326;307;288;269;250;231;212;193;175;194;213;232;251;270;289;308;327;346;328;309;290;271;252;233;214;195;176', '');
INSERT INTO `mountpark_data` VALUES ('9152', '503', '14', '14', '0', '-1', '3135000', '3135000', '', '', '467', '485', '', '429;448;467;486;505;524;543;562;581;600;582;563;544;525;506;487;468;449;411;412;450;469;488;507;526;545;564;546;527;508;489;470;451;432;413;394;357;376;395;433;452;471;509;528;510;491;472;453;434;415;396;377;358;339;321;340;378;397;416;435;454;473;492;474;455;436;417;398;379;360;341;322;303;285;304;323;342;361;380;399;418;437;456;438;419;400;381;362;343;324;305;286;249;268;287;306;325;344;363;382;401;420', '');
INSERT INTO `mountpark_data` VALUES ('9166', '560', '7', '7', '0', '-1', '1173500', '1173500', '', '', '524', '542', '', '524;543;562;581;600;505;486;467;449;468;487;506;525;544;563;582;564;545;526;507;488;469;450;432;451;470;489;508;527;546;528;509;490;471;452;433;414;395;510;491;472;453;434;415;396;377;358;339;320;301;302;283;321;416;435;454;473;492;474;455;436;417;398;379;378;284;265;247;266;267;286;305;324;361;380;399;418;437;456;438;419;400;381;362;343;248;229;287;306;325;344;363;382;401;420;269;288;307;326;308;289;270', '');
INSERT INTO `mountpark_data` VALUES ('9222', '222', '7', '7', '0', '-1', '1173500', '1173500', '', '', '194', '208', '', '194;209;224;179;164;149;134;119;104;89;75;90;105;120;135;150;165;180;195;210;196;181;166;151;136;121;106;91;76;92;107;122;152;167;182;168;153;124;95;94;108;93;78;79;110;139;154;80;65;125;140;126;111;96;81;66;52;67;82;97;112;98;83;68;53;54', '');
INSERT INTO `mountpark_data` VALUES ('9210', '226', '5', '5', '0', '-1', '1210000', '1210000', '', '', '198', '212', '', '198;183;168;213;228;214;199;184;169;154;125;110;95;140;155;170;200;186;171;156;127;126;111;96;81;67;82;97;112;142;157;172;158;143;128;113;98;83;68;53;39;54;69;84;99;114;129;144;115;100;85;70', '');
INSERT INTO `mountpark_data` VALUES ('9167', '449', '12', '12', '0', '-1', '2695000', '2695000', '', '', '413', '431', '', '413;432;451;470;489;508;394;375;356;337;318;299;280;262;281;300;319;338;357;376;395;414;433;452;471;490;472;453;434;415;396;377;358;339;320;301;282;263;244;226;245;264;283;302;321;340;359;378;397;416;435;454;436;417;398;379;360;341;322;303;246;227;208;190;209;228;247;304;323;342;361;380;399;418;400;381;362;343;324;305;286;229;210;191;172;192;211;287;344;363;382;364;345;174;156;175;193;268;250;232;251;270;289;308;327;346;328;309;290;271;252;233;214;176;157', '');
INSERT INTO `mountpark_data` VALUES ('9153', '603', '8', '8', '0', '-1', '1952500', '1952500', '', '', '567', '585', '', '567;586;605;624;643;548;529;510;491;473;511;530;549;568;587;606;625;607;588;569;550;531;512;493;474;455;589;570;551;532;513;494;475;456;437;418;399;380;343;342;324;362;381;400;419;438;457;476;495;514;496;477;458;439;420;401;382;363;344;325;306;288;307;326;345;364;383;402;421;440;459;478;270;289;308;327;346;365;347;328;309;290;271;252;234;253;272;291;310;329;311;292;273;254', '');
INSERT INTO `mountpark_data` VALUES ('9168', '468', '8', '8', '0', '-1', '1952500', '1952500', '', '', '432', '450', '', '432;451;470;489;508;527;546;565;413;394;375;356;338;357;376;395;414;433;452;471;490;509;547;529;510;491;472;453;434;415;396;377;358;339;320;302;321;340;359;378;397;416;435;454;473;492;511;493;474;455;436;417;398;379;360;341;322;303;284;266;285;304;323;342;418;437;456;475;438;419;400;363;344;325;324;305;286;267;248;230;249;268;287;306;382;401;420;439;421;402;383;364;345;326;307;288;269;250;231;212;194;213;232;251;270;289;308;327;346;365;384', '');
INSERT INTO `mountpark_data` VALUES ('9211', '429', '6', '6', '0', '-1', '1512500', '1512500', '', '', '401', '415', '', '401;416;431;446;386;371;356;341;327;342;357;372;387;402;417;432;418;403;388;373;358;343;328;313;299;314;329;344;359;374;389;404;390;375;360;345;330;315;300;285;271;286;301;316;331;346;361;376;347;332;317;302;287;272', '');
INSERT INTO `mountpark_data` VALUES ('9223', '197', '4', '4', '0', '-1', '962500', '962500', '', '', '169', '183', '', '169;154;139;124;109;94;79;184;199;185;170;155;140;125;110;95;80;65;51;66;96;111;126;141;156;171;157;142;127;112;97;82;67;83;143;128;113;98;69;84;99;114;129;115;100;85;70', '');
INSERT INTO `mountpark_data` VALUES ('9169', '414', '7', '7', '0', '-1', '1173500', '1173500', '', '', '378', '396', '', '378;397;416;435;359;322;285;266;265;284;341;360;379;398;417;399;400;401;438;475;512;530;494;380;361;342;323;304;247;248;267;286;305;324;343;362;381;457;439;420;382;363;344;325;306;287;268;249;230;211;193;212;231;250;269;288;307;326;345;364;383;402;421;422;403;384;365;346;327;308;289;270;251;232;213;194;175;214;233;252;271;253;234;215;196;178;197;216;235;217;198;179', '');
INSERT INTO `mountpark_data` VALUES ('9213', '206', '5', '5', '0', '-1', '1210000', '1210000', '', '', '178', '192', '', '178;193;179;165;151;122;108;109;94;136;150;164;163;149;135;121;107;93;79;64;78;92;106;120;119;105;91;77;63;49;34;48;62;76;90;104;118;103;89;75', '');
INSERT INTO `mountpark_data` VALUES ('9225', '219', '6', '6', '0', '-1', '1512500', '1512500', '', '', '191', '205', '', '191;176;206;207;178;163;148;147;118;103;133;193;179;164;149;134;119;104;89;74;75;90;105;120;135;150;165;151;136;121;106;91;92;107;122;137;123;108;93;78', '');
INSERT INTO `mountpark_data` VALUES ('8773', '270', '3', '3', '0', '-1', '770000', '770000', '', '', '242', '256', '', '242;257;258;243;228;213;198;183;168;227;169;154;184;199;214;200;185;170;155;140;126;141;156;171;186;201;216;202;187;172;157;142;127;112', '');
INSERT INTO `mountpark_data` VALUES ('8778', '263', '5', '5', '0', '-1', '1210000', '1210000', '', '', '235', '249', '', '235;250;265;220;205;190;161;176;191;206;221;236;251;237;222;207;192;177;162;148;163;178;223;209;194;165;164;149;134;119;105;120;135;150;180;195;181;166;151', '');
INSERT INTO `mountpark_data` VALUES ('8783', '326', '5', '5', '0', '-1', '1210000', '1210000', '', '', '298', '312', '', '298;313;328;343;283;268;269;284;299;314;329;315;300;285;270;255;240;241;256;271;286;301;272;257;242;228;213;198;183;168;153;138;123;109;124;139;154;169;184;199;137;152;167;182;196;195;209', '');
INSERT INTO `mountpark_data` VALUES ('8788', '384', '6', '6', '0', '-1', '1512500', '1512500', '', '', '356', '370', '', '356;341;326;311;296;371;386;401;387;372;357;342;327;312;297;282;268;283;298;313;328;343;358;373;254;269;284;299;314;329;344;359;345;330;315;300;285;270;255;240;256;301;316', '');
INSERT INTO `mountpark_data` VALUES ('8793', '370', '5', '5', '0', '-1', '1210000', '1210000', '', '', '299', '314', '', '299;313;327;285;271;257;228;214;242;256;270;284;298;312;297;283;269;255;226;240;254;268;282;197;183;184;198;211;225;239;253;267;252;238;224;210;196;182;237;223;209;195;181;167;153;139;154;169', '');
INSERT INTO `mountpark_data` VALUES ('8798', '323', '6', '6', '0', '-1', '1512500', '1512500', '', '', '295', '309', '', '295;310;325;340;326;312;298;284;270;256;242;228;214;199;213;227;241;255;269;283;297;311;296;282;268;254;240;226;212;198;184;169;183;197;211;225;239;253;267;281;280;266;252;238;224;210;196;182;168;154;139;153;167;181;166;152;138', '');
INSERT INTO `mountpark_data` VALUES ('8803', '160', '3', '3', '0', '-1', '770000', '770000', '', '', '132', '146', '', '132;117;102;147;162;177;192;178;163;148;133;118;103;88;74;89;60;75;46;61;76;91;106;121;136;107;92;77;62;47', '');
INSERT INTO `mountpark_data` VALUES ('8804', '125', '6', '6', '0', '-1', '1512500', '1512500', '', '', '153', '139', '', '153;138;123;108;168;183;198;213;228;243;258;272;257;227;212;197;182;167;152;137;122;136;151;166;181;196;211;226;241;271;286;300;285;284;255;240;225;210;195;180;165;150;164;179;194;209;224;239;254;269;299', '');
INSERT INTO `mountpark_data` VALUES ('8799', '383', '6', '6', '0', '-1', '1512500', '1512500', '', '', '355', '369', '', '355;370;385;400;340;325;310;295;281;296;311;326;341;356;371;386;372;357;342;327;312;297;282;267;253;268;283;298;313;328;314;299;284;269;254;239;224;210;225;240;255;270;285;300;286;271;256;241;226;211', '');
INSERT INTO `mountpark_data` VALUES ('8794', '398', '4', '4', '0', '-1', '962500', '962500', '', '', '370', '384', '', '370;385;400;356;371;386;372;357;342;328;343;358;373;388;403;418;433;404;389;374;359;344;329;314;299;284;269;255;270;285;300;315;330;345;360;375;390;241', '');
INSERT INTO `mountpark_data` VALUES ('8789', '180', '12', '12', '0', '-1', '2695000', '2695000', '', '', '208', '194', '', '208;223;238;253;268;283;193;178;163;148;133;132;103;117;147;162;177;192;207;222;237;252;267;282;297;311;296;281;266;251;236;221;206;191;176;161;146;131;160;175;190;205;220;235;250;265;280;295;310;325;339;324;309;294;279;264;249;234;219;204;189;218;233;248;263;278;293;308;323;338;353;352;337;322;307;292;277;306;321;336;351', '');
INSERT INTO `mountpark_data` VALUES ('8779', '157', '6', '6', '0', '-1', '1512500', '1512500', '', '', '127', '142', '', '183;168;153;169;154;139;124;109;94;79;65;51;37;23;24;39;54;69;84;99;113;127;141;155;140;125;110;95;80;66;81;96;111;126;112;97;82;67;52;38;53;68;83;98', '140;3000;2997|141;3000;3000|153;3000;2990|154;3000;2983');
INSERT INTO `mountpark_data` VALUES ('8774', '298', '2', '2', '0', '-1', '522500', '522500', '', '', '326', '312', '', '326;311;296;310;324;338;352;366;381;367;353;339;325;340;354;368;382;396;411;397;383;369;355;341;356;370;384;398;412', '');
INSERT INTO `mountpark_data` VALUES ('8770', '236', '5', '5', '0', '-1', '1210000', '1210000', '', '', '208', '222', '', '208;223;238;193;178;149;164;179;194;209;224;210;195;180;165;150;135;120;105;91;106;121;136;151;166;181;196;182;167;152;137;122;107;92;78;93;108;123;138;153', '');
INSERT INTO `mountpark_data` VALUES ('8780', '341', '4', '4', '0', '-1', '962500', '962500', '', '', '313', '327', '', '358;343;328;313;283;268;254;240;226;241;227;242;256;271;257;272;286;301;287;302;316;330;344;329;315;300;314;299;285;284;298', '');
INSERT INTO `mountpark_data` VALUES ('8805', '314', '5', '5', '0', '-1', '1210000', '1210000', '', '', '284', '299', '', '228;242;256;270;284;298;312;326;297;283;269;255;212;241;254;268;282;296;281;267;253;239;225;211;197;168;196;210;224;238;252;266;251;237;223;209;167;181;153;138;152;166;180;194;208;222;236;221;207;193;179;165;151;137;123', '');
INSERT INTO `mountpark_data` VALUES ('8806', '324', '6', '6', '0', '-1', '1512500', '1512500', '', '', '296', '310', '', '296;281;266;252;238;224;210;196;182;168;154;169;183;197;211;225;239;253;267;282;268;254;240;226;212;198;184;199;213;227;241;255;269;283;297;311;326;312;298;284;270;242;228;214;229;243;257;271;285;299;313;327', '');
INSERT INTO `mountpark_data` VALUES ('8832', '295', '8', '8', '0', '-1', '1952500', '1952500', '', '', '267', '281', '', '357;342;327;312;297;282;252;237;222;208;194;180;166;152;138;139;125;153;167;181;195;209;223;238;224;210;196;182;168;154;140;155;169;198;212;226;240;254;268;253;239;225;211;197;183;170;184;283;269;255;241;227;213;199;185;200;214;228;242;256;270;284;298;313;299;285;271;286;300;314;328;343;329;301;315', '');
INSERT INTO `mountpark_data` VALUES ('8791', '295', '4', '4', '0', '-1', '962500', '962500', '', '', '267', '280', '', '297;282;252;237;222;208;223;238;253;268;283;269;254;239;224;209;194;180;195;210;225;240;255;241;226;211;196;181;166;152;167;182;197;212;227', '');
INSERT INTO `mountpark_data` VALUES ('8786', '294', '4', '4', '0', '-1', '962500', '962500', '', '', '266', '280', '', '296;281;251;237;252;267;282;268;253;238;223;209;224;239;254;240;225;210;195;181;196;211;226;212;197;182;167;153;168;183;198;184;169;154;139', '251;3000;1733|252;3000;1217|267;3000;1537|296;3000;1108');
INSERT INTO `mountpark_data` VALUES ('8781', '314', '3', '3', '0', '-1', '770000', '770000', '', '', '284', '299', '', '284;298;312;326;270;256;242;228;213;227;241;255;269;283;297;311;296;282;268;254;240;226;212;198;183;197;211;225;239;253;267', '');
INSERT INTO `mountpark_data` VALUES ('8776', '196', '3', '3', '0', '-1', '770000', '770000', '', '', '224', '210', '', '224;209;194;208;222;236;250;279;293;292;278;307;265;251;237;223;238;252;266;280;309;323;322;337;295;281;267;253;239;254;268;282;310;324;338', '');
INSERT INTO `mountpark_data` VALUES ('8813', '298', '4', '4', '0', '-1', '962500', '962500', '', '', '270', '284', '', '270;285;300;315;330;345;255;241;256;271;286;301;316;331;302;287;272;257;242;227;213;228;243;258;273;288;303;229;214;199', '');
INSERT INTO `mountpark_data` VALUES ('8817', '315', '3', '3', '0', '-1', '770000', '770000', '', '', '285', '300', '', '285;271;299;313;298;284;270;256;241;255;269;254;240;226;211;225;239;238;224;210;196;181;195;209;223;208;194;180', '');
INSERT INTO `mountpark_data` VALUES ('8821', '278', '4', '4', '0', '-1', '962500', '962500', '', '', '250', '264', '', '250;265;280;295;310;235;221;236;251;266;281;296;282;267;252;237;222;207;208;223;238;253;268;254;239;224;209;194;180;195;210;225', '');
INSERT INTO `mountpark_data` VALUES ('8825', '326', '3', '3', '0', '-1', '770000', '770000', '', '', '298', '312', '', '298;313;328;343;358;344;329;314;299;284;270;285;300;315;330;256;271;286;242;257;272;228;243;258;244;229;214;200;215', '');
INSERT INTO `mountpark_data` VALUES ('8829', '298', '2', '2', '0', '-1', '522500', '522500', '', '', '268', '283', '', '268;282;254;240;226;212;197;211;225;239;253;267;252;238;224;210;196;182;167;181;195;209;223;237;222', '');
INSERT INTO `mountpark_data` VALUES ('8833', '283', '2', '2', '0', '-1', '522500', '522500', '', '', '253', '268', '', '253;239;225;211;267;281;266;252;238;224;210;196;181;195;209;251;237;208;194;180;166', '');
INSERT INTO `mountpark_data` VALUES ('8837', '215', '4', '4', '0', '-1', '962500', '962500', '', '', '185', '200', '', '185;171;157;143;199;213;227;241;255;269;254;240;226;212;198;184;170;156;142;113;112;141;155;169;183;197;211;225;239;224;209;195;196;182;168;154;126;98;83;97', '');
INSERT INTO `mountpark_data` VALUES ('8838', '313', '5', '5', '0', '-1', '1210000', '1210000', '', '', '283', '298', '', '283;297;269;255;241;227;213;199;185;171;156;170;169;198;212;226;240;254;268;282;267;253;239;225;211;197;183;155;141;126;140;154;168;182;196;210;167;153;139;125', '');
INSERT INTO `mountpark_data` VALUES ('8834', '146', '2', '2', '0', '-1', '522500', '522500', '', '', '90', '118', '', '146;89;104;119;134;148;133;118;46;61;76;91;106;121;135;120;105;90;75;60;88', '');
INSERT INTO `mountpark_data` VALUES ('8830', '366', '6', '6', '0', '-1', '1512500', '1512500', '', '', '338', '352', '', '338;353;368;383;323;308;293;279;294;309;324;339;354;369;355;340;325;310;295;280;265;251;266;281;296;311;326;341;327;312;297;282;267;252;237;222;223;238;253;268;283;298;313;299;284;269;254;239;224;209;225;240;255;241;226', '');
INSERT INTO `mountpark_data` VALUES ('8826', '295', '8', '8', '0', '-1', '1952500', '1952500', '', '', '267', '281', '', '267;252;237;222;207;192;282;297;312;327;342;328;313;298;283;268;253;238;223;208;193;178;179;194;209;224;239;254;269;284;299;314;300;285;270;255;240;225;210;195;180;166;181;196;211;226;241;256;271;286;272;257;242;227;212;197;182;167;152', '');
INSERT INTO `mountpark_data` VALUES ('8822', '190', '4', '4', '0', '-1', '962500', '962500', '', '', '162', '176', '', '162;177;147;132;117;102;73;88;103;118;133;148;163;149;134;119;104;89;74;59;45;60;75;90;105;120;135;121;106;91;76;61;46', '');
INSERT INTO `mountpark_data` VALUES ('8818', '299', '4', '4', '0', '-1', '962500', '962500', '', '', '269', '284', '', '269;255;241;227;283;297;311;325;339;353;338;324;310;281;282;268;254;240;226;212;197;211;225;253;267;295;309;323;308;293;279;265;251;237;223;209;195;181;167;152;166;180;194;208;222;236;250;264', '');
INSERT INTO `mountpark_data` VALUES ('8814', '396', '6', '6', '0', '-1', '1512500', '1512500', '', '', '368', '382', '', '368;383;353;338;323;308;293;279;294;309;324;339;354;369;355;340;325;296;281;266;265;251;295;280;311;326;341;327;312;297;282;267;252;223;208;238;253;268;283;298;313;299;284;269;254;239;224;209;194', '');
INSERT INTO `mountpark_data` VALUES ('4590', '622', '5', '5', '0', '-1', '1210000', '1210000', '', '', '586', '604', '', '586;567;548;605;624;643;625;606;587;568;549;530;512;531;550;569;588;607;589;570;551;532;513;494;476;495;514;533;552;515;496;477;458;440;459;478;422;441', '');
INSERT INTO `mountpark_data` VALUES ('4586', '511', '5', '5', '0', '-1', '1210000', '1210000', '', '', '475', '493', '', '475;456;437;418;494;513;495;476;457;438;419;400;382;401;420;439;458;477;459;440;403;402;383;364;346;365;384;422;423;404;385;366;347;328;386', '');
INSERT INTO `mountpark_data` VALUES ('4605', '395', '5', '5', '0', '-1', '1210000', '1210000', '', '', '359', '377', '', '321;340;359;378;397;379;360;341;322;303;285;304;323;342;361;343;324;305;286;267;249;268;287;306;325;307;288;269;250;231;213;232;251;270;289', '');
INSERT INTO `mountpark_data` VALUES ('4600', '494', '5', '5', '0', '-1', '1210000', '1210000', '', '', '456', '475', '', '456;474;492;510;438;420;402;384;365;383;401;419;437;455;473;491;472;454;436;418;400;382;364;346;327;345;363;381;399;417;435', '');
INSERT INTO `mountpark_data` VALUES ('4596', '674', '5', '5', '0', '-1', '1210000', '1210000', '', '', '620', '647', '', '582;564;546;547;585;623;661', '');
INSERT INTO `mountpark_data` VALUES ('4606', '454', '5', '5', '0', '-1', '1210000', '1210000', '', '', '416', '435', '', '380;379;360;341;322;285;304;323;342;303;321;339;357;376;358;340;359;377;395;414;396;378;397;416;434;452;433;415', '');
INSERT INTO `mountpark_data` VALUES ('4584', '561', '5', '5', '0', '-1', '1210000', '1210000', '', '', '434', '498', '', '487;469;414;396;378', '');
INSERT INTO `mountpark_data` VALUES ('4595', '190', '5', '5', '0', '-1', '1210000', '1210000', '', '', '226', '208', '', '169;188;207;226;245;264;282;263;244;225;206;187;205;224;243;262;281;300;318;299;280;261;242;223;241;260;279;298;317;336;354;335;316;297', '');
INSERT INTO `mountpark_data` VALUES ('4624', '181', '5', '5', '0', '-1', '1210000', '1210000', '', '', '145', '163', '', '183;164;145;126;107;88;70;89;108;127;146;165;147;128;109;90;71;52;34;53;72;91;110;73;54;35;36', '');
INSERT INTO `mountpark_data` VALUES ('4625', '694', '5', '5', '0', '-1', '1210000', '1210000', '', '', '640', '667', '', '602;584;566;620', '');
INSERT INTO `mountpark_data` VALUES ('4627', '177', '5', '5', '0', '-1', '1210000', '1210000', '', '', '141', '159', '', '141;122;160;179;198;217;199;180;161;142;123;104;86;105;124;143;162;181;163;144;125;106;87;68;50;69;88;107;126;145;127;108;89;70;51;32;33;34;53;72;91;54;90', '');
INSERT INTO `mountpark_data` VALUES ('4626', '697', '5', '5', '0', '-1', '1210000', '1210000', '', '', '661', '679', '', '661;642;680;699;718;737;756;775;776;757;738;719;700;681;662;643;624;606;625;644;663;682;701;720;739;702;683;664;645;626;607;588;570;589;608;627;646;665;628;609;590;571', '');
INSERT INTO `mountpark_data` VALUES ('8753', '226', '5', '5', '0', '-1', '1210000', '1210000', '', '', '188', '207', '', '224;205;186;167;149;130;168;187;206;188;169;150;131;112;93;75;113;151;170;152;133;114;77;95;76;20;57;39;58;97;116;134;59', '');
INSERT INTO `mountpark_data` VALUES ('8754', '598', '5', '5', '0', '-1', '1210000', '1210000', '', '', '634', '616', '', '577;596;615;634;653;672;690;671;652;633;614;595;613;632;651;670;689;708;726;707;688;669;650;631;649;668;687;706;725;744', '');
INSERT INTO `mountpark_data` VALUES ('8755', '560', '5', '5', '0', '-1', '1210000', '1210000', '', '', '615', '597', '', '615;634;653;672;691;596;577;558;576;595;614;633;652;671;690;709;727;708;689;670;651;632;613;594;612;631;650;669;688;707;726', '');
INSERT INTO `mountpark_data` VALUES ('8756', '606', '5', '5', '0', '-1', '1210000', '1210000', '', '', '644', '625', '', '644;626;608;590;662;680;698;717;699;681;663;645;627;609;628;646;664;682;700;718;736;755;737;719;701;683;665;702;720;738;756;774;775;757;739', '');
INSERT INTO `mountpark_data` VALUES ('4591', '525', '5', '5', '0', '-1', '1210000', '1210000', '', '', '487', '506', '', '451;469;487;505;523;541;522;504;486;468;450;432;413;431;449;467;485;503;484;466;448;430;412;394;375;411;429;447;465', '');
INSERT INTO `mountpark_data` VALUES ('4593', '225', '5', '5', '0', '-1', '1210000', '1210000', '', '', '189', '207', '', '189;208;170;151;132;113;76;95;114;133;152;171;190;172;153;134;115;96;77;58;39;21;40;59;78;97;116;135;154;136;117;98;79;60;41;22;23;42;61;80;99', '');
INSERT INTO `mountpark_data` VALUES ('4628', '675', '5', '5', '0', '-1', '1210000', '1210000', '', '', '639', '657', '', '529;548;567;586;605;624;642;623;604;585;566;584;603;622;641;660;678;659;640;621;602;583;601;620;639;658;677;696', '');
INSERT INTO `mountpark_data` VALUES ('4620', '263', '5', '5', '0', '-1', '1210000', '1210000', '', '', '225', '244', '', '225;243;207;189;171;153;134;152;170;188;206;224;205;187;169;151;133;115;96;114;132;150', '');
INSERT INTO `mountpark_data` VALUES ('4616', '618', '5', '5', '0', '-1', '1210000', '1210000', '', '', '654', '636', '', '654;635;616;634;652;670;688;706;724;743;725;707;689;671;653;672;690;708;726;744;762;763;745;727;709;691;673;692;710;728;746', '');
INSERT INTO `mountpark_data` VALUES ('4622', '712', '5', '5', '0', '-1', '1210000', '1210000', '', '', '674', '693', '', '638;656;674;692;710;691;673;655;637;619;600;618;636;654;672;690;671;653;635;617;599;581;562;580;598;616;634;652;633;615;597;579;561;543;524;542;560;578;596;577;559;541;523;505', '');
INSERT INTO `mountpark_data` VALUES ('4621', '378', '5', '5', '0', '-1', '1210000', '1210000', '', '', '342', '360', '', '285;304;323;342;361;380;399;381;362;343;324;305;286;267;248;230;249;268;287;306;325;344;363;345;326;307;288;269;250;231;212;194;213;232;251;270;289;308;327;309;290;271;252;233;214;195', '');
INSERT INTO `mountpark_data` VALUES ('4614', '355', '5', '5', '0', '-1', '1210000', '1210000', '', '', '317', '336', '', '353;335;317;281;299;263;244;262;280;298;316;334;315;297;279;243;261;225;206;224;242;260;278;241;223;205;187;168;186;204;167;149;130', '');
INSERT INTO `mountpark_data` VALUES ('4615', '204', '5', '5', '0', '-1', '1210000', '1210000', '', '', '168', '186', '', '168;187;149;130;93;112;131;150;169;151;132;113;94;75;56;19;38;57;76;95;114;133;115;96;77;58;39;20;21;40;59;78;97;79;60;41;22;23', '');
INSERT INTO `mountpark_data` VALUES ('4592', '542', '5', '5', '0', '-1', '1210000', '1210000', '', '', '506', '524', '', '506;487;525;544;563;545;526;507;488;469;451;470;489;508;527;509;490;471;452;433;491', '');
INSERT INTO `mountpark_data` VALUES ('4589', '269', '5', '5', '0', '-1', '1210000', '1210000', '', '', '233', '251', '', '233;214;195;252;271;253;234;215;196;177;159;178;197;216;235;217;198;179;160;141;123;142;161;180;199;181;162;143;124', '');
INSERT INTO `mountpark_data` VALUES ('4583', '752', '5', '5', '0', '-1', '1210000', '1210000', '', '', '716', '734', '', '716;697;678;659;735;754;773;774;755;736;717;698;679;660;641;642;661;680;699;718;737;756;775;776;757;738;719;700;681;662;643;624;605;587;606;625;644;663;682;701;720', '');
INSERT INTO `mountpark_data` VALUES ('4598', '598', '5', '5', '0', '-1', '1210000', '1210000', '', '', '634', '616', '', '634;615;596;577;653;672;690;671;652;633;614;595;613;632;651;670;689;708;726;707;688;669;650;631;649;668;687;706;725;744;762;686;667;704;723;722', '');
INSERT INTO `mountpark_data` VALUES ('5139', '641', '5', '5', '0', '-1', '1210000', '1210000', '', '', '679', '660', '', '607;625;643;661;679;697;716;698;680;662;644;626;645;663;681;699;717;735;754;736;718;700;682;664;683;719;737;755;773;774;756;738;720;702;739;757;775;776', '');
INSERT INTO `mountpark_data` VALUES ('5136', '563', '5', '5', '0', '-1', '1210000', '1210000', '', '', '527', '545', '', '527;508;546;565;547;528;509;490;453;472;491;510;529;511;492;473;454;435;416;397;379;398;417;436;455;474;493;475;456;437;418;399;380', '');
INSERT INTO `mountpark_data` VALUES ('4930', '421', '5', '5', '0', '-1', '1210000', '1210000', '', '', '385', '403', '', '385;366;348;330;312;294;276;258;295;313;331;349;367;404;386;368;350;332;369;387;405', '');
INSERT INTO `mountpark_data` VALUES ('4607', '578', '5', '5', '0', '-1', '1210000', '1210000', '', '', '614', '596', '', '614;595;576;557;575;593;611;648;630;612;594;613;631;649;667;685;722;704;686;668;650;632;633;651;669;687;705;723;741;760;742;724;706;688;670;652;671;689;707;725', '');
INSERT INTO `mountpark_data` VALUES ('4582', '359', '5', '5', '0', '-1', '1210000', '1210000', '', '', '395', '377', '', '395;376;357;375;393;411;429;447;465;484;466;448;430;412;394;413;431;449;467;485;503;522;504;486;468;450;432;414;433;469;487;505;523;541;560;542;524;506;488;470', '');
INSERT INTO `mountpark_data` VALUES ('4599', '376', '5', '5', '0', '-1', '1210000', '1210000', '', '', '340', '358', '', '340;359;378;321;302;283;265;284;303;322;341;360;342;323;304;285;266;247;229;248;267;286;305;324;306;287;268;249;230;211;193;212;231;250;269;288;270;251;232;213;194', '');
INSERT INTO `mountpark_data` VALUES ('4601', '363', '5', '5', '0', '-1', '1210000', '1210000', '', '', '292', '328', '', '270;252;234;216', '');
INSERT INTO `mountpark_data` VALUES ('4629', '599', '5', '5', '0', '-1', '1210000', '1210000', '', '', '635', '617', '', '635;616;597;578;559;654;673;691;672;653;634;615;596;577;614;633;652;671;690;709;727;708;726;744;763', '');
INSERT INTO `mountpark_data` VALUES ('4644', '583', '5', '5', '0', '-1', '1210000', '1210000', '', '', '619', '601', '', '505;524;543;562;581;600;619;638;657;675;656;637;618;599;580;561;542;523;541;560;579;598;617;636;655;674;693;711;692;673;654;635;616;597;578;559;577;596;615;634;653;672;691;710;729;595;614;633', '');
INSERT INTO `mountpark_data` VALUES ('4646', '578', '5', '5', '0', '-1', '1210000', '1210000', '', '', '614', '596', '', '614;633;652;671;595;576;557;575;594;613;632;651;670;689;707;688;669;650;631;612;593;611;630;649;668;687;706;725;743;724;705;686;667;648;685;704;723;742', '');
INSERT INTO `mountpark_data` VALUES ('4603', '580', '5', '5', '0', '-1', '1210000', '1210000', '', '', '542', '561', '', '542;560;578;524;506;488;469;487;505;523;541;559;540;522;504;486;468;450;431;449;467;485;503;521;502;484;466;448;430', '');
INSERT INTO `mountpark_data` VALUES ('4597', '452', '5', '5', '0', '-1', '1210000', '1210000', '', '', '414', '433', '', '391;337;319;265;284;302;320;338;356;374;392;410;429;411;393;375;357;321;339;303;322;340;358;376;394;412;430;448;467;449;431;413;395;377;359;341;360;378;396;414;432;450;468;486', '');
INSERT INTO `mountpark_data` VALUES ('4609', '245', '5', '5', '0', '-1', '1210000', '1210000', '', '', '209', '227', '', '209;190;171;228;210;191;172;153;135;154;173;192;174;155;136;117;99;118;137;156;138;119;100', '');
INSERT INTO `mountpark_data` VALUES ('4931', '618', '5', '5', '0', '-1', '1210000', '1210000', '', '', '582', '600', '', '620;601;582;563;525;506;487;469;488;526;545;564;583;602;584;565;546;508;489;470;451;433;452;471;490;509;528;547;566;548;529;510;491;472;453;434;415;397;416;435;454;473;492;511;530;512;493;474;455;436;417;398;379;361;380;399;418;437;456;475;494', '');
INSERT INTO `mountpark_data` VALUES ('5127', '617', '5', '5', '0', '-1', '1210000', '1210000', '', '', '579', '598', '', '579;597;561;543;525;507;488;506;524;542;560;578;559;541;523;505;487;469;450;468;486;504;522;540;521;503;485;467;449;431;412;430;448;466;484', '');
INSERT INTO `mountpark_data` VALUES ('5133', '578', '5', '5', '0', '-1', '1210000', '1210000', '', '', '540', '559', '', '612;594;576;558;540;522;504;503;521;539;557;575;593;574;556;538;520;502;483;501;519;537', '');
INSERT INTO `mountpark_data` VALUES ('5278', '322', '5', '5', '0', '-1', '1210000', '1210000', '', '', '358', '340', '', '358;339;320;338;356;374;392;410;428;447;429;411;393;375;357;376;394;412;430;448;466;485;467;449;431;413;395;377;396;414;432;450;468;486', '');
INSERT INTO `mountpark_data` VALUES ('5334', '251', '5', '5', '0', '-1', '1210000', '1210000', '', '', '215', '233', '', '215;196;177;234;253;272;291;273;254;235;216;197;178;159;141;160;179;198;217;236;255;237;218;199;180;142;123;105;124;125;162;181;200;219;238;257;239;220;201;182;163;144;106;87;69;88;107;126;145;164;183;202', '');
INSERT INTO `mountpark_data` VALUES ('4932', '637', '5', '5', '0', '-1', '1210000', '1210000', '', '', '601', '619', '', '601;582;620;639;621;602;583;564;545;527;546;565;584;603;585;566;547;528;509', '');
INSERT INTO `mountpark_data` VALUES ('4562', '524', '5', '5', '0', '-1', '1210000', '1210000', '', '', '560', '542', '', '560;579;598;541;522;503;521;540;559;578;597;616;634;615;596;577;558;539;557;576;595;614;633;652;670;651;632;613;594;575;593;612;631;650;669;688;706;687;668;649;630;611;705;686;667;648;724', '');
INSERT INTO `mountpark_data` VALUES ('4549', '418', '5', '5', '0', '-1', '1210000', '1210000', '', '', '380', '399', '', '321;303;285;267;249;213;232;250;268;286;304;322;340;359;341;323;305;287;269;251;270;288;306;324;342;360;378;397;379;361;343;325;307;289;308;326;344;362;380;398;416', '');
INSERT INTO `mountpark_data` VALUES ('4649', '154', '5', '5', '0', '-1', '1210000', '1210000', '', '', '116', '135', '', '116;98;80;134;152;170;151;133;115;97;79;61;42;60;78;96;114;132;113;95;77;59;41;23;22;40;58;76;94;75;57;39;21;20;38;56', '');
INSERT INTO `mountpark_data` VALUES ('4647', '731', '5', '5', '0', '-1', '1210000', '1210000', '', '', '695', '713', '', '695;676;714;733;752;771;772;753;734;715;696;677;658;640;659;678;716;735;754;773;755;774;736;717;698;679;660;641;622;604;623;642;661;680;699;718;737;756;775;757;738;719;700;681;662;643;624;605;586;568;587;606;625;644;663;682;701;720', '');
INSERT INTO `mountpark_data` VALUES ('4631', '579', '5', '5', '0', '-1', '1210000', '1210000', '', '', '543', '561', '', '543;562;581;524;505;486;467;449;468;487;506;525;544;563;545;526;507;488;469;450;432;451;470;489;508;527;509;490;471;452;433', '');
INSERT INTO `mountpark_data` VALUES ('4630', '523', '5', '5', '0', '-1', '1210000', '1210000', '', '', '485', '504', '', '485;467;449;431;413;503;521;539;520;502;484;466;448;430;412;394;375;393;411;429;447;465;483;501;482;464;446;428;410;392;374;356;337;355;373;391;409;427;445;463;354;336', '');
INSERT INTO `mountpark_data` VALUES ('4633', '544', '5', '5', '0', '-1', '1210000', '1210000', '', '', '580', '562', '', '580;561;542;560;578;596;614;632;650;668;687;706;725;744;745;727;709;691;673;655;637;618;599;579;598;617;636;654;635;616;597;615;634;653;672;690;671;652;633;651;670;689;708;726;707;688;669', '');
INSERT INTO `mountpark_data` VALUES ('4640', '616', '5', '5', '0', '-1', '1210000', '1210000', '', '', '652', '634', '', '652;671;690;708;726;744;762;761;743;725;707;689;670;688;706;724;742;760;759;741;723;705;687;669;651;633;614;632;650;668;686;704;722;685;667;649;631;613', '');
INSERT INTO `mountpark_data` VALUES ('4666', '231', '5', '5', '0', '-1', '1210000', '1210000', '', '', '193', '212', '', '193;175;157;139;211;229;247;228;210;192;174;156;138;120;101;119;137;155;173;191;209;190;172;154;136;118;100;82;171;153;135;117;99;81;63;44;62;80;116;152;61', '');
INSERT INTO `mountpark_data` VALUES ('4588', '620', '5', '5', '0', '-1', '1210000', '1210000', '', '', '656', '638', '', '656;675;693;711;729;747;765;764;746;728;710;692;674;637;655;673;691;709;727;745;763;762;744;726;708;690;672;654;636;618;599;617;635;653;671;689;707;725;743;761;760;742;724;706;688;670;652;634;616;598', '');
INSERT INTO `mountpark_data` VALUES ('4934', '709', '5', '5', '0', '-1', '1210000', '1210000', '', '', '673', '691', '', '692;673;655;674;693;636;618;600;582;564;546;527;545;563;565;583;601;619;637;656;638;620;602;584;603;621;639;657;675;711', '');
INSERT INTO `mountpark_data` VALUES ('5333', '488', '5', '5', '0', '-1', '1210000', '1210000', '', '', '452', '470', '', '452;471;490;509;433;414;396;415;434;453;472;491;473;454;435;416;397;378;360;379;398;417;436;455;437;418;399;380;361', '');
INSERT INTO `mountpark_data` VALUES ('4617', '303', '5', '5', '0', '-1', '1210000', '1210000', '', '', '339', '321', '', '339;358;377;396;320;338;357;376;395;414;432;413;394;375;356;374;393;412;431;450;468;449;430;411;392;410;429;448;467', '');
INSERT INTO `mountpark_data` VALUES ('4618', '434', '5', '5', '0', '-1', '1210000', '1210000', '', '', '470', '452', '', '470;489;508;527;451;432;413;431;450;469;488;507;526;545;563;544;525;506;487;468;449;467;486;505;524;543;562;581;599;580;561;542;523;504', '');
INSERT INTO `mountpark_data` VALUES ('5280', '619', '5', '5', '0', '-1', '1210000', '1210000', '', '', '583', '601', '', '583;602;621;640;659;678;564;545;526;508;527;546;565;584;603;622;641;660;642;623;604;585;566;547;528;509;490;472;491;510;529;624;587;568;549;531;494;493;492;473;454;435;416;398;417;436;455;474;550;569;588;570;551;532;513;475;456;437;418;399;380;362;381;400;419;438;457;476;495;496;515;552;534;477;458;439;420;401;382;364;383;402;421;440;459;478;497;516;498;479;460;441;422;403;384;365', '');
INSERT INTO `mountpark_data` VALUES ('5279', '734', '5', '5', '0', '-1', '1210000', '1210000', '', '', '698', '716', '', '698;717;736;755;679;756;775;737;718;699;680;661;624;643;662;681;700;719;738;757;776;739;720;701;682;663;644;625;606;588;607;626;645;664;683', '');
INSERT INTO `mountpark_data` VALUES ('5112', '152', '5', '5', '0', '-1', '1210000', '1210000', '', '', '114', '133', '', '114;96;78;132;150;131;113;95;77;59;40;58;76;94;112;93;75;57;39;21;20;38', '');
INSERT INTO `mountpark_data` VALUES ('5111', '266', '5', '5', '0', '-1', '1210000', '1210000', '', '', '228', '247', '', '228;210;246;264;282;300;263;262;243;206;225;244;245;226;207;188;151;169;170;189;208;227;209;190;171;152;133;115;134;153;172;191', '');
INSERT INTO `mountpark_data` VALUES ('5108', '433', '5', '5', '0', '-1', '1210000', '1210000', '', '', '471', '452', '', '471;453;489;507;525;472;490;508;526;544;562;580;598;616;634;652;670;688;706;724;742;760;778;796;814;832;850;868;886;904;922;940;958;976;994;1012;1030;1048;1066;1084;1102;1120;1138;1156;1174;1192;1210;1228;1246;1264;1282;1300;1318;1336;1354;1372;1390;1408;1426;1444;1462;1480;1498;1516;1534;1552;1570;1588;1606;1624;1642;1660;1678;1696;491;509;527;545;563;581;599;617;636;618;600;582;564;546;528;510;529;547;565;583;601;619;637;655;674;656;638;620;602;584;566', '');
INSERT INTO `mountpark_data` VALUES ('4941', '396', '5', '5', '0', '-1', '1210000', '1210000', '', '', '360', '378', '', '360;341;322;303;379;398;285;304;323;342;361;380;362;343;324;305;286;267;306;325;344;326;307;288;269;250;231;213;232;251;270;289;308;290;271;252;233;214;195;177;196;215;234;253', '');
INSERT INTO `mountpark_data` VALUES ('4937', '579', '5', '5', '0', '-1', '1210000', '1210000', '', '', '615', '597', '', '615;596;614;632;650;668;686;704;722;759;741;723;705;687;669;651;633;634;652;670;688;706;724;742;760;761;743;725;707;689;671;653;690;708;726;744', '');
INSERT INTO `mountpark_data` VALUES ('4639', '245', '5', '5', '0', '-1', '1210000', '1210000', '', '', '207', '226', '', '207;189;171;153;225;243;261;279;260;242;224;206;188;170;152;134;115;133;151;169;187;205;223;241;204;186;168;150;132;114;96;77;95;113;131;149;167;130;112;94;76;58;39;57;75;93;56;38', '');
INSERT INTO `mountpark_data` VALUES ('4637', '542', '5', '5', '0', '-1', '1210000', '1210000', '', '', '578', '560', '', '578;559;540;521;597;616;653;634;615;596;577;558;539;557;576;595;614;633;652;671;689;670;651;632;613;594;575;593;612;631;650;669;688;707;725;706;687;668;649;630;611;648;667;686;705;724;743;761;742;723;704;685;760;741', '');
INSERT INTO `mountpark_data` VALUES ('4690', '716', '5', '5', '0', '-1', '1210000', '1210000', '', '', '680', '698', '', '680;699;718;661;642;623;604;586;605;624;643;662;681;644;625;606;587;568;550;569;588;607;570;551;532;514;533', '');
INSERT INTO `mountpark_data` VALUES ('4935', '566', '5', '5', '0', '-1', '1210000', '1210000', '', '', '530', '548', '', '530;511;492;473;454;435;549;568;587;606;625;644;607;588;569;550;531;512;493;474;455;436;417;399;418;437;456;475;494;513;532;551;570;533;514;495;476;457;438;419;400;381;363;382;401;420;439;458;477;496;345;364;383;402;421;440', '');
INSERT INTO `mountpark_data` VALUES ('4936', '642', '5', '5', '0', '-1', '1210000', '1210000', '', '', '606', '624', '', '606;625;644;663;682;701;587;568;549;512;531;550;569;588;607;645;664;608;589;570;551;532;513;627;646;665;495;514;533;552;571;590;609;628;477;496;515;534;553;572', '');
INSERT INTO `mountpark_data` VALUES ('5277', '570', '5', '5', '0', '-1', '1210000', '1210000', '', '', '532', '551', '', '532;514;496;478;550;568;531;513;495;477;459;440;458;476;494;512;530;511;493;475;457;439;421;402;420;438;456;474', '');
INSERT INTO `mountpark_data` VALUES ('5324', '456', '5', '5', '0', '-1', '1210000', '1210000', '', '', '420', '438', '', '420;401;382;363;326;345;364;383;402;384;365;346;327;308;290;309;328;347;366;348;329;310;291', '');
INSERT INTO `mountpark_data` VALUES ('5113', '560', '5', '5', '0', '-1', '1210000', '1210000', '', '', '596', '578', '', '596;577;558;576;594;612;630;667;685;649;631;613;595;614;632;650;668;686;704;722;759;741;723;705;687;669;651;633;634;653;671;689;707;725;743;724;706;688;670;652;615', '');
INSERT INTO `mountpark_data` VALUES ('5304', '596', '5', '5', '0', '-1', '1210000', '1210000', '', '', '632', '614', '', '632;613;594;651;670;689;707;688;669;650;631;612;630;649;668;687;706;725;743;724;705;686;667;648;685;704;723;742;761;760;741;722', '');
INSERT INTO `mountpark_data` VALUES ('5311', '598', '5', '5', '0', '-1', '1210000', '1210000', '', '', '634', '616', '', '634;615;596;653;672;690;671;652;633;614;632;651;670;689;708;726;707;688;669;650;687;706;725;744;762;743;724;705;723;742;761;760;741', '');
INSERT INTO `mountpark_data` VALUES ('5326', '531', '5', '5', '0', '-1', '1210000', '1210000', '', '', '495', '513', '', '495;514;533;476;457;438;419;400;381;363;382;401;420;439;458;477;496;459;440;421;402;383;364;345;327;346;365;384;403;422;385;366;347;328;309;291;310;329', '');
INSERT INTO `mountpark_data` VALUES ('5331', '658', '5', '5', '0', '-1', '1210000', '1210000', '', '', '620', '639', '', '620;638;656;674;602;584;566;548;529;547;565;583;601;619;637;655;636;618;600;582;564;546;528;510;509;527;545;563;581;599;617;544;526;508;489;507;525;506;488', '');
INSERT INTO `mountpark_data` VALUES ('4611', '632', '5', '5', '0', '-1', '1210000', '1210000', '', '', '670', '651', '', '670;652;634;616;688;706;725;707;689;671;653;635;654;672;690;708;726;744;763;745;727;709;691;673;692;710;728;746;764;765;747;729;711;730;748;766', '');
INSERT INTO `mountpark_data` VALUES ('4613', '329', '5', '5', '0', '-1', '1210000', '1210000', '', '', '293', '311', '', '331;312;293;256;255;236;274;313;294;275;237;218;200;219;238;257;276;295;258;239;220;201', '');
INSERT INTO `mountpark_data` VALUES ('3821', '236', '4', '4', '0', '-1', '962500', '962500', '', '', '208', '222', '', '208;193;223;224;225;240;269;298;284;210;195;180;179;165;255;270;256;241;226;211;196;181;166;151;137;152;167;182;197;212;227;242;228;213;198;183;168;153;138', '');
INSERT INTO `mountpark_data` VALUES ('3786', '300', '5', '5', '0', '-1', '1210000', '1210000', '', '', '270', '285', '', '270;284;298;312;297;282;267;252;237;222;207;193;208;223;238;253;268;283;269;254;239;224;209;194;179;165;180;195;210;225;240;255;256;241;226;211;196;181;166;151;137;152;167;182;197;212;227;242;228;213;198;183;168;153;138', '');
INSERT INTO `mountpark_data` VALUES ('9154', '598', '14', '14', '0', '-1', '3135000', '3135000', '', '', '562', '580', '', '600;581;562;543;524;505;486;467;448;429;410;391;373;392;411;430;449;468;487;506;525;544;563;582;545;526;507;488;469;450;431;412;393;374;355;337;356;375;394;413;432;451;470;489;508;527;528;509;490;471;452;433;414;395;376;357;338;319;301;320;339;358;377;396;415;434;453;472;491;510;473;454;435;416;397;378;359;340;321;302;283;265;284;303;322;341;342;361;380;399;418;437;474;456;455;436;417;398;379;323;304;285;266;343;362;324;305;286;267;248;229;211;230;249;268;287;306;325;344;326;308;289;270;288;307;193;212;231;250;232;213;194', '');
INSERT INTO `mountpark_data` VALUES ('9205', '205', '12', '12', '0', '-1', '2695000', '2695000', '', '', '177', '191', '', '177;162;147;132;117;102;192;207;222;237;223;208;193;178;163;148;133;118;103;88;74;89;104;119;134;149;164;179;194;209;195;180;165;150;135;120;105;90;75;60;46;61;76;91;106;121;136;151;166;181;167;152;137;122;107;92;77;62;47;32;18;33;63;78;93;108;123;138;153', '');
INSERT INTO `mountpark_data` VALUES ('9736', '194', '12', '12', '0', '-1', '2695000', '2695000', '', '', '224', '209', '', '224;210;196;182;168;154;238;252;266;280;294;309;295;281;267;253;239;225;211;197;183;169;184;198;212;226;240;254;268;282;296;310;324;339;325;311;297;283;269;255;241;227;213;199;214;228;242;256;270;284;298;312;326;340;354;369;355;341;327;313;299;285;271;257;243;229;244;258;272;286;300;314;328;342;356;370;384;259;273;287;301;316;302;288;274;289;303;317;331;346;332', '');
INSERT INTO `mountpark_data` VALUES ('9737', '243', '12', '12', '0', '-1', '2695000', '2695000', '', '', '271', '257', '', '271;286;300;314;328;342;356;370;384;369;355;341;327;313;299;285;256;270;284;298;312;326;340;354;339;325;311;297;283;269;255;241;226;212;198;184;240;254;268;282;296;310;281;267;253;239;225;211;197;183;169;154;168;182;196;210;224;238;252;266;251;237;223;209;195;181;167;153;139;124;138;152;166;180;194;208', '');
INSERT INTO `mountpark_data` VALUES ('9738', '109', '5', '5', '0', '-1', '1210000', '1210000', '', '', '139', '124', '', '139;125;111;153;167;181;195;209;223;237;251;265;180;194;208;193;178;164;150;165;179;182;168;154;140;126;196;210;224;238;252;266;280;295;281;267;253;239;225;211;197;183;169;155;141;198;212;226;240;254;268;282;296;310;325;311;297;283;269;255;241;227;213;228;242;256;270;284;298;312;313;327;299;285;271;257;243;258;272;286;300;314;328;315;329;343;357;371;330;344;358;372;386;345;359;373;387', '');
INSERT INTO `mountpark_data` VALUES ('9739', '388', '5', '5', '0', '-1', '1210000', '1210000', '', '', '358', '373', '', '358;372;344;330;315;329;343;357;342;328;314;300;271;257;243;285;299;313;327;341;355;369;354;340;326;312;298;284;270;256;242;228;213;227;241;255;269;283;297;311;325;339;324;310;296;254;240;226;212;198;183;197;211;225;239;253;267;281;295;309;294;280;266;252;238;224;210;196;182;168;153;167;181;195;209;223;237;251;265;279;264;250;236;222;208;194;180;166;152', '');
INSERT INTO `mountpark_data` VALUES ('3713', '212', '5', '5', '0', '-1', '1210000', '1210000', '', '', '182', '197', '', '182;196;210;168;154;140;126;111;125;139;153;167;181;195;180;166;152;138;124;110;96;137;122;107;92;106;121;136;151;165;150;135', '');
INSERT INTO `mountpark_data` VALUES ('3672', '196', '5', '5', '0', '-1', '1210000', '1210000', '', '', '166', '181', '', '166;180;194;208;152;138;123;137;151;165;164;193;178;150;136;122;108;93;107;121;135;149;163;148;134;120;106;92', '');
INSERT INTO `mountpark_data` VALUES ('8598', '374', '6', '6', '0', '-1', '1512500', '1512500', '', '', '344', '359', '', '344;358;330;316;301;315;329;343;286;271;285;299;313;284;270;256;269;255;241;227;213;199;185;170;184;198;212;226;240;254;268;253;239;225;211;197;183;169;155;168;182;196;181;167;153;138;152', '');
INSERT INTO `mountpark_data` VALUES ('8604', '337', '2', '2', '0', '-1', '522500', '522500', '', '', '365', '351', '', '365;380;395;350;335;349;364;379;394;409;423;408;393;378;363;392;407;422;437;451;436;421', '');
INSERT INTO `mountpark_data` VALUES ('8564', '168', '5', '5', '0', '-1', '1210000', '1210000', '', '', '140', '154', '', '140;155;170;185;200;125;110;96;111;126;141;156;171;186;172;157;142;127;112;97;82;68;83;98;113;128;143;158;144;129;114;99;84;69;54;55;70;85;100;115;41;56;71', '');
INSERT INTO `mountpark_data` VALUES ('8567', '323', '9', '9', '0', '-1', '2117500', '2117500', '', '', '295', '309', '', '295;310;325;340;355;280;265;250;235;221;236;251;266;281;296;311;326;341;327;312;297;282;267;252;237;222;207;193;208;223;238;253;268;283;298;313;299;284;269;254;239;224;209;194;179;165;180;166;151;152;210;196;182;168;154;140;211;197;183;169;155;170;184;198;212;226;225;240;270;256;242;257;271', '');
INSERT INTO `mountpark_data` VALUES ('8570', '299', '4', '4', '0', '-1', '962500', '962500', '', '', '269', '284', '', '269;283;297;282;267;252;237;222;207;192;178;193;208;223;238;253;268;254;239;224;209;194;179;164;150;165;180;195;210;225;240;255;241;226;211;196;181;166', '');
INSERT INTO `mountpark_data` VALUES ('8610', '236', '6', '6', '0', '-1', '1512500', '1512500', '', '', '208', '222', '', '208;223;238;253;193;178;163;148;134;149;164;179;194;209;224;239;254;269;255;240;225;210;195;180;165;150;135;120;106;121;136;151;166;181;196;211;197;182;167;152;137;122;107;92;93;108;123;138;153;168', '');
INSERT INTO `mountpark_data` VALUES ('8607', '354', '6', '6', '0', '-1', '1512500', '1512500', '', '', '326', '340', '', '326;341;327;313;299;285;271;257;242;256;270;284;298;312;311;297;283;269;255;241;227;212;226;240;254;268;282;267;253;239;225;211;196;210;224;238;252;266;251;237;223;209;195;181;166;180;194;208;222', '');
INSERT INTO `mountpark_data` VALUES ('3816', '382', '10', '10', '0', '-1', '2282500', '2282500', '', '', '354', '368', '', '354;369;384;339;324;309;294;279;265;280;295;310;325;340;355;370;356;341;326;311;296;281;266;251;237;252;267;282;297;312;327;342;328;313;298;283;268;253;238;223;209;224;239;254;269;284;299;314;300;285;270;255;240;225;210;195;181;196;211;226;241;256;271', '');
INSERT INTO `mountpark_data` VALUES ('3817', '151', '6', '6', '0', '-1', '1512500', '1512500', '', '', '121', '136', '', '121;107;93;135;149;163;177;162;148;134;120;106;92;78;63;77;91;105;119;133;147;132;118;104;90;76;62;48;33;47;61;75;89;103;117;102;88;74;60;46;32;18;17;31;45;59;73;44;30', '');
INSERT INTO `mountpark_data` VALUES ('3782', '384', '8', '8', '0', '-1', '1952500', '1952500', '', '', '356', '370', '', '356;371;341;326;311;296;281;266;251;237;252;267;282;297;312;327;342;357;343;328;313;298;283;268;239;238;223;209;224;254;269;284;299;314;329;315;300;285;270;255;240;225;210;195;181;196;211;226;241;256;271;286;301;287;272;257;242;227;212;197;182;167;153;168;183;198;213;228;243;258', '');
INSERT INTO `mountpark_data` VALUES ('3737', '324', '8', '8', '0', '-1', '1952500', '1952500', '', '', '296', '310', '', '296;281;266;251;236;311;326;341;356;342;313;298;283;268;253;238;223;222;208;328;314;299;284;269;254;239;224;209;194;180;195;210;225;240;255;270;285;286;271;256;241;226;211;196;181;166;152;167;182;197;212;227;242;257;272;258;243;228;213;198;183;168;153', '');
INSERT INTO `mountpark_data` VALUES ('3079', '353', '14', '14', '0', '-1', '3135000', '3135000', '', '', '325', '339', '', '325;340;355;370;385;310;295;280;265;250;235;221;236;251;266;281;296;311;326;341;356;371;357;342;327;312;297;282;267;252;237;222;207;193;208;223;238;253;268;283;298;313;328;343;329;314;299;284;269;254;239;224;209;194;179;165;180;195;210;225;240;255;270;285;300;315;301;286;271;256;241;226;211;196;181;166;151;287;272;257;242;227;212;197;182;167;152;137;123;138;153;168;183;198;213;199;184;169;154;139;124;109;95;110;125;140;155;170;185;171;156;141;126', '');
INSERT INTO `mountpark_data` VALUES ('2708', '251', '12', '12', '0', '-1', '2695000', '2695000', '', '', '223', '237', '', '223;208;193;178;163;148;238;253;268;283;298;284;270;256;241;226;211;196;181;166;151;136;121;106;120;135;150;165;180;195;210;225;240;255;269;254;239;224;209;194;179;164;149;134;327;341;355;313;299;285;271;257;243;229;244;258;272;286;370;385;371;357;343;329;315;301;287;273;259;274;288;302;316;330;344;358;372;386', '');
INSERT INTO `mountpark_data` VALUES ('4471', '353', '6', '6', '0', '-1', '1512500', '1512500', '', '', '325', '339', '', '325;340;355;310;295;280;265;251;266;281;296;311;326;341;327;312;297;282;267;252;237;223;238;253;268;283;298;313;299;284;269;254;239;224;209;195;210;225;240;255;270;285;271;256;241;226;211;196;181;167;182;197;227;242', '');
INSERT INTO `mountpark_data` VALUES ('4489', '127', '5', '5', '0', '-1', '1210000', '1210000', '', '', '155', '141', '', '155;140;125;170;199;184;169;154;139;124;138;153;168;183;198;213;228;242;227;212;197;182;167;152;166;181;196;211;226;241;256;270;255;240;225;210;195;180;194;209;284;298;283;254;208;222;237;252;238;312', '');
INSERT INTO `mountpark_data` VALUES ('4485', '300', '4', '4', '0', '-1', '962500', '962500', '', '', '270', '285', '', '270;256;242;228;284;298;312;326;340;354;339;325;296;297;283;269;255;241;227;213;198;212;226;240;254;310;324;309;295;281;267;253;239;225;211;197;183;182;196;210;224;238;252;266;280;294;279;265;251;237;223;209;195;181', '');
INSERT INTO `mountpark_data` VALUES ('4468', '93', '4', '4', '0', '-1', '962500', '962500', '', '', '123', '108', '', '123;109;137;151;165;179;194;180;166;152;138;124;110;125;139;153;167;181;195;209;224;210;196;182;168;154;140;155;183;197;211;225;239;212;198;184;170;185;199;213;227;241', '');
INSERT INTO `mountpark_data` VALUES ('2879', '428', '6', '6', '0', '-1', '1512500', '1512500', '', '', '400', '414', '', '400;385;415;430;445;460;461;446;431;416;401;386;371;357;372;387;402;417;432;447;462;448;433;418;403;388;373;358;343;329;344;359;374;389;404;419;434;405;390;375;360;345;330;315;301;316;331;346;361;376;347;332;317;302', '');
INSERT INTO `mountpark_data` VALUES ('2924', '219', '6', '6', '0', '-1', '1512500', '1512500', '', '', '191', '205', '', '191;206;221;176;161;146;131;102;117;132;147;162;177;192;207;193;178;163;148;133;118;103;88;73;44;59;74;89;104;119;134;149;164;179;165;150;135;120;105;90;75;60;30;16;17;32;47;76;91;106;121;136;151;137;122;107;92;77;48;18;33;63;78;93;108', '');
INSERT INTO `mountpark_data` VALUES ('3736', '323', '8', '8', '0', '-1', '1952500', '1952500', '', '', '295', '309', '', '295;310;325;340;355;280;265;250;235;221;236;251;266;281;296;311;326;341;327;312;297;282;267;252;237;222;207;193;208;223;238;253;268;283;298;313;299;284;269;254;239;224;209;194;179;165;180;195;210;225;240;255;270;285;271;256;241;226;211;196;181;166;151;137;152;167;182;197;212;227;242;257;243;228;213;198;183;168;153;138', '');
INSERT INTO `mountpark_data` VALUES ('3534', '299', '5', '5', '0', '-1', '1210000', '1210000', '', '', '271', '285', '', '271;286;256;241;226;211;196;181;167;182;197;212;227;242;257;272;258;243;228;213;198;183;168;153;139;154;169;184;199;214;229;244;230;215;200;185;170;155;140;125;111;126;141;156;171;186;201', '');
INSERT INTO `mountpark_data` VALUES ('4432', '211', '4', '4', '0', '-1', '962500', '962500', '', '', '183', '197', '', '183;198;213;228;168;153;138;124;139;154;169;184;199;214;200;185;170;155;140;125;110;96;111;126;141;156;171;186;172;157;142;127;82;143', '');
INSERT INTO `mountpark_data` VALUES ('4757', '324', '5', '5', '0', '-1', '1210000', '1210000', '', '', '296', '310', '', '296;311;326;341;281;266;251;252;267;282;297;312;327;313;298;283;268;253;238;223;209;224;239;254;269;284;299;285;256;255;240;225;210;195;181;196;211;226;241;271;257;242;227;212;197;182;167;168;183;198;228', '');
INSERT INTO `mountpark_data` VALUES ('4634', '268', '4', '4', '0', '-1', '962500', '962500', '', '', '240', '254', '', '240;225;255;241;226;211;197;212;227;228;243;213;198;183;168;139;138;124;154;169;184;199;214;229;215;186;185;170;155;126;125;110;96;111;141;156;171;201;187;172;157;142;127;112;97;143;158;173;129;144', '');
INSERT INTO `mountpark_data` VALUES ('4805', '381', '5', '5', '0', '-1', '1210000', '1210000', '', '', '353', '367', '', '353;368;338;323;309;324;339;354;340;325;310;295;281;296;311;326;312;298;284;270;256;242;228;213;227;241;255;297;282;268;254;240;226;212;198;183;197;211;225;239;253;267;252;238;224;210;196;182;168;153;167;181;195;209;223;237;222;208;194;180;166;152', '');
INSERT INTO `mountpark_data` VALUES ('4806', '342', '8', '8', '0', '-1', '1952500', '1952500', '', '', '312', '327', '', '312;326;325;339;368;354;311;297;283;269;255;241;227;228;213;353;212;183;197;211;225;239;253;267;281;295;309;323;308;294;280;266;252;238;224;210;196;182;168;153;167;181;195;209;223;237;251;265;279;293;208;194;180;166;152;138;123;137;151;165;179;193;178;164;150;136;122;108;163;149;135;121', '');
INSERT INTO `mountpark_data` VALUES ('4810', '358', '6', '6', '0', '-1', '1512500', '1512500', '', '', '328', '343', '', '328;342;356;370;314;300;286;271;285;299;313;327;341;355;340;326;312;298;256;227;198;184;185;212;226;240;254;268;282;311;310;296;170;155;169;183;197;211;225;239;253;267;281;295;280;266;252;238;224;210;196;182;168;154;125;139;153;167;181;195;209;223;237;251;265;250;236;222;208;194;180;166;152;138;124', '');
INSERT INTO `mountpark_data` VALUES ('4809', '357', '5', '5', '0', '-1', '1210000', '1210000', '', '', '327', '342', '', '327;341;355;340;325;310;295;280;265;250;236;251;266;281;296;311;326;312;297;282;267;252;237;222;208;223;238;253;268;283;298;313;299;284;269;254;239;224;209;194;180;195;210;225;240;255;270;285;271;256;241;226;211;196;181', '');
INSERT INTO `mountpark_data` VALUES ('4852', '371', '6', '6', '0', '-1', '1512500', '1512500', '', '', '341', '356', '', '341;355;327;313;299;285;271;257;242;256;270;284;298;312;326;340;325;311;297;283;269;255;241;227;212;226;240;254;268;282;296;310;324;338;323;309;295;281;267;253;239;225;211;197;182;196;210;224;238;252;266;280;294;308;265;251;237;223;209;195;181;167;152;166;180;194;208;222;236;250;235;221;207;193;179;165', '');
INSERT INTO `mountpark_data` VALUES ('4851', '256', '5', '5', '0', '-1', '1210000', '1210000', '', '', '284', '270', '', '284;299;269;254;239;224;209;194;180;166;152;208;222;236;250;264;279;265;251;237;223;195;181;167;182;196;210;238;252;266;280;294;309;295;281;267;253;225;211;197;268;282;296;310;324;339;325;311;297;283;298;312;326;340;354;369;355;341;327;313', '');
INSERT INTO `mountpark_data` VALUES ('4855', '329', '8', '8', '0', '-1', '1952500', '1952500', '', '', '299', '314', '', '299;313;312;326;355;341;298;284;270;256;242;243;228;340;325;311;297;283;269;255;241;227;213;199;184;198;212;226;240;254;268;282;296;310;295;281;267;253;239;225;211;197;183;169;154;168;182;196;210;224;238;252;266;280;265;139;124;138;152;166;180;194;208;222;236', '');
INSERT INTO `mountpark_data` VALUES ('4856', '330', '5', '5', '0', '-1', '1210000', '1210000', '', '', '300', '315', '', '300;314;328;327;356;313;299;285;271;257;258;243;228;213;198;183;168;153;138;152;167;182;197;212;227;242;286;256;241;226;211;196;181;166;151;165;180;195;210;225;240;255;270;284;269;254;239;224;209;194;179;193;208;223;238;253;268;283;298;312;297;282;267;252;237;222;207;221;236;251;266;281;296;311;326;341', '');
INSERT INTO `mountpark_data` VALUES ('3441', '327', '8', '8', '0', '-1', '1952500', '1952500', '', '', '299', '313', '', '299;314;329;315;301;287;272;257;242;227;198;183;168;153;138;124;139;154;169;184;170;155;140;125;110;96;111;126;141;156;82;97;112;127;142;137;122;107;92;77;152;167;182;197;212;286;271;256;241;226;211;196;181;166;151;136;121;106;91;105;120;135;150;165;180;195;210;225;240;255;270;285;300;284;269;254;239;224;119;134;149;164;179;208;237;252;267;268;282;222;236;251;266;281;296;310;295;280;265;250;264;279;294;309', '');
INSERT INTO `mountpark_data` VALUES ('3440', '338', '6', '6', '0', '-1', '1512500', '1512500', '', '', '310', '324', '', '310;325;340;355;295;280;251;236;235;221;266;281;296;311;326;341;327;312;297;282;267;252;237;222;207;193;208;223;238;253;268;283;298;313;299;284;269;179;165;180;195;210;211;226;241;256;285;271;196;181;166;151;137;152;167;182;197;212;227;242;257;243;228;213;198;183;168;153;138', '');
INSERT INTO `mountpark_data` VALUES ('3479', '338', '8', '8', '0', '-1', '1952500', '1952500', '', '', '310', '324', '', '310;325;340;355;370;385;295;280;265;250;236;251;266;281;296;311;326;341;356;371;357;342;327;312;297;282;267;252;237;222;208;223;238;343;329;314;299;284;269;254;225;210;195;194;180;240;255;270;285;300;315;301;286;271;256;241;226;211;196;181;166;152;167;182;197;212;227;242;257;272;287;273;258;243;228;213;198;183;168;153;138;124;139;154;169;184;170;155;140;126;141;142;127', '');
INSERT INTO `mountpark_data` VALUES ('3480', '338', '8', '8', '0', '-1', '1952500', '1952500', '', '', '310', '324', '', '310;325;340;355;295;280;265;250;236;251;266;281;296;311;326;341;327;312;297;282;267;252;237;222;207;192;177;162;148;163;178;193;208;223;238;253;268;283;298;313;299;284;269;254;239;224;209;194;179;164;149;134;120;135;150;165;180;195;210;225;240;255;270;285;271;256;241;226;181;166;151;136;121;106;92;107;122;137;152;167;168;183;198;227;242;257;243;228;213;153;138;123;108;93;78;124;139;154;169;184;170;155;140;125;110;96;111;126;141', '');
INSERT INTO `mountpark_data` VALUES ('3187', '268', '6', '6', '0', '-1', '1512500', '1512500', '', '', '296', '282', '', '296;311;326;341;355;340;325;310;295;280;294;309;324;339;354;369;383;368;353;338;323;308;322;337;352;367;382;397;411;396;381;366;351;336;350;365;380;395;410;425;439;424;409;364;378;393;422;437;452;453;407', '');
INSERT INTO `mountpark_data` VALUES ('3142', '140', '6', '6', '0', '-1', '1512500', '1512500', '', '', '168', '154', '', '168;153;183;198;213;227;212;197;182;167;152;137;151;166;181;196;211;226;241;255;240;225;210;195;180;165;179;194;209;224;239;254;269;283;268;253;238;223;208;193;207;222;237;252;267;282;297;311;296;281;266;251;236;221;265;280;295;309;294;279;293;308', '');
INSERT INTO `mountpark_data` VALUES ('3312', '365', '5', '5', '0', '-1', '1210000', '1210000', '', '', '337', '351', '', '337;352;322;308;323;338;324;309;294;265;250;235;280;295;310;325;340;355;370;356;341;326;311;296;281;266;251;236;221;207;222;237;252;267;282;297;312;327;342;328;313;298;283;268;253;238;223;208;193;194;179;209;224;239;254;269;284;299;270;255;240;225;210;195;180', '');
INSERT INTO `mountpark_data` VALUES ('8582', '400', '5', '5', '0', '-1', '1210000', '1210000', '', '', '370', '385', '', '370;384;356;327;313;299;285;271;257;258;243;341;355;369;354;340;326;312;298;284;270;256;242;228;213;227;241;255;269;283;297;311;325;339;324;310;296;282;268;254;240;226;212;198;225;239;253;267;281;295;309;294;280;266;252;238;224;210;195;209;223;237;251;265', '');
INSERT INTO `mountpark_data` VALUES ('3664', '353', '6', '6', '0', '-1', '1512500', '1512500', '', '', '325', '339', '', '325;340;355;310;295;280;265;250;235;221;236;251;266;281;296;311;326;341;327;312;297;282;267;252;237;222;207;193;208;223;238;253;268;283;298;313;299;284;269;254;239;224;209;194;179;165;180;195;210;225;240;255;285;271;256;241;226;211;196;151;137;152;167;182;197;212;227;242;257;243;228;213;198;183;168;153;138', '');
INSERT INTO `mountpark_data` VALUES ('9467', '221', '3', '3', '0', '-1', '500000', '500000', '', '', '206', '221', '', '132;147;177;192;146;161;176;191;206;220;205;190;175;160', '');

-- ----------------------------
-- Table structure for `mounts_data`
-- ----------------------------
DROP TABLE IF EXISTS `mounts_data`;
CREATE TABLE `mounts_data` (
  `id` int(11) NOT NULL,
  `color` int(11) NOT NULL,
  `sexe` int(11) NOT NULL,
  `name` varchar(30) CHARACTER SET latin1 NOT NULL,
  `xp` int(32) NOT NULL,
  `level` int(11) NOT NULL,
  `endurance` int(11) NOT NULL,
  `amour` int(11) NOT NULL,
  `maturite` int(11) NOT NULL,
  `serenite` int(11) NOT NULL,
  `reproductions` int(11) NOT NULL,
  `fatigue` int(11) NOT NULL,
  `energie` int(11) NOT NULL,
  `items` text CHARACTER SET latin1 NOT NULL,
  `ancetres` varchar(50) CHARACTER SET latin1 NOT NULL DEFAULT ',,,,,,,,,,,,,',
  `ability` varchar(50) CHARACTER SET latin1 NOT NULL DEFAULT '0',
  `taille` int(11) NOT NULL,
  `CellID` int(11) NOT NULL,
  `mapID` int(11) NOT NULL,
  `maitre` int(11) unsigned zerofill NOT NULL,
  `orientation` int(11) NOT NULL,
  `fecondable` int(11) NOT NULL,
  `paire` int(11) NOT NULL,
  `sauvage` int(5) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of mounts_data
-- ----------------------------

-- ----------------------------
-- Table structure for `paroli`
-- ----------------------------
DROP TABLE IF EXISTS `paroli`;
CREATE TABLE `paroli` (
  `template_obvi` int(10) NOT NULL,
  `id_victime` int(10) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of paroli
-- ----------------------------

-- ----------------------------
-- Table structure for `percepteurs`
-- ----------------------------
DROP TABLE IF EXISTS `percepteurs`;
CREATE TABLE `percepteurs` (
  `guid` int(11) NOT NULL,
  `mapid` int(11) NOT NULL,
  `cellid` int(11) NOT NULL,
  `orientation` int(11) NOT NULL,
  `guild_id` int(11) NOT NULL,
  `poseur_id` int(11) NOT NULL,
  `date` longtext CHARACTER SET latin1 NOT NULL,
  `N1` int(11) NOT NULL,
  `N2` int(11) NOT NULL,
  `objets` text CHARACTER SET latin1 NOT NULL,
  `kamas` int(11) NOT NULL,
  `xp` int(11) NOT NULL,
  PRIMARY KEY (`guid`),
  UNIQUE KEY `guid` (`guid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of percepteurs
-- ----------------------------

-- ----------------------------
-- Table structure for `pets_data`
-- ----------------------------
DROP TABLE IF EXISTS `pets_data`;
CREATE TABLE `pets_data` (
  `id` int(11) NOT NULL,
  `template` int(11) NOT NULL,
  `LastEatDate` longtext CHARACTER SET latin1 NOT NULL,
  `quaEat` int(1) NOT NULL,
  `pdv` int(11) NOT NULL,
  `Corpulence` int(11) NOT NULL,
  `isEPO` int(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of pets_data
-- ----------------------------

-- ----------------------------
-- Table structure for `prismes`
-- ----------------------------
DROP TABLE IF EXISTS `prismes`;
CREATE TABLE `prismes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `alignement` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `carte` int(11) NOT NULL,
  `celda` int(11) NOT NULL,
  `area` int(11) NOT NULL DEFAULT '-1',
  `honor` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of prismes
-- ----------------------------

-- ----------------------------
-- Table structure for `quest_perso`
-- ----------------------------
DROP TABLE IF EXISTS `quest_perso`;
CREATE TABLE `quest_perso` (
  `guid` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  `finish` int(11) NOT NULL,
  `perso` int(11) NOT NULL,
  `etapeValidate` text CHARACTER SET latin1,
  PRIMARY KEY (`guid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of quest_perso
-- ----------------------------

-- ----------------------------
-- Table structure for `subarea_data`
-- ----------------------------
DROP TABLE IF EXISTS `subarea_data`;
CREATE TABLE `subarea_data` (
  `id` int(11) NOT NULL,
  `area` int(11) NOT NULL,
  `alignement` int(11) NOT NULL DEFAULT '0',
  `name` varchar(200) CHARACTER SET latin1 NOT NULL,
  `conquistable` int(11) NOT NULL DEFAULT '0',
  `prisme` int(11) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of subarea_data
-- ----------------------------
INSERT INTO `subarea_data` VALUES ('0', '0', '0', 'Amakna', '1', '0');
INSERT INTO `subarea_data` VALUES ('1', '0', '0', 'le Port de Madrestam', '1', '0');
INSERT INTO `subarea_data` VALUES ('2', '0', '0', 'la montagne des Craqueleurs', '1', '0');
INSERT INTO `subarea_data` VALUES ('3', '0', '0', 'le champ des Ingalsses', '1', '0');
INSERT INTO `subarea_data` VALUES ('4', '0', '0', 'la forÃªt d\'Amakna', '1', '0');
INSERT INTO `subarea_data` VALUES ('5', '0', '0', 'le coin des Bouftous', '0', '0');
INSERT INTO `subarea_data` VALUES ('6', '0', '0', 'le cimetiÃ©re', '1', '0');
INSERT INTO `subarea_data` VALUES ('7', '0', '0', 'les cryptes', '1', '0');
INSERT INTO `subarea_data` VALUES ('8', '0', '0', 'le campement des Bworks', '1', '0');
INSERT INTO `subarea_data` VALUES ('9', '0', '0', 'la forÃªt malÃ©fique', '1', '0');
INSERT INTO `subarea_data` VALUES ('10', '0', '0', 'le village', '1', '0');
INSERT INTO `subarea_data` VALUES ('11', '0', '0', 'le territoire des Porcos', '1', '0');
INSERT INTO `subarea_data` VALUES ('12', '0', '0', 'la pÃ©ninsule des gelÃ©es', '1', '0');
INSERT INTO `subarea_data` VALUES ('13', '0', '0', 'le temple FÃ©ca', '0', '0');
INSERT INTO `subarea_data` VALUES ('14', '0', '0', 'le temple Osamodas', '0', '0');
INSERT INTO `subarea_data` VALUES ('15', '0', '0', 'le temple Enutrof', '0', '0');
INSERT INTO `subarea_data` VALUES ('16', '0', '0', 'le temple Sram', '0', '0');
INSERT INTO `subarea_data` VALUES ('17', '0', '0', 'le temple XÃ©lor', '0', '0');
INSERT INTO `subarea_data` VALUES ('18', '0', '0', 'le temple Ecaflip', '0', '0');
INSERT INTO `subarea_data` VALUES ('19', '0', '0', 'le temple Iop', '0', '0');
INSERT INTO `subarea_data` VALUES ('20', '0', '0', 'le temple CrÃ¢', '0', '0');
INSERT INTO `subarea_data` VALUES ('21', '0', '0', 'le temple Sadida', '0', '0');
INSERT INTO `subarea_data` VALUES ('22', '0', '0', 'le bord de la fÃ´ret malÃ©fique', '1', '0');
INSERT INTO `subarea_data` VALUES ('23', '0', '0', 'la presqu\'Ã®le des Dragoeufs', '1', '0');
INSERT INTO `subarea_data` VALUES ('25', '1', '0', 'les sous-terrains des Wabbits', '1', '0');
INSERT INTO `subarea_data` VALUES ('26', '0', '0', 'le temple Eniripsa', '0', '0');
INSERT INTO `subarea_data` VALUES ('27', '0', '0', 'la cÃ´te d\'Asse', '0', '0');
INSERT INTO `subarea_data` VALUES ('28', '0', '0', 'la garnison d\'Amakna', '1', '0');
INSERT INTO `subarea_data` VALUES ('29', '0', '0', 'le souterrain', '0', '0');
INSERT INTO `subarea_data` VALUES ('30', '4', '0', 'le berceau', '0', '0');
INSERT INTO `subarea_data` VALUES ('31', '0', '0', 'le marÃ©cage', '0', '0');
INSERT INTO `subarea_data` VALUES ('32', '5', '0', 'Sufokia', '1', '0');
INSERT INTO `subarea_data` VALUES ('33', '6', '0', 'la forÃªt des Abraknydes', '1', '0');
INSERT INTO `subarea_data` VALUES ('34', '3', '0', 'la prison', '0', '0');
INSERT INTO `subarea_data` VALUES ('35', '0', '0', 'la porte de Sufokia', '1', '0');
INSERT INTO `subarea_data` VALUES ('37', '7', '1', 'Bonta', '0', '-108');
INSERT INTO `subarea_data` VALUES ('38', '8', '0', 'la plaine de Cania', '1', '0');
INSERT INTO `subarea_data` VALUES ('39', '0', '0', 'le repaire des Roublards', '0', '0');
INSERT INTO `subarea_data` VALUES ('41', '0', '0', 'le temple Sacrieur', '0', '0');
INSERT INTO `subarea_data` VALUES ('42', '8', '0', 'la route de Bonta', '1', '0');
INSERT INTO `subarea_data` VALUES ('43', '7', '0', 'la fortification de Bonta', '1', '0');
INSERT INTO `subarea_data` VALUES ('44', '7', '1', 'le quartier des Boulangers', '0', '-129');
INSERT INTO `subarea_data` VALUES ('45', '7', '1', 'le quartier de la Milice', '0', '-111');
INSERT INTO `subarea_data` VALUES ('46', '7', '1', 'le quartier des Bouchers', '0', '-126');
INSERT INTO `subarea_data` VALUES ('47', '7', '1', 'le quartier des Forgerons', '0', '-123');
INSERT INTO `subarea_data` VALUES ('48', '7', '1', 'le quartier des BÃ»cherons', '0', '-117');
INSERT INTO `subarea_data` VALUES ('49', '7', '1', 'le quartier des Bricoleurs', '0', '-120');
INSERT INTO `subarea_data` VALUES ('50', '7', '1', 'le quartier des Tailleurs', '0', '-114');
INSERT INTO `subarea_data` VALUES ('51', '7', '1', 'le quartier des Bijoutiers', '0', '-132');
INSERT INTO `subarea_data` VALUES ('53', '11', '2', 'BrÃ¢kmar', '0', '-105');
INSERT INTO `subarea_data` VALUES ('54', '8', '0', 'le massif de Cania', '1', '0');
INSERT INTO `subarea_data` VALUES ('55', '8', '0', 'la pÃ©nates du Corbac', '1', '0');
INSERT INTO `subarea_data` VALUES ('56', '8', '0', 'la forÃªt de Cania', '1', '0');
INSERT INTO `subarea_data` VALUES ('57', '12', '0', 'la lande de Sidimote', '1', '0');
INSERT INTO `subarea_data` VALUES ('59', '8', '0', 'le cimetiÃ¨re de Bonta', '1', '0');
INSERT INTO `subarea_data` VALUES ('61', '12', '0', 'le cimetiÃ¨re des Tortur', '1', '0');
INSERT INTO `subarea_data` VALUES ('62', '13', '0', 'le village Dopeul', '1', '0');
INSERT INTO `subarea_data` VALUES ('63', '13', '0', 'le prisme des Dopeuls', '1', '0');
INSERT INTO `subarea_data` VALUES ('64', '13', '0', 'la premiÃ¨re salle du prisme', '1', '0');
INSERT INTO `subarea_data` VALUES ('65', '13', '0', 'la seconde salle du prisme', '1', '0');
INSERT INTO `subarea_data` VALUES ('66', '13', '0', 'la troisiÃ¨me salle du prisme', '1', '0');
INSERT INTO `subarea_data` VALUES ('67', '13', '0', 'la quatriÃ¨me salle du prisme', '1', '0');
INSERT INTO `subarea_data` VALUES ('68', '8', '0', 'les champs de Cania', '1', '0');
INSERT INTO `subarea_data` VALUES ('69', '8', '0', 'le Bois de Litneg', '1', '0');
INSERT INTO `subarea_data` VALUES ('70', '8', '0', 'les plaines rocheuses', '1', '0');
INSERT INTO `subarea_data` VALUES ('71', '12', '0', 'Gisgoul, le village devastÃ©', '1', '0');
INSERT INTO `subarea_data` VALUES ('72', '12', '0', 'la faÃ§ade de BrÃ¢kmar', '1', '0');
INSERT INTO `subarea_data` VALUES ('73', '7', '0', 'l\'Ã©gout de Bonta', '1', '0');
INSERT INTO `subarea_data` VALUES ('74', '13', '0', 'l\'entrainement des Dopeuls', '1', '0');
INSERT INTO `subarea_data` VALUES ('75', '11', '0', 'l\'Ã©gout de BrÃ¢kmar', '1', '0');
INSERT INTO `subarea_data` VALUES ('76', '14', '0', 'le village Brigandins', '1', '0');
INSERT INTO `subarea_data` VALUES ('77', '0', '0', 'la gelaxiÃ¨me dimension', '1', '0');
INSERT INTO `subarea_data` VALUES ('78', '0', '0', 'la gelaxiÃ¨me dimension (royale)', '1', '0');
INSERT INTO `subarea_data` VALUES ('79', '14', '0', 'la premiÃ¨re plate-forme', '1', '0');
INSERT INTO `subarea_data` VALUES ('80', '14', '0', 'la seconde plate-forme', '1', '0');
INSERT INTO `subarea_data` VALUES ('81', '14', '0', 'le prisme Brigandin', '1', '0');
INSERT INTO `subarea_data` VALUES ('82', '4', '0', 'le donjon des Bouftous', '1', '0');
INSERT INTO `subarea_data` VALUES ('83', '17', '0', 'le tutorial', '1', '0');
INSERT INTO `subarea_data` VALUES ('84', '15', '0', 'la foire du Trool', '1', '0');
INSERT INTO `subarea_data` VALUES ('85', '16', '0', 'le jeu du Bouftou', '1', '0');
INSERT INTO `subarea_data` VALUES ('86', '15', '0', 'le Gladiatrool', '1', '0');
INSERT INTO `subarea_data` VALUES ('87', '0', '0', 'Amakna Sud', '1', '0');
INSERT INTO `subarea_data` VALUES ('88', '17', '0', 'Tainela', '1', '0');
INSERT INTO `subarea_data` VALUES ('89', '0', '0', 'le tournoi Monde du Jeu', '1', '0');
INSERT INTO `subarea_data` VALUES ('91', '0', '0', 'le souterrain mystÃ©rieux', '1', '0');
INSERT INTO `subarea_data` VALUES ('92', '18', '0', 'le contour d\'Astrub', '0', '0');
INSERT INTO `subarea_data` VALUES ('93', '2', '0', 'la plage de Moon', '1', '0');
INSERT INTO `subarea_data` VALUES ('94', '12', '0', 'le donjon du Bworker', '1', '0');
INSERT INTO `subarea_data` VALUES ('95', '18', '0', 'la citÃ© d\'Astrub', '0', '0');
INSERT INTO `subarea_data` VALUES ('96', '18', '0', 'l\'exploitation miniÃ¨re d\'Astrub', '0', '0');
INSERT INTO `subarea_data` VALUES ('97', '18', '0', 'la forÃªt d\'Astrub', '0', '0');
INSERT INTO `subarea_data` VALUES ('98', '18', '0', 'les champs d\'Astrub', '0', '0');
INSERT INTO `subarea_data` VALUES ('99', '18', '0', 'le souterrain d\'Astrub', '0', '0');
INSERT INTO `subarea_data` VALUES ('100', '18', '0', 'le souterrain profond d\'Astrub', '0', '0');
INSERT INTO `subarea_data` VALUES ('101', '18', '0', 'le coin des Tofus', '0', '0');
INSERT INTO `subarea_data` VALUES ('102', '25', '0', 'le champ du repos', '1', '0');
INSERT INTO `subarea_data` VALUES ('103', '0', '0', 'le territoire des Bandits', '1', '0');
INSERT INTO `subarea_data` VALUES ('105', '19', '0', 'Pandala Neutre', '1', '0');
INSERT INTO `subarea_data` VALUES ('106', '20', '0', 'la bordure d\'Akwadala', '1', '0');
INSERT INTO `subarea_data` VALUES ('107', '22', '0', 'la bordure de Feudala', '1', '0');
INSERT INTO `subarea_data` VALUES ('108', '23', '0', 'la bordure d\'Aerdala', '1', '0');
INSERT INTO `subarea_data` VALUES ('109', '21', '0', 'la bordure de Terrdala', '1', '0');
INSERT INTO `subarea_data` VALUES ('110', '0', '0', 'Amakna pass', '1', '0');
INSERT INTO `subarea_data` VALUES ('111', '23', '0', 'la porte Aerdala', '1', '0');
INSERT INTO `subarea_data` VALUES ('112', '22', '0', 'la porte Feudala', '1', '0');
INSERT INTO `subarea_data` VALUES ('113', '20', '0', 'la porte Akwadala', '1', '0');
INSERT INTO `subarea_data` VALUES ('114', '21', '0', 'la porte Terrdala', '1', '0');
INSERT INTO `subarea_data` VALUES ('115', '23', '0', 'le village d\'Aerdala', '1', '0');
INSERT INTO `subarea_data` VALUES ('116', '22', '0', 'le village de Feudala', '1', '0');
INSERT INTO `subarea_data` VALUES ('117', '20', '0', 'le village d\'Akwadala', '1', '0');
INSERT INTO `subarea_data` VALUES ('118', '21', '0', 'le village de Terrdala', '1', '0');
INSERT INTO `subarea_data` VALUES ('119', '19', '0', 'le village de Pandala', '1', '0');
INSERT INTO `subarea_data` VALUES ('120', '23', '0', 'le prisme Aerdala', '1', '0');
INSERT INTO `subarea_data` VALUES ('121', '22', '0', 'le prisme Feudala', '1', '0');
INSERT INTO `subarea_data` VALUES ('122', '20', '0', 'le prisme Akwadala', '1', '0');
INSERT INTO `subarea_data` VALUES ('123', '21', '0', 'le prisme Terrdala', '1', '0');
INSERT INTO `subarea_data` VALUES ('124', '24', '0', 'la caverne des Bulbes', '1', '0');
INSERT INTO `subarea_data` VALUES ('125', '24', '0', 'le repaire des Pandikazes - PremiÃ¨re plate-forme', '1', '0');
INSERT INTO `subarea_data` VALUES ('126', '24', '0', 'le repaire des Pandikazes - Seconde plate-forme', '1', '0');
INSERT INTO `subarea_data` VALUES ('127', '24', '0', 'le repaire des Pandikazes - TroisiÃ¨me plate-forme', '1', '0');
INSERT INTO `subarea_data` VALUES ('128', '24', '0', 'le repaire des Pandikazes - QuatriÃ¨me plate-forme', '1', '0');
INSERT INTO `subarea_data` VALUES ('129', '24', '0', 'le repaire des Pandikazes - CinquiÃ¨me plate-forme', '1', '0');
INSERT INTO `subarea_data` VALUES ('130', '24', '0', 'le repaire des Pandikazes - SixiÃ¨me plate-forme', '1', '0');
INSERT INTO `subarea_data` VALUES ('131', '24', '0', 'le repaire des Pandikazes - SeptiÃ¨me plate-forme', '1', '0');
INSERT INTO `subarea_data` VALUES ('132', '24', '0', 'le repaire des Pandikazes - HuitiÃ¨me plate-forme', '1', '0');
INSERT INTO `subarea_data` VALUES ('133', '24', '0', 'le donjon des Kitsounes - Salle 1', '1', '0');
INSERT INTO `subarea_data` VALUES ('134', '24', '0', 'le donjon des Kitsounes - Salle 2', '1', '0');
INSERT INTO `subarea_data` VALUES ('135', '24', '0', 'le donjon des Kitsounes - Salle 3', '1', '0');
INSERT INTO `subarea_data` VALUES ('136', '24', '0', 'le donjon des Kitsounes - Salle 4', '1', '0');
INSERT INTO `subarea_data` VALUES ('137', '24', '0', 'le donjon des Kitsounes - Salle 4', '1', '0');
INSERT INTO `subarea_data` VALUES ('138', '24', '0', 'le donjon des Kitsounes - Salle 4', '1', '0');
INSERT INTO `subarea_data` VALUES ('139', '24', '0', 'le donjon des Kitsounes - Salle 5', '1', '0');
INSERT INTO `subarea_data` VALUES ('140', '24', '0', 'le donjon des Kitsounes - Salle 6', '1', '0');
INSERT INTO `subarea_data` VALUES ('141', '24', '0', 'le donjon des Kitsounes - Repaire du Tanukou', '1', '0');
INSERT INTO `subarea_data` VALUES ('143', '19', '0', 'le pont de Pandala', '1', '0');
INSERT INTO `subarea_data` VALUES ('144', '24', '0', 'le donjon des Firefoux', '1', '0');
INSERT INTO `subarea_data` VALUES ('145', '24', '0', 'le donjon des Firefoux - Salle 1', '1', '0');
INSERT INTO `subarea_data` VALUES ('146', '24', '0', 'le donjon des Firefoux - Salle 2', '1', '0');
INSERT INTO `subarea_data` VALUES ('147', '24', '0', 'le donjon des Firefoux - Salle 3', '1', '0');
INSERT INTO `subarea_data` VALUES ('148', '24', '0', 'le donjon des Firefoux - Salle 4', '1', '0');
INSERT INTO `subarea_data` VALUES ('149', '24', '0', 'le donjon des Firefoux - Salle 5', '1', '0');
INSERT INTO `subarea_data` VALUES ('150', '24', '0', 'le donjon des Firefoux - Salle 6', '1', '0');
INSERT INTO `subarea_data` VALUES ('151', '24', '0', 'le donjon des Firefoux - Salle 7', '1', '0');
INSERT INTO `subarea_data` VALUES ('152', '19', '0', 'l\'Ã®le de Grobe', '1', '0');
INSERT INTO `subarea_data` VALUES ('153', '24', '0', 'le repaire des Pandikazes - Plate-forme finale', '1', '0');
INSERT INTO `subarea_data` VALUES ('154', '43', '0', 'le donjon Cochon - Salle 1', '1', '0');
INSERT INTO `subarea_data` VALUES ('155', '43', '0', 'le donjon Cochon - Salle 2', '1', '0');
INSERT INTO `subarea_data` VALUES ('156', '43', '0', 'le donjon Cochon - Salle 3', '1', '0');
INSERT INTO `subarea_data` VALUES ('157', '43', '0', 'le donjon Cochon - Salle 4', '1', '0');
INSERT INTO `subarea_data` VALUES ('158', '43', '0', 'le donjon Cochon - Salle 5', '1', '0');
INSERT INTO `subarea_data` VALUES ('159', '43', '0', 'le donjon Cochon - Salle 6', '1', '0');
INSERT INTO `subarea_data` VALUES ('161', '1', '0', 'l\'Ã®le des Wabbits', '1', '0');
INSERT INTO `subarea_data` VALUES ('162', '1', '0', 'l\'ilot des Wabbits', '1', '0');
INSERT INTO `subarea_data` VALUES ('163', '1', '0', 'l\'Ã®le des Wabbits Squelettes', '1', '0');
INSERT INTO `subarea_data` VALUES ('164', '1', '0', 'l\'Ã®le du chÃ¢teau des Wabbits', '1', '0');
INSERT INTO `subarea_data` VALUES ('165', '2', '0', 'la jungle profonde de Moon', '1', '0');
INSERT INTO `subarea_data` VALUES ('166', '2', '0', 'le chemin vers Moon', '1', '0');
INSERT INTO `subarea_data` VALUES ('167', '2', '0', 'le bateau pirate', '1', '0');
INSERT INTO `subarea_data` VALUES ('168', '6', '0', 'la forÃªt des Abraknydes Sombres', '1', '0');
INSERT INTO `subarea_data` VALUES ('169', '6', '0', 'l\'OrÃ©e de la forÃªt des Abraknydes', '1', '0');
INSERT INTO `subarea_data` VALUES ('170', '0', '0', 'la plaine des Scarafeuilles', '1', '0');
INSERT INTO `subarea_data` VALUES ('171', '19', '0', 'la forÃªt de Pandala', '1', '0');
INSERT INTO `subarea_data` VALUES ('173', '18', '0', 'la prairies d\'Astrub', '0', '0');
INSERT INTO `subarea_data` VALUES ('174', '18', '0', 'le campement des Bandits Manchots', '1', '0');
INSERT INTO `subarea_data` VALUES ('175', '27', '0', 'le donjon Abraknyde - Salle 1', '1', '0');
INSERT INTO `subarea_data` VALUES ('177', '8', '0', 'la prairie des Blops', '1', '0');
INSERT INTO `subarea_data` VALUES ('178', '8', '0', 'la plaine des Porkass', '1', '0');
INSERT INTO `subarea_data` VALUES ('179', '0', '0', 'le coin des Boos', '1', '0');
INSERT INTO `subarea_data` VALUES ('180', '0', '0', 'le chÃ¢teau d\'Amakna', '1', '0');
INSERT INTO `subarea_data` VALUES ('181', '0', '0', 'le souterrain du ChÃ¢teau d\'Amakna', '1', '0');
INSERT INTO `subarea_data` VALUES ('182', '28', '0', 'le village des Eleveurs', '1', '0');
INSERT INTO `subarea_data` VALUES ('200', '26', '0', 'le labyrinthe du Dragon Cochon', '1', '0');
INSERT INTO `subarea_data` VALUES ('201', '29', '0', 'le donjon des Tofus - Salle 1', '1', '0');
INSERT INTO `subarea_data` VALUES ('202', '29', '0', 'le donjon des Tofus - Salle 2', '1', '0');
INSERT INTO `subarea_data` VALUES ('203', '29', '0', 'le donjon des Tofus - Salle 3', '1', '0');
INSERT INTO `subarea_data` VALUES ('204', '29', '0', 'le donjon des Tofus - Salle 4', '1', '0');
INSERT INTO `subarea_data` VALUES ('205', '29', '0', 'le donjon des Tofus - Salle 5', '1', '0');
INSERT INTO `subarea_data` VALUES ('206', '29', '0', 'le donjon des Tofus - Salle 6', '1', '0');
INSERT INTO `subarea_data` VALUES ('207', '29', '0', 'le donjon des Tofus - Salle 7', '1', '0');
INSERT INTO `subarea_data` VALUES ('208', '29', '0', 'le donjon des Tofus - Salle 8', '1', '0');
INSERT INTO `subarea_data` VALUES ('209', '30', '0', 'l\'Ã®le du Minotoror', '0', '0');
INSERT INTO `subarea_data` VALUES ('210', '31', '0', 'le labyrinthe du Minotoror', '1', '0');
INSERT INTO `subarea_data` VALUES ('211', '32', '0', 'la bibliothÃ¨que du MaÃ®tre Corbac', '1', '0');
INSERT INTO `subarea_data` VALUES ('212', '33', '0', 'le donjon des CanidÃ©s - EntrÃ©e', '1', '0');
INSERT INTO `subarea_data` VALUES ('213', '33', '0', 'le donjon des CanidÃ©s - Salle 1', '1', '0');
INSERT INTO `subarea_data` VALUES ('214', '33', '0', 'le donjon des CanidÃ©s - Salle 2', '1', '0');
INSERT INTO `subarea_data` VALUES ('215', '33', '0', 'le donjon des CanidÃ©s - Salle 3', '1', '0');
INSERT INTO `subarea_data` VALUES ('216', '33', '0', 'le donjon des CanidÃ©s - Salle 4', '1', '0');
INSERT INTO `subarea_data` VALUES ('217', '33', '0', 'le donjon des CanidÃ©s - Salle 5', '1', '0');
INSERT INTO `subarea_data` VALUES ('218', '33', '0', 'le donjon des CanidÃ©s - Salle 6', '1', '0');
INSERT INTO `subarea_data` VALUES ('219', '33', '0', 'le donjon des CanidÃ©s - Salle 7', '1', '0');
INSERT INTO `subarea_data` VALUES ('220', '33', '0', 'le donjon des CanidÃ©s - Salle 8', '1', '0');
INSERT INTO `subarea_data` VALUES ('221', '33', '0', 'le donjon des CanidÃ©s - Salle 9', '1', '0');
INSERT INTO `subarea_data` VALUES ('222', '33', '0', 'le donjon des CanidÃ©s - Salle 10', '1', '0');
INSERT INTO `subarea_data` VALUES ('223', '34', '0', 'la caverne du Koulosse - Salle 1', '1', '0');
INSERT INTO `subarea_data` VALUES ('224', '34', '0', 'la caverne du Koulosse - Salle 2', '1', '0');
INSERT INTO `subarea_data` VALUES ('225', '34', '0', 'la caverne du Koulosse - Salle 3', '1', '0');
INSERT INTO `subarea_data` VALUES ('226', '34', '0', 'la caverne du Koulosse - Salle 4', '1', '0');
INSERT INTO `subarea_data` VALUES ('227', '34', '0', 'la caverne du Koulosse - Salle 5', '1', '0');
INSERT INTO `subarea_data` VALUES ('228', '34', '0', 'la caverne du Koulosse - Salle 6', '1', '0');
INSERT INTO `subarea_data` VALUES ('229', '34', '0', 'la caverne du Koulosse - L\'antre', '1', '0');
INSERT INTO `subarea_data` VALUES ('230', '28', '0', 'le cimetiÃ¨re primitif', '1', '0');
INSERT INTO `subarea_data` VALUES ('231', '28', '0', 'le lacs enchantÃ©', '1', '0');
INSERT INTO `subarea_data` VALUES ('232', '28', '0', 'le marÃ©cages nausÃ©abonds', '1', '0');
INSERT INTO `subarea_data` VALUES ('233', '28', '0', 'le marÃ©cages sans fond', '1', '0');
INSERT INTO `subarea_data` VALUES ('234', '28', '0', 'la forÃªt de Kaliptus', '1', '0');
INSERT INTO `subarea_data` VALUES ('235', '28', '0', 'le territoire des Dragodindes Sauvages', '1', '0');
INSERT INTO `subarea_data` VALUES ('236', '36', '0', 'le sanctuaire des Familiers', '0', '0');
INSERT INTO `subarea_data` VALUES ('243', '37', '0', 'le donjon des Craqueleurs - Salle 1', '1', '0');
INSERT INTO `subarea_data` VALUES ('244', '37', '0', 'le donjon des Craqueleurs - Salle 2', '1', '0');
INSERT INTO `subarea_data` VALUES ('245', '37', '0', 'le donjon des Craqueleurs - Salle 3', '1', '0');
INSERT INTO `subarea_data` VALUES ('246', '37', '0', 'le donjon des Craqueleurs - Salle 4', '1', '0');
INSERT INTO `subarea_data` VALUES ('247', '37', '0', 'le donjon des Craqueleurs - Salle 5', '1', '0');
INSERT INTO `subarea_data` VALUES ('248', '37', '0', 'le donjon des Craqueleurs - Salle 6', '1', '0');
INSERT INTO `subarea_data` VALUES ('249', '37', '0', 'le donjon des Craqueleurs - Salle 7', '1', '0');
INSERT INTO `subarea_data` VALUES ('250', '37', '0', 'le donjon des Craqueleurs - Salle 8', '1', '0');
INSERT INTO `subarea_data` VALUES ('251', '37', '0', 'le donjon des Craqueleurs - Salle 10', '1', '0');
INSERT INTO `subarea_data` VALUES ('252', '37', '0', 'le donjon des Craqueleurs - Salle 11', '1', '0');
INSERT INTO `subarea_data` VALUES ('253', '28', '0', 'le canyon sauvage', '1', '0');
INSERT INTO `subarea_data` VALUES ('254', '35', '0', 'le repaire de Skeunk - Salle 1', '1', '0');
INSERT INTO `subarea_data` VALUES ('255', '35', '0', 'le repaire de Skeunk - Salle 2', '1', '0');
INSERT INTO `subarea_data` VALUES ('256', '35', '0', 'le repaire de Skeunk - Salle 3', '1', '0');
INSERT INTO `subarea_data` VALUES ('257', '35', '0', 'le repaire de Skeunk - Emeraude', '1', '0');
INSERT INTO `subarea_data` VALUES ('258', '35', '0', 'le repaire de Skeunk - Rubise', '1', '0');
INSERT INTO `subarea_data` VALUES ('259', '35', '0', 'le repaire de Skeunk - Saphira', '1', '0');
INSERT INTO `subarea_data` VALUES ('260', '35', '0', 'le repaire de Skeunk - Diamantine', '1', '0');
INSERT INTO `subarea_data` VALUES ('261', '35', '0', 'le repaire de Skeunk - Antre', '1', '0');
INSERT INTO `subarea_data` VALUES ('266', '35', '0', 'le repaire de Skeunk - Salle 4', '1', '0');
INSERT INTO `subarea_data` VALUES ('267', '35', '0', 'le repaire de Skeunk - Salle 5', '1', '0');
INSERT INTO `subarea_data` VALUES ('268', '29', '0', 'le donjon des Tofus - Salle 9', '1', '0');
INSERT INTO `subarea_data` VALUES ('269', '29', '0', 'le donjon des Tofus - Salle 10', '1', '0');
INSERT INTO `subarea_data` VALUES ('270', '29', '0', 'le donjon des Tofus - Salle 11', '1', '0');
INSERT INTO `subarea_data` VALUES ('271', '29', '0', 'le donjon des Tofus - Salle 12', '1', '0');
INSERT INTO `subarea_data` VALUES ('272', '29', '0', 'le donjon des Tofus - Salle 13', '1', '0');
INSERT INTO `subarea_data` VALUES ('273', '29', '0', 'le donjon des Tofus - Salle 14', '1', '0');
INSERT INTO `subarea_data` VALUES ('274', '29', '0', 'le donjon des Tofus - Salle 15', '1', '0');
INSERT INTO `subarea_data` VALUES ('275', '28', '0', 'la vallÃ©e de la Morh\'Kitu', '1', '0');
INSERT INTO `subarea_data` VALUES ('276', '0', '0', 'le campement des Gobelins', '1', '0');
INSERT INTO `subarea_data` VALUES ('277', '0', '0', 'le village des Bworks', '1', '0');
INSERT INTO `subarea_data` VALUES ('278', '18', '0', 'l\'Ã©levage de Bouftous du ChÃ¢teau d\'Amakna', '0', '0');
INSERT INTO `subarea_data` VALUES ('279', '7', '0', 'la bordure de Bonta', '1', '0');
INSERT INTO `subarea_data` VALUES ('280', '11', '0', 'la bordure de BrÃ¢kmar', '1', '0');
INSERT INTO `subarea_data` VALUES ('284', '39', '0', 'le donjon des Bworks - Salle 1', '1', '0');
INSERT INTO `subarea_data` VALUES ('285', '39', '0', 'le donjon des Bworks - Salle 2', '1', '0');
INSERT INTO `subarea_data` VALUES ('286', '39', '0', 'le donjon des Bworks - Salle 3', '1', '0');
INSERT INTO `subarea_data` VALUES ('287', '39', '0', 'le donjon des Bworks - Cachot', '1', '0');
INSERT INTO `subarea_data` VALUES ('288', '39', '0', 'le donjon des Bworks - Salle 4', '1', '0');
INSERT INTO `subarea_data` VALUES ('289', '39', '0', 'le donjon des Bworks - Salle 5', '1', '0');
INSERT INTO `subarea_data` VALUES ('290', '39', '0', 'le donjon des Bworks - Salle 6', '1', '0');
INSERT INTO `subarea_data` VALUES ('291', '39', '0', 'le donjon des Bworks - Salle 7', '1', '0');
INSERT INTO `subarea_data` VALUES ('292', '33', '0', 'le donjon des CanidÃ©s - Salle 11', '1', '0');
INSERT INTO `subarea_data` VALUES ('293', '33', '0', 'le donjon des CanidÃ©s - Salle 12', '1', '0');
INSERT INTO `subarea_data` VALUES ('294', '33', '0', 'le donjon des CanidÃ©s - Salle 13', '1', '0');
INSERT INTO `subarea_data` VALUES ('295', '33', '0', 'le donjon des CanidÃ©s - Salle 14', '1', '0');
INSERT INTO `subarea_data` VALUES ('296', '33', '0', 'le donjon des CanidÃ©s - Salle 15', '1', '0');
INSERT INTO `subarea_data` VALUES ('297', '40', '0', 'le donjon des Scarafeuilles - Salle 1', '1', '0');
INSERT INTO `subarea_data` VALUES ('298', '40', '0', 'le donjon des Scarafeuilles - Salle 2', '1', '0');
INSERT INTO `subarea_data` VALUES ('299', '40', '0', 'le donjon des Scarafeuilles - Salle 3', '1', '0');
INSERT INTO `subarea_data` VALUES ('300', '40', '0', 'le donjon des Scarafeuilles - Salle 4', '1', '0');
INSERT INTO `subarea_data` VALUES ('301', '40', '0', 'le donjon des Scarafeuilles - Salle 5', '1', '0');
INSERT INTO `subarea_data` VALUES ('302', '40', '0', 'le donjon des Scarafeuilles - Salle 6', '1', '0');
INSERT INTO `subarea_data` VALUES ('303', '40', '0', 'le donjon des Scarafeuilles - Salle 7', '1', '0');
INSERT INTO `subarea_data` VALUES ('304', '40', '0', 'le donjon des Scarafeuilles - Salle 8', '1', '0');
INSERT INTO `subarea_data` VALUES ('306', '41', '0', 'le donjon des Champs - Salle 1', '1', '0');
INSERT INTO `subarea_data` VALUES ('307', '41', '0', 'le donjon des Champs - Salle 2', '1', '0');
INSERT INTO `subarea_data` VALUES ('308', '41', '0', 'le donjon des Champs - Salle 3', '1', '0');
INSERT INTO `subarea_data` VALUES ('309', '41', '0', 'le donjon des Champs - Salle 4', '1', '0');
INSERT INTO `subarea_data` VALUES ('310', '41', '0', 'le donjon des Champs - Salle 5', '1', '0');
INSERT INTO `subarea_data` VALUES ('311', '41', '0', 'le donjon des Champs - Salle 6', '1', '0');
INSERT INTO `subarea_data` VALUES ('312', '41', '0', 'le donjon des Champs - Salle 7', '1', '0');
INSERT INTO `subarea_data` VALUES ('313', '29', '0', 'le donjon des Tofus - Salle 16', '1', '0');
INSERT INTO `subarea_data` VALUES ('314', '0', '0', 'le sanctuaire des Dragoeufs', '0', '0');
INSERT INTO `subarea_data` VALUES ('315', '0', '0', 'le village des Dragoeufs', '0', '0');
INSERT INTO `subarea_data` VALUES ('316', '0', '0', 'le sous-terrain des Dragoeufs', '0', '0');
INSERT INTO `subarea_data` VALUES ('317', '37', '0', 'le donjon des Craqueleurs - Salle 9', '1', '0');
INSERT INTO `subarea_data` VALUES ('318', '18', '0', 'Alkatraz', '1', '0');
INSERT INTO `subarea_data` VALUES ('319', '31', '0', 'le labyrinthe du Minotoror', '1', '0');
INSERT INTO `subarea_data` VALUES ('320', '42', '0', 'l\'Ã®le de Nowel', '1', '0');
INSERT INTO `subarea_data` VALUES ('321', '42', '0', 'le donjon de Nowel', '1', '0');
INSERT INTO `subarea_data` VALUES ('322', '43', '0', 'le donjon Cochon - Salle 7', '1', '0');
INSERT INTO `subarea_data` VALUES ('323', '43', '0', 'le donjon Cochon - Salle 8', '1', '0');
INSERT INTO `subarea_data` VALUES ('324', '43', '0', 'le donjon Cochon - Salle 9', '1', '0');
INSERT INTO `subarea_data` VALUES ('325', '44', '0', 'le donjon Dragoeuf - Salle 1', '1', '0');
INSERT INTO `subarea_data` VALUES ('326', '44', '0', 'le donjon Dragoeuf - Salle 2', '1', '0');
INSERT INTO `subarea_data` VALUES ('327', '44', '0', 'le donjon Dragoeuf - Salle 3', '1', '0');
INSERT INTO `subarea_data` VALUES ('328', '44', '0', 'le donjon Dragoeuf - Salle 4', '1', '0');
INSERT INTO `subarea_data` VALUES ('329', '44', '0', 'le donjon Dragoeuf - Salle 5', '1', '0');
INSERT INTO `subarea_data` VALUES ('330', '44', '0', 'le donjon Dragoeuf - Salle 6', '1', '0');
INSERT INTO `subarea_data` VALUES ('331', '44', '0', 'le donjon Dragoeuf - Salle 7', '1', '0');
INSERT INTO `subarea_data` VALUES ('332', '44', '0', 'le donjon Dragoeuf - Salle 8', '1', '0');
INSERT INTO `subarea_data` VALUES ('333', '44', '0', 'le donjon Dragoeuf - Salle 9', '1', '0');
INSERT INTO `subarea_data` VALUES ('334', '8', '0', 'la baie de Cania', '1', '0');
INSERT INTO `subarea_data` VALUES ('335', '18', '0', 'les calanques d\'Astrub', '0', '0');
INSERT INTO `subarea_data` VALUES ('336', '18', '0', 'le donjon EnsablÃ©', '0', '0');
INSERT INTO `subarea_data` VALUES ('337', '7', '0', 'le donjon des Rats de Bonta', '1', '0');
INSERT INTO `subarea_data` VALUES ('338', '11', '0', 'le donjon des Rats de BrÃ¢kmar', '1', '0');
INSERT INTO `subarea_data` VALUES ('339', '0', '0', 'le donjon des Rats du ChÃ¢teau d\'Amakna', '1', '0');
INSERT INTO `subarea_data` VALUES ('440', '45', '0', 'les pitons rocheux', '1', '0');
INSERT INTO `subarea_data` VALUES ('441', '45', '0', 'la clairiÃ¨re', '1', '0');
INSERT INTO `subarea_data` VALUES ('442', '45', '0', 'le lac', '1', '0');
INSERT INTO `subarea_data` VALUES ('443', '45', '0', 'la forÃªt', '1', '0');
INSERT INTO `subarea_data` VALUES ('444', '45', '0', 'les champs', '1', '0');
INSERT INTO `subarea_data` VALUES ('445', '45', '0', 'la prairie', '1', '0');
INSERT INTO `subarea_data` VALUES ('446', '45', '0', 'le temple', '1', '0');
INSERT INTO `subarea_data` VALUES ('447', '45', '0', 'le donjon', '1', '0');
INSERT INTO `subarea_data` VALUES ('448', '45', '0', 'un tÃ©rritoire inconnu', '1', '0');
INSERT INTO `subarea_data` VALUES ('449', '45', '0', 'le cimetiÃ¨re', '1', '0');
INSERT INTO `subarea_data` VALUES ('450', '45', '0', 'la sortie du temple', '1', '0');
INSERT INTO `subarea_data` VALUES ('451', '46', '0', 'l\'Ã®le des naufragÃ©s', '1', '0');
INSERT INTO `subarea_data` VALUES ('452', '46', '0', 'la mer', '1', '0');
INSERT INTO `subarea_data` VALUES ('453', '46', '0', 'la plage de Corail', '1', '0');
INSERT INTO `subarea_data` VALUES ('454', '46', '0', 'la plaines herbeuses', '1', '0');
INSERT INTO `subarea_data` VALUES ('455', '46', '0', 'la jungle obscure', '1', '0');
INSERT INTO `subarea_data` VALUES ('457', '46', '0', 'la tourbiÃ¨re sans fond', '1', '0');
INSERT INTO `subarea_data` VALUES ('459', '46', '0', 'le canopÃ©e du Kimbo', '1', '0');
INSERT INTO `subarea_data` VALUES ('460', '46', '0', 'la grotte Hesque', '1', '0');
INSERT INTO `subarea_data` VALUES ('461', '46', '0', 'l\'arche d\'OtomaÃ¯', '1', '0');
INSERT INTO `subarea_data` VALUES ('462', '46', '0', 'la clairiÃ¨re de Floribonde', '1', '0');
INSERT INTO `subarea_data` VALUES ('463', '46', '0', 'le laboratoire du Tynril', '1', '0');
INSERT INTO `subarea_data` VALUES ('464', '46', '0', 'le tronc de l\'arbre Hakam', '1', '0');
INSERT INTO `subarea_data` VALUES ('465', '46', '0', 'le village des Ã©leveurs', '1', '0');
INSERT INTO `subarea_data` VALUES ('466', '46', '0', 'le village cÃ´tier', '1', '0');
INSERT INTO `subarea_data` VALUES ('467', '46', '0', 'le cimetiÃ¨re de l\'Ã®le d\'OtomaÃ¯', '1', '0');
INSERT INTO `subarea_data` VALUES ('468', '47', '0', 'le village des Zoths', '1', '0');
INSERT INTO `subarea_data` VALUES ('469', '46', '0', 'le village de la Canop', '1', '0');
INSERT INTO `subarea_data` VALUES ('470', '46', '0', 'le goulet du Rasboul', '1', '0');
INSERT INTO `subarea_data` VALUES ('471', '46', '0', 'TourbiÃ¨re nausÃ©abonde', '1', '0');
INSERT INTO `subarea_data` VALUES ('472', '46', '0', 'le feuillage de L\'arbre Hakam', '1', '0');
INSERT INTO `subarea_data` VALUES ('473', '46', '0', 'Le laboratoire cachÃ©', '1', '0');
INSERT INTO `subarea_data` VALUES ('474', '46', '0', 'la cale de l\'arche d\'OtomaÃ¯', '1', '0');
INSERT INTO `subarea_data` VALUES ('476', '19', '0', 'le pont de Grobe', '1', '0');
INSERT INTO `subarea_data` VALUES ('477', '47', '0', 'le prisme Zothier', '1', '0');
INSERT INTO `subarea_data` VALUES ('478', '47', '0', 'les gardiens de la porte des Zoths', '0', '0');
INSERT INTO `subarea_data` VALUES ('479', '0', '0', 'la riviÃ©re Kawaii', '1', '0');
INSERT INTO `subarea_data` VALUES ('480', '0', '0', 'la montagne basse des Craqueleurs', '1', '0');
INSERT INTO `subarea_data` VALUES ('481', '0', '0', 'la clairiÃ©re de Brouce Boulgour', '1', '0');
INSERT INTO `subarea_data` VALUES ('482', '0', '0', 'la Millifutaie ', '1', '0');
INSERT INTO `subarea_data` VALUES ('483', '0', '0', 'le chemin de fer abandonnÃ©', '1', '0');
INSERT INTO `subarea_data` VALUES ('484', '0', '0', 'l\'OrÃ©e de la Millifutaie', '1', '0');
INSERT INTO `subarea_data` VALUES ('485', '0', '0', 'la campagne', '1', '0');
INSERT INTO `subarea_data` VALUES ('486', '0', '0', 'le Bosquet du petit talus', '1', '0');
INSERT INTO `subarea_data` VALUES ('487', '19', '0', 'le grenier-cachot de Pandala Air', '1', '0');
INSERT INTO `subarea_data` VALUES ('488', '0', '0', 'les souterrains d\'Amakna', '1', '0');
INSERT INTO `subarea_data` VALUES ('490', '0', '0', 'le rivage du golfe sufokien', '1', '0');
INSERT INTO `subarea_data` VALUES ('491', '0', '0', 'le donjondes Larves', '1', '0');
INSERT INTO `subarea_data` VALUES ('492', '0', '0', 'le passage vers BrÃ¢kmar', '1', '0');
INSERT INTO `subarea_data` VALUES ('493', '8', '0', 'le donjon des Blops', '1', '0');
INSERT INTO `subarea_data` VALUES ('494', '12', '0', 'le donjon Fungus', '1', '0');
INSERT INTO `subarea_data` VALUES ('495', '12', '0', 'la caverne des Fungus', '1', '0');
INSERT INTO `subarea_data` VALUES ('496', '12', '0', 'le donjon de Ku\'tan', '1', '0');
INSERT INTO `subarea_data` VALUES ('497', '8', '0', 'le donjon d\'Ilyzaelle', '1', '0');
INSERT INTO `subarea_data` VALUES ('498', '8', '0', 'le sanctuaire Hotomani', '1', '0');
INSERT INTO `subarea_data` VALUES ('499', '30', '0', 'le cimetiÃ¨re de l\'Ã®le du Minotoror', '1', '0');
INSERT INTO `subarea_data` VALUES ('500', '47', '0', 'le cimetiÃ¨re du village des Zoths.', '1', '0');
INSERT INTO `subarea_data` VALUES ('501', '0', '0', 'l\'Ã®lot Estitch', '1', '0');
INSERT INTO `subarea_data` VALUES ('502', '11', '0', 'le quartier des bÃ»cherons', '1', '0');
INSERT INTO `subarea_data` VALUES ('503', '11', '0', 'le quartier des bouchers', '1', '0');
INSERT INTO `subarea_data` VALUES ('504', '11', '0', 'le quartier de la milice', '1', '0');
INSERT INTO `subarea_data` VALUES ('505', '11', '0', 'le quartier des boulangers', '1', '0');
INSERT INTO `subarea_data` VALUES ('506', '11', '0', 'le quartier des bijoutiers', '1', '0');
INSERT INTO `subarea_data` VALUES ('507', '11', '0', 'le quartier des tailleurs', '1', '0');
INSERT INTO `subarea_data` VALUES ('508', '11', '0', 'le quartier des forgerons', '1', '0');
INSERT INTO `subarea_data` VALUES ('509', '11', '0', 'le quartier des bricoleurs', '1', '0');
INSERT INTO `subarea_data` VALUES ('510', '11', '0', 'l\'arÃªne', '1', '0');
INSERT INTO `subarea_data` VALUES ('511', '11', '0', 'le centre-ville', '1', '0');
INSERT INTO `subarea_data` VALUES ('512', '7', '0', 'l\'arÃªne', '1', '0');
INSERT INTO `subarea_data` VALUES ('513', '7', '0', 'le centre-ville', '1', '0');
INSERT INTO `subarea_data` VALUES ('514', '7', '0', 'la tour des ordres', '1', '0');
INSERT INTO `subarea_data` VALUES ('515', '11', '0', 'la tour des ordres', '1', '0');
INSERT INTO `subarea_data` VALUES ('536', '0', '0', 'le Goultarminator', '1', '0');
INSERT INTO `subarea_data` VALUES ('1021', '1020', '0', 'le cube MJ', '1', '0');
INSERT INTO `subarea_data` VALUES ('1023', '1022', '0', 'la zone marchande', '1', '0');
