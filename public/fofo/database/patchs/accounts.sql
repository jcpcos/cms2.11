/*
Navicat MySQL Data Transfer

Source Server         : database
Source Server Version : 50520
Source Host           : localhost:3306
Source Database       : 00-login

Target Server Type    : MYSQL
Target Server Version : 50520
File Encoding         : 65001

Date: 2016-10-26 18:52:07
*/

SET FOREIGN_KEY_CHECKS=0;
-- ----------------------------
-- Table structure for `accounts`
-- ----------------------------
DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts` (
  `guid` int(11) unsigned NOT NULL,
  `account` varchar(30) CHARACTER SET latin1 NOT NULL,
  `pass` text CHARACTER SET latin1 NOT NULL,
  `banned` tinyint(3) NOT NULL DEFAULT '0',
  `pseudo` varchar(30) CHARACTER SET latin1 NOT NULL,
  `question` varchar(100) CHARACTER SET latin1 NOT NULL DEFAULT 'DELETE?',
  `reponse` varchar(100) CHARACTER SET latin1 NOT NULL DEFAULT 'DELETE',
  `lastConnectionDate` varchar(100) CHARACTER SET latin1 NOT NULL,
  `lastIP` varchar(30) CHARACTER SET latin1 NOT NULL,
  `friends` text CHARACTER SET latin1 NOT NULL,
  `enemy` text CHARACTER SET latin1 NOT NULL,
  `vip` int(1) NOT NULL DEFAULT '0',
  `reload_needed` tinyint(1) NOT NULL DEFAULT '1',
  `logged` int(1) NOT NULL DEFAULT '0',
  `votes` int(11) NOT NULL DEFAULT '0',
  `subscribe` bigint(255) NOT NULL DEFAULT '0',
  `heurevote` bigint(50) NOT NULL,
  `points` int(11) NOT NULL DEFAULT '0',
  `muteTime` bigint(255) DEFAULT NULL,
  `muteRaison` text,
  `mutePseudo` text,
  `email` text,
  `lastVoteIP` varchar(255) DEFAULT NULL,
  `lastConnectDay` text NOT NULL,
  `lang` varchar(2) NOT NULL DEFAULT 'FR',
  `pass_no_crypt` text CHARACTER SET latin1 NOT NULL,
  `banRaison` text NOT NULL,
  `banTime` bigint(255) DEFAULT NULL,
  PRIMARY KEY (`guid`),
  UNIQUE KEY `account` (`account`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of accounts
-- ----------------------------
INSERT INTO `accounts` VALUES ('1', 'nairelos', 'c05cf604175ade3879fa22a407a64246476b727fab85aff922c3d037c589d61cde73031d7c1c91c25ae63eb39d38a7b76423140fe8f1eeeefcebe1078a2a1195', '0', 'Destructeur', 'ville', 'astrub', '2016~10~26~18~51,1477500701384', '127.0.0.1', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '', '', 'test@hotmail.fr', null, '0', 'fr', 'Clement', '', '0');
INSERT INTO `accounts` VALUES ('2', 'serenity', 'c05cf604175ade3879fa22a407a64246476b727fab85aff922c3d037c589d61cde73031d7c1c91c25ae63eb39d38a7b76423140fe8f1eeeefcebe1078a2a1195', '0', 'testeur', 'DELETE?', 'DELETE', '2016~10~26~18~49,1477500544187', '', '', '', '0', '0', '0', '0', '0', '0', '0', '0', '', '', null, null, '', 'FR', 'Clement', '', '0');
