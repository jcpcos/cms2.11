<?php
return array (
  '\\IPS\\Data\\Store\\Database' => 
  array (
    1 => 
    array (
      'file' => 'applications/cms/hooks/Database.php',
      'class' => 'cms_hook_Database',
    ),
    11 => 
    array (
      'file' => 'applications/nexus/hooks/clientAreaLinkLoad.php',
      'class' => 'nexus_hook_clientAreaLinkLoad',
    ),
  ),
  '\\IPS\\forums\\Topic' => 
  array (
    2 => 
    array (
      'file' => 'applications/cms/hooks/Topic.php',
      'class' => 'cms_hook_Topic',
    ),
  ),
  '\\IPS\\Widget' => 
  array (
    3 => 
    array (
      'file' => 'applications/cms/hooks/Widget.php',
      'class' => 'cms_hook_Widget',
    ),
  ),
  '\\IPS\\forums\\Topic\\Post' => 
  array (
    4 => 
    array (
      'file' => 'applications/cms/hooks/Post.php',
      'class' => 'cms_hook_Post',
    ),
  ),
  '\\IPS\\Http\\Url' => 
  array (
    5 => 
    array (
      'file' => 'applications/cms/hooks/Url.php',
      'class' => 'cms_hook_Url',
    ),
  ),
  '\\IPS\\Theme\\class_core_front_global' => 
  array (
    6 => 
    array (
      'file' => 'applications/cms/hooks/websiteUrl.php',
      'class' => 'cms_hook_websiteUrl',
    ),
    12 => 
    array (
      'file' => 'applications/nexus/hooks/clientAreaLink.php',
      'class' => 'nexus_hook_clientAreaLink',
    ),
  ),
  '\\IPS\\Output' => 
  array (
    7 => 
    array (
      'file' => 'applications/cms/hooks/Output.php',
      'class' => 'cms_hook_Output',
    ),
  ),
  '\\IPS\\forums\\Forum' => 
  array (
    8 => 
    array (
      'file' => 'applications/downloads/hooks/Forums.php',
      'class' => 'downloads_hook_Forums',
    ),
  ),
  '\\IPS\\core\\modules\\front\\system\\register' => 
  array (
    9 => 
    array (
      'file' => 'applications/nexus/hooks/allowStoreRegistrationGuests.php',
      'class' => 'nexus_hook_allowStoreRegistrationGuests',
    ),
    10 => 
    array (
      'file' => 'applications/nexus/hooks/register.php',
      'class' => 'nexus_hook_register',
    ),
  ),
);